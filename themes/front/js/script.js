$(function () {
    $('.programSlider').iosSlider({
        infiniteSlider: true,
        snapToChildren: true,
        navPrevSelector: $('.slideSelectors .prev'),
        navNextSelector: $('.slideSelectors .next')
    });

    $('.popularSlider').iosSlider({
        desktopClickDrag: true,
        snapToChildren: true,
        infiniteSlider: true,
        navPrevSelector: $('.pages_popular .prev'),
        navNextSelector: $('.pages_popular .next'),
        keyboardControls: true
    });

    $('.programArchiveSlider').iosSlider({
        desktopClickDrag: true,
        snapToChildren: true,
        infiniteSlider: true,
        navPrevSelector: $('.program_archive .prev'),
        navNextSelector: $('.program_archive .next'),
        keyboardControls: true
    });

    var countnews = $('.news_tab_items_content .slide li').length;

    $('.partnerSlider').iosSlider({
        desktopClickDrag: true,
        snapToChildren: true,
        infiniteSlider: false,
        keyboardControls: true,
        navPrevSelector: $('.partners .prev'),
        navNextSelector: $('.partners .next')
    });

    $(document).on('click', '.programSliderC .slideSelectors .item', function () {
        $('.programSliderC .slideSelectors .item').removeClass('selected');
        $(this).addClass('selected');
    });

    $(document).on('click', '.newsAjax', function () {
        var self = $(this);
        $.ajax({
            url: $(this).attr('href'),
            success: function (data) {
                $('#news_tabs').html(data);
                $('.tabs li').removeClass('active');
                self.parent('li').addClass('active');
                var countnews = $('.news_tab_items_content .slide li').length;

                $('.news_tab_items_content').iosSlider({
                    desktopClickDrag: false,
                    snapToChildren: true,
                    infiniteSlider: true,
                    keyboardControls: true,
                    navPrevSelector: (countnews < 9) ? null : $('.paginator .prev'),
                    navNextSelector: (countnews < 9) ? null : $('.paginator .next'),
                    onSlideChange: slideChangeNewsTabs
                });
            }
        });

        return false;
    });

//    $('.datepicker').datepicker({
//        dateFormat: 'dd/mm/yy',
//        onSelect: function (date) {
//            location.href = '/' + language + '/archive_news/' + date;
//        }
//    });

    $('.datepicker-news').datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function (date) {
            location.href = '/' + language + '/news_archive/' + date;
        }
    });

    $('.datepicker-program').datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function (date) {
            var program = $(this).data('program');
            if (!!program) {
                location.href = '/' + language + '/archive_program/' + program + '/' + date;
            } else {
                location.href = '/' + language + '/archive_program/' + date;
            }
        }
    });

    $('.datepicker-journal').datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function (date) {
            var program = $(this).data('program');
            if (!!program) {
                location.href = '/' + language + '/archive_features/' + program + '/' + date;
            } else {
                location.href = '/' + language + '/archive_features/' + date;
            }
        }
    });


    $(function () {
        $('#slider').nivoSlider({
            effect: 'fade',
            boxCols: 8, // For box animations
            boxRows: 4, // For box animations
            animSpeed: 500, // Slide transition speed
            pauseTime: 10000, // How long each slide will show
            startSlide: 0, // Set starting Slide (0 index)
            directionNav: false, // Next & Prev navigation
            controlNav: false, // 1,2,3... navigation
            //   controlNavThumbs: false, // Use thumbnails for Control Nav
            pauseOnHover: true, // Stop animation while hovering
            manualAdvance: false, // Force manual transitions
            prevText: 'Prev', // Prev directionNav text
            nextText: 'Next', // Next directionNav text
            randomStart: false // Start on a random slide
        });
    });

    $('.news_item').click(function () {
        location.href = $(this).data('href');
    });

    var liveTop = parseInt($('#isshow').val()) * 20;
    if (liveTop > $('.timetable_live ul').height()) {
        liveTop = $('.timetable_live ul').heigth();
    }

    $(".timetable_live .timetablelist ul").stop().animate({
        top: (($('.timetablelist li.active').data('num')*20)) * -1
    }, 1100);

    $('.timetable_live .opener').click(function () {
        var self = $(this);
        if (self.hasClass('active')) {
            $('.timetable_live .timetablelist').css({'overflow': 'hidden'});
            $('.timetable_live .timetablelist ul').css({'visibility': 'hidden'});
            $('.timetable_live').stop().animate({width: 10});
            self.removeClass('active');
            self.find('img').attr('src', '/themes/front/images/opener_open.png');
        } else {
            $('.timetable_live .timetablelist').css({'overflow': 'auto'});
            $('.timetable_live .timetablelist ul').css({'visibility': 'visible'});
            $('.timetable_live').stop().animate({width: 500});
            self.addClass('active');
            self.find('img').attr('src', '/themes/front/images/opener_close.png');
           $('.timetablelist ul').scrollTop = 175;
        }

        return false;
    });

    $('.getTimeTable, .nextWeek a').live('click', function () {
        var date = $(this).data('date');
        $.get('/' + language + '/programms/default/timetable/' + date, {}, function (data) {
            $('.content.program.second').html(data);
        });

        return false;
    });

    $('.slider_yellow').click(function () {
        location.href = '/' + language + '/category/kazakhstan_overview';
    });

    $('.goto_top').click(function () {
        location.href = $(this).data('url');
    });

    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '500', // Distance from top before showing element (px)
        topSpeed: 300, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // Animation in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: '', // Text for element
        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        scrollImg: { active: true, type: 'background', src: '/themes/front/images/top.png' }
    });
});

function slideChange(args) {
    if (args.currentSlideNumber == 1) {
        $('.programSliderC .slideSelectors .item').removeClass('selected');
        $('.programSliderC .slideSelectors .item:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');
    }

}


function sliderLoaded(args) {
    slideChange(args);
}