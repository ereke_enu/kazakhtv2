var init = false;
var delta = 0;
var summer_time = 0;
var gtm = 6 * 3600000;
var daylight_saving = 0;
var year, month, month_name, day, day_name, hour, hour12, meridiem, minute, second;

function SetGTM(new_gtm) {
    gtm = new_gtm * 3600000;
}

function is_summer_time(usa) {
    var d, d_t;
    d = new Date();
    d.setTime(d.getTime() + delta + gtm);
    if (usa) {
        d_t = new Date(d.getUTCFullYear(), 2, 1, 2);
        while (d_t.getUTCDay() != 0) {
            d_t.setUTCDate(d_t.getUTCDate() + 1);
        }
        d_t.setDate(d_t.getUTCDate() + 7);
        if (d.getTime() < d_t.getTime()) {
            return false;
        }
        else {
            d_t = new Date(d.getUTCFullYear(), 10, 1, 1);
            while (d_t.getUTCDay() != 0) {
                d_t.setUTCDate(d_t.getUTCDate() + 1);
            }
            if (d.getTime() < d_t.getTime()) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    else {
        d_t = new Date(d.getUTCFullYear(), 2, 31, 2);
        while (d_t.getUTCDay() != 0) {
            d_t.setUTCDate(d_t.getUTCDate() - 1);
        }
        if (d.getTime() < d_t.getTime()) {
            return false;
        }
        else {
            d_t = new Date(d.getUTCFullYear(), 9, 31, 2);
            while (d_t.getUTCDay() != 0) {
                d_t.setUTCDate(d_t.getUTCDate() - 1);
            }
            if (d.getTime() < d_t.getTime()) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}

function Time(lang) {
    var d;
    d = new Date();
    d.setTime(d.getTime() + delta + summer_time * 3600000 + gtm);
    year = d.getUTCFullYear();
    month = d.getUTCMonth() + 1;
    switch (lang) {
        case 'ru':
            switch (month) {
                case 1:
                    month_name = 'января';
                    break;
                case 2:
                    month_name = 'февраля';
                    break;
                case 3:
                    month_name = 'марта';
                    break;
                case 4:
                    month_name = 'апреля';
                    break;
                case 5:
                    month_name = 'мая';
                    break;
                case 6:
                    month_name = 'июня';
                    break;
                case 7:
                    month_name = 'июля';
                    break;
                case 8:
                    month_name = 'августа';
                    break;
                case 9:
                    month_name = 'сентября';
                    break;
                case 10:
                    month_name = 'октября';
                    break;
                case 11:
                    month_name = 'ноября';
                    break;
                case 12:
                    month_name = 'декабря';
                    break;
            }
            break;
        case 'kz':
            switch (month) {
                case 1:
                    month_name = 'қаңтар';
                    break;
                case 2:
                    month_name = 'ақпан';
                    break;
                case 3:
                    month_name = 'наурыз';
                    break;
                case 4:
                    month_name = 'сәуір';
                    break;
                case 5:
                    month_name = 'мамыр';
                    break;
                case 6:
                    month_name = 'маусым';
                    break;
                case 7:
                    month_name = 'шілде';
                    break;
                case 8:
                    month_name = 'тамыз';
                    break;
                case 9:
                    month_name = 'қыркүйек';
                    break;
                case 10:
                    month_name = 'қазан';
                    break;
                case 11:
                    month_name = 'қараша';
                    break;
                case 12:
                    month_name = 'желтоқсан';
                    break;
            }
            break;
        case 'en':
            switch (month) {
                case 1:
                    month_name = 'January';
                    break;
                case 2:
                    month_name = 'February';
                    break;
                case 3:
                    month_name = 'March';
                    break;
                case 4:
                    month_name = 'April';
                    break;
                case 5:
                    month_name = 'May';
                    break;
                case 6:
                    month_name = 'June';
                    break;
                case 7:
                    month_name = 'July';
                    break;
                case 8:
                    month_name = 'August';
                    break;
                case 9:
                    month_name = 'September';
                    break;
                case 10:
                    month_name = 'October';
                    break;
                case 11:
                    month_name = 'November';
                    break;
                case 12:
                    month_name = 'December';
                    break;
            }
            break;
    }
    day = d.getUTCDate();
    hour = d.getUTCHours();
    hour12 = hour;
    minute = d.getUTCMinutes();
    second = d.getUTCSeconds();
    if (month < 10) {
        month = '0' + month;
    }
    if (day < 10) {
        day = '0' + day;
    }
    if (hour12 > 12) {
        hour12 = hour12 - 12;
        meridiem = 'PM';
    }
    else {
        meridiem = 'AM';
    }
    if (hour12 == 0) {
        hour12 = 12;
    }
    if (hour < 10) {
        hour = '0' + hour;
    }
    if (minute < 10) {
        minute = '0' + minute;
    }
    if (second < 10) {
        second = '0' + second;
    }
    switch (lang) {
        case 'ru':
            switch (d.getUTCDay()) {
                case 0:
                    day_name = 'воскресенье';
                    day_name_sm = 'вс';
                    break;
                case 1:
                    day_name = 'понедельник';
                    day_name_sm = 'пн';
                    break;
                case 2:
                    day_name = 'вторник';
                    day_name_sm = 'вт';
                    break;
                case 3:
                    day_name = 'среда';
                    day_name_sm = 'ср';
                    break;
                case 4:
                    day_name = 'четверг';
                    day_name_sm = 'чт';
                    break;
                case 5:
                    day_name = 'пятница';
                    day_name_sm = 'пт';
                    break;
                case 6:
                    day_name = 'суббота';
                    day_name_sm = 'сб';
                    break;
            }
            break;
        case 'kz':
            switch (d.getUTCDay()) {
                case 0:
                    day_name = 'жексенбі';
                    day_name_sm = 'жк';
                    break;
                case 1:
                    day_name = 'дүйсенбі';
                    day_name_sm = 'дс';
                    break;
                case 2:
                    day_name = 'сейсенбі';
                    day_name_sm = 'сс';
                    break;
                case 3:
                    day_name = 'сәрсенбі';
                    day_name_sm = 'ср';
                    break;
                case 4:
                    day_name = 'бейсенбі';
                    day_name_sm = 'бс';
                    break;
                case 5:
                    day_name = 'жұма';
                    day_name_sm = 'жм';
                    break;
                case 6:
                    day_name = 'сенбі';
                    day_name_sm = 'сн';
                    break;
            }
            break;
        case 'en':
            switch (d.getUTCDay()) {
                case 0:
                    day_name = 'Sunday';
                    day_name_sm = 'Sun';
                    break;
                case 1:
                    day_name = 'Monday';
                    day_name_sm = 'Mon';
                    break;
                case 2:
                    day_name = 'Tuesday';
                    day_name_sm = 'Tue';
                    break;
                case 3:
                    day_name = 'Wednesday';
                    day_name_sm = 'Wed';
                    break;
                case 4:
                    day_name = 'Thursday';
                    day_name_sm = 'Thu';
                    break;
                case 5:
                    day_name = 'Friday';
                    day_name_sm = 'Fri';
                    break;
                case 6:
                    day_name = 'Saturday';
                    day_name_sm = 'Sut';
                    break;
            }
            break;
    }
}

function str_FirstToUp(text) {
    var t1, t2;
    t1 = text.substr(0, 1);
    t1 = t1.toUpperCase();
    t2 = text.substr(1);
    return t1 + t2;
}

function initTime() {
    var d, d_temp;
    d = new Date();
    delta = -d.getTime() + (serv_time + daylight_saving_on_serv * 3600) * 1000;
    d_temp = new Date(2006, 6, 1, 0, 0, 0, 0);
    if (d_temp.getTime() == 1151686800000) delta -= 3600000;
    init = true;
}

function setupTime(lang) {
    if (!init) {
        initTime();
    }
    void Time(lang);
    document.getElementById('realdate').innerHTML = day + ' ' + month_name + ' ' + year;
    document.getElementById('realtime').innerHTML = hour + ':' + minute;
    document.getElementById('realday_name').innerHTML = str_FirstToUp(day_name);
}
