<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="/themes/lxadmin/js/jquery.js"></script>
    <title>Панель управления :: <?= $this->pageTitle ?></title>
    <style type="text/css">
        body {
            padding-top: 60px;
        }

        .navbar, .alert {
            margin-bottom: 5px;
        }

        .filter-container input, .filter-container select {
            width: 100%;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="row">

        <?php $this->widget('bootstrap.widgets.TbNavbar', array(
            'type'     => null, // null or 'inverse'
            'brand'    => 'Панель управления',
            'brandUrl' => '#',
            'collapse' => true, // requires bootstrap-responsive.css
            'items'    => array(
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'items' => (Yii::app()->user->isGuest) ? array() :array(
                        array('label' => 'Главная', 'url' => '/admin.php'),
                        array('label' => 'Структура', 'items' => array(
                            array('label' => 'Разделы', 'url' => $this->createUrl('/sections')),
                            '---',
                            array('label' => 'Категории', 'url' => $this->createUrl('/categories')),
                            array('label' => 'Создать категорию', 'url' => $this->createUrl('/categories/default/create')),
                            '---',
                            array('label' => 'Категории программ', 'url' => $this->createUrl('/programms/categories')),
                            array('label' => 'Создать категорию программ', 'url' => $this->createUrl('/programms/categories/create')),
                        ), 'active'   => (@$this->getModule()->id == 'sections')),
                        array('label' => 'Контент', 'url' => $this->createUrl('/pages'), 'active' => (@$this->getModule()->id == 'pages'),
                              'items' => array(
                                  array('label' => 'Страницы', 'url' => $this->createUrl('/pages?archive=0')),
                                  array('label' => 'Создать страницу', 'url' => $this->createUrl('/pages/default/create')),
                                  array('label' => 'Архив страниц', 'url' => $this->createUrl('/pages/default/admin?archive=1')),
                                  '---',
                                  array('label' => 'Страницы программ', 'url' => $this->createUrl('/programms')),
                                  array('label' => 'Создать страницу программ', 'url' => $this->createUrl('/programms/default/create')),
                                  array('label' => 'Расписание программ', 'url' => $this->createUrl('/programms/timetable')),
                                  array('label' => 'Создать расписание программ', 'url' => $this->createUrl('/programms/timetable/create')),
                                  array('label' => 'Импорт расписания из Excel', 'url' => $this->createUrl('/programms/timetable/import')),
                                  '---',
                                  array('label' => 'Комментарии', 'url' => $this->createUrl('/comments/default/admin')),
                                  '---',
                                  array('label' => 'Видео', 'url' => $this->createUrl('/video')),
                                  array('label' => 'Видео реклама', 'url' => $this->createUrl('/video/videoAdv')),
                                  array('label' => 'Видео архив', 'url' => $this->createUrl('/video/videoArchive')),
                                  '---',
                                  array('label' => 'Блоги', 'url' => $this->createUrl('/blogs/blogs')),
                                  array('label' => 'Страницы блогов', 'url' => $this->createUrl('/blogs')),
                              )),
                        array('label' => 'Баннеры', 'url' => $this->createUrl('/baners'), 'active' => (@$this->getModule()->id == 'baners'),
                              'items' => array(
                                  array('label' => 'Клиенты', 'url' => $this->createUrl('/baners/banerClients')),
                                  '---',
                                  array('label' => 'Баннерные места', 'url' => $this->createUrl('/baners/banerPlaces')),
                                  array('label' => 'Файлы банеров', 'url' => $this->createUrl('/baners/banerFiles')),
                                  '---',
                                  array('label' => 'Размещеннные баннера', 'url' => $this->createUrl('/baners/banerGrid/')),
                                  '---',
                                  array('label' => 'Таргетинг', 'url' => $this->createUrl('/baners/banerTargeting')),
                                  array('label' => 'Статистика переходов', 'url' => $this->createUrl('/baners/banerStat')),
                                  array('label' => 'Статистика показов', 'url' => $this->createUrl('/baners/banerShow')),
                              )),
                        array('label' => 'Пользователи', 'url' => $this->createUrl('/users'), 'active' => (@$this->getModule()->id == 'users')),
                    ),

                ),
                array(
                    'class'       => 'bootstrap.widgets.TbMenu',
                    'htmlOptions' => array('class' => 'pull-right'),
                    'items'       => array(
                        array('label' => 'Выход', 'url' => '/admin.php/users/default/logout/'),
                    ),
                ),
            ),
        )); ?>
    </div>
    <div class="row">
        <div class="span12">
            <?= $content ?>
        </div>
    </div>
</div>
</body>
</html>