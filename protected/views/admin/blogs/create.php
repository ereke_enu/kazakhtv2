<?php
$this->breadcrumbs=array(
	'Blogs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Blogs','url'=>array('index')),
	array('label'=>'Manage Blogs','url'=>array('admin')),
);
?>

<h1>Create Blogs</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>