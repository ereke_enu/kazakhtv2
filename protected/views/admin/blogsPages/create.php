<?php
$this->breadcrumbs=array(
	'Blogs Pages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BlogsPages','url'=>array('index')),
	array('label'=>'Manage BlogsPages','url'=>array('admin')),
);
?>

<h1>Create BlogsPages</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>