﻿<?php 
if(($this->uniqueid=="mainpage/default")&&($this->action->Id=="index"))
{ ?>
    <footer class="footer" style="height:108px;margin-top:-18px;">
        <div>
            <div class="footer_content">
                <div style="float:left;">
                    <span class="footer_logo" style="float:left;">
                        <a href="/"><img src="/themes/front/images/footer_loho.png" alt=""/></a>
                        <!--<a href="/"><img src="upload/2bm.png" alt=""/></a>-->
                    </span>
                    <span class="copyright" style="float:left;margin-top:5px;">Copyright © <?=date('Y', time())?> <a href="http://szh.kz" style="color:#fff;text-decoration: none;" title="Сұрақ-Жауап сайты">Khabar Agency</a></span>
                    
                   <div style="clear:both;"></div>
                    <div class="khabar_copyright">
                        <?php $lang = Yii::app()->language;?>
                        <? if($lang=="ru"){ ?>
                            Все права на материалы сайта защищены Законом РК об авторском праве и смежных правах. <br>
                            Использование материалов Агентства "Хабар" без ссылки на источник преследуется по закону.
                        <? } if($lang=="kz"){ ?>
                            Барлық материалдар ҚР авторлық құқық жөніндегі Заңына сәйкес қорғалады. <br>
                            «Хабар» агенттігінің материалдарын сілтемесіз пайдалану заңмен тыйым салынады.
                        <? } if($lang=="en"){ ?>
                            All materials are protected by the law of the RK on copyright and related rights.<br>
                            Using materials of the "Khabar" Agency without reference to the source is prohibited.
                        <? } ?>

                    </div>
                     <div style="margin-top:4px;">
                        <!-- Start Zero.KZ code  -->
                        <script type="text/javascript"><!--
                            cz_user=55976;cz_type=1;cz_str="&wd="+screen.width;
                            cz_str+="&hg="+screen.height+"&du="+escape(document.URL);
                            cz_str+="&rf="+escape(document.referrer)+"&ce="+navigator.cookieEnabled;
                            cz_str+="&"+Math.random();
                            cz_str="<a href='http://zero.kz/?u="+cz_user+"' target='_blank'>"+ "<img src='http://zero.kz/c.php?u="+cz_user+"&diff=1&t="+cz_type+cz_str+"'"+
                                " border='0px' width='88' height='31' alt='Zero.KZ' /><\/a>"; document.write(cz_str);//--></script><noscript>
                            <a href='http://zero.kz/?u=55976' target='_blank'> <img src='http://zero.kz/c.php?u=55976' border='0px' width='88'
                                                                                    height='31' alt='Zero.KZ' /></a></noscript>
                        <!-- End Zero.KZ code  -->
                            <!-- Yandex.Metrika informer --><a href="http://metrika.yandex.ru/stat/?id=21175468&amp;from=informer" target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/21175468/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:21175468,lang:'ru'});return false}catch(e){}"/></a><!-- /Yandex.Metrika informer --><!-- Yandex.Metrika counter --><script type="text/javascript">var yaParams = {/*Здесь параметры визита*/};</script><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter21175468 = new Ya.Metrika({id:21175468, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true,params:window.yaParams||{ }}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script>
                            <noscript>
                            <div><img src="//mc.yandex.ru/watch/21175468" style="position:absolute; left:-9999px;" alt="" />
                                </div></noscript><!-- /Yandex.Metrika counter -->
							<!--LiveInternet counter--><script type="text/javascript"><!--
							document.write("<a href='//www.liveinternet.ru/click' "+
							"target=_blank><img src='//counter.yadro.ru/hit?t26.5;r"+
							escape(document.referrer)+((typeof(screen)=="undefined")?"":
							";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
							screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
							";"+Math.random()+
							"' alt='' title='LiveInternet: показано число посетителей за"+
							" сегодня' "+
							"border='0' width='88' height='15'><\/a>")
							//--></script><!--/LiveInternet-->
                     </div>   
                     
               </div>
               <div style="float:right;width:476px;">
                    <div>
                        <ul class="single_line_list" style="float:right;">
                            <?php if($lang=='ru'){ ?>
                                <li><a href="/ru/category/site_editors">Редакция сайта</a></li>
                                
                                <li><a href="/ru/category/contacts">Контакты</a></li>
                                
                                <li><a href="/ru/category/vacancy">Вакансии</a></li>
                                
                                <li><a href="/ru/category/rules">Правила</a></li>
                                
                                <li><a href="/ru/category/sitemap">Карта сайта</a></li>
                            <?php }elseif($lang=='kz'){ ?>
                                <li><a href="/kz/category/site_editors">Сайт редакциясы</a></li>
                                
                                <li><a href="/kz/category/contacts">Байланыс</a></li>
                                
                                <li><a href="/kz/category/vacancy">Бос қызмет орындары</a></li>
                                
                                <li><a href="/kz/category/rules">Ережелер</a></li>
                                
                                <li><a href="/kz/category/sitemap">Сайт картасы</a></li>
                            <?php }elseif($lang=='en'){ ?>
                                <li><a href="/en/category/site_editors">Editorial website</a></li>
                                
                                <li><a href="/en/category/contacts">Contacts</a></li>
                                
                                <li><a href="/en/category/vacancy">Vacancies</a></li>
                                
                                <li><a href="/en/category/rules">Regulations</a></li>
                                
                                <li><a href="/en/category/sitemap">Sitemap</a></li>
                            <?php }else{ ?>
                                <li><a href="/ru/category/site_editors">Редакция сайта</a></li>
                                
                                <li><a href="/ru/category/contacts">Контакты</a></li>
                                
                                <li><a href="/ru/category/vacancy">Вакансии</a></li>
                                
                                <li><a href="/ru/category/rules">Правила</a></li>
                                
                                <li><a href="/ru/category/sitemap">Карта сайта</a></li>
                            <?php } ?> 
                        </ul>
                    </div>
                    <div class="clear"></div>
                    <div style="float:right;">
                        <a href="http://www.khabar.kz" target="_blank" rel="nofollow"><img width="100px" style="padding-bottom:10px;" src="/upload/logofooter1.png"/></a>
                        <a href="http://www.24.kz" target="_blank" rel="nofollow"><img width="113px" style="padding-bottom:10px;" src="/upload/logofooter2.png"/></a>
                        <a href="http://www.bmtv.kz/" target="_blank" rel="nofollow"><img width="123px" style="padding-bottom:10px;" src="/upload/logofooter3.png"/></a>
                    </div>
                    <div class="footer_content">    
               
               
            </div>
                </div>    
            </div>
            <div style="clear:both;"></div>
            
        </div>      
    </footer>

<?php 
}else{
    $this->renderPartial('//layouts/_singlePageFooter'); 
}

?>
    <?php

        Yii::app()->translate->renderMissingTranslationsEditor();

    ?>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36439258-1']);
        _gaq.push(['_setDomainName', 'caspionet.kz']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <script type="text/javascript" src="/themes/front/player/uppod.js"></script>
    <script type="text/javascript" src="/themes/front/js/time.js"></script>
    <script type="text/javascript">
        var serv_time =<?=time()?>;
        var daylight_saving_on_serv = 0;
        setupTime('<?=Yii::app()->language?>');
        window.setInterval("setupTime('<?=Yii::app()->language?>');", 1000)
    </script>
    <script type="text/javascript" src="/themes/front/js/orphus.js"></script>
    <script src="/themes/front/js/jquery.nivo.slider.pack.js" type="text/javascript"></script>
    <script src="/themes/front/js/time.js" type="text/javascript"></script>
    <script type="text/javascript" src="/themes/front/js/jquery.iosslider.min.js"></script>
    <script type="text/javascript" src="/themes/front/js/jquery.scrollUp.min.js"></script>
    <script type="text/javascript" src="/themes/front/js/script.js"></script>
    <script>
        setInterval(function(){$('div.piar .baner:first-child').appendTo('div.piar')},'12000');
        setInterval(function(){$('div.banner .baner:first-child').appendTo('div.banner')},'12000');
        setInterval(function(){$('div.banner_inside .baner:first-child').appendTo('div.banner_inside')},'12000');
    </script>
    <script type="text/javascript">
        //<![CDATA[
        function addLink() {
            var body_element = document.getElementsByTagName('body')[0];
            var selection = window.getSelection();
            var pagelink = "<p><a href='"+document.location.href+"'>"+document.location.href+"</a> </p><p>If you want to share article from Kazakh-tv.kz, use hyperlink below.</p>";
            if(language=="ru"){
                pagelink = "<p>Подробнее: <a href='"+document.location.href+"'>"+document.location.href+"</a> </p><p>Любое использование материалов сайта допускается только при наличии гиперссылки на Kazakh-tv.kz.</p>";
            }else if(language=="kz"){
                pagelink = "<p>Қосымша ақпарат алу үшін: <a href='"+document.location.href+"'>"+document.location.href+"</a> </p><p>Кез келген сайт ақпараттарын қолдану тек Kazakh-tv.kz сілтемесі болған кезде рұқсат етіледі.</p>";
            }
            var copytext = selection + pagelink;
            var newdiv = document.createElement('div');
            newdiv.style.position = 'absolute';
            newdiv.style.left = '-99999px';
            body_element.appendChild(newdiv);
            newdiv.innerHTML = copytext;
            selection.selectAllChildren(newdiv);
            window.setTimeout( function() {
                body_element.removeChild(newdiv);
            }, 0);
        }
        document.oncopy = addLink;
        //]]>
    </script>
    </body>
    </html>