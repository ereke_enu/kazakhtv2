<?php 

$this->renderPartial('//layouts/_header'); ?>


<div id="content" class="second">
    <div class="content_wrap" style="width:765px;" >
        <?= $content ?>
    </div>
    <aside class="rightSidebar" style="margin-top:0px;" >
    <? if($this->action->id=="dontmiss"||$this->action->id=="events"){ ?>
        <div  style="margin-top:-15px;">
        <?php $this->widget('PagesListWidget1', array('category' => 'news', 'title' => Yii::t('pages', 'Последние новости'), 'date' => true));?>
        </div>
       <div style="margin-top:20px;"> 
                    <?php $this->widget("AuthorPagesListWidget");?></div>
                    <? } ?>
    </aside>


</div>
<div class='banner_inside'>
    <?php
        $this->widget('ShowBanerWidget', array('position' => 1));
    ?>
</div>

<div style="clear: both"></div>

<?php $this->renderPartial('//layouts/_footer'); ?>
