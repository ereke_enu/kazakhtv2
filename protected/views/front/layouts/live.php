﻿<?php $this->renderPartial('//layouts/_header'); ?>
<div id="content" class="second">
    <div class="content_wrap">
        <?=$content?>
    </div>
    <aside class="rightSidebar">
        <!--<div class="datepicker datepicker-news"></div>-->
        <?php $this->widget('PagesListWidget', array('category' => 'news', 'title' => Yii::t('pages', 'Последние новости'), 'date' => true)); ?>
        <?php include('lightbanner.php'); ?>
        <br>
        <a href="http://orphus.ru" id="orphus" target="_blank"><img alt="Система Orphus"
                                                                    src="/themes/front/images/orphus.gif" border="0"
                                                                    width="125" height="115"/></a>
    </aside>

</div>
<div class='banner_inside'>
    <?php
        $this->widget('ShowBanerWidget', array('position' => 1));
    ?>
</div>

<div style="clear: both"></div>

<?php $this->renderPartial('//layouts/_footer'); ?>
