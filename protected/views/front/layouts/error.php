<?php
$this->pageTitle = Yii::app()->name . ' - Ошибка';
$this->breadcrumbs = array(
    'Ошибка',
);
?>

<div class="h_fon">
    <h1 class="ib"><em class='h1_left_pic'></em>Ошибка <?php echo $code; ?></h1>
</div>

<div id="text">
    <?php echo CHtml::encode($message); ?>
</div>