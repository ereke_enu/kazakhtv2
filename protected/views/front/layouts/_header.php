<?php 
//include "./protected/components/Translator.php";
	$is_main_page=0;
	if(($this->uniqueid=="mainpage/default")&&($this->action->Id=="index")) $is_main_page=1;		
	$language = Yii::app()->language;
 ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name='wmail-verification' content='ec005b6b1dc0088fe34cb0047cf416c0' />
<meta name="msvalidate.01" content="46B0EE7582244C27B5AB4B9882CB2EF2" />
<meta name='yandex-verification' content='4000d310625250d5' />
    <meta charset="utf-8"/>
    <script src="/themes/front/js/jquery.min.js" type="text/javascript"></script>
    <script src="/themes/front/js/slides.min.jquery.js" type="text/javascript"></script>
    <script src="/themes/front/js/jquery.cookie.js" type="text/javascript"></script>
    <script src="/themes/front/js/jqueryui.js" type="text/javascript"></script>
	<script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script> 
	<script type="text/javascript" src="/themes/front/js/jquery.totemticker.js"></script>
    <?php if ($language != 'en') { ?>
        <script src="/themes/front/js/jquery.ui.datepicker-<?= Yii::app()->language ?>.js"
                type="text/javascript"></script>
    <?php } ?>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title><?=$this->pageTitle ?></title>
    <meta name="keywords" content="<?= $this->metaTags ?>"/>
    <meta name="description" content="<?= $this->metaDescription ?>"/>
	<link rel="stylesheet" href="/themes/front/css/globalslider.css" type="text/css" media="screen, projection"/>
    <link rel="stylesheet" href="/themes/front/css/stylenew.css?c=<?= time() ?>" type="text/css" media="screen, projection"/>
    
    <link rel="stylesheet" href="/themes/front/css/scroller.css?c=<?= time() ?>" type="text/css" media="screen, projection"/>
    <link rel="stylesheet" href="/themes/front/css/demos.css" type="text/css" media="screen, projection"/>
    <link rel="alternate" type=”application/rss+xml” title=">Первый национальный спутниковый канал Республики Казахстан" href="<?= Yii::app()->createAbsoluteUrl('/pages/default/rss') ?>"/>
    <script type="text/javascript">
        var language = '<?=$language ?>';
        var menu_category ="";
    </script>
	<script>


		$(function(){
			$('#top-menu-'+language).css('display','block');
			$('#slides').slides({
				preload: true,
				preloadImage: 'img/loading.gif',
				play: 5000,
				pause: 2500,
				hoverPause: true,
				animationStart: function(){
					$('.caption').animate({
						bottom:-35
					},100);
				},
				animationComplete: function(current){
					$('.caption').animate({
						bottom:0
					},200);
					if (window.console && console.log) {
						// example return of current slide number
						console.log(current);
					};
				}
			});
			
		});
		
		$(document).ready(function() {
			
			
			if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
			    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) 
			{
				var offered =$.cookie('MobileAppIsOffered');
				if(offered!="1"){
					$('#wrapper').css('display','none');
					$('#start_background').css('display','block');
				}
			}

			$('#start_footer_title').click(function(){
				var date = new Date;
				date.setDate(date.getDate() + 30);
				$.cookie('MobileAppIsOffered', '1',  { expires: date });

				$('#wrapper').css('display','block');
				$('#start_background').css('display','none');
			});
			
		
		})		
	</script>
</head>
<body>
	<?php if (stripos(Yii::app()->request->getHostInfo(), 'caspionet')) { ?>
    <div id="blocker">
    </div>
    <div id="mainpopup">
        <div class="popup_block">
            <div class="lang_abbr">KZ</div>
            <div class="description">
                Қазақстан Республикасының тұңғыш ұлттық спутниктік <span class="fs18">Caspionet</span> телеарнасы <span
                    class="fs18">Kazakh TV</span> деген жаңа атаумен
                хабар таратады. <br>
                <a href="//kazakh-tv.kz/kz/mainpage/index">www.kazakh-tv.kz</a> сайтына Қош келдіңіз
            </div>
        </div>
        <div class="popup_block">
            <div class="lang_abbr">RU</div>
            <div class="description">
                Первый национальный спутниковый канал Республики Казахстан <span class="fs18">Caspionet</span> вещает
                под новым названием <span class="fs18">Kazakh
                TV</span>. <br>
                Добро пожаловать на сайт <a href="//kazakh-tv.kz/ru/mainpage/index">www.kazakh-tv.kz</a>
            </div>
        </div>
        <div class="popup_block">
            <div class="lang_abbr">EN</div>
            <div class="description">
                Kazakhstan's first satellite channel <span class="fs18">Caspionet</span> has been renamed to <span
                    class="fs18">Kazakh TV</span>. <br>
                Welcome to our website <a href="//kazakh-tv.kz/en/mainpage/index">www.kazakh-tv.kz</a>
            </div>
        </div>
    </div>
    <?php } ?>
    <div id="start_background" >
				<div id="logo_wrapper" class="start_block">
					<img id="start_logo" src="https://pp.vk.me/c628519/v628519366/14b29/ztmlynyvsI0.jpg" />
				</div>

				<div id="start_title_wrapper">
					<div id="start_title">
						<? if($language =="ru") {?>
							Первый Национальный Спутниковый канал Республики Казахстан
						<? } if($language =="en") {?>
							The First National Satellite channel  of the Republic of Kazakhstan
						<? } if($language =="kz") {?>
							Қазақстан Республикасының Тұңғыш Ұлттық спутниктік каналы
						<? } ?>
					</div>
				</div>

				<div id="start_footer">
					<div id="start_footer_title">
						<? if($language =="ru") {?>
							Перейти на сайт
						<? } if($language =="en") {?>
							Go to the website
						<? } if($language =="kz") {?>
							Cайтқа өту
						<? } ?>
					</div>
				</div>	

				<div id="playmarket_wrapper" class="start_block">
					<a href="https://play.google.com/store/apps/details?id=com.khabar.kazakhtv">
					<img id="playmarket" src="/upload/playmarket.jpg" /></a>
				</div>

				<div id="appstore_wrapper" class="start_block">
				<a href="https://itunes.apple.com/us/app/kazakh-tv/id912334448?mt=8">
					<img id="appstore" src="/upload/AppStore.png" />
					</a>
				</div>
			</div>

	<div id="wrapper">
		

   		<div class="headerline" style="height: 25px;background: #00354c;width: 100%;color: #EEE9E9;font-size: 12px;padding-top:5px;">
			<div style="width:998px; margin:0px auto;">		
				<span id="realday_name" class="day_name" style="border-left: 1px solid #EEE9E9;padding-left: 10px;"></span>,
				<span id="realdate" class="date"></span>
				<span id="realtime" class="time" style="margin-right:5px;"></span><span style="margin-right: 40px;padding-right:10px;border-right: 1px solid #EEE9E9;">AST</span>
				<span  style="padding-left: 10px;font-weight:bold;">
					
				</span>
				<div class="lang" style="padding-top:5px;">
					<?php $this->widget('blockLanguageSelector') ?>
				</div>
			</div>
		</div>

		<div id="border">
			<header id="header">
				<section class="header_content" >
		    		<div class="logo">
                		<a href="/"><img src="https://pp.vk.me/c628519/v628519366/14b29/ztmlynyvsI0.jpg"/></a>
            		</div>
         			<div class="slogan">
						<h3 style="text-transform: uppercase;  font-size: 14px;  font-weight: bold;  margin-top: 13px;  color: #00344b;">
						<?php switch ($language) {
							case 'ru':
								echo "ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ";
								break;
							case 'kz':
								echo "Қазақстан Республикасының";
								break;
							case 'en':
								echo "THE FIRST NATIONAL SATELLITE CHANNEL";
								break;
							
							default:
								echo "ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ";
								break;
						 }?></h3>
						<br>
						<h3 style="text-transform: uppercase;  font-size: 14px;  font-weight: bold;  margin-top: -13px;  color: #00344b;"><?php switch (Yii::app()->language) {
						case 'ru':
							echo "РЕСПУБЛИКИ КАЗАХСТАН";
							break;
						case 'kz':
							echo "алғашқы ұлттық спутниктік арнасы";
							break;
						case 'en':
							echo "OF THE REPUBLIC OF KAZAKHSTAN";
							break;
						
						default:
							echo "РЕСПУБЛИКИ КАЗАХСТАН";
							break;
					 	}?></h3>
            		</div>
					<div>	
            			<div class="social">
                			<ul>
                				<li><a href="http://kazakhtv.yvision.kz/" target="_blank"><img  class="icon" src="/images/social/yvs.png" onmouseover="this.src='/images/social/yv.png'" onmouseout="this.src='/images/social/yvs.png'"></a></li>
                				<li><a href="https://plus.google.com/+KazakhTV/posts" target="_blank"><img  class="icon" src="/images/social/gg.png" onmouseover="this.src='/images/social/gplus.png'" onmouseout="this.src='/images/social/gg.png'"></a></li>
							  	<li><a href="http://vk.com/kazakhtv" target="_blank"><img  class="icon" src="/images/social/vkg.png" onmouseover="this.src='/images/social/vkc.png'" onmouseout="this.src='/images/social/vkg.png'"></a></li>
			                    <li><a href="http://twitter.com/Kazakh_TV" target="_blank"><img  class="icon" src="/images/social/tg.png" onmouseover="this.src='/images/social/tc.png'" onmouseout="this.src='/images/social/tg.png'"></a></li>
			                    <li><a href="https://www.youtube.com/KazakhTVchannel" target="_blank"><img  class="icon" src="/images/social/yg.png" onmouseover="this.src='/images/social/yc.png'" onmouseout="this.src='/images/social/yg.png'"></a></li>
			                    <li><a href="http://facebook.com/KazakhTV" target="_blank"><img  class="icon" src="/images/social/fg.png" onmouseover="this.src='/images/social/fc.png'" onmouseout="this.src='/images/social/fg.png'"></a></li>
			                    <!--<li><a href="#" title="RSS"><img  class="icon" src="/images/social/rssg.png" onmouseover="this.src='/images/social/rssc.png'" onmouseout="this.src='/images/social/rssg.png'"></a></li>-->
			                    <li><a href="http://instagram.com/kazakh_tv" target="_blank"><img  class="icon" src="/images/social/ig.png" onmouseover="this.src='/images/social/inst.png'" onmouseout="this.src='/images/social/ig.png'"></a></li>
								<li><a href="/pda.php" target="_blank"><img  class="icon" src="/images/social/mg.png" onmouseover="this.src='/images/social/mc.png'" onmouseout="this.src='/images/social/mg.png'"></a></li>
							</ul>
            			</div>
			            <div class="search_block">
			                <form action="<?= Yii::app()->createUrl('/search/default/index') ?>" method="GET">
			                    <input type="text" placeholder="<?//= Yii::t('search', 'Искать'); ?>" name="query">
			                    <button></button>
			                </form>
			            </div>
					</div>	
        		</section>
    		</header>

			<div>
    			<div class="shadow">
        			<div class="menu">
            			<?php require_once("mainmenu.php");   ?>
        			</div>
    			</div>
    		<div class="informer_content" >
    		</div>

