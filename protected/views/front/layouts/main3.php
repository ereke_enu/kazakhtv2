<?php $this->renderPartial('//layouts/_header'); ?>
<?php $lang=Yii::app()->language; ?>
<div id="content">
	<table >
		<tr>
			<td style="vertical-align: top;" >
				<div id="left">
				    <div class="block_inf">
						<div id="slides">
							<div id="top_news"><?=Yii::t('pages', 'Новости');?></div>
							<div class="slides_container" style="width:480px;height:270px;">
							<?php
							
                 $categories = Categories::model()->cache(1800)->find('t.sefname=:sefname AND lang_id=:lang_id', array(':sefname' => 'news', ':lang_id' => LXController::getLangId()));;

			        $criteria = new CDbCriteria;
			        $criteria->addInCondition('category_id', CHtml::listData($categories->children()->findAll(), 'id', 'id'));
			        $criteria->limit = '30';
			        $criteria->order = 'date DESC, time DESC';
			        $criteria->compare('active', 1);
			        $criteria->compare('is_archive', 0);

			        $pages = Pages::model()->cache(600)->findAll($criteria);
                 
                 $news;
                 $kzNews; $worldNews; $businessNews; $sportNews; $htNews; $cultureNews; $healthNews; $cinemaNews;
                 
                 $kzNewsIndex=0; $worldNewsIndex=0; $businessNewsIndex=0; $sportNewsIndex=0; $htNewsIndex=0; 
                 $cultureNewsIndex=0; $healthNewsIndex=0; $cinemaNewsIndex=0;
                 $i=0;
                 
                 foreac($pages as $row)
                 {

                      $news[$i]->category_sefname = $row->category_sefname;
                      $news[$i]->sefname = $row->sefname;
                      $news[$i]->title = $row->title;
                      $news[$i]->short_text = $row->short_text;
                      $news[$i]->image  = $row->image;
                      $news[$i]->date = $row->date;
                      $news[$i]->time = $row->time;
                      
                      if($news[$i]->category_sefname=='news_kazakhstan'){
                        if($kzNewsIndex==0){
                          $kzNews[]=$news[$i];
                          $kzNewsIndex++;
                        }
                      }elseif($news[$i]->category_sefname=='world_news'){
                        if($worldNewsIndex<4){
                          $worldNews[]=$news[$i];
                          $worldNewsIndex++;
                        }
                      }elseif($news[$i]->category_sefname=='business'){
                        if($businessNewsIndex<3){
                          $businessNews[]=$news[$i];
                          $businessNewsIndex++;
                        }
                      }elseif($news[$i]->category_sefname=='sport'){
                        if($sportNewsIndex<3){
                          $sportNews[]=$news[$i];
                          $sportNewsIndex++;
                        }
                        
                      }elseif($news[$i]->category_sefname=='hi-tech'){
                        if($htNewsIndex<3){
                          $htNews[]=$news[$i];
                          $htNewsIndex++;
                        }
                      }elseif($news[$i]->category_sefname=='culture'){
                        if($cultureNewsIndex<3){
                          $cultureNews[]=$news[$i];
                          $cultureNewsIndex++;
                        }
                      }elseif($news[$i]->category_sefname=='health'){
                        if($healthNewsIndex<3){
                          $healthNews[]=$news[$i];
                          $healthNewsIndex++;
                        }
                      }elseif($news[$i]->category_sefname=='cinema'){
                        if($cinemaNewsIndex<3){
                          $cinemaNews[]=$news[$i];
                          $cinemaNewsIndex++;
                        }
                      }
                      
                      $i++;
                 }
                 mysql_close($conn);
							?>
							
										<?php $this->widget('SliderPageWidget', array('page'=>$worldNews[0])); ?>
										<?php $this->widget('SliderPageWidget', array('page'=>$kzNews[0])); ?>
										<?php $this->widget('SliderPageWidget', array('page'=>$businessNews[0])); ?>
										<?php $this->widget('SliderPageWidget', array('page'=>$sportNews[0])); ?>
										<?php $this->widget('SliderPageWidget', array('page'=>$cultureNews[0])); ?>
										<?php $this->widget('SliderPageWidget', array('page'=>$htNews[0])); ?>
										<?php //if(isset($cinemaNews[0])) $this->widget('SliderPageWidget', array('page'=>$cinema[0])); ?>
										<?php //if(isset($healthNews[0])) $this->widget('SliderPageWidget', array('page'=>$healthNews[0])); ?>
												</div>
							<a href="#" class="prev"><img src="/upload/l_arr1.png" width="7" height="20" alt="Arrow Prev"></a>
							<a href="#" class="next"><img src="/upload/r_arr1.png" width="7" height="20" alt="Arrow Next"></a>
						</div>
						<div  style="float:right;width:266px;top:0px;right:12px;position:absolute;z-index:30;color:##00354c;padding-bottom:10px; border-bottom:1px solid #d7d7d7;" >
							<?php $this->widget('LatestNewsWidget', array('pages'=>$news)); ?>	
						</div> 
		   			</div>
				    <div class="latest_second_news bigboxblock">
				     	<?php $this->widget('KzNewsWidget', array('page'=>$kzNews[0])); ?>
				        <?php $this->widget('WorldNewsWidget', array('pages'=>$worldNews)); ?>
				        
    		 <div style="-webkit-column-count: 2;-moz-column-count: 2;-o-column-count: 2;column-count: 2;width: 98%;">
				
			</div>
				    </div> 
				    <table id="news_widgets_table" style="margin-top:60px;width:98%;">
				    	<tr>
				    		<td valign="top" style="width:370px;">
				    			<div class="bizsport bigboxblock" data-block="business"  data-parent="bizsport">
			                		<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'business'))?>"><?=Yii::t('pages', 'Бизнес');?></a></h2>
					                <?php $this->widget('CategoryNewsWidget', array('pages'=>$businessNews)); ?>
					                
	            				</div>
				    		</td>
				    		<td valign="top" style="width:370px;">
				    			<div class="bizsport bigboxblock" data-block="sport"  data-parent="bizsport">
					                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'sport'))?>"><?=Yii::t('pages', 'Спорт');?></a></h2>
					                <?php $this->widget('CategoryNewsWidget', array('pages'=>$sportNews)); ?>
					               
					            </div>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td valign="top" style="width:370px;padding-top:20px;">
				    			<div class="bizsport bigboxblock" data-block="business" data-parent="bizsport">
				               		<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'hi-tech'))?>"><?=Yii::t('pages', 'HI-TECH');?></a></h2>
				                	<?php $this->widget('CategoryNewsWidget', array('pages'=>$htNews)); ?>
				                	
							    </div>
				    		</td>
				    		<td valign="top" style="width:370px;padding-top:20px;">
				    			 <div class="bizsport bigboxblock" data-block="sport" data-parent="bizsport">
									<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'culture'))?>"><?=Yii::t('pages', 'Культура');?></a></h2>
					                <?php $this->widget('CategoryNewsWidget', array('pages'=>$cultureNews)); ?>
					                
					            </div>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td valign="top" style="width:370px;padding-top:20px;">
				    			<div class="bizsport bigboxblock" data-block="business" data-parent="bizsport">
				               		<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'health'))?>">
				               			<? if($lang=="ru"){ echo "Здоровье"; }
											else if($lang=="en"){ echo "Health";}
											else { echo "Денсаулық";}
										?>	
				               		</a></h2>
				                	<?php $this->widget('CategoryNewsWidget', array('pages'=>$healthNews)); ?>
				                	
							    </div>
				    		</td>
				    		<td valign="top" style="width:370px;padding-top:20px;">
				    			<div class="bizsport bigboxblock" data-block="sport" data-parent="bizsport">
									<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'cinema'))?>">
										<? if($lang=="ru"){ echo "Кино"; }
											else if($lang=="en"){ echo "Cinema";}
											else { echo "Кино";}
										?>	
									</a></h2>
					                <?php $this->widget('CategoryNewsWidget', array('pages'=>$cinemaNews)); ?>
					               
					            </div>
				    		</td>
				    	</tr>
				    </table>	
	<!--Start:Programms-->
	<div id="programms" style="display:block;">
		<h2 class="section_title" style="width:740px;margin-left:-10px;">
			<? if ($lang == 'ru'){?>
				Программы
			<?} if ($lang == 'en'){?>
				Programmes
			<?}	if ($lang == 'kz'){?>
				Бағдарламалар
			<?}?>
		</h2>
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td valign="top" style="width:43px;" class="programm_widget_item">
					<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							История
						<?} if ($lang == 'en'){?>
							History
						<?}	if ($lang == 'kz'){?>
							Тарих
						<?}?>
					</h2>
					<img src="/upload/programms/main/hist-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget1', array('category_sefname'=>'history',)); ?>
				</td>
				<td valign="top" style="width:43px;" class="programm_widget_item">
					<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Культура
						<?} if ($lang == 'en'){?>
							Culture
						<?}	if ($lang == 'kz'){?>
							Мәдениет
						<?}?>
					</h2>
					<img src="/upload/programms/main/cult-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget1', array('category_sefname'=>'culture',)); ?>
				</td>
				<td valign="top" style="width:43px;" class="programm_widget_item">
					<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Стиль
						<?} if ($lang == 'en'){?>
							Style
						<?}	if ($lang == 'kz'){?>
							Стиль
						<?}?>
					</h2>
					<img src="/upload/programms/main/style-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget1', array('category_sefname'=>'fashion',)); ?>
				</td>
				<td valign="top" style="width:43px;" class="programm_widget_item">
					<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Туризм
						<?} if ($lang == 'en'){?>
							Tourism
						<?}	if ($lang == 'kz'){?>
							Туризм
						<?}?>

					</h2>
					<img src="/upload/programms/main/tas-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget1', array('category_sefname'=>'tourism',)); ?>
				</td>
				
			</tr>
				<tr>
					<td valign="top" style="width:43px;" class="programm_widget_item">
						<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Кухня
						<?} if ($lang == 'en'){?>
							Kitchen
						<?}	if ($lang == 'kz'){?>
							Асхана
						<?}?>

					</h2>
					<img src="/upload/programms/main/kit-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget1', array('category_sefname'=>'kitchen',)); ?>
					</td>
					<td valign="top" style="width:43px;" class="programm_widget_item">
						<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Инновации
						<?} if ($lang == 'en'){?>
							Innovations
						<?}	if ($lang == 'kz'){?>
							Инновациялар
						<?}?>
					</h2>
					<img src="/upload/programms/main/innov-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget1', array('category_sefname'=>'innovations',)); ?>
					</td>
					<td valign="top" style="width:43px;" class="programm_widget_item">
						<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Экономика
						<?} if ($lang == 'en'){?>
							Economics
						<?}	if ($lang == 'kz'){?>
							Экономика
						<?}?>
					</h2>
					<img src="/upload/programms/main/econ-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget1', array('category_sefname'=>'economy',)); ?>
					</td>
					<td valign="top" style="width:43px;" class="programm_widget_item">
						<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Наука
						<?} if ($lang == 'en'){?>
							Science
						<?}	if ($lang == 'kz'){?>
							Ғылым
						<?}?>
					</h2>
					<img src="/upload/programms/main/sci-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget1', array('category_sefname'=>'science',)); ?>
					</td>
			</tr>
		</table>
	</div>
	
	<!--Start:Programms-->
	<div id="social_projects" >
		<div class="section_title" style="width:740px;margin-left:-10px;">
			<?
				if ($lang == 'ru')
				{?>
				ФИЛЬМЫ И СЕРИАЛЫ
				<?}
				if ($lang == 'en')
				{?>
				FILMS ON KAZAKH TV
				<?}
				if ($lang == 'kz')
				{?><a href="kz/program/teleserial">
				ФИЛЬМДЕР ЖӘНЕ ТЕЛЕХИКАЯЛАР</a>
				<?}?>
		</div>
		<?php $this->widget("FilmsAndSerialsWidget");?>	
		
		
	</div>
				</div>
			</td>
			<td style="vertical-align: top;">
				<div id="right" >
				<div id="anounce" style="border:1px solid #e9e9e9;width:225px; margin-top:0px;">
					<div id="anounce-h" style="height:40px;"><div class="rightblock"><?=Yii::t('pages', 'Не пропустите');?></div>
				</div>

<div>

<div class="containerimg">
        <img src="/upload/anons/anons_kiv.jpg" style="width:225px; height:300px;" class="startimg">
        <!--<div class="descriptionimg" style="margin-top: -109px;background: rgba(0,0,0,.65);z-index: 55;padding: 5px;position: relative;"> -->
 <?
if ($lang == 'ru')
{?>
<div class="descriptionimg" style="margin-top: -90px;
background: rgba(0,0,0,.65);
z-index: 55;
padding: 5px;height:auto;
position: relative;"> 
<a href="http://kazakh-tv.kz/ru/programms/program/program_458_kazakhstan-international-vectors" style="color: #fff; font-size: 14px;">«Kazakhstan: International vectors»:<br/>Международная организация труда
<br/>16 октября в 17:40 и 23:30

</a>
 
<?}
if ($lang == 'en')
{?>
<div class="descriptionimg" style="margin-top: -71px;
background: rgba(0,0,0,.65);
z-index: 55;
padding: 5px;height:auto;
position: relative;"> 
<a href="http://kazakh-tv.kz/en/programms/program/program_458_kazakhstan-international-vectors" style="color: #fff; font-size: 14px;">«Kazakhstan: International vectors»:<br/>The international Labor Organization
<br/>16 october at 9:30 am

</a>
<?}
if ($lang == 'kz')
{?>
<div class="descriptionimg" style="margin-top: -70px;
background: rgba(0,0,0,.65);
z-index: 55;
padding: 5px;
height:auto;
position: relative;"> 
<a href="http://kazakh-tv.kz/kz/programms/program/program_458_kazakhstan-international-vectors" style="color: #fff; font-size: 14px;">«Kazakhstan: International vectors»:<br/>Халықаралық еңбек ұйымы
<br/>16 қазан 12:30 және 20:30
</a>
 
<?}?>
</div></div></div></div>
					<div>
      					<div class="linkedFlashContainer" style="margin-top:20px;">
							<a target="_blank" href="http://kazakh-tv.kz/ru/allNews/estafeta_khabar" class="flashLink"></a>
							<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="225" height="375" style="margin-left:2px;" align="middle">
							<param name="movie" value="/upload/relay.swf" />
							<param name="wmode" value="opaque">
							<embed src="/upload/relay.swf" quality="high" wmode="opaque" width="225" height="375" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
							</object>
						</div>
      				</div>
					<div style="margin-top:20px;border:1px solid #ececec;">
	      				<?php $this->widget("PopularPagesListWidget1");?>
	      				<?php include('lightbanner.php'); ?>
      				</div>
      				
				</div>
				
			</td>
		</tr>
	</table>
</div>
			  

			</div>
	</div></div>

<?php 
	$this->renderPartial('//layouts/_footer2'); 
?>
