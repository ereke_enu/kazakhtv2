<!DOCTYPE html>
<html style="margin-top:-5px" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name='yandex-verification' content='4000d310625250d5' />
    <meta charset="utf-8"/>
    <title><?= $this->pageTitle ?></title>
    <script src="/themes/front/js/jquery.js" type="text/javascript"></script>
    <link rel='stylesheet' href='/themes/front/css/stylenew1.css' type="text/css" />
</head>    
<script type="text/javascript">
function get_language() {
	var str = window.location.href;
	if(str.indexOf('/kz/') + 1) {
		return "kz";
	}else if(str.indexOf('/ru/') + 1){
		return "ru";
	}else if(str.indexOf('/en/') + 1)
	{
		return "en";
	}
}

$(document).ready(function(){ 
	
	var item_more_index=0;
	$('#item_more').click(function() {
		if(item_more_index==0){
			$('#header1').animate({height:"67px"},1000);
			$("#menu_part").css('display','block');
			item_more_index=1;
		}else{
			$('#header1').animate({height:"37px"},1000);	
			$("#menu_part").animate({'opacity':'hide'})
			item_more_index=0;
		}
	});

})
	
</script>
<?php $lang=Yii::app()->language; ?>
	<body>    
		<div id="header1">
			<div class="wrapper" style="height:37px;margin-top:5px;">
				<div id="logo1">
					<a href="http://kazakh-tv.kz/<?=$lang ?>/mainpage/index">
						<img src="/upload/logolast.png" /></a>
				</div>
				
				<ul id="menu">
				<?php if($lang=='ru'){ ?>
					<li><a href="/ru/mainpage/index">Главная</a></li>
					<li><a href="/ru/allNews/news">Новости</a></li>
					<li><a href="/ru/category/programms">Программы</a></li>
					<li><a href="/ru/category/worldwide">WorldWide</a></li>
					<li><a href="/ru/category/live_now">Прямой эфир</a></li>
					<li id='item_more' class="item_more"><a href="#">Еще...</a></li>
					<li>
						<a href="/kz/mainpage/weather">KZ</a>
						|
						<a href="/ru/mainpage/weather">RU</a>
						|
						<a href="/en/mainpage/weather">EN</a>
					</li>
				<?php }elseif($lang=='kz'){?>
					<li><a href="/kz/mainpage/index">Басты бет</a></li>
					<li><a href="/kz/allNews/news">Жаңалықтар</a></li>
					<li><a href="/kz/category/programms">Бағдарламалар</a></li>
					<li><a href="/kz/category/worldwide">WorldWide</a></li>
					<li><a href="/kz/category/live_now">Тікелей эфир</a></li>
					<li id='item_more'><a href="#">Тағы...</a></li>
					<li>
						<a href="/kz/mainpage/weather">Kz</a>
						|
						<a href="/ru/mainpage/weather">Ru</a>
						|
						<a href="/en/mainpage/weather">En</a>
					</li>
				<?php }elseif($lang=='en'){ ?>
					<li><a href="/en/mainpage/index">Home</a></li>
					<li><a href="/en/allNews/news">News</a></li>
					<li><a href="/en/category/programms">Programms</a></li>
					<li><a href="/en/category/worldwide">WorldWide</a></li>
					<li><a href="/en/category/live_now">On air</a></li>
					<li id='item_more'><a href="#">More...</a></li>
					<li>
						<a href="/kz/mainpage/weather">Kz</a>
						|
						<a href="/ru/mainpage/weather">Ru</a>
						|
						<a href="/en/mainpage/weather">En</a>
					</li>
				<?php }else{ ?>
					<li><a href="/ru/mainpage/index">ГЛАВНАЯ</a></li>
					<li><a href="/ru/allNews/news">Новости</a></li>
					<li><a href="/ru/category/programms">Программы</a></li>
					<li><a href="/ru/category/worldwide">WorldWide</a></li>
					<li><a href="/ru/category/live_now">Прямой эфир</a></li>
					<li id='item_more'><a href="#">Еще...</a></li>
					<li>
						<a style="font-weight:boldl" href="/kz/mainpage/weather">Kz</a>
						|
						<a href="/ru/mainpage/weather">Ru</a>
						|
						<a href="/en/mainpage/weather">En</a>
					</li>
				<?php } ?>
					
				</ul>
			</div>
			<div class="clear"></div>
			<div id='menu_part' >
				<div class="wrapper">
					<ul>
					<?php if($lang=='ru'){ ?>
						<li><a href="/ru/category/kazakhstan_overview">Казахстан</a></li>
						<li><a href="/ru/category/about_company">О нас</a></li>
						<!--<li><a href="">Не пропустите</a></li>-->
						<li><a href="/ru/category/program_schedule">Программы передач</a></li>
					<?php }elseif($lang=='kz'){ ?>
						<li><a href="/kz/category/kazakhstan_overview" >Қазақстан</a></li>
						<li><a href="/kz/category/about_company">Біз жайлы</a></li>
						<!--<li><a href="">Өткізіп алмаңыз</a></li>-->
						<li><a href="/kz/category/program_schedule">Бағдарламалар кестесі</a></li>
					<?php }elseif($lang=='en'){ ?>
						<li><a href="/en/category/kazakhstan_overview" >Kazakhstan</a></li>
						<li><a href="/en/category/about_company">About us</a></li>
						<!--<li><a href="">Do not miss</a></li>-->
						<li><a href="/en/category/program_schedule">TV Schedule</a></li>
					<?php }else{ ?>
						<li><a href="/ru/category/kazakhstan_overview">Казахстан</a></li>
						<li><a href="/ru/category/about_company">О нас</a></li>
						<!--<li><a href="">Не пропустите</a></li>-->
						<li><a href="/ru/category/program_schedule">Программы передач</a></li>
					<?php } ?>
					</ul>
				</div>
			</div>
		</div>
		
		<div id="bg_top"> 
			<div class="wrapper"  id="content">
				<?php echo $content; ?>
		</div>
			<?php $this->renderPartial('//layouts/_singlePageFooter'); ?>
		</div>
		
    </body>
</html>