<?php $lang = Yii::app()->language;?>
<script type="text/javascript">
$(function(){

	$.each($('div.menu li a'), function(){ 
			   if($(this).attr('href')==location.pathname)
			   {
			     $('div.menu .active').removeClass('active');
			     $('div.menu .active').removeClass('current_item');
			     $(this).parents('li').addClass('active'); 
			     $(this).parents('li').addClass('current_item');

			     //$(this).css('text-decoration','underline');
			     
			    $('.active').removeClass('active');
				$('.current_item').addClass('active');	 
			   }  

			   if(menu_category!=""){
				   if($(this).attr('href').includes(menu_category))
	               {
	                 $('div.menu .active').removeClass('active');
	                 $('div.menu .active').removeClass('current_item');
	                 $(this).parents('li').parents('li').addClass('active'); 
	                 $(this).parents('li').parents('li').addClass('current_item'); 
	                 
	                 $('.active').removeClass('active');
	                $('.current_item').addClass('active');  
	                 
	                    $(this).css('text-decoration','underline');
	                 
	               }  
			   }
			});

	$('div .menu ul li').hover(function(){
				
				$('.active').removeClass('active');
				$(this).addClass('active');
			});

			$('div .menu ul li').mouseleave(function(){
				$('.active').removeClass('active');
				$('.current_item').addClass('active');	
			});

});
</script>
<div id="footer" >
				<div class="wrapper" id="footer_content">
					<div id='footer_right_part'  >
						<ul id='footer_menu'  >
							<?php if($lang=='ru'){ ?>
								<li><a href="/ru/category/site_editors">Редакция сайта</a></li>
								<li>|</li>
								<li><a href="/ru/category/contacts">Контакты</a></li>
								<li>|</li>
								<li><a href="/ru/category/vacancy">Вакансии</a></li>
								<li>|</li>
								<li><a href="/ru/category/rules">Правила</a></li>
								<li>|</li>
								<li><a href="/ru/category/sitemap">Карта сайта</a></li>
							<?php }elseif($lang=='kz'){ ?>
								<li><a href="/kz/category/site_editors">Сайт редакциясы</a></li>
								<li>|</li>
								<li><a href="/kz/category/contacts">Байланыс</a></li>
								<li>|</li>
								<li><a href="/kz/category/vacancy">Бос орын</a></li>
								<li>|</li>
								<li><a href="/kz/category/rules">Ережелер</a></li>
								<li>|</li>
								<li><a href="/kz/category/sitemap">Сайт картасы</a></li>
							<?php }elseif($lang=='en'){ ?>
								<li><a href="/en/category/site_editors">Editorial website</a></li>
								<li>|</li>
								<li><a href="/en/category/contacts">Contacts</a></li>
								<li>|</li>
								<li><a href="/en/category/vacancy">Vacancies</a></li>
								<li>|</li>
								<li><a href="/en/category/rules">Regulations</a></li>
								<li>|</li>
								<li><a href="/en/category/sitemap">Sitemap</a></li>
							<?php }else{ ?>
								<li><a href="/ru/category/site_editors">Редакция сайта</a></li>
								<li>|</li>
								<li><a href="/ru/category/contacts">Контакты</a></li>
								<li>|</li>
								<li><a href="/ru/category/vacancy">Вакансии</a></li>
								<li>|</li>
								<li><a href="/ru/category/rules">Правила</a></li>
								<li>|</li>
								<li><a href="/ru/category/sitemap">Карта сайта</a></li>
							<?php } ?> 
						</ul>
						<div  style="float:right;margin-top:10px;">

				             <!-- Start Zero.KZ code  -->
                        <script type="text/javascript"><!--
                            cz_user=55976;cz_type=1;cz_str="&wd="+screen.width;
                            cz_str+="&hg="+screen.height+"&du="+escape(document.URL);
                            cz_str+="&rf="+escape(document.referrer)+"&ce="+navigator.cookieEnabled;
                            cz_str+="&"+Math.random();
                            cz_str="<a href='http://zero.kz/?u="+cz_user+"' target='_blank'>"+ "<img src='http://zero.kz/c.php?u="+cz_user+"&diff=1&t="+cz_type+cz_str+"'"+
                                " border='0px' width='88' height='31' alt='Zero.KZ' /><\/a>"; document.write(cz_str);//--></script>

                                <noscript>
                            <a href='http://zero.kz/?u=55976' target='_blank'> <img src='http://zero.kz/c.php?u=55976' border='0px' width='88'
                                                                                    height='31' alt='Zero.KZ' /></a></noscript>
                        <!-- End Zero.KZ  code -->
                            <!-- Yandex.Metrika informer --><a href="http://metrika.yandex.ru/stat/?id=21175468&amp;from=informer" target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/21175468/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:21175468,lang:'ru'});return false}catch(e){}"/></a><!-- /Yandex.Metrika informer --><!-- Yandex.Metrika counter --><script type="text/javascript">var yaParams = {/*Здесь параметры визита*/};</script><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter21175468 = new Ya.Metrika({id:21175468, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true,params:window.yaParams||{ }}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script>
                            <noscript>
                            <div><img src="//mc.yandex.ru/watch/21175468" style="position:absolute; left:-9999px;" alt="" />
                                </div></noscript><!-- /Yandex.Metrika counter -->
	            	</div>
					</div>
					<div id="footer_left_part" >
						<img src="/upload/logolast.png" id="footer_logo" />
						<span id="copyright" >| Copyright © Khabar Agency</span>
						<br /><br />
						<div id='copyright_text'>
							
							 <? if($lang=="ru"){ ?>
                            Все права на материалы сайта защищены Законом РК об авторском праве и смежных правах. <br>
                            Использование материалов Агентства "Хабар" без ссылки на источник преследуется по закону.
                        <? } if($lang=="kz"){ ?>
                            Барлық материалдар ҚР авторлық құқық жөніндегі Заңына сәйкес қорғалады. <br>
                            «Хабар» агенттігінің материалдарын сілтемесіз пайдалану заңмен тыйым салынады.
                        <? } if($lang=="en"){ ?>
                            All materials are protected by the law of the RK on copyright and related rights.<br>
                            Using materials of the "Khabar" Agency without reference to the source is prohibited.
                        <? } ?>
						</div>
					</div>
				</div>
			</div>
