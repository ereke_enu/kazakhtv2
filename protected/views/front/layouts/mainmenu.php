									<ul id="top-menu-ru" style="display:none">
										<li class="active" ><a href="/ru/mainpage/index">Главная</a>
											<ul>
												<!--
												<li><a href="/ru/mainpage/dontmiss">Не пропустите</a></li>
												<li><a href="/ru/allNews/column">Авторская колонка</a></li>-->
												
												
												<!--<li><a href="/ru/allNews/column">Авторская колонка</a></li>
												-->
												<!--<li><a href="#">Смотрите и читайте</a></li>-->
												
											</ul>
										</li>
										<li><a href="/ru/allNews/news">Новости</a>
											<ul>
												<li><a href="/ru/allNews/news_kazakhstan">Казахстан</a></li>
												<li><a href="/ru/allNews/world_news">Мир</a></li>
												<li><a href="/ru/allNews/business">Бизнес</a></li>
												<li><a href="/ru/allNews/sport">Спорт</a></li>
												<li><a href="/ru/allNews/culture">Культура</a></li>
												<li><a href="/ru/allNews/hi-tech">HI-TECH</a></li>
												<li><a href="/ru/allNews/health">Здоровье</a></li>
												<li><a href="/ru/allNews/cinema">Кино</a></li>
												<li><a href="/ru/allNews/sobytia">События</a></li>
												<!--<li><a href="#">Курсы валют</a></li>
												<li><a href="#">Популярное за неделю</a></li>-->
											</ul>
										</li>
										<li><a href="/ru/category/programms">Программы</a>
											<ul>
												<li><a href="/ru/category/programms">Программы</a></li>
												<li><a href="/ru/program/features">Журналы</a></li>
												<li><a href="/ru/program/sundayprogrammes">Воскресные программы</a></li>
												<li><a href="/ru/program/films_tv">Фильмы</a></li>
												<li><a href="/ru/program/teleserial">Телесериалы</a></li>
												<!--<li><a href="/ru/category/newproject">Новые проекты</a></li>
												<li><a href="/ru/category/special">Специальные проекты</a></li>-->
											</ul>
										</li>
										
										<li><a href="/ru/category/kazakhstan_overview">Казахстан</a>
											<ul>
												<li><a href="/ru/category/kazakhstan_overview">Общая информация</a></li>
												<li><a href="/ru/category/history">История</a></li>
												<li><a href="/ru/category/geography">География</a></li>
												<li><a href="/ru/category/population">Население</a></li>
												<li><a href="/ru/category/kazakhstan_culture">Культура</a></li>
												<li><a href="/ru/category/economy">Экономика</a></li>
												<li><a href="/ru/category/kazakhstan_sience">Наука</a></li>
												<li><a href="/ru/category/kazakhstan_tourism">Туризм</a></li>
												<li><a href="/ru/category/kazakhstan_health">Здравоохранение</a></li>

												<li><a href="/ru/category/kazakhstan_finance">Финансы</a></li>
												<li><a href="/ru/category/kazakhstan_sport">Спорт</a></li>
												<!-- <li><a href="/ru/category/kazakhstan_helpful">Полезная информация</a></li>
												<li><a href="/ru/category/government">Государственное устройство</a></li>
												<li><a href="/ru/category/how_to_view_us">Зоны вещания</a></li>
												<li><a href="/ru/category/admindivision">Административное деление</a></li>-->
											</ul>
										</li>
										<li><a href="/ru/category/about_company">О нас</a>
											<ul>
												<li><a href="/ru/allNews/estafeta_khabar">Агентству "Хабар" - 20 лет!</a></li>
												<li><a href="/ru/category/about_company">Компания</a></li>
												<li><a href="/ru/category/about_us_team">Команда</a></li>
												<li><a href="/ru/allNews/press_about_us">Пресса о нас</a></li>
												<!--<li><a href="/ru/category/partners_slider">Партнеры</a></li>-->
												<!--<li><a href="#">Рекламодателям</a></li>-->
												<li><a href="/ru/category/vacancy">Вакансии</a></li>
												<li><a href="/ru/category/contacts">Контакты</a></li>
											</ul>
										</li>
										<li>
											<a href="/ru/mainpage/weather?city=35188">Погода</a>
										</li>
										
										<li>
											<a href="/ru/category/program_schedule">Программа передач</a>
										</li>
										
										<li><a href="/ru/category/worldwide">WorldWide</a>
										    <!-- <ul>
										<li><a href="/ru/category/worldwide">Зоны вещания</a></li>
											</ul>	-->
										</li>
										<li><a href="/ru/category/live_now">Прямой эфир</a>
											
										</li>
										<li>
											<a href="/ru/category/sotrudnichestvo">Будем партнерами!</a>
										</li>
									</ul>


									
									<ul id="top-menu-kz" style="display:none">
										<li class="active" ><a href="/kz/mainpage/index">Негізгі</a>
											<ul>
												<!--<li><a href="/kz/mainpage/dontmiss">Өткізіп алмаңыз</a></li>
												<li><a href="/kz/allNews/column">Авторлық мақала</a></li>-->
												
												
											</ul>
										</li>
										<li><a href="/kz/allNews/news">Жаңалықтар</a>
											<ul>
												<li><a href="/kz/allNews/news_kazakhstan">Қазақстан</a></li>
												<li><a href="/kz/allNews/world_news">Әлем</a></li>
												<li><a href="/kz/allNews/business">Бизнес</a></li>
												<li><a href="/kz/allNews/sport">Спорт</a></li>
												<li><a href="/kz/allNews/culture">Мәдениет</a></li>
												<li><a href="/kz/allNews/hi-tech">HI-TECH</a></li>
												<li><a href="/kz/allNews/health">Денсаулық</a></li>
												<li><a href="/kz/allNews/cinema">Кино</a></li>
												<li><a href="/kz/allNews/sobytia">Оқиғалар</a></li>
											</ul>
										</li>
										<li><a href="/kz/category/programms">Бағдарламалар</a>
											<ul>
												<li><a href="/kz/category/programms">Бағдарламалар</a></li>
												<li><a href="/kz/program/features">Журналдар</a></li>
												<li><a href="/kz/program/sundayprogrammes">Жексенбілік бағдарламалар</a></li>
												<li><a href="/kz/program/films_tv">Фильмдер</a></li>
												<li><a href="/kz/program/teleserial">Телехикаялар</a></li>
												<!--<li><a href="/kz/category/newproject">Жаңа жобалар</a></li>
												<li><a href="/kz/category/special">Арнайы жобалар</a></li>-->
											</ul>
										</li>
											
										<li><a href="/kz/category/kazakhstan_overview">Қазақстан</a>
											<ul>
												<li><a href="/kz/category/kazakhstan_overview">Жалпы мағлұмат</a></li>
												<li><a href="/kz/category/history">Тарих</a></li>
												<li><a href="/kz/category/geography">География</a></li>
												<li><a href="/kz/category/population">Халқы</a></li>
												<li><a href="/kz/category/kazakhstan_culture">Мәдениет</a></li>
												<li><a href="/kz/category/economy">Экономика</a></li>
												<li><a href="/kz/category/kazakhstan_sience">Ғылым</a></li>
												<li><a href="/kz/category/kazakhstan_tourism">Туризм</a></li>
												<li><a href="/kz/category/kazakhstan_health">Денсаулық сақтау</a></li>
											    <li><a href="/kz/category/kazakhstan_finance">Қаржы</a></li>
												<li><a href="/kz/category/kazakhstan_sport">Спорт</a></li>
												<!--<li><a href="/kz/category/kazakhstan_helpful">Пайдалы сілтемелер</a></li>
												<li><a href="/kz/category/how_to_view_us">Арнаның таралу аралығы</a></li>
												<li><a href="/kz/category/admindivision">Әкімшілік бөлімі</a></li>
												<li><a href="/kz/category/government">Мемлекеттік құрылыс</a></li> -->
											</ul>
										</li>
										<li><a href="/kz/category/about_company">Біз жайлы</a>
											<ul>
												<li><a href="/kz/allNews/estafeta_khabar">"Хабар" агенттігіне 20 жыл!</a></li>
												<li><a href="/kz/category/about_company">Компания</a></li>
												<li><a href="/kz/category/about_us_team">Команда</a></li>
												<li><a href="/kz/allNews/press_about_us">Біз жайлы баспасөз</a></li>
												<!--<li><a href="/kz/category/partners_slider">Серіктестер</a></li>-->
												<li><a href="/kz/category/vacancy">Бос қызмет орындары</a></li>
												<li><a href="/kz/category/contacts">Байланыс</a></li>
											</ul>
										</li>
										<li>
											<a href="/kz/mainpage/weather?city=35188">Ауа-райы</a>
										</li>
										<!--<li>
											<a href="/kz/mainpage/dontmiss">Өткізіп алмаңыз</a>
										</li>-->
										<li>
											<a href="/kz/category/program_schedule">Бағдарламалар кестесі</a>
										</li>
										
										<li>
											<a href="/kz/category/worldwide">WorldWide</a>
										     <!--<ul>
												<li><a href="/kz/category/worldwide">Арнаның таралу аралығы</a></li>
											</ul>-->	
										</li>
										<li><a href="/kz/category/live_now">Тікелей эфир</a>
											
										</li>
										<li>
											<a href="/kz/category/sotrudnichestvo">Серіктес болайық!</a>
										</li>
									</ul>

									<ul id="top-menu-en" style="display:none">
										<li class="active">
										<a href="/en/mainpage/index">Home</a>
											<ul>
												<!--<li><a href="/en/mainpage/dontmiss">Don't miss</a></li>
												<li><a href="/en/allNews/column">Editor's column</a></li>-->
												
												
											</ul>
										</li>

										<li><a href="/en/category/news">News</a>
											<ul>
												<li><a href="/en/allNews/news_kazakhstan">Kazakhstan</a></li>
												<li><a href="/en/allNews/world_news">World</a></li>
												<li><a href="/en/allNews/business">Business</a></li>
												<li><a href="/en/allNews/culture">Culture</a></li>
												<li><a href="/en/allNews/sport">Sport</a></li>
												<li><a href="/en/allNews/hi-tech">HI-TECH</a></li>
												<li><a href="/en/allNews/health">Health</a></li>
												<li><a href="/en/allNews/cinema">Cinema</a></li>
												<li><a href="/en/allNews/sobytia">Events</a></li>
											</ul>
										</li>
										<li><a href="/en/category/programms">Programmes</a>
											<ul>
												<li><a href="/en/category/programms">Programmes</a></li>
												<li><a href="/en/program/features">Features</a></li>
												<li><a href="/en/program/sundayprogrammes">Sunday Programmes</a></li>
												<!--<li><a href="/en/category/newproject">New projects</a></li>
												<li><a href="/en/category/special">Special projects</a></li>-->
												
											</ul>
										</li>

										<li><a href="/en/category/kazakhstan_overview#content">Kazakhstan</a>
											<ul>
												<li><a href="/en/category/kazakhstan_overview">General Information</a></li>
												<li><a href="/en/category/history">History</a></li>
												<li><a href="/en/category/geography">Geography</a></li>
												<li><a href="/en/category/population">Population</a></li>
												<li><a href="/en/category/kazakhstan_culture">Culture</a></li>
												<li><a href="/en/category/economy">Economy</a></li>
												<li><a href="/en/category/kazakhstan_sience">Science</a></li>
												<li><a href="/en/category/kazakhstan_tourism">Tourism</a></li>
												<li><a href="/en/category/kazakhstan_health">Health</a></li>
												<li><a href="/en/category/kazakhstan_finance">Finance</a></li>
												<li><a href="/en/category/kazakhstan_sport">Sports</a></li>
											<!--<li><a href="/en/category/how_to_view_us">Broadcast area</a></li>
										  		<li><a href="/en/category/news_archive">News</a></li>		
												<li><a href="/en/category/government">State structure</a></li>
												<li><a href="/en/category/admindivision">Administrative division</a></li>
												<li><a href="/en/category/kazakhstan_helpful">Useful information</a></li> -->
											</ul>
										</li>
										<li><a href="/en/category/about_company">About Us</a>
											<ul>
												<li><a href="/en/allNews/estafeta_khabar">20th anniversary of "Khabar" agency!</a></li>
												<li><a href="/en/category/about_company">Company</a></li>
												<li><a href="/en/category/about_us_team">Team</a></li>
												<li><a href="/en/allNews/press_about_us">Press about us</a></li>
												<!--<li><a href="#">Partners</a></li>-->
												<li><a href="/en/category/vacancy">Vacancies</a></li>
												<li><a href="/en/category/contacts">Contact us</a></li>
											</ul>
										</li>
											<li>
											<a href="/en/mainpage/weather?city=35188">Weather</a>
										</li>
										<!--<li>
											<a href="/en/mainpage/dontmiss">Don't miss</a>
										</li>-->
										<li>
											<a href="/en/category/program_schedule">TV schedule</a>
										</li>
									 	<!--<li><a href="/en/category/archive">Archive</a>
										        <ul>
												<li><a href="/en/category/news_archive">News</a></li>
												<li><a href="/en/program/tv_shows_spisok">Series</a></li>
												<li><a href="/en/program/features">Features</a></li>
												</ul>
										</li>-->
										<!--<li class="active"><a href="/en/category/interview">Interview</a></li>-->
									 	<li>
											<a href="/en/category/worldwide">WorldWide</a>
										     <!--<ul>
												<li><a href="/en/category/worldwide">Broadcast area</a></li>
											</ul>-->	
										</li>
									 	<li><a href="/en/category/live_now">On Air</a>
									 		
									 	</li>
										<li>
											<a href="/en/category/sotrudnichestvo">Let’s be the partners!</a>
										</li>
									</ul>

							