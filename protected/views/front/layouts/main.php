
<?php $this->renderPartial('//layouts/_header'); ?>
<? $lang =Yii::app()->language; ?>
<div id="content">
	<table >
		<tr>
			<td style="vertical-align: top;" >
				<div id="left">
				    <div class="block_inf">
						<div id="slides">
							<div id="top_news"><?=Yii::t('pages', 'Новости');?></div>
							<div class="slides_container" style="width:480px;height:270px;">
							   <?php $this->widget('mainNewsWidgetNew'); ?>
							   <?php $this->widget('mainNewsWidgetKaz'); ?>
							   <?php $this->widget('mainNewsWidgetBiz'); ?>
							   <?php $this->widget('mainNewsWidgetSport'); ?>
							   <?php $this->widget('mainNewsWidgetCul'); ?>
							   <?php $this->widget('mainNewsWidgetHt'); ?>
							   <?php $this->widget('mainNewsWidgetCin'); ?>
							   <?php $this->widget('mainNewsWidgetHealth'); ?>
							</div>
							<a href="#" class="prev"><img src="/upload/l_arr1.png" width="7" height="20" alt="Arrow Prev"></a>
							<a href="#" class="next"><img src="/upload/r_arr1.png" width="7" height="20" alt="Arrow Next"></a>
						</div>
						<div  style="float:right;width:266px;top:0px;right:12px;position:absolute;z-index:30;color:##00354c;padding-bottom:10px; border-bottom:1px solid #d7d7d7;" >
							<?php $this->widget('blockNewsNew') ?>	
						</div>
		   			</div>
				    <div class="latest_second_news bigboxblock">
				     	<?php $this->widget('blockNewsKz')?>
				        <?php $this->widget('blockNewsW')?>
				    </div> 
				    <table id="news_widgets_table" style="margin-top:60px;width:98%;">
				    	<tr>
				    		<td valign="top" style="width:370px;">
				    			<div class="bizsport bigboxblock" data-block="business"  data-parent="bizsport">
			                		<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'business'))?>"><?=Yii::t('pages', 'Бизнес');?></a></h2>
					                <?php $this->widget('businessNewsWidgetNew'); ?>
	            				</div>
				    		</td>
				    		<td valign="top" style="width:370px;">
				    			<div class="bizsport bigboxblock" data-block="sport"  data-parent="bizsport">
					                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'sport'))?>"><?=Yii::t('pages', 'Спорт');?></a></h2>
					                <?php $this->widget('sportNewsWidgetNew'); ?>
					            </div>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td valign="top" style="width:370px;padding-top:20px;">
				    			<div class="bizsport bigboxblock" data-block="business" data-parent="bizsport">
				               		<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'hi-tech'))?>"><?=Yii::t('pages', 'HI-TECH');?></a></h2>
				                	<?php $this->widget('htNewsWidgetNew'); ?>
							    </div>
				    		</td>
				    		<td valign="top" style="width:370px;padding-top:20px;">
				    			 <div class="bizsport bigboxblock" data-block="sport" data-parent="bizsport">
									<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'culture'))?>"><?=Yii::t('pages', 'Культура');?></a></h2>
					                <?php $this->widget('culNewsWidgetNew'); ?>
					            </div>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td valign="top" style="width:370px;padding-top:20px;">
				    			<div class="bizsport bigboxblock" data-block="business" data-parent="bizsport">
				               		<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'health'))?>">
				               			<? if($lang=="ru"){ echo "Здоровье"; }
											else if($lang=="en"){ echo "Health";}
											else { echo "Денсаулық";}
										?>	
				               		</a></h2>
				                	<?php $this->widget('HealthNewsWidget'); ?>
							    </div>
				    		</td>
				    		<td valign="top" style="width:370px;padding-top:20px;">
				    			<div class="bizsport bigboxblock" data-block="sport" data-parent="bizsport">
									<h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'cinema'))?>">
										<? if($lang=="ru"){ echo "Кино"; }
											else if($lang=="en"){ echo "Cinema";}
											else { echo "Кино";}
										?>	
									</a></h2>
					                <?php $this->widget('CinemaNewsWidget'); ?>
					            </div>
				    		</td>
				    	</tr>
				    </table>	
	<!--Start:Programms-->
	<div id="programms">
		<h2 class="section_title" style="width:740px;margin-left:-10px;">
			<? if ($lang == 'ru'){?>
				Программы
			<?} if ($lang == 'en'){?>
				Programmes
			<?}	if ($lang == 'kz'){?>
				Бағдарламалар
			<?}?>
		</h2>
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td valign="top" style="width:43px;" class="programm_widget_item">
					<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							История
						<?} if ($lang == 'en'){?>
							History
						<?}	if ($lang == 'kz'){?>
							Тарих
						<?}?>
					</h2>
					<img src="/upload/programms/main/hist-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget', array('category_sefname'=>'history',)); ?>
				</td>
				<td valign="top" style="width:43px;" class="programm_widget_item">
					<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Культура
						<?} if ($lang == 'en'){?>
							Culture
						<?}	if ($lang == 'kz'){?>
							Мәдениет
						<?}?>
					</h2>
					<img src="/upload/programms/main/cult-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget', array('category_sefname'=>'culture',)); ?>
				</td>
				<td valign="top" style="width:43px;" class="programm_widget_item">
					<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Стиль
						<?} if ($lang == 'en'){?>
							Style
						<?}	if ($lang == 'kz'){?>
							Стиль
						<?}?>
					</h2>
					<img src="/upload/programms/main/style-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget', array('category_sefname'=>'fashion',)); ?>
				</td>
				<td valign="top" style="width:43px;" class="programm_widget_item">
					<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Туризм
						<?} if ($lang == 'en'){?>
							Tourism
						<?}	if ($lang == 'kz'){?>
							Туризм
						<?}?>

					</h2>
					<img src="/upload/programms/main/tas-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget', array('category_sefname'=>'tourism',)); ?>
				</td>
				
			</tr>
				<tr>
					<td valign="top" style="width:43px;" class="programm_widget_item">
						<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Кухня
						<?} if ($lang == 'en'){?>
							Kitchen
						<?}	if ($lang == 'kz'){?>
							Асхана
						<?}?>

					</h2>
					<img src="/upload/programms/main/kit-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget', array('category_sefname'=>'kitchen',)); ?>
					</td>
					<td valign="top" style="width:43px;" class="programm_widget_item">
						<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Инновации
						<?} if ($lang == 'en'){?>
							Innovations
						<?}	if ($lang == 'kz'){?>
							Инновациялар
						<?}?>
					</h2>
					<img src="/upload/programms/main/innov-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget', array('category_sefname'=>'innovations',)); ?>
					</td>
					<td valign="top" style="width:43px;" class="programm_widget_item">
						<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Экономика
						<?} if ($lang == 'en'){?>
							Economics
						<?}	if ($lang == 'kz'){?>
							Экономика
						<?}?>
					</h2>
					<img src="/upload/programms/main/econ-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget', array('category_sefname'=>'economy',)); ?>
					</td>
					<td valign="top" style="width:43px;" class="programm_widget_item">
						<a href="">
					<h2>
						<? if ($lang == 'ru'){?>
							Наука
						<?} if ($lang == 'en'){?>
							Science
						<?}	if ($lang == 'kz'){?>
							Ғылым
						<?}?>
					</h2>
					<img src="/upload/programms/main/sci-01.jpg">
					</a>
					<?php $this->widget('programmNewsWidget', array('category_sefname'=>'science',)); ?>
					</td>
			</tr>
		</table>
	</div>
	<div style="width:998px; height:350px;margin-top: 20PX;display:none;" >
		<div>
			<div class="section_title" style="width:730px; margin-left:-2px;margin-bottom:10px;">
				<?// if ($lang == 'ru'){?>
					Специальные проекты
				<?//} if ($lang == 'en'){?>
					Special projects
				<?//}	if ($lang == 'kz'){?>
					Арнайы жобалар
				<?//}?>
			</div>
			<?//php $this->widget("SpecialProjectsWidget");?>					
		</div>			
	</div>
	<!--Start:Programms-->
	<div id="social_projects">
		<div class="section_title" style="width:740px;margin-left:-10px;">
			<?
				if ($lang == 'ru')
				{?>
				ФИЛЬМЫ И СЕРИАЛЫ
				<?}
				if ($lang == 'en')
				{?>
				FILMS ON KAZAKH TV
				<?}
				if ($lang == 'kz')
				{?><a href="kz/program/teleserial">
				ФИЛЬМДЕР ЖӘНЕ ТЕЛЕХИКАЯЛАР</a>
				<?}?>
		</div>
		<?php $this->widget("FilmsAndSerialsWidget");?>	
	</div>
				</div>
			</td>
			<td style="vertical-align: top;">
				<div id="right" >
				<div id="anounce" style="border:1px solid #e9e9e9;width:225px; margin-top:0px;">
					<div id="anounce-h" style="height:40px;"><div class="rightblock"><?=Yii::t('pages', 'Не пропустите');?></div>
				</div>

<div>

<div class="containerimg">
        <img src="/upload/anons/anons_pe.jpg" style="width:225px; height:300px;" class="startimg">
        <!--<div class="descriptionimg" style="margin-top: -109px;background: rgba(0,0,0,.65);z-index: 55;padding: 5px;position: relative;"> -->
 <?
if ($lang == 'ru')
{?>
<div class="descriptionimg" style="margin-top: -90px;
background: rgba(0,0,0,.65);
z-index: 55;
padding: 5px;height:auto;
position: relative;"> 
<a href="http://kazakh-tv.kz/ru/programms/program/program_580_people-and-economy" style="color: #fff; font-size: 14px;">«People and Economy»:<br/>Рынок подделок
<br/>19 октября в 21:30

</a>
 
<?}
if ($lang == 'en')
{?>
<div class="descriptionimg" style="margin-top: -71px;
background: rgba(0,0,0,.65);
z-index: 55;
padding: 5px;height:auto;
position: relative;"> 
<a href="http://kazakh-tv.kz/en/programms/program/program_582_people-and-economy" style="color: #fff; font-size: 14px;">«People and Economy»:<br/>Counterfeit market
<br/>22 october at 3:30 am, 9:30 am(AST)

</a>
<?}
if ($lang == 'kz')
{?>
<div class="descriptionimg" style="margin-top: -70px;
background: rgba(0,0,0,.65);
z-index: 55;
padding: 5px;
height:auto;
position: relative;"> 
<a href="http://kazakh-tv.kz/kz/programms/program/program_581_people-and-economy" style="color: #fff; font-size: 14px;">«People and Economy»:<br/>Жасанды тауар нарығы
<br/>19 қазан 00:25 және 18:25
</a>
 
<?}?>
</div></div></div></div>
					<div>
      					<div class="linkedFlashContainer" style="margin-top:20px;">
							<a target="_blank" href="http://kazakh-tv.kz/ru/allNews/estafeta_khabar" class="flashLink"></a>
							<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="225" height="375" style="margin-left:2px;" align="middle">
							<param name="movie" value="/upload/relay.swf" />
							<param name="wmode" value="opaque">
							<embed src="/upload/relay.swf" quality="high" wmode="opaque" width="225" height="375" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
							</object>
						</div>
      				</div>
					<div style="margin-top:20px;border:1px solid #ececec;">
      				<?php $this->widget("PopularPagesListWidgetNew");?></div>
					<?php include('lightbanner.php'); ?>

				</div>
				
			</td>
		</tr>
	</table>
</div>
			  

			</div>
	</div></div>

<?php 
	$this->renderPartial('//layouts/_footer'); 
?>
