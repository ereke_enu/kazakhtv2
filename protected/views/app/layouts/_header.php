<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable = no, width=device-width">
	<title></title>

	<link href="/themes/app/css/bootstrap.min.css" rel="stylesheet">
	<link href="/themes/app/css/bootstrap-theme.css?v=1.2.4" rel="stylesheet">
	<link href="/themes/app/css/styles.css?v=1.2.4" rel="stylesheet">
	<link href="/themes/app/css/idangerous.swiper.css">

	<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&subset=cyrillic-ext,latin' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=PT+Serif&subset=cyrillic-ext,latin' rel='stylesheet' type='text/css'>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>