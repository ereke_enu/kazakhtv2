<?php
/*
if (Yii::app()->request->getUserHostAddress() == '89.218.61.2' || Yii::app()->request->getUserHostAddress() == '82.200.130.64') {
	Yii::app()->translate->renderMissingTranslationsEditor();
}
*/
?>

	<script src="/themes/app/js/jquery.min.js"></script>
	<script src="/themes/app/js/jquery-migrate.min.js"></script>
	<script src="/themes/app/js/bootstrap.min.js"></script>
	<script src="/themes/app/js/jquery-ias.js"></script>
	<script src="/themes/app/js/extension/trigger.js"></script>
	<script src="/themes/app/js/extension/spinner.js"></script>
	<script src="/themes/app/js/extension/noneleft.js"></script>
	<script src="/themes/app/js/callbacks.js"></script>
	<script src="/themes/app/js/moment.min.js"></script>
	<script src="/themes/app/js/locale/ru.js"></script>
	<script src="/themes/app/js/locale/kz.js"></script>
	<script src="/themes/app/js/idangerous.swiper.js"></script>
	<script>
		/*<![CDATA[*/

		$(document).ready(function() {

			if($('.block-news').length > 0) {
				var ias = $.ias({
					container: ".block-news .row",
					item: ".news-item",
					pagination: "#pagination",
					next: ".next:not(.hidden) a"
				});

				ias.extension(new IASTriggerExtension({offset: 10, text: '<?= Yii::t('app', 'Загрузить ещё') ?>'}));
				ias.extension(new IASSpinnerExtension());
			}

			if($('.searchResult').length > 0) {
				var ias = $.ias({
					container: ".searchResult ul",
					item: "li",
					pagination: "#pagination",
					next: ".next:not(.hidden) a"
				});

				ias.extension(new IASTriggerExtension({offset: 10, text: '<?= Yii::t('app', 'Загрузить ещё') ?>'}));
				ias.extension(new IASSpinnerExtension());
			}

			if($('#searchForm').length > 0) {
				if($('#searchForm input[name=query]').val().length > 0) {
					var delButton = $('<button type="button" class="btn"><i class="glyphicon glyphicon-remove"></i></button>');
					var delSpan = $('<span class="input-group-btn reset-form-block"></span>');

					delButton.on('click', function(e) {
						e.preventDefault();
						$('#searchForm input[name=query]').val('');
						$('#searchForm .reset-form-block').remove();
					});

					delSpan.append(delButton);
					$('#searchForm input[name=query]').parent('.input-group').append(delSpan);
				}


				$('#searchForm input[name=query]').on('keyup', function() {
					var _this = $(this);
					if(_this.val().length > 0) {
						if($('#searchForm .reset-form-block').length <= 0) {
							var delButton = $('<button type="button" class="btn"><i class="glyphicon glyphicon-remove"></i></button>');
							var delSpan = $('<span class="input-group-btn reset-form-block"></span>');

							delButton.on('click', function(e) {
								e.preventDefault();
								$('#searchForm input[name=query]').val('');
								$('#searchForm .reset-form-block').remove();
							});

							delSpan.append(delButton);
							_this.parent('.input-group').append(delSpan);
						}
					} else {
						$('#searchForm .reset-form-block').remove();
					}
				});
			}

			if($('.timetable_all').length > 0 && $('.onair').length > 0) {
				$('html,body').animate({
					scrollTop: $('.onair').closest('tr').prev().prev().offset().top - 52
				}, 'slow');
			}

			var calendar = $('.timetable-calendar-new');
			var today;
			moment.locale('<?= Yii::app()->language ?>');

			if(calendar.length > 0) {
				calendar.find('.days').swiper({
					mode:'horizontal',
					loop: false,
					wrapperClass: 'wrapper',
					slideClass: 'slide',
					slideElement: 'li',
					slideActiveClass: 'active',
					initialSlide: calendar.find('.days li').index(calendar.find('.days li.active')),
					onSlideChangeEnd: function(swiper) {
						today = calendar.find('.days li.active');
						calendar.find('.years .wrapper').html('<li class="active">'+moment(today.data('value')).format('YYYY')+'</li>');
						calendar.find('.monthes .wrapper').html('<li class="active">'+moment(today.data('value')).format('MMMM')+'</li>');
					}
				});
				$.each(calendar.find('.days li'), function(i, e) {
					var el = $(e);
					el.find('.weekday').html(moment(el.data('value')).format('ddd'));
				});
				today = calendar.find('.days li.active');
				calendar.find('.years .wrapper').html('<li class="active">'+moment(today.data('value')).format('YYYY')+'</li>');
				calendar.find('.monthes .wrapper').html('<li class="active">'+moment(today.data('value')).format('MMMM')+'</li>');
			}

			$('#openTimetable').on('click', function(e) {
				e.preventDefault();
				today = calendar.find('.days li.active');

				window.location = $(this).attr('href')+'?date='+today.data('value');
			});
		});

		/*]]>*/
	</script>

<?php /*
	<script>
		//<![CDATA[
		$(document).ready(function() {
			var IMG_WIDTH = 120,
				currentImg = 0,
				maxImages = $(".timetable-calendar .years li").length,
				speed = 500,
				imgs = $(".timetable-calendar .years");

			//Init touch swipe
			imgs.swipe( {
				triggerOnTouchEnd : true,
				swipeStatus : swipeStatus,
				allowPageScroll:"vertical"
			});

			function swipeStatus(event, phase, direction, distance, fingers) {
				//If we are moving before swipe, and we are going L or R, then manually drag the images
				if( phase=="move" && (direction=="left" || direction=="right") ) {
					var duration=0;

					if (direction == "left")
						scrollImages((IMG_WIDTH * currentImg) + distance, duration);

					else if (direction == "right")
						scrollImages((IMG_WIDTH * currentImg) - distance, duration);
				}

				//Else, cancel means snap back to the begining
				else if ( phase == "cancel") {
					scrollImages(IMG_WIDTH * currentImg, speed);
				}

				//Else end means the swipe was completed, so move to the next image
				else if ( phase =="end" ) {
					if (direction == "right")
						previousImage();
					else if (direction == "left")
						nextImage();
				}
			}

			function previousImage() {
				currentImg = Math.max(currentImg-1, 0);
				scrollImages( IMG_WIDTH * currentImg, speed);
			}

			function nextImage() {
				currentImg = Math.min(currentImg+1, maxImages-1);
				scrollImages( IMG_WIDTH * currentImg, speed);
			}

			function scrollImages(distance, duration) {
				imgs.css("-webkit-transition-duration", (duration/1000).toFixed(1) + "s");
				imgs.css("-moz-transition-duration", (duration/1000).toFixed(1) + "s");

				//inverse the number we set in the css
				var value = (distance<0 ? "" : "-") + Math.abs(distance).toString();

				imgs.css("-webkit-transform", "translate3d("+value +"px,0px,0px)");
				imgs.css("-moz-transform", "translate3d("+value +"px,0px,0px)");
			}
		});
		//]]>
	</script>
*/ ?>

	<script src="/themes/app/js/video-js/video.js"></script>
	<script>
		videojs.options.flash.swf = "/themes/app/js/video-js/video-js.swf"
	</script>
</body>
</html>