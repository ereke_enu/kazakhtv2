<?php $this->renderPartial('//layouts/_header'); ?>

<body id="search">
	<div class="container-fluid">
		<div id="searchContent">
		<?= $content ?>
	</div>
</div>

<?php $this->renderPartial('//layouts/_footer'); ?>
