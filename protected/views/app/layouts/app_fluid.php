<?php $this->renderPartial('//layouts/_header'); ?>

<body>
<div id="page">
	<div class="container-fluid">
		<?= $content ?>
	</div>
</div>

<?php $this->renderPartial('//layouts/_footer'); ?>
