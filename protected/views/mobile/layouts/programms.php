<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <!--<div class="container scroll inner">-->
    <div class="container inner">
        <div class="row">
            <div class="col-sm-9">
                <div class="row margin-bottom">
				<style>
		@media (max-width: 480px){
		.news_menu li{
		display:block;
		border:none;}
		
		.news_menu li:last-child{
		border:none;}
		}
		
        </style>
                    <?= $content ?>
                </div>
            </div>
            <div class="col-sm-3" data-program="<?=$this->program_id?>">
                <?php $this->widget('ArchiveProgrammsListWidget', array('categoryId' => 0, 'program' => $this->program_id)); ?>
            </div>
        </div>
    </div>



<?php $this->renderPartial('//layouts/_footer'); ?>
