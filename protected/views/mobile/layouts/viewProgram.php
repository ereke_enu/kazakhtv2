<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <!--<div class="container scroll inner">-->
    <div class="container inner">
        <div class="row">
            <div class="col-sm-9">
                <div class="row margin-bottom">
				<style>
		 .breadcrumbs{
		 display:none;
		 }
		 .anounce_program{
		 display:none;
		 }
		 </style>
                    <?= $content ?>
                </div>
            </div>
            <div class="col-sm-3" data-program="<?=$this->program_id?>">
                <?php $this->widget('ProgramsListWidget', array('category' => $this->program_category_id, 'program' => $this->program_id, 'title' => $this->program_category_title)); ?>
            </div>
        </div>
    </div>
    

<?php $this->renderPartial('//layouts/_footer'); ?>
