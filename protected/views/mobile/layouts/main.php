﻿<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <div class="container main" >
        
        
        <div class="row margin-bottom">
            <!-- Start: Новости -->
            <div class="col-sm-7"><hr class="bold" />
        <h2><a href="<?=Yii::app()->createUrl('/pages/default/category', array('sefname' => 'news'))?>"><?=Yii::t('pages', 'Новости');?></a></h2>
                <?php $this->widget('mainNewsWidget'); ?>
				  
            </div><!-- End: Новости -->
			
            <!-- Start: Мир -->
            <div class="col-sm-5">
                <!-- Start: Меню для xs -->
                <div class="row visible-xs">
                    <div class="col-lg-12">
                        <hr />
                    </div>
                    <div class="col-xs-6">
                        <h2 style="border:0;border-right:1px solid #d7d7d7;margin-bottom:0;padding-left:5px;background:#d7d7d7" id="hoverKaz" >
						
						<a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'news_kazakhstan'))?>" data-for="kazakhstan" data-parent="news" onclick="getElementById('hoverKaz').style.background = '#d7d7d7'; getElementById('hoverWorld').style.background = '#FFF';"><?=Yii::t('pages', 'Казахстaн');?></a>
						
						</h2>
                    </div>
                    <div class="col-xs-6">
                        <h2 style="border:0;margin-bottom:0;margin-left:-20px;padding-left:5px;" id="hoverWorld" ><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'world_news'))?>" data-for="world" data-parent="news" onclick="getElementById('hoverWorld').style.background = '#d7d7d7'; getElementById('hoverKaz').style.background = '#FFF';"><?=Yii::t('pages', 'Мир');?></a></h2>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr style="margin-bottom:15px;" />
                    </div>
                </div><!-- End: Меню для xs -->
<hr class="bold hidden-xs" />
        <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/category', array('sefname' => 'news'))?>"><?=Yii::t('pages', 'Лента новостей');?></a></h2>
              <!-- <h2 class="hidden-xs" style="margin-top:-15px;"><a href="<?//=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'world_news'))?>"><?//=Yii::t('pages', 'Мир');?>Лента новостей</a></h2>-->
                
                <div  class="hidden-xs" id="blockNewsScroll" style="font-size: 12px;overflow-y: scroll;height: 360px;">
				<style>
				#blockNewsScroll{
					-webkit-overflow-scrolling: touch;
				}
				
				#blockNewsScroll h2{
					display:none;
				}
				
				#blockNewsScroll .archive{
					display:none;
				}
				
				#blockNewsScroll li{
					list-style-type:none;
					padding:0px 0px 20px 0px;
				}
				
				#blockNewsScroll ul{
					padding:0px;
				}
				
				#blockNewsScroll::-webkit-scrollbar-track
				{
					-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
					background-color: #F5F5F5;
					border-radius: 10px;
				}

				#blockNewsScroll::-webkit-scrollbar
				{
					width: 10px;
					background-color: #F5F5F5;
				}

				#blockNewsScroll::-webkit-scrollbar-thumb
				{
					background-color: #AAA;
					border-radius: 10px;

				}

				.latest_news{
					padding-right:5px;
				}

				</style>
                    <?php //$this->widget('worldNewsWidget'); ?>
					<?php $this->widget('blockNewsMobile'); ?>
                    <a href="<?//=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'world_news'))?>" class="to-all arrow pull-right"><?//=Yii::t('pages', 'Все новости');?></a>
                </div>
            </div><!-- End: Мир -->

        </div>
        <hr class="hidden-xs" />
        
        <!-- Start: Казахстан -->
       <!-- <div class="row margin-bottom" data-block="kazakhstan" data-parent="news">
            <div class="col-sm-12">
                <h2 class="hidden-xs"><a href="<?//=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'news_kazakhstan'))?>"><?//=Yii::t('pages', 'Казахстан');?></a></h2>
            </div>
            <?php //$this->widget('kazNewsWidget'); ?>
        </div>-->
        <!--<hr />-->
		
		<!-- End: Казахстан -->

		
		 <div style="clear:both;"></div>
		 <div class="row margin-bottom">
            <div class="col-sm-6" data-block="kazakhstan" data-parent="news">
                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'news_kazakhstan'))?>"><?=Yii::t('pages', 'Казахстан');?></a></h2>
                
                <?php $this->widget('kazNewsWidget'); ?>
                <a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'news_kazakhstan'))?>" style="text-decoration: underline;" class="to-all arrow pull-right"><?=Yii::t('pages', 'Читать еще');?></a>
            </div><!-- End: Бизнес -->

            <!-- Start: Спорт -->
            <div class="col-sm-6 hidden-xs" data-block="world" data-parent="news">
                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'world_news'))?>"><?=Yii::t('pages', 'Мир');?></a></h2>
                
                <?php $this->widget('worldNewsWidget'); ?>
                <a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'world_news'))?>" style="text-decoration: underline;" class="to-all arrow pull-right"><?=Yii::t('pages', 'Читать еще');?></a>
            </div><!-- End: Спорт -->
        
		
		</div>
		
		
		
		
		
		
        <!-- Start: Бизнес/Новости -->
        <div class="row margin-bottom">
            <div class="col-xs-12 visible-xs">
			<hr>
                <!-- Start: Меню для xs -->
                <div class="row">
				<style>
				
				</style>
                    <div class="col-xs-6">
					
                        <h2 style="border:0;border-right:1px solid #d7d7d7;margin-bottom:0;padding-left:5px;background:#d7d7d7" id="hoverBiz" ><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'business'))?>" data-for="business" data-parent="bizsport"  onclick="getElementById('hoverBiz').style.background = '#d7d7d7'; getElementById('hoverSport').style.background = '#FFF';"><?=Yii::t('pages', 'Бизнес');?></a></h2>
                    </div>
                    <div class="col-xs-6">
                        <h2 style="border:0;margin-bottom:0;margin-left:-20px;padding-left:5px;" id="hoverSport" ><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'sport'))?>" data-for="sport" data-parent="bizsport" onclick="getElementById('hoverSport').style.background = '#d7d7d7'; getElementById('hoverBiz').style.background = '#FFF';"><?=Yii::t('pages', 'Спорт');?></a></h2>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr style="margin-bottom:15px;" />
                    </div>
                </div><!-- End: Меню для xs -->
            </div>

            <!-- Start: Бизнес --><hr>
            <div style="clear:both;"></div>
            <div class="col-sm-6" data-block="business" data-parent="bizsport">
                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'business'))?>"><?=Yii::t('pages', 'Бизнес');?></a></h2>
                
                <?php $this->widget('sefnameNewsWidget', array('sefname' => 'business')); ?>
                <a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'business'))?>" style="text-decoration: underline;" class="to-all arrow pull-right"><?=Yii::t('pages', 'Читать еще');?></a>
            </div><!-- End: Бизнес -->

            <!-- Start: Спорт -->
            <div class="col-sm-6 hidden-xs" data-block="sport" data-parent="bizsport">
                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'sport'))?>"><?=Yii::t('pages', 'Спорт');?></a></h2>
                
                <?php $this->widget('sefnameNewsWidget', array('sefname' => 'sport')); ?>
                <a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'sport'))?>" style="text-decoration: underline;" class="to-all arrow pull-right"><?=Yii::t('pages', 'Читать еще');?></a>
            </div><!-- End: Спорт -->
        </div><!-- End: Бизнес/Новости -->

        <!-- Start: HiTech/Культура -->
        <div class="row margin-bottom">
            <div class="col-xs-12 visible-xs">
            <hr>
                <!-- Start: Меню для xs -->
                <div class="row">
                <style>
                
                </style>
                    <div class="col-xs-6">
                    
                        <h2 style="border:0;border-right:1px solid #d7d7d7;margin-bottom:0;padding-left:5px;background:#d7d7d7" id="hoverHt" ><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'hi-tech'))?>" data-for="hi-tech" data-parent="htculture"  onclick="getElementById('hoverHt').style.background = '#d7d7d7'; getElementById('hoverCulture').style.background = '#FFF';"><?=Yii::t('pages', 'HiTech');?></a></h2>
                    </div>
                    <div class="col-xs-6">
                        <h2 style="border:0;margin-bottom:0;margin-left:-20px;padding-left:5px;" id="hoverCulture" ><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'culture'))?>" data-for="culture" data-parent="htculture" onclick="getElementById('hoverCulture').style.background = '#d7d7d7'; getElementById('hoverHt').style.background = '#FFF';"><?=Yii::t('pages', 'Культура');?></a></h2>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr style="margin-bottom:15px;" />
                    </div>
                </div><!-- End: Меню для xs -->
            </div>

            <!-- Start: HiTech --><hr>
            <div style="clear:both;"></div>
            <div class="col-sm-6" data-block="hi-tech" data-parent="htculture">
                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'hi-tech'))?>"><?=Yii::t('pages', 'HI-TECH');?></a></h2>
                
                <?php $this->widget('sefnameNewsWidget', array('sefname' => 'hi-tech')); ?>
                <a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'hi-tech'))?>" style="text-decoration: underline;" class="to-all arrow pull-right"><?=Yii::t('pages', 'Читать еще');?></a>
            </div><!-- End: HiTech -->

            <!-- Start: Культура -->
            <div class="col-sm-6 hidden-xs" data-block="culture" data-parent="htculture">
                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'culture'))?>"><?=Yii::t('pages', 'Культура');?></a></h2>
                
                <?php $this->widget('sefnameNewsWidget',array('sefname' => 'culture')); ?>
                <a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'culture'))?>" style="text-decoration: underline;" class="to-all arrow pull-right"><?=Yii::t('pages', 'Читать еще');?></a>
            </div><!-- End: Культура -->
        </div><!-- End: HiTech/Культура -->
        
            <!-- Start: Здоровье/Кино -->
        <div class="row margin-bottom">
            <div class="col-xs-12 visible-xs">
            <hr>
                <!-- Start: Меню для xs -->
                <div class="row">
                <style>
                
                </style>
                    <div class="col-xs-6">
                    
                        <h2 style="border:0;border-right:1px solid #d7d7d7;margin-bottom:0;padding-left:5px;background:#d7d7d7" id="hoverHealth" ><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'health'))?>" data-for="health" data-parent="healthcinema"  onclick="getElementById('hoverHealth').style.background = '#d7d7d7'; getElementById('hoverCinema').style.background = '#FFF';"><?=Yii::t('pages', 'Здоровье');?></a></h2>
                    </div>
                    <div class="col-xs-6">
                        <h2 style="border:0;margin-bottom:0;margin-left:-20px;padding-left:5px;" id="hoverCinema" ><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'cinema'))?>" data-for="cinema" data-parent="healthcinema" onclick="getElementById('hoverCinema').style.background = '#d7d7d7'; getElementById('hoverHealth').style.background = '#FFF';"><?=Yii::t('pages', 'Кино');?></a></h2>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr style="margin-bottom:15px;" />
                    </div>
                </div><!-- End: Меню для xs -->
            </div>

            <!-- Start: Здоровье --><hr>
            <div style="clear:both;"></div>
            <div class="col-sm-6" data-block="health" data-parent="healthcinema">
                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'health'))?>"><?=Yii::t('pages', 'Здоровье');?></a></h2>
                
                <?php $this->widget('sefnameNewsWidget', array('sefname' => 'health')); ?>
                <a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'health'))?>" style="text-decoration: underline;" class="to-all arrow pull-right"><?=Yii::t('pages', 'Читать еще');?></a>
            </div><!-- End: Здоровье -->

            <!-- Start: Кино -->
            <div class="col-sm-6 hidden-xs" data-block="cinema" data-parent="healthcinema">
                <h2 class="hidden-xs"><a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'cinema'))?>"><?=Yii::t('pages', 'Кино');?></a></h2>
                
                <?php $this->widget('sefnameNewsWidget', array('sefname' => 'cinema')); ?>
                <a href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'cinema'))?>" style="text-decoration: underline;" class="to-all arrow pull-right"><?=Yii::t('pages', 'Читать еще');?></a>
            </div><!-- End: Кино -->
        </div><!-- End: Здоровье/Кино -->

        
        <!-- Start: Программы -->
        

            <?php 
                $lang = Yii::app()->language; 
                if($lang=="ru"){
                    $analytics="Аналитика";
                    $educational = "Познавательное";
                    $publicism="Публицистика";
                    $culture="Культура";
                    $tourism="Туризм";
                    $sport="Спорт";
                }elseif ($lang=="en") {
                    $analytics="Analytics";
                    $educational = "Educational";
                    $publicism="Publicism";
                    $culture="Culture";
                    $tourism="Tourism";
                    $sport="Sport";
                }else{
                    $analytics="Сараптама";
                    $educational = "Танымдық";
                    $publicism="Публицистика";
                    $culture="Мәдениет";
                    $tourism="Туризм";
                    $sport="Спорт";
                }
            ?>
        <!-- End: Программы -->
        <div class="row margin-bottom journals">
            <div class="col-sm-12">
                <hr class="bold ">
                <h2><a href="<?=Yii::app()->createUrl('/pages/default/category', array('sefname' => 'programms'))?>"><?=Yii::t('pages', 'Программы');?></a></h2>
            </div>
            
                        <div class="col-xs-6 col-sm-4 clearfix">
                
                <h3><a href="/pda.php/<?=$lang?>/program/economics_and_innovations"><?=$analytics ?></a></h3>
                <a href="/pda.php/<?=$lang?>/program/economics_and_innovations"><img src="/upload/programms/main/analytics_pda.jpg" class="img-responsive"></a>
                <!--<p><a href=""></a></p>-->
            </div>
            <div class="col-xs-6 col-sm-4 clearfix">
                
                <h3><a href="/pda.php/<?=$lang?>/program/economics_and_innovations"><?=$educational ?></a></h3>
                <a href="/pda.php/<?=$lang?>/program/economics_and_innovations"><img src="/upload/programms/main/science.jpg" class="img-responsive"></a>
                <!--<p><a href=""></a></p>-->
            </div>
            <div class="col-xs-6 col-sm-4 clearfix">
                
                <h3><a href="/pda.php/<?=$lang?>/program/publicism"><?=$publicism ?></a></h3>
                <a href="/pda.php/<?=$lang?>/program/publicism"><img src="/upload/programms/main/publicism_pda.jpg" class="img-responsive"></a>
                <!--<p><a href=""></a></p>-->
            </div>
            <div class="col-xs-6 col-sm-4 clearfix">
                
                <h3><a href="/pda.php/<?=$lang?>/program/history_and_culture_tv"><?=$culture ?></a></h3>
                <a href="/pda.php/<?=$lang?>/program/history_and_culture_tv"><img src="/upload/programms/main/culture.jpg" class="img-responsive"></a>
                <!--<p><a href=""></a></p>-->
            </div>
            <div class="col-xs-6 col-sm-4 clearfix hidden-xs">
                
                <h3><a href="/pda.php/<?=$lang?>/program/tourism"><?=$tourism ?></a></h3>
                <a href="/pda.php/<?=$lang?>/program/tourism"><img src="/upload/programms/main/tourism.jpg" class="img-responsive"></a>
                <!--<p><a href=""></a></p>-->
            </div>
            <div class="col-xs-6 col-sm-4 clearfix hidden-xs">
                
                <h3><a href="/pda.php/<?=$lang?>/program/sport"><?=$sport ?></a></h3>
                <a href="/pda.php/<?=$lang?>/program/sport"><img src="/upload/programms/main/sp.jpg" class="img-responsive"></a>
                <!--<p><a href=""></a></p>-->
            </div>
        </div>
        <!-- Start: Журналы -->
        <div class="row margin-bottom journals">
            <div class="col-sm-12">
                <hr class="bold " />
                <h2><a href="<?=Yii::app()->createUrl('/programms/default/index', array('id' => 'features'))?>"><?=Yii::t('pages', 'Журналы');?></a></h2>
            </div>
            
            <?php $this->widget('listJournalsWidget'); ?>
            <div class="col-sm-12">
                <!--<hr />-->
                <a href="<?=Yii::app()->createUrl('/programms/default/index', array('id' => 'features'))?>" class="to-all to-journals arrow pull-right" style="border-top: 1px solid #ccc;width: 100%;text-align: right; text-decoration: underline;font-size:1.33em;" ><?=Yii::t('pages', 'Все журналы');?></a>
            </div>
        </div><!-- End: Журналы -->
    </div>
<?php $this->renderPartial('//layouts/_footer'); ?>
