<?php
$this->pageTitle = Yii::app()->name . ' - Ошибка';
$this->breadcrumbs = array(
    'Ошибка',
);
?>

<div class="col-sm-12">
    <hr class="bold" />
    <h1>Ошибка <?php echo $code; ?></h1>
    <hr class="margin-bottom" />
    <div class="article-text">
        <?php echo CHtml::encode($message); ?>
    </div>
</div>