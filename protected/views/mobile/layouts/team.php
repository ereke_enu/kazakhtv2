﻿<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <div class="container scroll inner">
        <div class="row margin-bottom">
            <?= $content ?>
        </div>
    </div>
    <div class="container inner">
        <?php $this->widget('PagesListWidget', array('category' => 'about_us_team', 'title' => Yii::t('pages', 'Читайте также'))); ?>
    </div>

<?php $this->renderPartial('//layouts/_footer'); ?>