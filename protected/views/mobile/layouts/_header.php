<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <script src="/themes/mobile/js/jquery-2.0.3.min.js"></script>
        <script src="/themes/mobile/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="/themes/mobile/js/bootstrap.min.js"></script>
        <script src="/themes/mobile/js/jquery-ias.min.js"></script>
        <title><?= $this->pageTitle ?></title>
        <meta name="keywords" content="<?= $this->metaTags ?>"/>
        <meta name="description" content="<?= $this->metaDescription ?>"/>
        <link href="/themes/mobile/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="/themes/mobile/css/style.css" rel="stylesheet" type="text/css">
        
        <script type="text/javascript">
            $(function() {
                $('#kazakhstan-rubrics').on('click', function(e) {
                    $(this).parents('div.kazakhstan').find('ul li.collapse').toggleClass('in');
                    if($(this).hasClass('down')) {
                        $(this).removeClass('down');
                        $(this).addClass('up');
                    } else {
                        $(this).removeClass('up');
                        $(this).addClass('down');
                    }
                    return false;
                });

                $(window).bind('resize orientationchange load ready', this, function(e) {
                    if($('#videoplayer_content')) {
                        $('#videoplayer_content').height(Math.floor($('#videoplayer_content').width() * 1080 / 1920));
                    }
                });

                if($('#iasNews')) {
                    var ias = jQuery.ias({
                        container : '.scroll',
                        item: '.element',
                        pagination: '.scroll #iasNews.pager',
                        next: 'li[class=next] a',
                        triggerPageThreshold: 5,
                        trigger: '<center><span type="button" class="btn btn-default"><?=Yii::t('pages', 'Загрузить ещё новости');?></span></center>',
                        loader: '<img src="/themes/mobile/img/loader.gif" width="24" height="24" class="center-block" />'
                    });
                }
                
                $('a[data-for]').on('click', function() {
                    if($('div[data-block='+$(this).attr('data-for')+']').hasClass('hidden-xs'))
                        $('div[data-parent='+$(this).attr('data-parent')+']').toggleClass('hidden-xs');
                    
                    return false;
                });
                
                $('a[data-forc]').on('click', function() {
                    //if($('ul[data-block='+$(this).attr('data-forc')+']').hasClass('hidden-xs'))
                        $('ul[data-block='+$(this).attr('data-forc')+']').toggleClass('hidden-xs');
                    
                    return false;
                });
                
                $('#navbar-lang-mobile li.active').on('click', function() {
                    $('#navbar-lang-mobile li[data-select=lang]').toggleClass('hidden');
                });
            });
        </script>
        <script type="text/javascript">
            var language = '<?= Yii::app()->language ?>';
        </script>
		<style>
		.breadcrumbs{
		display:none;
		}
		
		iframe{
		width:100%;
		}
		
		
		
	
		.breadcrumbs + hr { margin-top: -1px; }
		.news_menu + hr { margin-top: -1px; }
	
	
	a[href="/pda.php/ru/category/live_now"] img {
    width:20px;
    }
	
		@media (max-width: 480px){
		
		#videoplayer_content {
    position: relative;
    overflow: hidden;
    height: 0;
    padding-bottom: 54.15%;
}
#videoplayer_content iframe,  
#videoplayer_content object,  
#videoplayer_content embed {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
}
		
		
		#player{
		width:100%;
		}
		
		#videoplayer_content{
		height:400px;
		}
		
		/*.breadcrumbs + hr { margin-top: -1px; }*/
		
		.navbar-collapse a[href="/pda.php/ru/category/live_now"]:before  {
		content: url(/upload/1-22.gif);
		}
		
		
		img{
		max-width: 100%;
		height: auto;
		margin-left:0px;
		}
		
		
		.news_menu li{
		display:block;
		border:none;
		text-align:left;
		line-height: 27px;
border: none;
border-bottom: 1px solid #DCDBDB;}
		
		.news_menu li:last-child{
		border:none;}
		
			.journals h3{
			height: 26px;
			}
		}
		
		.col-sm-offset-3 ul{
		margin-left: 87px;
		}
		
		.social li i {
display: block;
height: 26px;
width: 26px;
}

@media (min-width: 768px){
			.programms h3{
			height:34px;
			}
			
			.journals h3{
			height: 34px;
			}
			
			#imgarrow0{
			display:none;
			}
			#imgarrow1{
			display:none;
			}
			#imgarrow2{
			display:none;
			}
			#imgarrow3{
			display:none;
			}
			#imgarrow4{
			display:none;
			}
			#imgarrow5{
			display:none;
			}
			}
			
		
		</style>
    </head>

    <body>
        <div id="page">	
		
		 <nav class="navbar navbar-default" role="navigation">
                    <div class="menu">
                        <div class="container" id="navbar-container">
						
						
                            <!--<a class="logo" href="/pda.php/"><img src="/themes/mobile/img/logo.png" width="165" class="img-responsive visible-xs" alt="KazakhTV" style="position:absolute; top:10px;" /><!--<p style="font-weight:bold;font-size: 1.4em; text-transform: uppercase;color:#024561;margin-top:15px; margin-left:8px;"><font color="#CDB000">Kazakhstan</font> in the world</p>--><!--</a>-->
                            <a class="logo" href="/pda.php/"><img src="/themes/mobile/img/logo.png" width="165" class="img-responsive visible-xs" alt="KazakhTV" style="position:absolute; top:10px;" /><!--<p style="font-weight:bold;font-size: 1.4em; text-transform: uppercase;color:#024561;margin-top:15px; margin-left:8px;"><font color="#CDB000">Kazakhstan</font> in the world</p>--></a>
							
                            
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle toggle-search" data-parent="#navbar-container" data-toggle="collapse" data-target="#navbar-search"><i class="sprite sprite-search"></i></button>
                                <button type="button" class="navbar-toggle toggle-lang" data-parent="#navbar-container" data-toggle="collapse" data-target="#navbar-langs">
                                    <?php $this->widget('blockLanguageSelector', array('first'=>true)) ?>
                                </button>
                                <button type="button" class="navbar-toggle" data-parent="#navbar-container" data-toggle="collapse" data-target="#navbar-xs">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                            </div>
                            
							
							
                            <div class="collapse navbar-collapse" id="navbar-xs">
							<style>
							.navbar-collapse ul li ul{
							display:none;}
			
							@media (max-width: 480px) {
							
							.navbar-collapse ul li{
							padding:10px 0px;}		
							
							.navbar-collapse ul {
							margin-top:10px;}	
							}		
							</style>
							<?php $this->widget('MainMenuWidget', array('sectionId' => 1)); ?>
                                <div class="visible-xs">
                                    <?php //$this->widget('MainMenuWidget', array('sectionId' => 3)); ?>
                                </div>
                            </div>
                          <!--<div class="hidden-xs pull-right">
                                <?php //$this->widget('blockLanguageSelector') ?>
                            </div>-->

                            <div class="collapse" id="navbar-search">
                                <div class="visible-xs" style="padding:10px 0 0;">
                                    <form action="<?= Yii::app()->createUrl('/pda.php/search/default/index') ?>" method="GET">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-search" placeholder="<?= Yii::t('search', 'Поиск...'); ?>" name="query">
                                        <span class="input-group-btn" style="border-left: 1px solid #D7D7D7;padding-left: 2px;"> 
                                            <button class="btn btn-default btn-search pull-right" type="submit" title="<?= Yii::t('search', 'Поиск'); ?>"><img style="width:21px;" src="/upload/find.png"></button>
                                        </span>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
						
                    </div>
                </nav>
		
		
		
		<div id="fontab">
			<div id="wrappertab">
<style>


.scrollbar{
border-radius:0px;
}

@media (max-width: 768px){
.navbar .nav.navbar-menu{
font-size:1.2em;

}

.navbar-collapse ul{
text-transform:none;
}
}

@media (min-width: 768px){

.navbar-nav>li>a{
padding-top:8px;
padding-bottom:0px;
}


.social img{
width:24px;
margin-top:5px;
margin-bottom:5px;
}

#fontab{background: #F7F7F7;
width: 100%;
padding-bottom: 3px;
height: 100%;
}

#wrappertab{
background: #FFF;
width: 762px;
padding-top:15px;
margin: 0px auto;
box-shadow: 0px 0 3px rgba(0, 0, 0, 0.25)
}

.scrollbar{
border-radius:0px;
}

.navbar .nav.navbar-menu{
margin-left:-30px;
font-size:1.0em;
}

#navbar-lang{
margin-left:80px;
text-transform: uppercase;
font-weight:bold;
font-size:1.4em;
color:#024561;
}

#navbar-lang a{
color:#024561;
line-height: 23px;
}

#navbar-lang li.active a {
background: none;
color: #CDB000;
}
} 

/*h1 + hr {display:none;} */

.navbar-collapse ul :nth-child(11) {
					display:none;
					}
					
.navbar-collapse ul li:nth-child(9) {
					display:none;
					}
					
.navbar .nav.navbar-menu li:nth-child(9) a{
					display:none;
					}
					.navbar .nav.navbar-menu li:nth-child(11) a{
					display:none;
					}
					.navbar .nav.navbar-menu li:nth-child(12) a{
					display:none;
					}
					.navbar-collapse ul li :nth-child(9){
							display:none;}
</style>
            <header>
                <!-- Start: Навигация -->
               <!-- End: Навигация -->

                <!-- Start: Шапка -->
			
                <div class="container">
                    <div class="row margin-bottom" style="margin-bottom:0px;">
                        <div class="col-sm-4 hidden-xs">
                            <!--<a class="logo" href="/pda.php/"><img src="/themes/mobile/img/logo.png" width="219" height="60" class="img-responsive" alt="KazakhTV" />-->
                            <a class="logo" href="/pda.php/"><img src="/themes/mobile/img/logo.png" width="219" height="60" class="img-responsive" alt="KazakhTV" />
							
							</a><p style="font-weight:bold;font-size: 1.4em; text-transform: uppercase;color:#024561;"><font color="#CDB000">Kazakhstan</font> in the world</p>
                        </div>
                        <div class="col-sm-5 col-sm-offset-3" style="margin-top:-10px;">
						      <div class="hidden-xs">
							  <style>
							  	@media (min-width: 768px) {.navbar .nav.navbar-lang li, .navbar .nav.navbar-lang li a{color: #fad234;}
							}</style>
							  <?php $this->widget('blockLanguageSelector') ?>
                            </div>
                            <ul class="social hidden-xs">
                              <!--  <li><a href="http://vk.com/kazakhtv" target="_blank"><i class="sprite sprite-vk"></i></a></li>
                                <li><a href="http://twitter.com/Kazakh_TV" target="_blank"><i class="sprite sprite-twitter"></i></a></li>
                                <li><a href="https://www.youtube.com/KazakhTVchannel" target="_blank"><i class="sprite sprite-youtube"></i></a></li>
                                <li><a href="#" target="_blank"><i class="sprite sprite-google"></i></a></li>
                                <li><a href="http://facebook.com/KazakhTV" target="_blank"><i class="sprite sprite-facebook"></i></a></li>
                                <li><a href="<?//= Yii::app()->createUrl('/pages/default/rss') ?>" target="_blank"><i class="sprite sprite-rss"></i></a></li>
                                <li><a href="#" target="_blank"><i class="sprite sprite-instagram"></i></a></li>-->
								
								<li><a href="http://vk.com/kazakhtv" target="_blank"><img src="/images/social/vkg.png"/></a></li>
                                <li><a href="http://twitter.com/Kazakh_TV" target="_blank"><img src="/images/social/tg.png"/></a></li>
                                <li><a href="https://www.youtube.com/KazakhTVchannel" target="_blank"><img src="/images/social/yg.png"/></a></li>
                                <li><a href="https://plus.google.com/+KazakhTV/posts" target="_blank"><img src="/images/social/gg.png"/></a></li>
                                <li><a href="http://facebook.com/KazakhTV" target="_blank"><img src="/images/social/fg.png"/></a></li>
                                <li><a href="http://kazakhtv.yvision.kz/" target="_blank"><img src="/images/social/yvs.png"/></a></li>
                                <li><a href="http://instagram.com/kazakh_tv" target="_blank"><img src="/images/social/ig.png"/></a></li>
								
								
                            </ul>
                            <div class="hidden-xs">
                                <form action="<?= Yii::app()->createUrl('/pda.php/search/default/index') ?>" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control input-search" placeholder="<?= Yii::t('search', 'Поиск...'); ?>" name="query">
                                    <span class="input-group-btn" style="border-left: 1px solid #E3E3E3;">
                                        <button class="btn btn-default btn-search pull-right" type="submit" title="<?= Yii::t('search', 'Поиск'); ?>"><!--<i class="sprite sprite-search"></i>--><img style="width: 23px;" src="/upload/find.png" /></button>
                                    </span>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Start: Scroll -->
					<style>
						@media (max-width: 480px) {
						.margin-bottom{
						margin-top:8px;
						margin-bottom:8px;
						}
						}
						</style>
                    <div class="row margin-bottom">
                        <div class="col-sm-12">
                            <div class="scrollbar">
                                <div style="margin-left:2px" direction="left" scrollamount="2" scrolldelay="0">
                                    <span id="realday_name" class="day_name"></span>,
                                    <span id="realdate" class="date"></span>
                                    <span id="realtime" class="time"></span>
                                    <?php //$this->widget('CurrencyWidget'); ?>
                                </div>
                            </div>
                        </div>
                    </div><!-- End: Scroll -->
                </div><!-- End: Шапка -->
            </header>

