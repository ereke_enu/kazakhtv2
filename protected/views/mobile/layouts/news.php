﻿<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <div class="container scroll inner">
        <div class="row margin-bottom">
		 <style>
		 .breadcrumbs{
		 display:none;
		 }
		 
		 @media (max-width:480px){
		 .news_menu li{
		 border-bottom:1px solid #DCDBDB;
		 }
		 }
		
		 </style>
            <?= $content ?>
        </div>
    </div>
    <div class="container inner">
        <?php $this->widget('PagesListWidgetMob', array('category' => 'news', 'title' => Yii::t('pages', 'Последние новости'), 'date' => true)); ?>
    </div>

<?php $this->renderPartial('//layouts/_footer'); ?>
