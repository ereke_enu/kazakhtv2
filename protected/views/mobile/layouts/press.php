<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <div class="container scroll inner">
        <div class="row">
            <div class="col-sm-9">
                <div class="row">
                    <?= $content ?>
                </div>
            </div>
            <div class="col-sm-3" style="margin-top: -14px;">
                <?php $this->widget('PagesListWidget', array('category' => 'press_about_us', 'title' => Yii::t('pages', 'Читайте также'))); ?>
            </div>
        </div>
        
    </div>

<?php $this->renderPartial('//layouts/_footer'); ?>
