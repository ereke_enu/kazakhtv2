﻿<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <!--<div class="container scroll inner">-->
    <div class="container inner">
        <div class="row margin-bottom">
		<style>
		 .breadcrumbs{
		 display:none;
		 }	 
		 .anounce_program{
		 display:none;
		 }
		</style>
            <?= $content ?>
        </div>
    </div>
    <div class="container inner">
        <?php $this->widget('ProgramsListWidget', array('category' => 'features', 'program' => $this->program_id, 'title' => Yii::t('programms', 'Тележурналы'))); ?>
        <hr class="bold" style="margin-top: 20px;" />
        <h2><a href="<?=Yii::app()->createUrl('/pda.php/pages/default/category', array('sefname' => 'programms'))?>"><?=Yii::t('pages', 'Программы');?></a></h2>
        <div class="row margin-bottom programms">
            <?php $this->widget('listCategoriesWidget'); ?>
        </div>
    </div>

<?php $this->renderPartial('//layouts/_footer'); ?>
