<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <div class="container inner">
        <div class="row margin-bottom">
		<style>
		@media (max-width: 480px){
		.news_menu li{
		display:block;
		border:none;
		border-bottom:1px solid #DCDBDB;
		 }
		
		.news_menu li:last-child{
		border:none;}
		}
        </style>
			<?= $content ?>
        </div>
    </div>
    <div class="container inner">
        <?php $this->widget('ArchivePagesListWidgetMob', array('categoryId' => $this->newsCategory)); ?>
    </div>

<?php $this->renderPartial('//layouts/_footer'); ?>
