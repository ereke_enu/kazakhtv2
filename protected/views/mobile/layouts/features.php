<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <!--<div class="container scroll inner">-->
    <div class="container inner">
        <div class="row margin-bottom">
		<style>
		 .breadcrumbs{
		 display:none;
		 }
		  	  .anounce_program{
		 display:none;
		 }
		@media (max-width: 480px){
		.news_menu li{
		display:block;
		border:none;}
		
		.news_menu li:last-child{
		border:none;}
		}
     
		 </style>
            <?= $content ?>
        </div>
    </div>
    <div class="container inner">
        <?php //$this->widget('ArchiveProgrammsListWidget', array('categoryId' => 'features', 'title' => Yii::t('programms', 'Последние тележурналы'))); ?>
		 <?php $this->widget('PagesListWidgetMob', array('category' => 'news', 'title' => Yii::t('pages', 'Последние новости'), 'date' => true)); ?>
    </div>
    
<?php $this->renderPartial('//layouts/_footer'); ?>
