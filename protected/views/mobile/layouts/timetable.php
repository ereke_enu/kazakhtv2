﻿<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <div class="container scroll inner">
        <div class="row margin-bottom">
            <?= $content ?>
        </div>
    </div>
    <div class="container inner">
        <?php $this->widget('PopularPagesListWidget', array('title' => Yii::t('pages', 'Популярные новости'))); ?>
    </div>

<?php $this->renderPartial('//layouts/_footer'); ?>
