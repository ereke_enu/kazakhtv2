    <!-- Start: Подвал -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 nav-footer">
                    <div class="footer-cols">
					<style>
							
							@media (min-width: 768px) {
							footer .nav-footer .navbar-nav li{
							margin: 0px 0px 15px 0px;}
							}
							
							@media (max-width:480px){
							
							.social{
							margin-left:20px;
							}
							
							footer .footer-cols{
							column-count:0;
							-webkit-column-count:0;
							}
							
							}
							
							#yw2 li:nth-child(6) a{
							display:none;
							} 								
					</style>
					
                    <div class="hidden-xs"><?php $this->widget('MainMenuWidget', array('sectionId' => 1));?></div>
                    <?php $this->widget('MainMenuWidget', array('sectionId' => 3));?>
					<ul class="nav navbar-nav navbar-menu">
                    <?php
                            $lang=Yii::app()->language;
                            $linkName="ru";
                            switch ($lang) {
                                case 'kz': $linkName="Толық нұсқа";break;
                                case 'ru': $linkName="Полная версия";break;
                                case 'en':$linkName="Full version";break;
                            }
                            echo '<li><a href="/'.$lang.'/mainpage/index">'.$linkName.'</a></li>';
                         ?>
                    </ul>
					
                    </div>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-xs-12 col-sm-4">
                    <ul class="hidden-xs">
                        <li><?=Yii::t('pages', 'Оставайтесь с Kazаkh TV');?></li>
                        <li><hr /></li>
                    </ul>
					
					<!--<?=Yii::t('pages', 'Лента новостей:');?>-->
			
                    <ul class="social" style="/*margin-top: 20px;*/">
                        <li><a href="http://vk.com/kazakhtv" target="_blank"><i class="sprite sprite-vk-large"></i> <span class="visible-sm">ВКонтакте</span></a></li>
                        <li><a href="http://twitter.com/Kazakh_TV" target="_blank"><i class="sprite sprite-twitter-large"></i> <span class="visible-sm">Twitter</span></a></li>
                        <li><a href="https://www.youtube.com/KazakhTVchannel" target="_blank"><i class="sprite sprite-youtube-large"></i> <span class="visible-sm">YouTube</span></a></li>
                        <li><a href="https://plus.google.com/+KazakhTV/posts"  target="_blank"><i class="sprite sprite-google-large"></i> <span class="visible-sm">Google+</span></a></li>
                        <li><a href="http://facebook.com/KazakhTV" target="_blank"><i class="sprite sprite-facebook-large"></i> <span class="visible-sm">Facebook</span></a></li>
                        <li><a href="http://kazakhtv.yvision.kz/" target="_blank"><i class="sprite sprite-yvi-large"></i> <span class="visible-sm">Yvision</span></a></li>
                        <li><a href="http://instagram.com/kazakh_tv"  target="_blank"><i class="sprite sprite-instagram-large"></i> <span class="visible-sm">Instagram</span></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-12" style="/*margin-top: 20px;*/">
                    <hr class="margin-bottom" />
                </div>
                <div class="col-xs-6 col-sm-6 pull-left"><img src="/themes/mobile/img/logo-footer.png" class="img-responsive" /></div>
                

                <div class="col-xs-6 col-sm-6 pull-right text-right copy">
                    Copyright &copy; 2014 Khabar Agency
                </div>
                <div class="col-xs-12 col-sm-12 pull-left">Все права на материалы сайта защищены Законом РК об авторском праве и смежных правах. Использование материалов Агентства "Хабар" без ссылки на источник преследуется по закону.</div>
                <div class="clearfix"></div>
            </div>
        </div>
    </footer><!-- End: Подвал -->
</div>
<?php
if (Yii::app()->request->getUserHostAddress() == '89.218.61.2') {
    Yii::app()->translate->renderMissingTranslationsEditor();
} else if (Yii::app()->user->checkAccess('Admin')) {
    Yii::app()->translate->renderMissingTranslationsEditor();
}
?>
<script type="text/javascript" src="/themes/mobile/js/time.js"></script>
<script type="text/javascript">
    var serv_time =<?=time()?>;
    var daylight_saving_on_serv = 0;
    setupTime(language);
    window.setInterval("setupTime('"+language+"');", 1000)
</script>

</body>
</html>