﻿<?php $this->renderPartial('//layouts/_header'); ?>

    <!-- Start: Контент -->
    <div class="container scroll inner">
        <div class="row margin-bottom">
            <?= $content ?>
        </div>
    </div>
   <div class="container inner">
        <?php $this->widget('PagesListWidget', array('category' => 'press_about_us', 'limit' => 0, 'title' => Yii::t('pages', 'Пресса о нас')));?>
    </div>
    <!--<div class="container inner">
        <?php //$this->widget('PagesListWidgetMob', array('category' => 'news', 'title' => Yii::t('pages', 'Последние новости'), 'date' => true)); ?>
    </div>-->
<?php $this->renderPartial('//layouts/_footer'); ?>
