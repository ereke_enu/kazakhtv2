<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <script src="/themes/front/js/jquery.js" type="text/javascript"></script>
    <script src="/themes/front/js/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="/themes/front/js/slides.min.jquery.js" type="text/javascript"></script>
    <script src="/themes/front/js/jqueryui.js" type="text/javascript"></script>
	<script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script> 

	<script type="text/javascript" src="/themes/front/js/jquery.totemticker.js"></script>
    <?php if (Yii::app()->language != 'en') { ?>
        <script src="/themes/front/js/jquery.ui.datepicker-<?= Yii::app()->language ?>.js"
                type="text/javascript"></script>
    <?php } ?>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title><?= $this->pageTitle ?></title>
    <meta name="keywords" content="<?= $this->metaTags ?>"/>
    <meta name="description" content="<?= $this->metaDescription ?>"/>
	<link rel="stylesheet" href="/themes/front/css/globalslider.css" type="text/css"
          media="screen, projection"/>
    <link rel="stylesheet" href="/themes/front/css/stylenew
	.css?c=<?= time() ?>" type="text/css"
          media="screen, projection"/>
    <link rel="stylesheet" href="/themes/front/css/demos.css" type="text/css"
          media="screen, projection"/>
    <link rel="alternate" type=”application/rss+xml” title=">Первый национальный спутниковый канал Республики Казахстан"
          href="<?= Yii::app()->createAbsoluteUrl('/pages/default/rss') ?>"/>

    <script type="text/javascript">
        var language = '<?=Yii::app()->language?>';
    </script>
	
	<script>
		$(function(){
			$('#slides').slides({
				preload: true,
				preloadImage: 'img/loading.gif',
				play: 5000,
				pause: 2500,
				hoverPause: true,
				animationStart: function(){
					$('.caption').animate({
						bottom:-35
					},100);
				},
				animationComplete: function(current){
					$('.caption').animate({
						bottom:0
					},200);
					if (window.console && console.log) {
						// example return of current slide number
						console.log(current);
					};
				}
			});
			
			
$('#vertical-ticker').totemticker({ 
row_height : '18px', 
next : '#ticker-next', 
previous : '#ticker-previous', 
stop : '#stop', 
start : '#start', 
mousestop : true, 
}); 
			
			
		}
		);
		
	
		$(document).ready(function() {
		$(".slides_container,.next,.prev").mouseenter(function(){
			$(".next").css('display','block');
			$(".prev").css('display','block');
			})
		$(".slides_container,.next,.prev").mouseleave(function(){
			$(".next").css('display','none');
			$(".prev").css('display','none');
			})
		})
	</script>
</head>
<body>
<?php if (stripos(Yii::app()->request->getHostInfo(), 'caspionet')) { ?>
    <div id="blocker">
    </div>
    <div id="mainpopup">
        <div class="popup_block">
            <div class="lang_abbr">KZ</div>
            <div class="description">

                ?аза?стан Республикасыны? т???ыш ?лтты? спутниктік <span class="fs18">Caspionet</span> телеарнасы <span
                    class="fs18">Kazakh TV</span> деген жа?а атаумен
                хабар таратады. <br>
                <a href="//kazakh-tv.kz/kz/mainpage/index">www.kazakh-tv.kz</a> сайтына ?ош келді?із
            </div>
        </div>
        <div class="popup_block">
            <div class="lang_abbr">RU</div>
            <div class="description">
                Первый национальный спутниковый канал Республики Казахстан <span class="fs18">Caspionet</span> вещает
                под новым названием <span class="fs18">Kazakh
                TV</span>. <br>
                Добро пожаловать на сайт <a href="//kazakh-tv.kz/ru/mainpage/index">www.kazakh-tv.kz</a>
            </div>
        </div>
        <div class="popup_block">
            <div class="lang_abbr">EN</div>
            <div class="description">
                Kazakhstan's first satellite channel <span class="fs18">Caspionet</span> has been renamed to <span
                    class="fs18">Kazakh TV</span>. <br>
                Welcome to our website <a href="//kazakh-tv.kz/en/mainpage/index">www.kazakh-tv.kz</a>
            </div>
        </div>
    </div>
    <?php
} ?>
<div id="wrapper">
   <div class="headerline" style="height: 25px;background: #00354c;width: 100%;color: #EEE9E9;font-size: 12px;padding-top:5px;">
			 <div style="width:998px; margin:0px auto;">
				
				<span id="realday_name" class="day_name" style="border-left: 1px solid #EEE9E9;padding-left: 10px;"></span>,
				<span id="realdate" class="date"></span>
				<span id="realtime" class="time" style="margin-right:40px;padding-right:10px;border-right: 1px solid #EEE9E9;"></span>
				<span  style="padding-left: 10px;font-weight:bold;">
			<?=Yii::t('pages', 'Курсы валют:');?><span style="font-size:12px;"><?php $this->widget('CurrencyWidget'); ?></span>
				</span>
				<div class="lang" style="padding-top:5px;">
					<?php $this->widget('blockLanguageSelector') ?>
				</div>
			
		</div>
		</div>

		<!--<div id="border">-->
		
		<header id="header">
        
		
		<section class="header_content">
		
		    <div class="logo">
                <a href="/"><img src="/upload/LOGOBETA2.png"/></a>
				<!--<p style="font-weight:bold;font-size: 0.7em; text-transform: uppercase;color:#024561;"><font color="#CDB000">Kazakhstan</font> in the world</p>-->
            </div>
            <div class="slogan">
			<!--<a href="/category/live_now"><img src="/upload/1-22.gif"/> <?//= Yii::t('page', 'Прямой эфир'); ?></a>-->
			
			<!--<img src="/upload/1-22.gif"/>--><!--<img src="/upload/icon/pryamoi1.png"/>--><a href="<?=Yii::app()->createUrl('/pages/default/category', array('sefname' => 'live_now'))?>"><?=Yii::t('pages', 'Прямой эфир');?></a>
			<br>
			<!--<img src="/upload/glob.gif" style="width:20px;"/>--><!--<a href="#"> WorldWide</a>
                <!--<h1><?//= Yii::t('layout', 'Первый национальный спутниковый <br> канал  республики казахстан'); ?></h1>-->
				 <!--<img src="/upload/icon/setka2.png"/>--><?php //$this->widget('OnAirWidgetNew');?>
				 <a href="/ru/category/program_schedule"><?=Yii::t('pages', 'Программа передач');?></a>
            </div>
			
			<div id="weather" style="margin-left:490px;padding-top:32px;">
			<img src="/upload/glob.gif"/><a style="text-transform: uppercase;
font-size: 14px;
font-weight: bold;
margin-left: 10px;
color: #9e9e9e;
margin-top: 4px;" href="<?=Yii::app()->createUrl('/pages/default/category', array('sefname' => 'live_now'))?>"><?=Yii::t('pages', 'WorldWide');?></a>
			<!-- weather widget start --><!--<div id="m-booked-small-t3-9359"> <div class="booked-weather-160x36 w160x36-06" style="color:#333333; border-radius:4px; -moz-border-radius:4px; border:none"> <a style="color:#08488D;" href="http://ibooked.ru/weather/astana-11233" class="booked-weather-160x36-city">Астана</a> <div class="booked-weather-160x36-degree" style="font-size:10px;"><span class="plus">+</span>13&deg;<span>C</span></div> </div> </div><script type="text/javascript"> var css_file=document.createElement("link"); css_file.setAttribute("rel","stylesheet"); css_file.setAttribute("type","text/css"); css_file.setAttribute("href",'http://s.bookcdn.com/css/w/bw-160-36.css?v=0.0.1'); document.getElementsByTagName("head")[0].appendChild(css_file); function setWidgetData(data) { if(typeof(data) != 'undefined' && data.results.length > 0) { for(var i = 0; i < data.results.length; ++i) { var objMainBlock = document.getElementById('m-booked-small-t3-9359'); if(objMainBlock !== null) { var copyBlock = document.getElementById('m-bookew-weather-copy-'+data.results[i].widget_type); objMainBlock.innerHTML = data.results[i].html_code; if(copyBlock !== null) objMainBlock.appendChild(copyBlock); } } } else { alert('data=undefined||data.results is empty'); } } </script> <script type="text/javascript" charset="UTF-8" src="http://widgets.booked.net/weather/info?action=get_weather_info&ver=4&cityID=11233&type=13&scode=10235&ltid=3539&domid=589&cmetric=1&wlangID=20&color=fff5d9&wwidth=158&header_color=fff5d9&text_color=333333&link_color=08488D&border_form=3&footer_color=fff5d9&footer_text_color=333333&transparent=1"></script><!-- weather widget end -->


<!-- weather widget start --><!--<div id="m-booked-small-t3-16754"> <div class="booked-weather-160x36 w160x36-01" style="color:#333333; border-radius:4px; -moz-border-radius:4px; border:none"> <a style="color:#08488D;" href="http://ibooked.ru/weather/almaty-10297" class="booked-weather-160x36-city">Алматы</a> <div class="booked-weather-160x36-degree" style="font-size:10px;"><span class="plus">+</span>2&deg;<span>C</span></div> </div> </div><script type="text/javascript"> var css_file=document.createElement("link"); css_file.setAttribute("rel","stylesheet"); css_file.setAttribute("type","text/css"); css_file.setAttribute("href",'http://s.bookcdn.com/css/w/bw-160-36.css?v=0.0.1'); document.getElementsByTagName("head")[0].appendChild(css_file); function setWidgetData(data) { if(typeof(data) != 'undefined' && data.results.length > 0) { for(var i = 0; i < data.results.length; ++i) { var objMainBlock = document.getElementById('m-booked-small-t3-16754'); if(objMainBlock !== null) { var copyBlock = document.getElementById('m-bookew-weather-copy-'+data.results[i].widget_type); objMainBlock.innerHTML = data.results[i].html_code; if(copyBlock !== null) objMainBlock.appendChild(copyBlock); } } } else { alert('data=undefined||data.results is empty'); } } </script> <script type="text/javascript" charset="UTF-8" src="http://widgets.booked.net/weather/info?action=get_weather_info&ver=4&cityID=10297&type=13&scode=10235&ltid=3539&domid=589&cmetric=1&wlangID=20&color=fff5d9&wwidth=158&header_color=fff5d9&text_color=333333&link_color=08488D&border_form=3&footer_color=fff5d9&footer_text_color=333333&transparent=1"></script><!-- weather widget end -->

			</div>
			
			
			
		<div>	
            <div class="social">
                <ul>
					<!--<li><a class="icon-vk" href="http://vk.com/kazakhtv" target="_blank">&nbsp;</a></li>
                    <li><a class="icon-twitter" href="http://twitter.com/Kazakh_TV" target="_blank">&nbsp;</a></li>
                    <li><a class="icon-youtube" href="https://www.youtube.com/KazakhTVchannel" target="_blank">
                            &nbsp;</a></li>
                    <li><a class="icon-rss" href="<?= Yii::app()->createUrl('/pages/default/rss') ?>">&nbsp;</a></li>
                    <li><a class="icon-facebook" target="_blank" href="http://facebook.com/KazakhTV">&nbsp;</a></li>
                    <li><a class="icon-mobile" href="//kazakh-tv.kz/pda.php">&nbsp;</a></li>
					--><style>
					.icon {
					width:20px;
					}
					
					.menu ul :nth-child(11) {
					display:none;
					}
					
					.menu ul :nth-child(9) {
					display:none;
					}
					</style>
				  	<li><a href="http://vk.com/kazakhtv" target="_blank"><img  class="icon" src="/images/social/vkg.png" onmouseover="this.src='/images/social/vkc.png'" onmouseout="this.src='/images/social/vkg.png'"></a></li>
                    <li><a href="http://twitter.com/Kazakh_TV" target="_blank"><img  class="icon" src="/images/social/tg.png" onmouseover="this.src='/images/social/tc.png'" onmouseout="this.src='/images/social/tg.png'"></a></li>
                    <li><a href="https://www.youtube.com/KazakhTVchannel" target="_blank"><img  class="icon" src="/images/social/yg.png" onmouseover="this.src='/images/social/yc.png'" onmouseout="this.src='/images/social/yg.png'"></a></li>
                    <li><a href="https://plus.google.com/u/0/b/109544296696427815045/+KazakhtvKzchannel/posts" target="_blank"><img  class="icon" src="/images/social/gg.png" onmouseover="this.src='/images/social/gplus.png'" onmouseout="this.src='/images/social/gg.png'"></a></li>
                    <li><a href="http://facebook.com/KazakhTV" target="_blank"><img  class="icon" src="/images/social/fg.png" onmouseover="this.src='/images/social/fc.png'" onmouseout="this.src='/images/social/fg.png'"></a></li>
                    <li><a href="<?//= Yii::app()->createUrl('/pages/default/rss') ?>" target="_blank"><img  class="icon" src="/images/social/rssg.png" onmouseover="this.src='/images/social/rssc.png'" onmouseout="this.src='/images/social/rssg.png'"></a></li>
                    <li><a href="http://instagram.com/kazakhtv" target="_blank"><img  class="icon" src="/images/social/ig.png" onmouseover="this.src='/images/social/inst.png'" onmouseout="this.src='/images/social/ig.png'"></a></li>
					<li><a href="/pda.php" target="_blank"><img  class="icon" src="/images/social/mg.png" onmouseover="this.src='/images/social/mc.png'" onmouseout="this.src='/images/social/mg.png'"></a></li>
								
                </ul>
            </div>
            <div class="search_block">
                <form action="<?= Yii::app()->createUrl('/search/default/index') ?>" method="GET">
                    <input type="text" placeholder="<?//= Yii::t('search', 'Искать'); ?>" name="query">
                    <button></button>
                </form>
            </div>
		</div>	
			
			
			
			
			
            <div class="lang" >
                <?php //$this->widget('blockLanguageSelector') ?>
            </div>
        </section>
    </header>
    <!-- #header-->
	
	<div id="border">
    <div class="shadow">
        <div class="menu">
            <?php $this->widget('MainMenuWidget', array('sectionId' => 1)); ?>
        </div>
    </div>
    <div class="informer_content">
        <div class="informer"><b><?=Yii::t('pages', 'Лента новостей:');?></b>
		<div id="Newline" style="overflow: hidden; position: relative;padding-left: 124px;top: -16px;width:400px;"> <?php $this->widget('NewsLine') ?>
         <!--  <marquee behavior="scroll" scrollAmount="2" width="600" height="20" onMouseOver="this.scrollAmount=0" onMouseOut="this.scrollAmount=2">-->
		
		   
<!--</marquee>-->
</div>
<div style="width: 133px;
margin-top: -37px;
float: right;
">
<a href="#" id="ticker-previous"><img src="/upload/1arrow.png" style="height:20px;"/></a>    <a id="stop" href="#"><img src="/upload/4arrow.png" style="height:20px;"/></a>  <a id="start" href="#"><img src="/upload/3arrow.png" style="height:20px;"/></a> <a href="#" id="ticker-next"><img src="/upload/2arrow.png" style="height:20px;"/></a></div>
		   <!--<marquee direction="left" scrollamount="3">
               <!-- <span id="realday_name" class="day_name"></span>,
                <span id="realdate" class="date"></span>
                <span id="realtime" class="time"></span>
                <?php //$this->widget('CurrencyWidget'); ?>-->
           <!-- </marquee>
-->        </div>
    </div>
