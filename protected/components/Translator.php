<?php

function ttt($word)
{
    $to_En = array(
    "Курсы валют" => "Exchange rate",
    "Войти через социальные сети" => "Login via social network",
    "Отправить" => "Send",
    "Просмотров" => "Views",
    "Эфир данной передачи невозможен"=>"Broadcasting of this program is not possible",
    "Поделиться новостью" => "Share the news",
    "Последние передачи"=>"Last transmission",
    "Не пропустите"=>"Don't miss",
    "Архив"=>"Archive",);

    $to_Kz = array(
    "Курсы валют" => "Валюта бағамы",
    "Войти через социальные сети" => "Әлеуметтік желілер арқылы кіру",
    "Отправить" => "Жіберу",
    "Просмотров" => "Көргендер саны",
    "Эфир данной передачи невозможен"=>"Бұл бағдарлама эфирде көрсетілмейді",
    "Поделиться новостью" => "Жаңалықпен бөлісу",
    "Последние передачи"=>"Соңғы бағдарламалар",
    "Архив"=>"Мұрағат",
    "Не пропустите"=>"Өткізіп алмаңыз",);

    $lang = Yii::app()->language;
        switch ($lang) {
             case 'en':
                 return $to_En[$word];
                 
             case 'kz':
                 return $to_Kz[$word];
                 
             default:
                 return $word;
         } 
         return $word;
    return $word;
}
?>