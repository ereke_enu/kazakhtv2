<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 21.02.13
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */

class Rss extends CWidget {

   public function run() {
     /*   $category = Categories::model()->findBySefnameObj('news');

        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $category->id);
        $criteria->compare('active', '1');
		$criteria->compare('is_archive', '0');
        $criteria->order = 'date DESC, time DESC';
        $criteria->limit = 50;
        $criteria->compare('is_main', '0');


        $pages = Pages::model()->findAll($criteria);
        
        $this->render('rss/index', array('pages' => $pages));*/
		
		$criteria = new CDbCriteria;
        $criteria->compare('sefname', 'news');
        $criteria->compare('lang_id', LXController::getLangId());
        $criteria->order = 'lft ASC';
        $categories = Categories::model()->find($criteria);


        $pageCriteria = new CDbCriteria;
        $pageCriteria->addInCondition('category_id', CHtml::listData($categories->children()->findAll('active=1'), 'id', 'id'));
        $pageCriteria->compare('active', '1');
        $pageCriteria->compare('is_archive', '0');
        $pageCriteria->limit = 40;
        $pageCriteria->compare('is_main', '0');
        $pageCriteria->order = 'date DESC, time DESC';
        $pages = Pages::model()->findAll($pageCriteria);
        $this->render('rss/index', array('pages' => $pages, 'categories' => $categories));
    }
}