<div id="films_and_serials">
        <div>
            <? if (Yii::app()->language == 'ru'){?>
                <div class="containerimg" >
                <img src="/upload/1acc.png"/>
                <div class="descriptionimg">
                <a href="/ru/programms/program/program_202_accent" class="title" ><span class="h2title">ACCENT</span><span class="text"><br/>Цикл программ о диаспорах, проживающих на территории Казахстана. </span></a>
            <?} if (Yii::app()->language == 'en'){?>
                <div class="containerimg">
                    <img src="/upload/1acc.png" />
                    <div class="descriptionimg" > 
                        <a href="/en/programms/program/program_202_accent" class="title" ><span class="h2title">ACCENT</span><span class="text"><br/>The rich cultural diversity of different national minorities who live in one territory.</span></a>                                    
            <?} if (Yii::app()->language == 'kz'){?>
                <div class="containerimg">
                    <img src="/upload/1acc.png" />
                    <div class="descriptionimg" > 
                        <a href="/kz/programms/program/program_59_accent" class="title" ><span class="h2title">ACCENT</span><span class="text"><br/>Қазақстан Республикасы аумағында өмір сүріп жатқан диаспоралардың өмірі туралы бағдарлама</span></a>
            <? } ?>
                    </div>
                </div>
                
            <? if (Yii::app()->language == 'ru'){?>
                <div class="containerimg" >
                    <img src="/upload/mn1.png" />
                    <div class="descriptionimg" > 
                        <a href="/kz/programms/program/program_608_modern-nomads" class="title" ><span class="h2title">MODERN NOMADS</span><span class="text"><br/>Информационная передача о репатриантах. Проект осуществляется со Всемирной Ассоциацией казахов.</span></a>
            <?} if (Yii::app()->language == 'en'){?>
                <div class="containerimg" >
                    <img src="/upload/mn1.png" />
                    <div class="descriptionimg" >
                        <a href="/en/programms/program/program_608_modern-nomads" class="title" ><span class="h2title">MODERN NOMADS</span><span class="text"><br/>Kazakh history is full of events that have forced the ethnic Kazakhs to leave their homeland.</span></a>
            <?} if (Yii::app()->language == 'kz'){?>
                <div class="containerimg" >
                    <img src="/upload/mn1.png" />
                    <div class="descriptionimg" > 
                        <a href="/kz/programms/program/program_608_modern-nomads" class="title" ><span class="h2title">MODERN NOMADS</span><span class="text"><br/>"Modern Nomads" ата жұртты аңсап жеткен ағайын жайлы бағдарлама.</span></a>
            <?}?>
                    </div>
                </div>


            <? if (Yii::app()->language == 'ru') {?>
                <div class="containerimg" >
                    <img src="/upload/agh.jpg" />
                    <div class="descriptionimg" >
                        <a href="/ru/programms/program/program_653_aghain" class="title" ><span class="h2title">AGHAIN</span><span class="text"><br/>Программа о жизни казахских диаспор по всему миру. Это не просто истории отдельных героев и диаспоры в целом.</span></a>
            <?} if (Yii::app()->language == 'en'){?>
                <div class="containerimg" >
                    <img src="/upload/agh.jpg" />
                        <div class="descriptionimg" >
                            <a href="/en/programms/program/program_653_aghain" class="title" ><span class="h2title">AGHAIN</span><span class="text"><br/>Journalist survey on the life of the Kazakh diasporas living abroad.</span></a>
            <?}if (Yii::app()->language == 'kz'){?>
                <div class="containerimg" >
                    <img src="/upload/agh.jpg" />
                    <div class="descriptionimg" >
                        <a href="/kz/programms/program/program_653_aghain" class="title" ><span class="h2title">AGHAIN</span><span class="text"><br/>Тағдырдың тәлкегімен әлемнің түпкір-түпкіріне тарыдай шашылған 4 млн-ға жуық қандастарымыз бар.</span></a>
            <?}?>
                    </div>
                </div>


        </div>
    </div>