<?php $lang=Yii::app()->language; ?>
<table border="0" cellspacing="0">
            <tr>
                <td class="social_project_item">
                    <? if ($lang == 'ru') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/ru/programms/program/program_235_nebo-moego-detstva">Небо моего детства</a></h2>
                    <a href="http://www.kazakh-tv.kz/ru/programms/program/program_235_nebo-moego-detstva">
                        <div class="text">
                            Фильм рассказывает о детстве Н.А.Назарбаева. <br/>16 декабря в 13:00
                        </div>
                        <img style="float:left;"  src="/upload/films/nebo_moego_detstva.jpg"  />
                    </a>
                    <?} if ($lang == 'en') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/ru/programms/program/program_235_nebo-moego-detstva">Sky of my childhood</a></h2>
                    <a href="http://www.kazakh-tv.kz/ru/programms/program/program_235_nebo-moego-detstva">
                        <div class="text">
							The film tells about the childhood of Nursultan Nazarbayev. <br/> On 16 December at 13:00 
                        </div>
                        <img style="float:left;"  src="/upload/films/nebo_moego_detstva.jpg"  />
                    </a>
                    <?} if ($lang == 'kz') {?>
					
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/kz/programms/program/program_236_menin-balalyk-shahymnyn-aspany">Балалық шағымның аспаны</a></h2>
                    <a href="http://www.kazakh-tv.kz/kz/programms/program/program_236_menin-balalyk-shahymnyn-aspany">
                        <div class="text">
                              Н.Ә.Назарабаевтың балалық шағы жайлы фильм.<br/> 16 желтоқсан 13:00-де
                        </div>
                        <img src = "/upload/films/nebo_moego_detstva.jpg"/>
                    </a>
                    <? } ?>
                </td>
                <td class="social_project_item">
                    <? if ($lang == 'ru') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/kz/programms/program/program_809_makhabbatym-zhyregimde">Любовь всегда в моем сердце</a></h2>
                    <a href="http://www.kazakh-tv.kz/kz/programms/program/program_809_makhabbatym-zhyregimde">
                        <div class="text">
                          Сериал расскажет об истории любви двух совершенно разных людей.   Каждую субботу в 20:00 и <br/> воскресенье в 14:00, 20:00 
                        </div>
                        <img src = "/upload/films/makhabbatym_zhuregimde.jpg"/>
                    </a>
                    <?} if ($lang == 'en') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/kz/programms/program/program_809_makhabbatym-zhyregimde">Love in my heart</a></h2>
                    <a href="http://www.kazakh-tv.kz/kz/programms/program/program_809_makhabbatym-zhyregimde">
                        <div class="text">
							 The film tells about the love story of two completely different people.  <br/> Every saturday at 20:00 and sunday at 14:00,20:00 
                        </div>
                        <img src="/upload/films/makhabbatym_zhuregimde.jpg"  />
                    </a>
                    <?} if ($lang == 'kz') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/kz/programms/program/program_809_makhabbatym-zhyregimde">Махаббатым жүрегімде</a></h2>
                    <a href="http://www.kazakh-tv.kz/kz/programms/program/program_809_makhabbatym-zhyregimde">
                        <div class="text">
                           Бұл телехикая шынайы махаббат пен сезім жайлы баяндайды. <br/>Сенбі 20:00-де және жексенбі 14:00, 20:00-де
                        </div>
                        <img src="/upload/films/makhabbatym_zhuregimde.jpg"  />
                    </a>
                    <? } ?>
                </td>
            </tr>
            <tr>
                <td class="social_project_item">
                    <? if ($lang == 'ru') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/kz/programms/program/program_563_zhauzhyrek-myn-bala">Жаужүрек мың бала</a></h2>
                    <a href="http://www.kazakh-tv.kz/kz/programms/program/program_563_zhauzhyrek-myn-bala">
                        <div class="text">
                          Фильм рассказывает об истории казахского народа во время казахско-джунгарской войны. <br/>19 декабря в 12:00 и 20 декабря 00:00   
                        </div>
                        <img src="/upload/films/zhauzhurek_myn_bala.jpg"  />
                    </a>
                    <?} if ($lang == 'en') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/kz/programms/program/program_563_zhauzhyrek-myn-bala">Zhauzhurek myn bala</a></h2>
                    <a href="http://www.kazakh-tv.kz/kz/programms/program/program_563_zhauzhyrek-myn-bala">
                        <div class="text">
						The film tells the history of kazakh nation in kazakh-Jungar wars. <br/>On 19 December at 12:00 and 20 December at 00:00 
                        </div>
                        <img src="/upload/films/zhauzhurek_myn_bala.jpg"  />
                    </a>
                    <?} if ($lang == 'kz') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/kz/programms/program/program_563_zhauzhyrek-myn-bala">Жаужүрек мың бала</a></h2>
                    <a href="http://www.kazakh-tv.kz/kz/programms/program/program_563_zhauzhyrek-myn-bala">
                        <div class="text">
						 Қазақ-Жоңғар соғысы кезіндегі қазақ халқының тарихы баяндалады. <br/>19 желтоқсан 12:00-де, 20 желтоқсан 00:00-де 
                        </div>
                        <img src="/upload/films/zhauzhurek_myn_bala.jpg"  />
                    </a>
                    <? } ?>
                </td>
                <td class="social_project_item">
                  <? if ($lang == 'ru') {?>
                   <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/ru/programms/program/program_718_kochevnik">Кочевник</a></h2>
                    <a href="http://www.kazakh-tv.kz/ru/programms/program/program_718_kochevnik">
                        <div class="text">
						Фильм об исторических событиях, происходивших в XVIII веке. <br/>20 декабря в 04:15 и 21:15
                        </div>
                        <img src="/upload/films/koshpendiler.jpg"  />
                    </a>
                    <?} if ($lang == 'en') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/ru/programms/program/program_718_kochevnik">Nomad</a></h2>
                    <a href="http://www.kazakh-tv.kz/ru/programms/program/program_718_kochevnik">
                        <div class="text">
                             The film is about the historical events that took place in the XVIII century.<br/>On 20 December at 04:15 and 21:15
                        </div>
                        <img src="/upload/films/koshpendiler.jpg"  />
                    </a>
                    <?} if ($lang == 'kz') {?>
                    <h2 class="hidden-xs"><a href="http://www.kazakh-tv.kz/kz/programms/program/program_719_koshpendiler">Көшпенділер</a></h2>
                    <a href="http://www.kazakh-tv.kz/kz/programms/program/program_719_koshpendiler">
                        <div class="text">
                           XVIII ғасырда орын алған тарихи оқиғалар жайлы фильм. <br/> 20 желтоқсан 04:15-те және 21:15-те
                        </div>
                        <img src="/upload/films/koshpendiler.jpg"  />
                    </a> 
                    <? } ?>
                </td>
            </tr>
        </table>