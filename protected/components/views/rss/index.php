<?='<?xml version="1.0" encoding="UTF-8"?>'; ?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/">
    <channel>
        <title>Kazakh TV</title>
        <link>http://kazakh-tv.kz</link>
        <description>Kazakhstan in the World</description>
        
            <?php  foreach ($pages as $page) { ?>
            <item>
               <title><?= $page->title?></title> 
               <link><?=Yii::app()->createUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname));?></link> 
               <description><img src="/upload/anounces/<?= $page->image?>"/><?= $page->text?></description> 
               <pubDate><?=date('d.m.Y H:i', strtotime($page->date . ' ' . $page->time));?></pubDate>
            </item>
            <?php } ?>
            
    </channel>
</rss>