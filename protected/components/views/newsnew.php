<div id="latest-h" >
<div class="rightblock" style="margin-bottom:5px;"><?=Yii::t('pages', 'Последние события');?></div></div>
<div id="blockNewsScroll" class="latest_news1">
<ul>
  <?php foreach ($pages as $page) { ?>
    <li>
     <span class="news_time"><?=date('d.m.Y H:i', strtotime($page->date . ' ' . $page->time));?></span><br>
    <a href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname));?>">
                    <span class="title" style="color:#2b2b2b ; font-size:14px;"><?=$page->title?></span></a>
    </li>
  <?php } ?>
  <!--
  <li>
  <a style="float:right;" href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'news'))?>"><?=Yii::t('pages', 'Читать еще');?></a>
</li>
-->
</ul>
</div>
