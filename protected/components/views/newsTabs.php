<h2>
    <a href="<?=Yii::app()->createUrl('/pages/default/category', array('sefname' => 'news'));?>">
        <?=Yii::t('pages', 'Новости');?>
    </a>
</h2>
<div class="news_tab_items" id="news_tabs">
    <div class="news_tab_items_content">
        <ul>
            <?php
            foreach ($pages as $key => $page) {

                if ($page->image == '') {
                    $image = '/themes/front/images/noimage.jpg';
                } else {
                    $image = '/upload/anounces/' . $page->image;
                }
                ?>
                <li>
                    <a href="/<?=Yii::app()->language?>/view/<?=$page->category->sefname?>/<?=$page->sefname?>"><img
                            src="<?=$image?>">
                        <span class="items_title"><?=$page->title?></span>
                        <?php if (!empty($page->videos)) { ?>
                            <div class="play"><img src="/themes/front/images/play.png" alt=""></div>
                        <?php } ?>
                    </a>
                </li>
            <?php } ?>

        </ul>
    </div>
</div>
