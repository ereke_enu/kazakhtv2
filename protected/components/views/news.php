<div class="latest_news">
    <h2><?=Yii::t('pages', 'Лента новостей');?></h2>
    <a class='archive' href="<?=Yii::app()->createUrl('/pages/default/allNews', array('sefname' => 'news'))?>"><?=Yii::t('pages', 'ВСЕ НОВОСТИ');?></a>
    <ul>
        <?php foreach ($pages as $page) { ?>
            <li>
                <a href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname));?>">
                    <span class="news_time"><?=date('d.m.Y H:i', strtotime($page->date . ' ' . $page->time));?>
                        - </span>
                    <span class="title"><?= $page->title?></a></span>
            </li>
        <?php } ?>
    </ul>
</div>