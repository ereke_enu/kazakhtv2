<div style="-webkit-column-count: 2;-moz-column-count: 2;-o-column-count: 2;column-count: 2;width: 98%;">
	<? foreach($pages as $page) {
	    $url = Yii::app()->createUrl('/pages/default/view', array('category' => $page->category_sefname, 'sefname' => $page->sefname ));
	    $image = Pages::getAnounceImg($page->image, '');
	?>
		<article class="clearfix" id="lineNews" style="height:180px;">			 
			<table height='' style="margin-bottom:10px;">
				<tr>
					<td colspan="2" style="padding-bottom: 5px;"><h3><a style="font-size: 14px;font-weight: bold;" href="<?= $url ?>"><?= $page->title ?></a></h3></td>
				</tr>
				<tr>
					<td class="imgtableW" style="vertical-align: top;" width='30%'>			
						<a href="<?= $url ?>" id="lineNewsA"><img style="width:192px;height: 108px;margin-top: 4px;" src="<?= $image ?>" class="img-responsive col-sm-6 no-padding" /></a>
	                </td>
					<td class="texttable">
						<p><a href="<?= $url ?>" class="arrow"><?= $page->short_text ?></a></p>
					</td>
				</tr>
			</table>
	    </article>
	<?}?>
</div>