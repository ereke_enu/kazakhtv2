<?php 
$url = Yii::app()->createUrl('/pages/default/view', array('category' => $page->category_sefname, 'sefname' => $page->sefname ));
$image = Pages::getAnounceImg($page->image, '');

?>
<div style="width: 485px;">
    <article class="clearfix">
		<hr style="width:98%;">
		<table style="padding-top:5px;height:194px;">
			<tr>
				<td class="imgtable" valign="top">
                    <a href="<?= $url ?>"><img style="width:240px;height:135px;margin-top:5px;" src="<?= $image ?>" class="img-responsive col-sm-6 no-padding" /></a>
				</td>
				<td class="texttable">		
                    <h3><a href="<?= $url ?>"><?= $page->title ?></a></h3>
                    <p style="padding-right:5px;"><a style="color:#2b2b2b;" href="<?= $url ?>" class="arrow"><?= $page->short_text ?></a></p>
                </td>	
		    </tr>
		</table>
    </article>
</div>