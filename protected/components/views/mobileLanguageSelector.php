<ul id="navbar-lang-mobile">
    <?php
    
    echo CHtml::tag('li', array('class' => 'active'), $currentLang);
    
    foreach ($languages as $key => $lang) {
        if ($key != $currentLang)
            echo CHtml::tag('li', array('class' => 'select hidden', 'data-select'=>'lang'), CHtml::link($key, $this->getOwner()->createMultilanguageReturnUrl($key)));
    }
    ?>
</ul>