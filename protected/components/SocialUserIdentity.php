<?php

    /**
     * UserIdentity represents the data needed to identity a user.
     * It contains the authentication method that checks if the provided
     * data can identity the user.
     */
    class SocialUserIdentity extends EAuthUserIdentity {
        private $_id;
        public $sid;

        /**
         * Authenticates a user.
         * The example implementation makes sure if the username and password
         * are both 'demo'.
         * In practical applications, this should be changed to authenticate
         * against some persistent user identity storage (e.g. database).
         * @return boolean whether authentication succeeds.
         */


        public function authenticate() {
            parent::authenticate();

            if (!$this->errorCode) {
                switch ($this->service->serviceName) {
                    case 'vkontakte':
                        $srv = 'vk';
                        break;
                    case 'facebook':
                        $srv = 'fb';
                        break;
                    case 'twitter':
                        $srv = 'tw';
                        break;
                    case 'mailru':
                        $srv = 'mr';
                        break;
                    default:
                        $srv = false;
                        break;
                }

                if ($srv === false) {
                    $this->errorCode = self::ERROR_NOT_AUTHENTICATED;
                } else {

                    $criteria = new CDbCriteria;
                    $criteria->compare($srv, $this->service->id);
                    $model = Users::model()->find($criteria);

                    if ($model === null) {
                        $model = new Users;
                        $model->username = $this->service->username;
                        $model->$srv = $this->service->id;
                        $model->data_name = $this->service->fname;
                        //$model->fam = $this->service->lname;
                        if (!empty($this->service->email)) {
                            $model->email = $this->service->email;
                        } else {
                            $model->email = '';
                        }
                        $model->password = md5($this->service->id . $this->name);
                        $model->active = 1;
                        if ($model->save(false)) {
                            Yii::app()->db->createCommand("INSERT IGNORE INTO AuthAssignment SET itemname=:itemname, userid=:userid")
                                ->execute(array(':itemname' => 'Authenticated', ':userid' => $model->id));
                        }
                    }


                }
                $this->errorCode = self::ERROR_NONE;
                $this->_id = $model->id;
                $this->id = $model->id;
                $this->setState('id', $this->id);
            }

            return !$this->errorCode;
        }
    }