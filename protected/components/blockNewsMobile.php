<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ugeen Alex
 * Date: 10.05.12
 * Time: 14:21
 * To change this template use File | Settings | File Templates.
 */
class blockNewsMobile extends CWidget {

    public function run() {

        $categories = Categories::model()->cache(300)->find('t.sefname=:sefname AND lang_id=:lang_id', array(':sefname' => 'news', ':lang_id' => LXController::getLangId()));;

        $criteria = new CDbCriteria;
        $criteria->addInCondition('category_id', CHtml::listData($categories->children()->findAll(), 'id', 'id'));
        $criteria->limit = '20';
        $criteria->order = 'date DESC, time DESC';
        $criteria->compare('active', 1);
        $criteria->compare('is_archive', 0);

        $pages = Pages::model()->cache(300)->findAll($criteria);

        $this->render('news', array('pages' => $pages));
    }
}