<?php
    /**
     * Controller is the customized base controller class.
     * All controller classes for this application should extend from this base class.
     */
    class LXController extends RController {

        public $layout = '//layouts/second';
        private static $_lang_id;

        public $metaTags;
        public $metaDescription;
        public $newsCategory = 'news';
        public $program_id = 0;
        public $program_category_id = 0;
        public $program_category_title = '';

        public function filters() {
            return array(
                'rights',
                array(
                    'ext.YXssFilter',
                    'clean'   => '*',
                    'tags'    => 'strict',
                    'actions' => 'all'
                )
            );
        }

        public static function getLangId() {
            if (empty(self::$_lang_id)) {
                self::$_lang_id = Langs::getLangIdByCode(Yii::app()->language);
            }

            return self::$_lang_id;
        }

        public function setPageTitle($title) {
            return $this->pageTitle = Yii::app()->name . ' - ' . $title;
        }

        public function sendMail($subject, $text, $from = false, $to = false, $alt = 'main') {
            $content = $text;

            $mail_params['main'] = Yii::app()->params['mail'];
            $mail_params['alt0'] = Yii::app()->params['mail_alt0'];
            $mail_params['alt1'] = Yii::app()->params['mail_alt1'];
            $mail_params['alt2'] = Yii::app()->params['mail_alt2'];


            spl_autoload_unregister(array('YiiBase', 'autoload'));
            $SM = Yii::app()->swiftMailer;
            spl_autoload_register(array('YiiBase', 'autoload'));

            $transport = $SM->smtpTransport('mail.softdeco.net', 25, null,
                'e.dymov@softdeco.com', 'Kai1eif7');


            $mailer = $SM->mailer($transport);


            $Message = $SM->newMessage($subject)
                ->setFrom($from)
                ->setTo($to)
                ->addPart($content, 'text/html')
                ->setBody($content);

            $result = $mailer->send($Message);
        }

        public function __construct($id, $module = null) {
            parent::__construct($id, $module);
            // If there is a post-request, redirect the application to the provided url of the selected language
            if (isset($_POST['language'])) {
                $lang = $_POST['language'];
                $MultilangReturnUrl = $_POST[$lang];
                $this->redirect($MultilangReturnUrl);
            }
            // Set the application language if provided by GET, session or cookie
            if (isset($_GET['language'])) {
                Yii::app()->language = $_GET['language'];
                Yii::app()->user->setState('language', $_GET['language']);
                $cookie = new CHttpCookie('language', $_GET['language']);
                Yii::app()->request->cookies['language'] = $cookie;
            } elseif (Yii::app()->user->hasState('language')) {
                Yii::app()->language = Yii::app()->user->getState('language');
            } elseif (isset(Yii::app()->request->cookies['language'])) {
                Yii::app()->language = Yii::app()->request->cookies['language']->value;
            } else {
                Yii::app()->language = 'en';
            }


            // TODO: перенести в крон
            // $category = Categories::model()->findBySefnameObj('news');
            // if ($category !== null) {
            //     $categories = CHtml::listData($category->children()->findAll(), 'id', 'id');
            // }
            // if (!empty($categories)) {
            //     $criteria = new CDbCriteria;
            //     $criteria->addCondition('UNIX_TIMESTAMP(CONCAT(date, " ", time)) < (UNIX_TIMESTAMP(NOW())-86400*3)');
            //     $criteria->compare('active', '1');
            //     $criteria->compare('is_archive', '0');
            //     $criteria->compare('category_id', $categories);

            //     Pages::model()->updateAll(array('is_archive' => '1'), $criteria);
            // }

        }

        public function createMultilanguageReturnUrl($lang = 'ru') {
            if (count($_GET) > 0) {
                $arr = $_GET;
                $arr['language'] = $lang;
            } else
                $arr = array('language' => $lang);

            return $this->createUrl('', $arr);
        }

        public function getRoute() {
            if (($action = $this->getAction()) !== null)
                return '/' . Yii::app()->language . '/' . $this->getUniqueId() . '/' . $action->getId();
            else
                return '/' . Yii::app()->language . '/' . $this->getUniqueId();
        }


    }