<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ugeen Alex
 * Date: 10.05.12
 * Time: 14:21
 * To change this template use File | Settings | File Templates.
 */
class NewsLine extends CWidget {

 public function run() {
        $criteria = new CDbCriteria;
        $criteria->compare('sefname', 'news');
        $criteria->compare('lang_id', LXController::getLangId());
        $criteria->order = 'lft ASC';
        $categories = Categories::model()->cache(300)->find($criteria);


        $pageCriteria = new CDbCriteria;
        $pageCriteria->addInCondition('category_id', CHtml::listData($categories->children()->findAll('active=1'), 'id', 'id'));
        $pageCriteria->compare('active', '1');
        $pageCriteria->compare('is_archive', '0');
        $pageCriteria->limit = 4;
        $pageCriteria->compare('is_main', '0');
        $pageCriteria->order = 'date DESC, time DESC';
        $pages = Pages::model()->cache(300)->findAll($pageCriteria);
        $this->render('NewsLine', array('pages' => $pages, 'categories' => $categories));
    }
}