<?php
/**
 * Copyright (c) 2012 by Yevgeniy Dymov / johnluxor@hotmail.com
 */

return CMap::mergeArray(
    require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main.php'),
    array(
        'defaultController' => 'Admin',
        'preload' => array('bootstrap'),
        'name' => 'Панель управления',
        'components' => array(
            'user' => array(
                'loginUrl' => '/admin.php/users/default/login/'
            ),
            'urlManager' => array(
                'urlFormat' => 'path',
                'rules' => array(
                    '<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
                    '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
                    '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>'
                ),
            ),
            'errorHandler' => array(
                // use 'site/error' action to display errors
                'errorAction' => 'admin/error',
            ),
        )
    )
);