<?php
/**
 * Copyright (c) 2012 by Yevgeniy Dymov / johnluxor@hotmail.com
 */

return CMap::mergeArray(
    require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main.php'),
    array(
        'defaultController' => 'mainpage/default',
        'name' => 'Kazakh TV',
        'import' => array(
            'application.modules.pages.mobile.components.*',
            'application.modules.comments.mobile.components.*',
            'application.modules.categories.mobile.components.*',
            'application.modules.users.mobile.components.*',
            'application.modules.shop.mobile.components.*',
            'application.modules.forum.mobile.components.*',
            'application.modules.categories.models.*',
            'application.modules.langs.models.*',
            'application.modules.pages.models.*',
            'application.modules.shop.models.*',
            'application.modules.forum.models.*',
            'application.modules.programms.*',
            'application.modules.baners.mobile.components.*',
            'application.modules.baners.models.*',
            'application.modules.programms.mobile.components.*'
        ),
        'modules' => array(
            'mainpage',
            'pages'
        ),
        'params' => array(
            'mobile' => true,
            'index' => '/newpda.php'
        ),
        'components' => array(
            'user' => array(
                'loginUrl' => '/users/default/login/'
            ),
            'urlManager' => array(
                'class' => 'application.components.lxUrlManager',
                'urlFormat' => 'path',
                'showScriptName' => true,
                'rules' => array(
                    '<language:(ru|kz|en)>/view/<category>/<sefname>' => 'pages/default/view/',
                    '<language:(ru|kz|en)>/allNews/<sefname>' => 'pages/default/allNews/',
                    '<language:(ru|kz|en)>/team/<sefname>' => 'pages/default/team/',
                    '<language:(ru|kz|en)>/allNews/<sefname>/<date>' => 'pages/default/allNews/',
                    '<language:(ru|kz|en)>/category/<sefname:news>' => 'pages/default/allNews/',
                    '<language:(ru|kz|en)>/category/<sefname:about_us_team>' => 'pages/default/team/',
                    '<language:(ru|kz|en)>/category/<sefname:sitemap>' => 'pages/default/siteMap/',

                    '<language:(ru|kz|en)>/newsAjax' => 'pages/default/newsAjax/',
                    '<language:(ru|kz|en)>/rss' => 'pages/default/rss/',
                    
                    '<language:(ru|kz|en)>/program/<id:\w+>' => 'programms/default/index',
                    
                    '<language:(ru|kz|en)>/category/feedback' => 'feedback/default/index',
                    '<language:(ru|kz|en)>/category/faq' => 'faq/default/index',
                    '<language:(ru|kz|en)>/category/questions_answers' => 'faq/default/index',
                    '<language:(ru|kz|en)>/category/forum' => 'forum/default/index',
                    '<language:(ru|kz|en)>/category/program_schedule' => 'programms/default/timetable',
                    '<language:(ru|kz|en)>/category/programms' => 'programms/default/index',
                    '<language:(ru|kz|en)>/category/live_now' => 'programms/default/live',
                    '<language:(ru|kz|en)>/archive/<sefname>' => 'pages/default/archive',
                    '<language:(ru|kz|en)>/news_archive/<date>' => 'pages/default/archive',
                    '<language:(ru|kz|en)>/category/archive' => 'pages/default/archive',
                    '<language:(ru|kz|en)>/category/news_archive' => 'pages/default/archive',
                    '<language:(ru|kz|en)>/category/tv_shows_archive' => 'programms/default/archive',
                    '<language:(ru|kz|en)>/category/television_magazine_archive' => 'programms/default/archive/sefname/features',
                    '<language:(ru|kz|en)>/programms/program/<sefname>' => 'programms/default/program',
                    '<language:(ru|kz|en)>/archive_program/<day>/<month>/<year>' => 'programms/default/archive',
                    '<language:(ru|kz|en)>/archive_features/<day>/<month>/<year>' => 'programms/default/archive/sefname/features',
                    '<language:(ru|kz|en)>/archive_program/<program_id>/<day>/<month>/<year>' => 'programms/default/archive',
                    '<language:(ru|kz|en)>/archive_features/<program_id>/<day>/<month>/<year>' => 'programms/default/archive/sefname/features',

                    '<language:(ru|kz|en)>/category/<sefname:\w+>' => 'pages/default/category',
                    

                    '<language:(ru|kz|en)>/users' => 'users/default/index',
                    '<language:(ru|kz|en)>/search' => 'search/default/index',
//                    '<language:(ru|kz|en)>/goads' => 'baners/default/goads',
//                    '<language:(ru|kz|en)>/goads/<link>/<place>' => 'baners/default/goads',


                    '<language:(ru|kz|en)>/mainpage/<action:\w+>' => 'mainpage/default/<action>',
                    '<language:(ru|kz|en)>/feedback/<action:\w+>' => 'feedback/default/<action>',
                    '<language:(ru|kz|en)>/faq/<action:\w+>' => 'faq/default/<action>',
                    '<language:(ru|kz|en)>/users/<action:\w+>' => 'users/default/<action>',
                    '<language:(ru|kz|en)>/forum/<action:\w+>' => 'forum/default/<action>',
                    '<language:(ru|kz|en)>/programms/<action:\w+>' => 'programms/default/<action>',

                    '<language:(ru|kz|en)>/programms/default/timetable/<date>' => 'programms/default/timetable',

//                    '<language:(ru|kz|en)>/<module:\w+>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/view',
//                    '<language:(ru|kz|en)>/<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
//                    '<language:(ru|kz|en)>/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                ),
            ),
            'errorHandler' => array(
                // use 'site/error' action to display errors
                'errorAction' => 'site/error',
            ),
        )
    )
);