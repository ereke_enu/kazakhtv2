<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'id' => 'Kazakh TV',
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'ХАБАР',

    // preloading 'log' component
    'preload' => array('log', 'translate'),

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.modules.rights.*',
        'application.modules.sections.admin.models.*',
        'application.modules.rights.components.*',
        'application.modules.pages.front.components.*',
        'application.modules.comments.front.components.*',
        'application.modules.programms.front.components.*',
        'application.modules.mainpage.front.components.*',
        'application.modules.programms.models.*',
        'application.modules.comments.models.*',
        'application.modules.rights.models.*',
        'application.modules.users.models.*',
        'application.modules.video.models.*',
        'application.modules.programms.*',
        'application.extensions.UrlTransliterate',
        'application.extensions.helpers.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
        'ext.eauth.custom_services.*',

    ),
    'sourceLanguage' => 'ru',
    'language' => 'en',
    'aliases' => array(
        'bootstrap' => 'ext.bootstrap'
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'rights' => array(
            'layout' => 'application.modules.rights.views.layouts.main',
            'appLayout' => '//layouts/main',
            'install' => false,
            'userClass' => 'Users',
            'userNameColumn' => 'username',
            'userIdColumn' => 'id',
            'debug' => true,
        ),
      /*  'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => false,
            'generatorPaths' => array(
                'ext.bootstrap.gii',
            ),
        ),*/
        'baners',
        'categories',
        'comments',
        'faq',
        'feedback',
        'forum',
        'langs',
        'pages',
        'search',
        'sections',
        'shop',
        'users',
        'programms',
        'video',
        'translate'
    ),
    'behaviors' => array(
        'runEnd' => array(
            'class' => 'application.extensions.EndBehavior',
        ),
    ),
    // application components
    'components' => array(
        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'services' => array( // You can change the providers and their classes.
                'vkontakte' => array(
                    // register your app here: https://vk.com/editapp?act=create&site=1
                    'class' => 'CustomVKontakteService',
                    'client_id' => '3946207',
                    'client_secret' => 'hsE2cKL8S5yHbN8PPPVL',
                ),
                'twitter' => array(
                    // register your app here: https://dev.twitter.com/apps/new
                    'class' => 'CustomTwitterService',
                    'key' => 'hVb84n1DZXkHrrEacnJtXw',
                    'secret' => 'rrvkolhrSLqI6e7ZuQWWz0XWp9WExzVcBQdkVq6lrs',
                ),
                'facebook' => array(
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'CustomFacebookService',
                    'client_id' => '589951314397273',
                    'client_secret' => '6e6081971a4c8b3830ec68ed92e272af',
                ),
                'mailru' => array(
                    // register your app here: http://api.mail.ru/sites/my/add
                    'class' => 'CustomMailruService',
                    'client_id' => '711917',
                    'client_secret' => '25f655c0861d47a8e73c1cdb597c62ce',
                ),
            ),
        ),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
        ),
        'request' => array(
            'enableCookieValidation' => true,
            'enableCsrfValidation' => false,
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'class' => 'RWebUser',
            'loginUrl' => '/admin.php/site/login/'
        ),
        'authManager' => array(
            'class' => 'RDbAuthManager',
            'defaultRoles' => array('Guest')
        ),
        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array( /* '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',*/
            ),
        ),
        'swiftMailer' => array(
            'class' => 'ext.swiftMailer.SwiftMailer',
        ),

        // uncomment the following to use a MySQL database

        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=db_kazakhtv',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'autoConnect' => false,
            'tablePrefix' => 'lx_',
            // включаем профайлер
            'enableProfiling' => false,
            // показываем значения параметров
            'enableParamLogging' => false,
            'schemaCachingDuration' => 0,
        ),
        'dbcurrency' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'mysql:host=127.0.0.1;dbname=currency',
            'emulatePrepare' => true,
            'username' => 'currency',
            'password' => 'H7dh634hd',
            'charset' => 'utf8',
            'autoConnect' => false,
            // включаем профайлер
            'enableProfiling' => false,
            // показываем значения параметров
            'enableParamLogging' => false,
        ),
        'cache' => array(
            'class' => 'system.caching.CFileCache',
        ),
        /*'memcache'=>array(
        'class'=>'system.caching.CMemCache',
        'servers'=>array(
                array('host'=>'127.0.0.1', 'port'=>11211),
            ),
        ),*/
        'session' => array(
            'class' => 'CDbHttpSession',
            'connectionID' => 'db',
            'autoCreateSessionTable' => false, //!!!
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'messages' => array(
            'class' => 'CDbMessageSource',
            'onMissingTranslation' => array('Ei18n', 'missingTranslation'),
            'sourceMessageTable' => 'SourceMessage',
            'translatedMessageTable' => 'Message'
        ),
        /* setup global translate application component */
        'translate' => array(
            'class' => 'translate.components.Ei18n',
            'createTranslationTables' => false,
            'connectionID' => 'db',
            'languages' => array(
                'kz' => 'Казахский',
                'ru' => 'Русский',
                'en' => 'Английский'
            )
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages

//                  array(
//                      'class'=>'CWebLogRoute',
//                      //'levels' => 'error, warning, info',
//
//                  ),
//                array(
//                    'class' => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
//                    'ipFilters' => array('127.0.0.1', '89.218.61.2'),
//                ),

            ),
        ),
        'clientScript' => array(
            'scriptMap' => array(
                'jquery.js' => false,
                'jquery.min.js' => false,
                'jqueryui.js' => false,
            )
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'languages' => array(
            'kz' => 'КАЗ',
            'ru' => 'РУС',
            'en' => 'ENG'
        ),
        'editorOptions' => array('filebrowserBrowseUrl' => array('/admin.php/pages/default/browse'),
            'extraPlugins' => 'Media',
            'toolbar' => array(
                array('Source', '-', 'Save', '-'),
                array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'),
                array('Undo', 'Redo', ' - ', 'Find', 'Replace', ' - ', 'SelectAll', 'RemoveFormat'),
                array('Bold', 'Italic', 'Underline', 'Strike', ' - ', 'Subscript', 'Superscript'),
                array('NumberedList', 'BulletedList', ' - ', 'Outdent', 'Indent', 'Blockquote'),
                array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
                array('Link', 'Unlink', 'Anchor'),
                array('Styles', 'Format', 'FontSize', 'Font'),
                array('Image', 'Media', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar'),
                array('Maximize', 'ShowBlocks')),
        ),
        'livePlayer' => array(
            'debug' => 0,
            'pro' => 1,
            //автовопспроизведения
            'auto' => 'play',
            // путь к файлу
            'file' => 'http://sgw.ott.otautv.kz:8081/streamingGateway/GetLivePlayList.m3u8?source=Kazakhtv.m3u8&amp;serviceArea=KTR_SA',
            //панель контролов
            'controls' => 'play,|,volume,volbarline,space,hd,|,menu,|,full,run_volume',
            // цвет фона
            'bgcolor' => 'ffffff',
            //тип медиа
            'm' => 'video',
            //высота
            'h' => 322,
            //ширина
            'w' => 570,
            //фон контролапнели
            'cntrlbgcolor' => '0|0',
            //язык
            'lang' => 'ru',
            // пропорции видео
            'aspect' => 1.77,
            // растягивать панель при полноэкранном
            'cntrlfull' => 1,
            // панель вне экрана
            'cntrlout' => 1,
            // фон панели контролов
            'cntrlposter' => '/themes/front/player/images/control_bg.png',
            //высота панели контролов
            'cntrloutheight' => 27,
            //вывод кода для расшаривания
            'codewithembed' => 1,
            // вывод большого меню
            'menubig' => 1,
            // авто меню построение
            'menuauto' => 1,
            // настройки скина

            //кнопка плей пауза
            'cntrl_play' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/icon_play.png|http://kazakh-tv.kz/themes/front/player/images/icon_pause.png',
                'pic_w' => 20,
                'pic_h' => 20,
                'margintop' => 1
            ),
            // линия громкости
            'cntrl_volbarline' => array(
                'h' => 4,
                'color_all' => '5b5c5a',
                'color_play' => '00364c',
                'bgcolor' => '5b5c5a',
                'bg_a' => 1,
                'all_a' => 1,
                'play_a' => 1,
                'a' => 0
            ),
            // бегунок громкости
            'cntrl_run_volume' => array(
                "h" => 12,
                "color" => "999999",
                "bg_o" => 0,
                "o" => 0,
                "w" => 4
            ),
            //иконка громкости
            'cntrl_volume' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/vol_on.png|http://kazakh-tv.kz/themes/front/player/images/vol_off.png',
                'margintop' => 1
            ),
            // бегунок прогресса показа
            'cntrl_run' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/play_run.png',
                'w' => 20,
                'h' => 20
            ),
            // общее время
            'cntrl_time_all' => array(
                'color' => '838382',
                'margintop' => 1
            ),
            // настройки полноэкрана
            'cntrl_full' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/icon_full.png',
                'margintop' => 1
            ),
            //иконка меню
            'cntrl_menu' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/icon_share.png',
                'margintop' => 1
            ),
        ),
        'videoPlayer' => array(
            'debug' => 0,
            //автовопспроизведения
            'auto' => 'none',
            // путь к файлу
            //'file' => 'rtmp://89.218.20.67:1938/live/caspionet',
            //панель контролов
            'controls' => 'play,|,volume,volbarline,time_play,time_all,line,space,hd,|,menu,|,download,full,run_volume,run_line,playlist,start',
            // цвет фона
            'bgcolor' => 'ffffff',
            //тип медиа
            'm' => 'video',
            //высота
            'h' => 322,
            //ширина
            'w' => 570,
            //фон контролапнели
            'cntrlbgcolor' => '0|0',
            //язык
            'lang' => 'ru',
            // пропорции видео
            'aspect' => 1.77,
            // растягивать панель при полноэкранном
            'cntrlfull' => 1,
            // панель вне экрана
            'cntrlout' => 1,
            // фон панели контролов
            'cntrlposter' => '/themes/front/player/images/control_bg.png',
            //высота панели контролов
            'cntrloutheight' => 27,
            //вывод кода для расшаривания
            'codewithembed' => 1,
            // вывод большого меню
          //  'menubig' => 1,
            // авто меню построение
         //   'menuauto' => 1,

            'plplace' => 'right',
            //'hd' => '240,320,480',
            'pltumbs' => 1,
            // настройки скина
            'hd' => '240,320,480',

            //кнопка плей пауза
            'cntrl_play' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/icon_play.png|http://kazakh-tv.kz/themes/front/player/images/icon_pause.png',
                'pic_w' => 20,
                'pic_h' => 20,
                'margintop' => 1
            ),

            'cntrl_hd' => array(
                'bgcolor' => '000000',
                'bg_a' => 0
            ),
            //линия показа
            'cntrl_line' => array(
                'h' => 8,
                'full' => 1,
                'margintop' => -17,
                'position' => 1,
                'color_all' => '5b5c5a',
                'color_play' => '00364c',
                'bgcolor' => '5b5c5a',
                'bg_a' => 1,
                'all_a' => 1,
                'play_a' => 1,
                'a' => 0,
                'marginleft' => -10,
                'marginright' => -10,
                'margins' => 0,
           ),
            // линия громкости
            'cntrl_volbarline' => array(
                'h' => 4,
                'color_all' => '5b5c5a',
                'color_play' => '00364c',
                'bgcolor' => '5b5c5a',
                'bg_a' => 1,
                'all_a' => 1,
                'play_a' => 1,
                'a' => 0
            ),
            // бегунок громкости
            'cntrl_run_volume' => array(
                "h" => 12,
                "color" => "999999",
                "bg_o" => 0,
                "o" => 0,
                "w" => 4
            ),
            //иконка громкости
            'cntrl_volume' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/vol_on.png|http://kazakh-tv.kz/themes/front/player/images/vol_off.png',
                'margintop' => 1
            ),
            // бегунок прогресса показа 
            'cntrl_run' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/play_run.png',
                'w' => 20,
                'h' => 20,
                'o' => 0,
                'a' => 1,
                'position' => 0
            ),
            // общее время
            'cntrl_time_all' => array(
                'color' => '838382',
                'margintop' => 1
            ),
            // настройки полноэкрана
            'cntrl_full' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/icon_full.png',
                'margintop' => 1
            ),
            //иконка меню
            'cntrl_menu' => array(
                'icon' => 'http://kazakh-tv.kz/themes/front/player/images/icon_share.png',
                'margintop' => 1
            ),
        )
    )
);