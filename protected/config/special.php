<?php
return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'),
    array(
        'name' => Yii::t('pda_layout', '<span class="small">Официальный сайт</span> Президента Республики Казахстан'),
        'theme' => 'special',
        'defaultController' => 'mainpage',
        'controllerPath' => 'protected/controllers/special',
        'language' => 'kz',
        'components' => array(
            'urlManager' => array(
                'class' => 'application.components.lxUrlManager',
                //'urlFormat' => 'path',
                'showScriptName' => true,
                'rules' => array(
                    '<language:(ru|kz|en)>/news' => 'page/news',
                    '<language:(ru|kz|en)>/rss' => 'page/rss',
                    '<language:(ru|kz|en)>/rss/<category_id>' => 'page/rss',
                    '<language:(ru|kz|en)>/category/rss' => 'page/rss',
                    '<language:(ru|kz|en)>/archive/<date>' => 'page/archive',
                    '<language:(ru|kz|en)>/allNews' => 'page/allNews',
                    '<language:(ru|kz|en)>/page/<sefname>' => 'page/view',
                    '<language:(ru|kz|en)>/category/<sefname>' => 'page/category',
                    '<language:(ru|kz|en)>/<controller:\w+>' => '<controller>/index',
                    '<language:(ru|kz|en)>/<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<language:(ru|kz|en)>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<language:(ru|kz|en)>/<controller:\w+>/<action:\w+>/*' => '<controller>/<action>',
                ),
            ),
            'image' => array(
                'class' => 'application.extensions.image.CImageComponent',
                // GD or ImageMagick
                'driver' => 'GD',
            ),
        )
    )
);