<?php
/**
 * Copyright (c) 2012 by Yevgeniy Dymov / johnluxor@hotmail.com
 */

return CMap::mergeArray(
    require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main.php'),
    array(
        'defaultController' => 'terminal/default',
        'name' => '������',
        'import' => array(
            'application.modules.categories.front.components.*',
            'application.modules.users.front.components.*',
            'application.modules.shop.front.components.*',
            'application.modules.categories.models.*',
            'application.modules.langs.models.*',
            'application.modules.pages.models.*',
            'application.modules.shop.models.*',
            'ext.shoppingCart.*'

        ),
        'modules' => array(
            'terminal'
        ),
        'components' => array(
            'user' => array(
                'loginUrl' => '/terminal.php/users/default/login/'
            ),
            'cart' => array(
                'class' => 'ext.shoppingCart.EShoppingCart',
            ),
            'urlManager' => array(
                'class' => 'application.components.lxUrlManager',
                'urlFormat' => 'path',
                'showScriptName' => true,
                'rules' => array(
                    '<language:(ru|kz|en)>/terminal/default/postPay/<id>' => 'terminal/default/postPay',
                    '<language:(ru|kz|en)>/terminal/default/backPay/<id>' => 'terminal/default/backPay',
                    '<language:(ru|kz|en)>/terminal/<action:\w+>' => 'terminal/default/<action>',
                    '<language:(ru|kz|en)>/terminal/<action:\w+>/<id:\w+>' => 'terminal/default/<action>/<id>',
                    '<language:(ru|kz|en)>/terminal/<action:\w+>/<id:\d+>' => 'terminal/default/<action>/<id>',

                ),
            ),
             'errorHandler' => array(
                // use 'site/error' action to display errors
                'errorAction' => 'terminal/default/error',
            ),
        )
    )
);