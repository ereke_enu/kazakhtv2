<?php
return array(
    // У вас этот путь может отличаться. Можно подсмотреть в config/main.php.
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Cron',

    'preload' => array('log'),

    'import' => array(
        'application.components.*',
        'application.models.*',
    ),
    // Перенаправляем журнал для cron-а в отдельные файлы
    'components' => array(
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'logFile' => 'cron.log',
                    'levels' => 'error, warning',
                )
            ),
        ),
        'swiftMailer' => array(
            'class' => 'ext.swiftMailer.SwiftMailer',
        ),


        // Соединение с СУБД
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=v_8054_problema',
            'emulatePrepare' => false,
            'username' => 'v_8054_problema',
            'password' => 'vo2AIV8X',
            'charset' => 'utf8',
            'tablePrefix' => 'lx_',
            // включаем профайлер
            'enableProfiling' => false,
            // показываем значения параметров
            'enableParamLogging' => false,
            'schemaCachingDuration' => 0
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'noreply@problema.kz',
        'adminName' => 'Problema.kz', //JHbGm4ya
        'mail' => array(
            'host' => 'mail.problema.kz',
            'port' => '25',
            'username' => 'noreply@problema.kz',
            'password' => 'JHbGm4ya',
            'ssl' => null,
            'email' => 'noreply@problema.kz'
        ),
    ),
);