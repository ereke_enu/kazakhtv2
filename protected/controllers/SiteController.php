<?php

class SiteController extends LXController {

    public function actionError() {
        //$this->redirect('/');
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
}