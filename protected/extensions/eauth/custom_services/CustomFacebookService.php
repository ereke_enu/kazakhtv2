<?php
    class CustomFacebookService extends FacebookOAuthService {
        /**
         * https://developers.facebook.com/docs/authentication/permissions/
         */
        protected $scope = 'email';

        /**
         * http://developers.facebook.com/docs/reference/api/user/
         *
         * @see FacebookOAuthService::fetchAttributes()
         */
        protected function fetchAttributes() {
            $this->attributes = (array)$this->makeSignedRequest('https://graph.facebook.com/me');

            $this->attributes['fname'] = $this->attributes['first_name'];
            $this->attributes['lname'] = $this->attributes['last_name'];
        }
    }
