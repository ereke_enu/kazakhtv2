<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 24.01.13
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */
//require_once __FILE__ . '/../uppod_codec.php';
Yii::import('ext.uppod.uppod_codec', true);
class LXUppodWidget extends CWidget {
    public $width = 570;
    public $height = 349;
    public $params;
    public $id = 'player';
    public $type = 'json';

    public function run() {
        Yii::app()->clientScript->registerScriptFile('https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js', CClientScript::POS_HEAD);

        echo CHtml::openTag('div', array('id' => $this->id, 'class' => 'player'));
        echo CHtml::closeTag('div');
        $params = uppod_encode(CJSON::encode($this->params));

        $js = 'var flashvars = {st:\'' . $params . '\'};
               var params = {bgcolor:"#ffffff", wmode:"opaque", allowFullScreen:"true", allowScriptAccess:"always"};
               swfobject.embedSWF("/themes/front/player/uppod.swf", "' . $this->id . '", "' . $this->width . '", "' . $this->height . '", "10.0.0.0", false, flashvars, params);';


        Yii::app()->clientScript->registerScript('lxUppodPlayer_'.$this->id, $js, CClientScript::POS_END);


    }
}
?>
