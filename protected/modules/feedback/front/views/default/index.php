<h1>Обратная связь</h1>

<article>
    <div class="registration_form">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'method' => 'POST',
            'id' => 'feedback-form',
            'enableAjaxValidation' => false
        ));?>
        <table>
            <tr>
                <td><?=$form->labelEx($model, 'name')?></td>
                <td>
                    <?=$form->textField($model, 'name', array('class' => 'field'))?>
                    <?=$form->error($model, 'name')?>
                </td>
            </tr>
            <tr>
                <td><?=$form->labelEx($model, 'email')?></td>
                <td>
                    <?=$form->textField($model, 'email', array('class' => 'field'))?>
                    <?=$form->error($model, 'email')?>
                </td>
            </tr>
            <tr>
                <td><?=$form->labelEx($model, 'subject')?></td>
                <td>
                    <?=$form->textField($model, 'subject', array('class' => 'field'))?>
                    <?=$form->error($model, 'subject')?>
                </td>
            </tr>
            <tr>
                <td><?=$form->labelEx($model, 'message')?></td>
                <td>
                    <?=$form->textArea($model, 'message', array('rows' => 3, 'cols' => 30))?>
                    <?=$form->error($model, 'message')?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><?php  $this->widget('CCaptcha', array('buttonLabel' => '<br />Обновить код')); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->label($model, 'captcha')?></td>
                <td>

                    <?php echo $form->textField($model, 'captcha', array('class' => 'field'))?>
                    <?php echo $form->error($model, 'captcha');?></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button class="blue_button"><?=Yii::t('feedback', 'Отправить')?></button>
                </td>
            </tr>
        </table>
        <?php $this->endWidget();?>
    </div>
</article>