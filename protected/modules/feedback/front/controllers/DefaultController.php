<?php

class DefaultController extends LXController {

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
            ),
        );
    }


    public function actionIndex() {

        $model = new FeedbackForm;

        if (isset($_POST['FeedbackForm'])) {
            $model->attributes = $_POST['FeedbackForm'];

            if ($model->send()) {
                $this->redirect(array('successSend'));
            }
        }

        $this->render('index', array('model' => $model));
    }

    public function actionSuccessSend() {
        $this->render('successSend');
    }
}