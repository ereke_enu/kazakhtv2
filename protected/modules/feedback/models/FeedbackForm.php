<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 19.12.12
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */
class FeedbackForm extends CFormModel {
    public $name;
    public $email;
    public $subject = 'Сообщение из формы обратной связи';
    public $message;
//    public $captcha;


    public function rules() {
        return array(
            array('name, email, subject, message', 'required'),
            array('email', 'email'),
//            array('captcha', 'captcha')
        );
    }

    public function attributeLabels() {
        return array(
            'name' => 'Ваше имя',
            'email' => 'Ваш e-mail',
            'subject' => 'Тема сообщения',
            'message' => 'Текст сообщения',
            'captcha' => 'Защитный код'
        );
    }

    public function send() {
        if ($this->validate()) {
            $content = '
            <p>Пользователь ' . $this->name . ' отправил письмо:</p>
<p><strong>Тема:</strong> ' . $this->subject . '</p>
<p><strong>e-mail:</strong> ' . $this->email . '</p>
<p><strong>Сообщение:</strong> ' . $this->message . '</p>';

            Yii::app()->getController()->sendMail('Обратная связь с сайта Kazakh-tv.kz', $content, $this->email, 'kaztv@khabar.kz');
            return true;

        }

        return false;


    }

}
