<div class="block-form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'method' => 'POST',
		'action' => Yii::app()->createUrl('feedback/app/index', array('sefname'=>'contacts')),
		'id' => 'feedback-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true
	));?>

	<div class="form-group">
		<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
		<?= $form->textField($model, 'name', array('class' => 'form-control')) ?>
		<?= $form->error($model, 'name', array('class'=>'help-block')) ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'email', array('class'=>'control-label')) ?>
		<?= $form->textField($model, 'email', array('type'=>'email', 'class' => 'form-control')) ?>
		<?= $form->error($model, 'email', array('class'=>'help-block')) ?>
	</div>

	<div class="form-group" style="margin-bottom: 40px">
		<?= $form->labelEx($model, 'message', array('class'=>'control-label')) ?>
		<?= $form->textArea($model, 'message', array('rows' => 5, 'class'=>'form-control input-default')) ?>
		<?= $form->error($model, 'message', array('class'=>'help-block')) ?>
	</div>

	<?php if(!Yii::app()->request->isAjaxRequest) { ?>
		<?= CHtml::submitButton(Yii::t('feedback', 'Отправить'), array(
			'ajax' => array(
				'dataType'=>'json',
				'type'=>'POST',
				'url'=>Yii::app()->createUrl('feedback/app/index', array('sefname'=>'contacts')),
				'success'=>'function(data) {
					if(data.status == \'success\') {
						$("#feedback-form")[0].reset();
						$("#feedback-form").closest(".nur-modal").find("div.success-block").fadeIn();
					} else {
						for (var item in data) {
							console.log(item);
						}
					}
				}'),
			'class'=>'btn btn-grey pull-right'
		)); ?>
	<?php } else { ?>
		<?= CHtml::submitButton(Yii::t('feedback', 'Отправить'), array('class'=>'btn btn-grey pull-right')); ?>
	<?php } ?>

	<?php $this->endWidget();?>

	<div class="success-block text-center" style="display: none;">
		<p><?= Yii::t('feedback', 'Сообщение успешно отправлено') ?>!</p>
		<button class="btn btn-blue btn-ok">Ok</button>
	</div>
</div>