<?php

class AppController extends LXController {

	public $layout = '//layouts/app';

	public function actionIndex() {
		$model = new FeedbackForm;

		if(isset($_POST['ajax']) && $_POST['ajax']==='feedback-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if (isset($_POST['FeedbackForm'])) {
			$model->attributes = $_POST['FeedbackForm'];
			$valid = $model->validate();
			if($valid) {
				if ($model->send()) {
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('status'=>'success'));
						Yii::app()->end();
					} else {
						$this->redirect(array('successSend'));
					}
				}
			} else {
				$error = CActiveForm::validate($model);
				if($error!='[]')
					echo $error;
				Yii::app()->end();
			}
		}

		if (Yii::app()->request->isAjaxRequest)
			$this->renderPartial('_feedback_form', array('model' => $model));
		else
			$this->render('index', array('model' => $model));
	}

	public function actionSuccessSend() {
		$this->breadcrumbs = array(
			Yii::t('title', 'Контакты') => Yii::app()->createUrl('/pages/default/category', array('sefname'=>'contacts'))
		);

		if (Yii::app()->request->isAjaxRequest)
			$this->renderPartial('successSend');
		else
			$this->render('successSend');
	}

}