<script type="text/javascript">        
    $(document).ready(function(){
        var scroller_items_count = $('.sliderItem').length;
        var current_scroller_index=0;
        var current_scroller_id = '#scrollerItem';

        $('#btnScrollToward').click(function(){
            $(current_scroller_id+current_scroller_index).css('display','none');
            $(current_scroller_id+(current_scroller_index+6)).css('display','block');
            if((current_scroller_index+7)<scroller_items_count)
                current_scroller_index++;
        })

        $('#btnScrollBack').click(function(){
            if(current_scroller_index>0){
                $(current_scroller_id+(current_scroller_index+5)).css('display','none');
                $(current_scroller_id+(current_scroller_index-1)).css('display','block');
                current_scroller_index--;
            }
        })
    });
</script>

    <div  id="scroller" >
        <div id="btnScrollBack" class="scroller_arrow">
            <div id="triangle-left">
            </div>
        </div>
        <div id="scrollerItem0" class="sliderItem" >
                <div class="text">0 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem1" class="sliderItem">
            <div class="text">1 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem2" class="sliderItem">
            <div class="text">2 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem3" class="sliderItem">
            <div class="text">3 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem4" class="sliderItem">
            <div class="text">4 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem5" class="sliderItem">
            <div class="text">5 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem6" class="sliderItem display_none">
            <div class="text">6 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem7" class="sliderItem display_none">
            <div class="text">7Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem8" class="sliderItem display_none">
            <div class="text">8 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem9" class="sliderItem display_none">
            <div class="text">9 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="scrollerItem10" class="sliderItem display_none">
            <div class="text">10 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
            <img src="/upload/test.jpg" />
        </div>
        <div id="btnScrollToward" class="scroller_arrow">
            <div id="triangle-right"></div>
        </div>
    </div>
