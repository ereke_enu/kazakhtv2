<?php 
$i = 0;
foreach($pages as $page) {
    $url = Yii::app()->createUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname ));
    $image = Pages::getAnounceImg($page->image, '');
	if($i == 0) {            
?>
		<article class="clearfix" id="lineNews">		 
			<table>
				<tr>
					<td colspan="2" style="padding-bottom: 5px;"  class="bizsport-h"><h3><a style="font-size:14px;" href="<?= $url ?>"><?= $page->title ?></a></h3></td>
				</tr>
				<tr>
					<td class="" style="vertical-align: top;">		
						<a href="<?= $url ?>" id="lineNewsA"><img style="width:192px;height:108px;" src="<?= $image ?>" class="img-responsive col-sm-6 no-padding" /></a>
					</td>
					<td class="bizsport-p"  style="vertical-align: top;">
				   		<p style="margin-left:5px;"><a style="font-size:12px;color:#2b2b2b;" href="<?= $url ?>" class="arrow"><?= $page->short_text ?></a></p>
						
					</td>
					
				</tr>
			</table>
        </article>
        <hr />
<?php } else { ?>
        <article>
            <h3 class="bizsport-small" style= "font-size:14px;"><a href="<?= $url ?>" class="arrow"><?= $page->title ?></a></h3>
        </article>
        <hr />
<?php
        }
        $i++;
    }
?>