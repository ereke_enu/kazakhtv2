<?php
    $url = Yii::app()->createUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname ));
    $image = Pages::getAnounceImg($page->image, 'top_');
    
?>
<div>
    <a href="<?= $url ?>" title="<?= $page->title ?>" target="_blank"><img src="<?= $image ?>" width="480" height="270" alt="Slide 1"></a>
    <div class="caption" style="bottom:0">
        <h3><a href="<?= $url ?>"><?= $page->title ?></a></h3>
        <p><a href="<?= $url ?>" class="arrow"><?= $page->short_text ?></a></p>
    </div>
</div>
