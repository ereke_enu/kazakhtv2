<?php $image = Pages::getAnounceImg($page->image, 'top_'); ?>
<div class="goto_top" style="background: url(<?=$image?>) no-repeat left 14px;"
     data-url="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname))?>">
    <div class="top_news">
        <h2><?=Yii::t('pages', 'Главная новость');?></h2>

        <div class="caption">
            <h4><?=$page->title?></h4>

            <p><?=$page->short_text?></p>
        </div>

    </div>

</div>