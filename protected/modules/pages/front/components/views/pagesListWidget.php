<div class="lastComment" style="margin-top:20px;">
    <div class="anounce">
        <div class="anounce-h"><h2><?=$title?></h2></div>
    </div>
    <div class="comments_content">
        <ul>
            <?php foreach ($pages as $page) {
                ?>
                <li>
                    <?php if($date) {?>
                    <span class="date"><?=date('d.m.Y H:i', strtotime($page->date . ' ' . $page->time))?></span>
                    <?php } ?>
                    <a href="<?=Yii::app()->createUrl('pages/default/view', array('sefname' => $page->sefname, 'category' => $page->category->sefname))?>">
                        <?=$page->title?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>