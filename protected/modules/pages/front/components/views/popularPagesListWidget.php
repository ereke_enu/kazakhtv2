<div class="lastComment">
    <div class="title">
        <h2><?= $title ?></h2>
    </div>
    <div class="comments_content">
        <ul>
            <?php foreach ($pages as $page) {
                ?>
                <li>
                    <span class="date"><?= date('d.m.Y H:i', strtotime($page->date . ' ' . $page->time)) ?></span>
                    <a href="<?= Yii::app()->createUrl('pages/default/view', array('sefname' => $page->sefname, 'category' => $page->category->sefname)) ?>">
                        <?= $page->title ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>