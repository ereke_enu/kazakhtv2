<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 01.02.13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */

class PopularPagesListWidgetNew extends CWidget {
    public $category;
    public $title = false;

    public function run() {
         $category = Categories::model()->findBySefnameObj('news');
        if ($category == null) {
            return false;
        }

        $categories = $category->children()->findAll();
        $categoryId = CHtml::listData($categories, 'id', 'id');
        $categories="";
        
        foreach ($categoryId as $value) {
            $categories=$categories.$value.", ";
        }


        $categories=$categories."1";
        
        if ($this->title === false) {
            $this->title = $category->title;
        }

        $criteria = new CDbCriteria;
        $date = date('Y-m-d', strtotime('-7 days'));
        $criteria->params= array(':date' =>$date,);
        $criteria->condition='date>:date and  is_archive=0 and active=1 and category_id in ('.$categories.')';
        
        //$criteria->condition='is_archive=0 and active=1 and category_id in ('.$categories.')';
        $criteria->order = 'views DESC';
        $criteria->limit = 7;
        $pages = Pages::model()->cache(86400)->findAll($criteria);
        $this->render('popularPagesListWidgetNew', array('pages' => $pages, 'category' => $category, 'title' => $this->title));
    }

}