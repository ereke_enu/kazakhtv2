<?php

class programmNewsWidget1 extends CWidget {

    public $category_sefname='none';
    
    public function run() {
		
        $lang=Yii::app()->language;
        $langId = Langs::getLangIdByCode($lang);
        
        $programIds ="625,616";
        
        $arr = array();

        $cache = new CacheBuilder("programmNewsWidgets/programmNewsWidget_".$this->category_sefname,$lang);

        if($cache->checkFile()==true){
            include($cache->getFullPath());
        }else{
            $programIds =$this->getProgrammIds($this->category_sefname,$lang);
            $criteria = new CDbCriteria;
            $criteria->limit=3;
            $criteria->params=array(':lang_id',$langId);
            $criteria->order = 'date DESC';
            $criteria->join='left join lx_programms_timetable pt on t.timetable_id=pt.id';
            $criteria->condition='pt.programm_id in ('.$programIds.')';
            $model = VideoArchive::model()->findAll($criteria);
            $i=0;
            $txt="<"."?php ";
            foreach ($model as $row) {
                $arr[$i]->id = $row->id;
                $txt.="$"."arr[".$i."]->id='".$row->id."';";
                $arr[$i]->title = $row->title;
                
                if($lang=="en"){
                    $txt.="$"."arr[".$i."]->title='".addslashes($arr[$i]->title)."';";
                }else{ $txt.="$"."arr[".$i."]->title='".$arr[$i]->title."';"; }
                

                $i++;
            }
            
            $txt.=" ?".">";
            $cache->writeToFile($txt);

        }
        

        $this->render('programmNewsWidget1', array('model' => $arr,));
    }

    public function getObjectCacheText($model, $propName,$i){
        return "$"."arr[".$i."]->".$propName."='".$model->$propName."';";
    }

    public function getProgrammIds($category_sefname, $lang){
        $programIds ="";
        if($this->category_sefname=='history'){
            switch ($lang) {
                case 'ru':
                    $programIds ="43";
                    break;
                case 'kz':
                    $programIds ="44";
                    break;
                case 'en':
                    $programIds ="45";
                    break;
            }
        }else if($this->category_sefname=='culture'){
            switch ($lang) {
                case 'ru':
                    $programIds ="604,598";
                    break;
                case 'kz':
                    $programIds ="605,599";
                    break;
                case 'en':
                    $programIds ="606,600";
                    break;
            }
        }else if($this->category_sefname=='fashion'){
            switch ($lang) {
                case 'ru':
                    $programIds ="616,610";
                    break;
                case 'kz':
                    $programIds ="617,611";
                    break;
                case 'en':
                    $programIds ="618,612";
                    break;
            }

        }else if($this->category_sefname=='tourism'){
            switch ($lang) {
                case 'ru':
                    $programIds ="625,70";
                    break;
                case 'kz':
                    $programIds ="626,71";
                    break;
                case 'en':
                    $programIds ="627,72";
                    break;
            }
        }else if($this->category_sefname=='kitchen'){
            switch ($lang) {
                case 'ru':
                    $programIds ="781";
                    break;
                case 'kz':
                    $programIds ="782";
                    break;
                case 'en':
                    $programIds ="783";
                    break;
            }
        }else if($this->category_sefname=='innovations'){
            switch ($lang) {
                case 'ru':
                    $programIds ="577,34";
                    break;
                case 'kz':
                    $programIds ="578,35";
                    break;
                case 'en':
                    $programIds ="579,36";
                    break;
            }
        }else if($this->category_sefname=='economy'){
            switch ($lang) {
                case 'ru':
                    $programIds ="754,580";
                    break;
                case 'kz':
                    $programIds ="755,581";
                    break;
                case 'en':
                    $programIds ="756,582";
                    break;
            }
        }else if($this->category_sefname=='science'){
            switch ($lang) {
                case 'ru':
                    $programIds ="37,775";
                    break;
                case 'kz':
                    $programIds ="38,776";
                    break;
                case 'en':
                    $programIds ="39,777";
                    break;
            }
        }
        return $programIds;
    }
}