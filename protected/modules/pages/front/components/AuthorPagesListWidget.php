<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 01.02.13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */

class AuthorPagesListWidget extends CWidget {
    public $category;
    public $title = false;

    public function run() {
        $category = Categories::model()->findBySefnameObj('column');
        if ($category == null) {
            return false;
        }
        
        if ($this->title === false) {
            $this->title = $category->title;
        }

        $criteria = new CDbCriteria;
        //SELECT column FROM table ORDER BY RAND() LIMIT 10
        //$date = date('Y-m-d', strtotime('-30 days'));
        //$criteria->params= array(':date' =>$date,);
        //$criteria->condition='date>:date and  is_archive=0 and active=1 and category_id in ('.$category->id.')';
        $criteria->condition='is_archive=0 and active=1 and category_id in ('.$category->id.')';
        $criteria->order = 'RAND()';
        $criteria->limit = 4;
        $pages = Pages::model()->cache(300)->findAll($criteria);
        $this->render('authorPagesListWidget', array('pages' => $pages, 'category' => $category, 'title' => $this->title));
    }

}