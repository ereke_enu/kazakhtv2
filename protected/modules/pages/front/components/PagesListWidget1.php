<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 01.02.13
     * Time: 15:43
     * To change this template use File | Settings | File Templates.
     */

    class PagesListWidget1 extends CWidget {
        public $category;
        public $title = false;
        public $date = false;
        public $limit = 10;

        public function run() {

            $pages;
            $lang = Yii::app()->language;
            $cache = new CacheBuilder("pagesListWidgets/pages",$lang);

            if($cache->checkFile()==true){
                include($cache->getFullPath());
            }else{
                $category = Categories::getChildCategories($this->category);
                if (empty($category)) {
                    return false;
                }

                $curSefname = Yii::app()->request->getParam('sefname', false);

                if ($this->title === false) {
                    $this->title = $category->title;
                }


                $criteria = new CDbCriteria;
                $criteria->compare('category_id', $category);
                if ($curSefname !== false) {
                    $criteria->compare('sefname', '<>' . $curSefname);
                }
                $criteria->order = 'date DESC, time DESC';
                $criteria->compare('is_archive', '0');
                $criteria->compare('active', '1');
                if ($this->limit > 0) {
                    $criteria->limit = $this->limit;
                }
                $pages = Pages::model()->findAll($criteria);

                $i=0;
                $txt="<"."?php ";
                foreach ($pages as $page) {
                    $txt.="$"."pages[".$i."]->category->sefname='".$page->category->sefname."';";
                    $txt.="$"."pages[".$i."]->sefname='".$page->sefname."';";
                    //if($lang!="en")
                    //$page->title = str_replace("'", "\'", $page->title);
                    if($lang=="en"){
                        $txt.="$"."pages[".$i."]->title='".addslashes($page->title)."';";
                    }else{
                        $txt.="$"."pages[".$i."]->title='".$page->title."';";    
                    }
                    
                    $txt.="$"."pages[".$i."]->date='".$page->date."';";
                    $txt.="$"."pages[".$i."]->time='".$page->time."';";
                    $i++;
                }
                
                $txt.=" ?".">";
                $cache->writeToFile($txt);
            }
            
            $this->render('pagesListWidget', array('pages' => $pages, 'category' => $category, 'title' => $this->title, 'date' => $this->date));
        }

    }