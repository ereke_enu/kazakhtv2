<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ugeen Alex
 * Date: 10.05.12
 * Time: 14:21
 * To change this template use File | Settings | File Templates.
 */
class LatestNewsWidget extends CWidget {
    public $pages;
    public function run() {
        $this->render('latestNewsWidget', array('pages' => $this->pages));
    }
}