<?php

class CinemaNewsWidget extends CWidget {

    public function run() {
        $category = Categories::model()->findBySefnameObj('cinema');

        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $category->id);
        $criteria->compare('active', '1');
        $criteria->compare('is_archive', '0');
        $criteria->order = 'date DESC, time DESC';
        $criteria->limit = 3;
        $criteria->compare('is_main', '0');


        $pages = Pages::model()->cache(300)->findAll($criteria);
        
        $this->render('cinemaNews', array('pages' => $pages));
    }
}