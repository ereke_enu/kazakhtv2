<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 01.02.13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */

class PopularPagesListWidget extends CWidget {
    public $category;
    public $title = false;

    public function run() {
        $category = Categories::model()->cache(21600)->findBySefnameObj('news');
        if ($category == null) {
            return false;
        }

        $categories = $category->children()->cache(21600)->findAll();
        $categoryId = CHtml::listData($categories, 'id', 'id');

        if ($this->title === false) {
            $this->title = $category->title;
        }


        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $categoryId);
        $criteria->order = 'views DESC';
        $criteria->compare('is_archive', '0');
        $criteria->compare('active', '1');
        $criteria->limit = 10;
        $pages = Pages::model()->cache(21600)->findAll($criteria);

        $this->render('popularPagesListWidget', array('pages' => $pages, 'category' => $category, 'title' => $this->title));
    }

}