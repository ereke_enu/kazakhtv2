<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 01.02.13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */

class PopularPagesListWidget1 extends CWidget {
    public $category;
    public $title = false;

    public function run() {
        $lang = Yii::app()->language;
        $pop_pages;

        $cache = new CacheBuilder("popularPagesLists/popularPagesList",$lang);
        
        if($cache->checkFile()==true){
            include($cache->getFullPath());
        }else{

            $category = Categories::model()->findBySefnameObj('news');
            if ($category == null) {
                return false;
            }

            $categories = $category->children()->findAll();
            $categoryId = CHtml::listData($categories, 'id', 'id');
            $categories="";
            
            foreach ($categoryId as $value) {
                $categories=$categories.$value.", ";
            }


            $categories=$categories."1";
            
            if ($this->title === false) {
                $this->title = $category->title;
            }

            $criteria = new CDbCriteria;
            $date = date('Y-m-d', strtotime('-7 days'));
            $criteria->params= array(':date' =>$date,);
            $criteria->condition='date>:date and  is_archive=0 and active=1 and category_id in ('.$categories.')';
            
            //$criteria->condition='is_archive=0 and active=1 and category_id in ('.$categories.')';
            $criteria->order = 'views DESC';
            $criteria->limit = 7;
            $pop_pages = Pages::model()->findAll($criteria);

            $i=0;
            $txt="<"."?php ";
            foreach ($pop_pages as $page) {
                $txt.="$"."pop_pages[".$i."]-".">"."cat"."egory->sefname='".$page->category->sefname."';";
                $txt.="$"."pop_pages[".$i."]->sefname='".$page->sefname."';";
                //if($lang=="en")
                //$page->title = str_replace("'", "\'", $page->title);
                if($lang=="en"){
                    $txt.="$"."pop_pages[".$i."]->title='".addslashes($page->title)."';";
                }else{ $txt.="$"."pop_pages[".$i."]->title='".$page->title."';"; }
                
                $txt.="$"."pop_pages[".$i."]->image='".$page->image."';";
                $i++;
            }
            $txt.=" ?".">";
            $cache->writeToFile($txt);
        }

        $this->render('popularPagesListWidgetNew', array('pages' => $pop_pages, ));
    }

}