<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 01.02.13
     * Time: 15:43
     * To change this template use File | Settings | File Templates.
     */

    class SimilarPagesWidget extends CWidget {
        public $page_id;

        public function run() {
            $pages = Pages::model()->cache(300)->getSimilar($this->page_id);
            $this->render('similarPagesWidget', array('pages' => $pages));
        }
    }