<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 21.02.13
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */

class mainNewsWidgetMob extends CWidget {

    public function run() {
        $category = Categories::model()->findBySefnameObj('news');

        $criteria = new CDbCriteria;
        $criteria->addInCondition('category_id', CHtml::listData($category->children()->findAll('active=1'), 'id', 'id'));
        $criteria->compare('active', '1');
        $criteria->compare('is_archive', '0');
        $criteria->order = 'date DESC, time DESC';
        $criteria->limit = 1;
        $criteria->compare('is_main', '1');


        $page = Pages::model()->find($criteria);


        $this->render('mainNewsWidgetMob', array('page' => $page));
    }
}