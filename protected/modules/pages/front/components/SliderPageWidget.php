<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 01.02.13
     * Time: 15:43
     * To change this template use File | Settings | File Templates.
     */

    class SliderPageWidget extends CWidget {
        public $page;
        public function run() {
            if($this->page->title=="")return;
            $this->render('sliderPageWidget', array('page' => $this->page, ));
        }
    }