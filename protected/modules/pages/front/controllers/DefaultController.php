<?php
    include "./protected/components/Translator.php";  
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 05.12.12
     * Time: 15:08
     * To change this template use File | Settings | File Templates.
     */
    class DefaultController extends LXController {

        public $layout = '//layouts/second';

        public function actionCategory($sefname) {

            $lang=Yii::app()->language;

            $category = Categories::model()->findBySefnameObj($sefname);

            if ($category->sefname == '' || $category->parent_id == 11 || $category->sefname=='press_about_us') {
                $this->layout = '//layouts/company';
            }

            /*
            if($sefname=='worldwide'){
                $this->layout = '//layouts/singlePageLayout';   
            }
            */
            
            if ($category === null) {
                throw new CHttpException(404, 'Категория не найдена');
            }

            $criteria = new CDbCriteria;
            $criteria->compare('category_id', $category->id);
            $criteria->compare('active', '1');
            $criteria->order = 'date DESC, time DESC';

            $page = Pages::model()->find($criteria);

            $this->pageTitle = $category->title;
            $this->metaTags = $page->meta_tags;
            $this->metaDescription = $page->meta_description;
            if($page->title!="") $this->pageTitle = $page->title;
            
            if($sefname=='kazakhstan_overview')
            {
                switch ($lang) {
                  case 'kz':
                        $this->pageTitle="Қазақстан - ҚАЗАҚСТАН РЕСПУБЛИКАСЫНЫҢ АЛҒАШҚЫ ҰЛТТЫҚ СПУТНИКТІК АРНАСЫ Kazakh TV";
                        break;
                  case 'ru':
                        $this->pageTitle="Казахстан - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                        break;
                    case 'en':
                        $this->pageTitle="Kazakhstan - THE FIRST NATIONAL SATELLITE CHANNEL OF THE REPUBLIC OF KAZAKHSTAN Kazakh TV";
                        break;
                  default:
                        $this->pageTitle="Казахстан - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                        break;
                }
            }else if($sefname=='about_company')
            {
                switch ($lang) {
                  case 'kz':
                        $this->pageTitle="Біз жайлы - ҚАЗАҚСТАН РЕСПУБЛИКАСЫНЫҢ АЛҒАШҚЫ ҰЛТТЫҚ СПУТНИКТІК АРНАСЫ Kazakh TV";
                        break;
                  case 'ru':
                        $this->pageTitle="О нас - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                        break;
                    case 'en':
                        $this->pageTitle="About us - THE FIRST NATIONAL SATELLITE CHANNEL OF THE REPUBLIC OF KAZAKHSTAN Kazakh TV";
                        break;
                  default:
                        $this->pageTitle="О нас - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                        break;
                }
            }
            else if($sefname=='worldwide')
            {
                switch ($lang) {
                  case 'kz':
                        $this->pageTitle="worldWide - ҚАЗАҚСТАН РЕСПУБЛИКАСЫНЫҢ АЛҒАШҚЫ ҰЛТТЫҚ СПУТНИКТІК АРНАСЫ Kazakh TV";
                        break;
                  case 'ru':
                        $this->pageTitle="worldWide - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                        break;
                    case 'en':
                        $this->pageTitle="worldWide - THE FIRST NATIONAL SATELLITE CHANNEL OF THE REPUBLIC OF KAZAKHSTAN Kazakh TV";
                        break;
                  default:
                        $this->pageTitle="worldWide - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                        break;
                }
            }

            $pl = array();
            if (count($page->videos) > 1) {
                foreach ($page->videos as $video) {
                    if ($video->upload_ext == 'flv') {
                        $filename = $video->upload_file;
                    } else {
                        $filename = $video->filename . '_[240,320,480].mp4';
                    }
                    $pl['playlist'][] = array(
                        'file'      => '/upload/video/pages/' . $video->page_id . '/' . $filename,
                        'comment'   => $video->title,
                        'poster'    => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
                        'bigposter' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
                    );
                }
            } elseif (count($page->videos) == 1) {
                $video = $page->videos[0];
                if ($video->upload_ext == 'flv') {
                    $filename = $video->upload_file;
                } else {
                    $filename = $video->filename . '_[240,320,480].mp4';
                }
                $params = array(
                    'file'       => '/upload/video/pages/' . $video->page_id . '/' . $filename,
                    'comment'    => $video->title,
                    'poster'     => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
                    'big_poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
                );
            }


            if (empty($params)) {
                $params = array('pl' => $pl, 'file' => '');
            }


            $this->render('category', array('category' => $category, 'pages' => $page, 'params' => $params));
        }

        public function actionTeam($sefname) {
            $this->layout = '//layouts/company';
            $model = Categories::model()->findBySefnameObj($sefname);

            if ($model === null) {
                throw new CHttpException(404, 'Странциа не найдена');
            }

            $children = $model->children()->findAll();

            if (!empty($children)) {
                $categories = CHtml::listData($children, 'id', 'id');
            } else {
                $categories = $model->id;
            }

            $criteria = new CDbCriteria;
            $criteria->compare('category_id', $categories);
            $criteria->compare('lang_id', LXController::getLangId());
            $criteria->compare('active', '1');
            $criteria->order = 'date DESC, time DESC';

            $dataProvider = new CActiveDataProvider('Pages', array(
                'criteria'   => $criteria,
                'pagination' => false
            ));

            $this->render('team', array('category' => $model, 'categories' => $children, 'dataProvider' => $dataProvider));
        }

        public function actionAllNews($sefname, $date = 0) {
            $this->layout = '//layouts/allNews';

            $lang=Yii::app()->language;
            switch ($lang) {
              case 'kz':
                $this->metaTags="Қазақстан, әлем, бизнес, спорт, мәдениет, HI-TECH, денсаулық, Кино, 24 сағат, іс-шара, өзекті, трейлер, жарнама, бағаның құлдырауы, жоғары баға, жедел, жаңа, әлем жаңалықтары, теңге, доллар, қымбаттау, қолжетімді тұрғын үй, Қазақстан қалалары, денсаулық сақтау, білім беру, жастар, Астана, Алматы, Еуразия, Азия, Еуропа, Америка, Жаңа өнімдер";
                $this->metaDescription="Kazakh-tv.kz – тәулік бойы жаңартылатын ақпараттар легі. Қазақстан, Ресей және әлемдегі бизнес экономика, оқиға, спорт туралы соңғы жаңалықтар";
                break;
              case 'ru':
                $this->metaTags="Казахстан, Мир, Бизнес, Спорт, Культура, HI-TECH, Здоровье, Кино, 24 часа, актуально, новое, новости мира, тенге, доллар, в курсе событии, события, происшествия, афиша, трейлер, реклама, падение цен, подорожание, доступное жилье, города Казахстана, медицина, образование, молодежь, Астана, Алматы, Евразия, Азия, Европа, Америка, Новинки";
                $this->metaDescription="Kazakh-tv.kz - круглосуточно обновляемая лента новостей. Последние новости Казахстана, России и мира о бизнесе, экономике, происшествиях, спорте";
                break;
                case 'en':
                $this->metaTags="Kazakhstan, World, Business, Sport, Culture, HI-TECH, Health, Cinema, 24 hours, urgent, new, world news, tenge, dollar, up to date, events, incident, poster, trailer, advertising, falling prices, higher prices , affordable housing, the city of Kazakhstan, medicine, education, youth, Astana, Almaty, Eurasia, Asia, Europe, America, New Products";
                $this->metaDescription="Kazakh-tv.kz - 24 hours updated news. The latest business, economics, sports and accedent news from Kazakhstan, Russia and the world";
                break;
              default:
                $this->metaTags="Қазақстан, әлем, бизнес, спорт, мәдениет, HI-TECH, денсаулық, Кино, 24 сағат, іс-шара, өзекті, трейлер, жарнама, бағаның құлдырауы, жоғары баға, жедел, жаңа, әлем жаңалықтары, теңге, доллар, қымбаттау, қолжетімді тұрғын үй, Қазақстан қалалары, денсаулық сақтау, білім беру, жастар, Астана, Алматы, Еуразия, Азия, Еуропа, Америка, Жаңа өнімдер";
                $this->metaDescription="Kazakh-tv.kz – тәулік бойы жаңартылатын ақпараттар легі. Қазақстан, Ресей және әлемдегі бизнес экономика, оқиға, спорт туралы соңғы жаңалықтар";
                break;
            }

            $category = Categories::model()->findBySefnameObj($sefname);
            if ($category === null) {
                throw new CHttpException(404, 'Страница не найдена');
            }

            if ($sefname=='news'||$sefname=='sobytia') {
                $categoryId = CHtml::listData($category->children()->findAll('active=1'), 'id', 'id');
            }else{
                $categoryId = $category->id;
            } 

            $this->newsCategory = $categoryId;

            $criteria = new CDbCriteria;
            $criteria->compare('category_id', $categoryId);
            if ($date != 0) {
                $date = date('Y-m-d', strtotime($date));
                $criteria->compare('date', $date);
            }

            $criteria->compare('is_archive', 0);
            $criteria->order = 'date DESC, time DESC';
            $criteria->compare('active', '1');


            $this->pageTitle = $category->title;
            
            if($sefname=='news')
            {
                switch ($lang) {
                  case 'kz':
                        $this->pageTitle="Жаңалықтар - ҚАЗАҚСТАН РЕСПУБЛИКАСЫНЫҢ АЛҒАШҚЫ ҰЛТТЫҚ СПУТНИКТІК АРНАСЫ Kazakh TV";
                    break;
                  case 'ru':
                        $this->pageTitle="Новости - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                    break;
                    case 'en':
                        $this->pageTitle="News - THE FIRST NATIONAL SATELLITE CHANNEL OF THE REPUBLIC OF KAZAKHSTAN Kazakh TV";
                    break;
                  default:
                        $this->pageTitle="Новости - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                    break;
                }
            }
            
            if($sefname=='column'||$sefname=='sobytia'||$sefname=='press_about_us'){
                $this->layout = '//layouts/listProgrammsNoSidebar';
            }

            $dataProvider = new CActiveDataProvider('Pages', array(
                'criteria'   => $criteria,
                'pagination' => array(
                    'pageSize' => 20
                )
            ));

           
            $this->render('allNews', array('dataProvider' => $dataProvider, 'category' => $category, 'sefname' => $sefname, ));
        }
        
        public function actionView($category, $sefname) {
            $this->layout = '//layouts/content';
            
            $arr = explode('_',$sefname); 
            $id = $arr[1];
            $model = Pages::model()->findById($id);
            
            if ($model === null) {
                $this->redirect(array('allNews', 'sefname' => $category));
                //throw new CHttpException(404, 'Страница не найдена');
            }

            if ($category == 'press_about_us') {
                $this->layout = '//layouts/press';
            } elseif ($category=='about_us_team') {
                $this->layout = '//layouts/team';
            }

            $model->views = $model->views + 1;
            $model->save();

            $pl = array();
            if (count($model->videos) > 1) {
                foreach ($model->videos as $video) {
                    if ($video->upload_ext == 'flv') {
                        $filename = $video->upload_file;
                    } else {
                        $filename = $video->filename . '_[240,320,480].mp4';
                    }
                    $pl['playlist'][] = array(
                        'file'      => '/upload/video/pages/' . $video->page_id . '/' . $filename,
                        'comment'   => $video->title,
                        'poster'    => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
                        'bigposter' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
                    );
                }
            } elseif (count($model->videos) == 1) {
                $video = $model->videos[0];
                if ($video->upload_ext == 'flv') {
                    $filename = $video->upload_file;
                } else {
                    $filename = $video->filename . '_[240,320,480].mp4';
                }
                $params = array(
                    'file'       => '/upload/video/pages/' . $video->page_id . '/' . $filename,
                    'comment'    => $video->title,
                    'poster'     => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
                    'big_poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
                );
            }


            if (empty($params)) {
                $params = array('pl' => $pl, 'file' => '');
            }

            $this->pageTitle = $model->title;
            $this->metaTags = $model->meta_tags;
            $this->metaDescription = $model->meta_description;

            if ($model->category->parent->sefname == 'news') {
                $this->layout = '//layouts/news';
                $this->newsCategory = $model->category_id;
            }

            $this->render('view', array('model' => $model, 'params' => $params));
        }

        public function actionNewsAjax($id) {
            if (Yii::app()->request->isAjaxRequest) {
                $pages = Pages::model()->findAllByAttributes(array('category_id' => (int)$id, 'active' => '1', 'is_archive' => 0), array('order' => 'date DESC, time DESC'));

                $this->renderPartial('_newsTabsToMain', array('pages' => $pages));

                Yii::app()->end();
            } else {
                $this->redirect('/');
            }
        }

        public function actionArchive($sefname = '', $date = 0) {
            
            $langId = Langs::getLangIdByCode(Yii::app()->language);
            if ($sefname == '') {
                $category = Categories::model()->findAll('lang_id=:lang_id AND active=1', array(':lang_id' => $langId));
                $categoryId = CHtml::listData($category, 'id', 'id');
            } else {
                $category = Categories::model()->findBySefname($sefname);
                $categoryId = $category['id'];
            }
            if ($category === null) {
                throw new CHttpException(404, 'Страница не найдена');
            }

            $criteria = new CDbCriteria;
            $criteria->compare('category_id', $categoryId);
            if ($date != 0) {
                $date = date('Y-m-d', strtotime($date));
                $criteria->compare('date', $date);
            }

            $criteria->compare('active', '1');
            $criteria->order = 'date DESC, time DESC';

            $this->pageTitle = (count($category) > 1) ? 'Архив' : $category['title'];

            $dataProvider = new CActiveDataProvider('Pages', array(
                'criteria'   => $criteria,
                'pagination' => array(
                    'pageSize' => 8
                )
            ));

            $this->render('allNewsArchive', array('dataProvider' => $dataProvider, 'category' => $category, 'sefname' => $sefname));
        }

        public function actionRss($category_id = 0) {
            $pages = Pages::model()->getPagesForRSS($category_id);

            Yii::import('ext.feed.*');

            $feed = new EFeed();
            $feed->title = '';
            $feed->link = 'http://www.kazakh-tv.kz';
            $feed->description = 'Первый национальный спутниковый канал Республики Казахстан ';
            foreach ($pages as $page) {
                $item = $feed->createNewItem();

                $item->title = $page->title;
                $item->link = $this->createAbsoluteUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname));
                $item->date = strtotime($page->date . ' ' . $page->time);
                $item->description = $page->short_text;
                $item->addTag('category', $page->category->title);
                $feed->addItem($item);
            }

            $feed->generateFeed();
        }


        public function actionSiteMap() {
            $this->layout = '//layouts/content';
            $this->render('sitemap');
        }

    }
