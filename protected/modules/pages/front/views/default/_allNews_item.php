<div class="news_item"
     data-href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname,))?>">
    <a href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname))?>">
        <?php
        if (($image = Pages::getAnounceImg($data->image, '')) !== false) {
            ?>
            <div class="img">
                <img src="<?=$image?>" alt="">
            </div>
        <?php
        }
        ?>
        <div class="news_anounce">
            
            <h3 class="title_bold"><?=$data->title ?></h3>
            <p>
                <?=$data->short_text ?>
            </p>
             <div class="date" style="float:left;"><?=date('d.m.Y', strtotime($data->date))?></div>
            <div class="date" style="float:right;"><?=ttt('Просмотров')?>: <?=$data->views?></div>
        </div>
        <?php if (!empty($data->videos)) { ?>
            <div class="play"><img src="/themes/front/images/play.png" alt=""></div>
        <?php } ?>

    </a>
</div>
