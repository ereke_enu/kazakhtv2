 
 <div class="section_title" <? if($category->title=="WorldWide") {?> style="width:975px" <? } ?>  >
        <?=$category->title?>
    </div>

<?php if (!empty($pages)) {
    ?>
    <div id="text">

        <h2 style="margin-top:20px;" ><?=$pages->title ?></h2>

            <span class="date">
                <?php if ($pages->show_pub_date == 1 || ($pages->show_pub_date == -1 && $pages->category->show_pub_date == 1)) { ?>
                    <?= date('d.m.Y H:i', strtotime($pages->date . ' ' . $pages->time)) ?>
                <?php } ?>
            </span>

        <?php
        if (!empty($pages->videos)) {

            ?>
            <div id="videoplayer_content">
                <?php $this->widget('ext.uppod.LXUppodWidget', array('width' => 688, 'params' => CMap::mergeArray(Yii::app()->params['videoPlayer'], $params)))?>

            </div>
        <? }?>

        <article>
            <?=$pages->text?>
        </article>
    </div>

<?php } ?>

 <script type="text/javascript">
    $(document).ready(function(){
        $('#bg_top').css('background','url("/upload/cosmos1.jpg") no-repeat');
        $('#bg_top').css('-moz-background-size','100%');
        $('#bg_top').css('-webkit-background-size','100%');
        $('#bg_top').css('-o-background-size','100%');
        $('#bg_top').css('background-size','100%');
    })
</script>