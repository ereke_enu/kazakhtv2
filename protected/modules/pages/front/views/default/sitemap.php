<div class="content second program">
    <div class="section_title">
        <?=Yii::t('pages', 'Карта сайта')?>
    </div>
    <div id="text">
        <article class="sitemap" style="margin-top:20px;">
            <?php $this->widget('MainMenuWidget', array('sectionId' => 1));?>
            <?php $this->widget('MainMenuWidget', array('sectionId' => 3));?>
        </article>
    </div>
</div>
