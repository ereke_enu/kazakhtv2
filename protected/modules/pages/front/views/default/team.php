 <div class="section_title">
        <?=$category->title?>
    </div>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_team_item',
    'summaryText' => '',
    'id' => 'left_news',
    'pager' => array(
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
        'header' => ''
    ),
    'emptyText' => 'В данной категории еще нет статей'
));
?>