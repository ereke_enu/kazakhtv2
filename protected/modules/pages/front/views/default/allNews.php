<? 
    $all_news_item="_allNews_item";
    if($category->sefname=="sobytia"||$category->sefname=='press_about_us') {$all_news_item="_allEvents_item";}
?>
<div class="section_title"><?=Yii::t('pages', $category->title);?></div>
<div id="div_<?=$category->sefname ?>" style="padding-left:0px;">
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => $all_news_item,
        'summaryText' => '',
        'id' => 'left_news',
        'pager' => array(
            'header' => '',
            'prevPageLabel' => '<img src="/themes/front/images/left_btn_partners.png">',
            'nextPageLabel' => '<img src="/themes/front/images/right_btn_partners.png">'
        ),
        'emptyText' => 'В данной категории еще нет новостей'
    ));
    ?>
</div>
