<div class="section_title"><?=Yii::t('pages', 'Архив новостей');?></div>
<?
$menu_id = Categories::model()->findBySefname('news');
$menu = Categories::model()->findByPk($menu_id['id']);

?>
<ul class="news_menu">
    <?
    foreach ($menu->children()->findAll() as $item) {
        if ($item->sefname == $sefname) $active = 'class="active"';
        else $active = '';
        echo '<li style="margin:1px 2px;" ' . $active . '>' . CHtml::link($item->title, Yii::app()->createUrl('pages/default/archive', array('sefname' => $item->sefname))) . '</li>';
    }
    ?>
</ul>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_allNews_item',
    'summaryText' => '',
    'id' => 'left_news',
    'pager' => array(
        'header' => '',
        'prevPageLabel' => '<img src="/themes/front/images/left_btn_partners.png">',
        'nextPageLabel' => '<img src="/themes/front/images/right_btn_partners.png">'
    ),
    'emptyText' => Yii::t('pages', 'В данной категории еще нет новостей')
));
?>

