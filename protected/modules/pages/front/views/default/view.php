
<script type="text/javascript">

    menu_category = '<?= $model->category->sefname ?>';
</script>
<div class="content second program">
    <div class="section_title">
        <?= $model->category->title ?>
    </div>
    <div id="text" style="margin-top:10px;">
        <h3><?= $model->title ?></h3>
            <span class="date">
                <?php if ($model->show_pub_date == 1 || ($model->show_pub_date == -1 && $model->category->show_pub_date == 1)) { ?>
                    <?= date('d.m.Y H:i', strtotime($model->date . ' ' . $model->time)) ?>
                <?php } ?>
            </span>


        

        <article>
            <?= $model->text ?>

        </article>
        
        <?php
            if (!empty($model->videos)) {

                ?>
                <div id="videoplayer_content">
                    <?php $this->widget('ext.uppod.LXUppodWidget', array('width' => 688, 'params' => CMap::mergeArray(Yii::app()->params['videoPlayer'], $params))) ?>

                </div>
            <? } ?>
        <div  style="width:100%;border-top:1px solid #00506E; margin-top:20px;">
            <div style="float:right;margin-top:5px;"><?= ttt('Просмотров')?>: <?php echo $model->views; ?></div>
            <div style="width:350px;">
                <table>
                    <tr>
                        <td><?=ttt('Поделиться новостью')?></td>
                        <td><?php $this->widget('shareWidget'); ?></td>
                    </tr>
                </table>
            </div>
        </div>
        
        
    </div>
</div>



