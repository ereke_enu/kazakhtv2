<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 05.12.12
 * Time: 15:08
 * To change this template use File | Settings | File Templates.
 */
class DefaultController extends LXController {

    public $layout = '//layouts/second';

    public function actionCategory($sefname) {
        $category = Categories::model()->findBySefnameObj($sefname);

        if ($category->sefname == '' || $category->parent_id == 11 || $category->sefname = 'press_about_us') {
            $this->layout = '//layouts/company';
        }

        if ($category === null) {
            throw new CHttpException(404, 'Категория не найдена');
        }

        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $category->id);
        $criteria->compare('active', '1');
        $criteria->order = 'date DESC, time DESC';

        $page = Pages::model()->find($criteria);
        
        $params = array();
        if($page) {
            $this->pageTitle = $category->title;
            $this->metaTags = $page->meta_tags;
            $this->metaDescription = $page->meta_description;
            $this->pageTitle = $page->title;


            $pl = array();
            if (count($page->videos) > 1) {
                foreach ($page->videos as $video) {
                    if ($video->upload_ext == 'flv') {
                        $filename = $video->upload_file;
                    } else {
                        $filename = $video->filename . '_[240,320,480].mp4';
                    }
                    $pl['playlist'][] = array(
                        'file' => '/upload/video/pages/' . $video->page_id . '/' . $filename,
                        'comment' => $video->title,
                        'poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
                        'bigposter' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
                    );
                }
            } elseif (count($page->videos) == 1) {
                $video = $page->videos[0];
                if ($video->upload_ext == 'flv') {
                    $filename = $video->upload_file;
                } else {
                    $filename = $video->filename . '_[240,320,480].mp4';
                }
                $params = array(
                    'file' => '/upload/video/pages/' . $video->page_id . '/' . $filename,
                    'comment' => $video->title,
                    'poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
                    'big_poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
                );
            }

            if (empty($params)) {
                $params = array('pl' => $pl, 'file' => '');
            }
        }
        
        if($category->parent)
            $categories = Categories::model()->findBySefnameObj($category->parent->sefname)->children()->findAll();
        else
            $categories = array();

        $this->render('category', array('category' => $category, 'pages' => $page, 'params' => $params, 'categories' => $categories));
    }

    public function actionTeam($sefname) {
        $this->layout = '//layouts/company';
        $model = Categories::model()->findBySefnameObj($sefname);

        if ($model === null) {
            throw new CHttpException(404, 'Странциа не найдена');
        }

        $children = $model->children()->findAll();

        if (!empty($children)) {
            $categories = CHtml::listData($children, 'id', 'id');
        } else {
            $categories = $model->id;
        }

        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $categories);
        $criteria->compare('lang_id', LXController::getLangId());
        $criteria->compare('active', '1');
        $criteria->order = 'date DESC, time DESC';
        
        $teamCount = Pages::model()->count($criteria);

        $teamPages = new CPagination($teamCount);
        $teamPages->applyLimit($criteria);

        $teamModels = Pages::model()->findAll($criteria);
/*
        $dataProvider = new CActiveDataProvider('Pages', array(
            'criteria' => $criteria,
            'pagination' => false
        ));
 *
 */
        
        $categories = Categories::model()->findBySefnameObj($model->parent->sefname)->children()->findAll();

        $this->render('team', array('category' => $model, 'categories' => $categories, 'teamModels' => $teamModels, 'teamPages' => $teamPages));
    }

    public function actionAllNews($sefname, $date = 0) {
        $this->layout = '//layouts/allNews';
        $category = Categories::model()->findBySefnameObj($sefname);
        if ($category === null) {
            throw new CHttpException(404, 'Страница не найдена');
        }

        if ($category->isRoot()) {
            $categoryId = CHtml::listData($category->children()->findAll('active=1'), 'id', 'id');
        } else {
            $categoryId = $category->id;
        }

        $this->newsCategory = $categoryId;

        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $categoryId);
        if ($date != 0) {
            $date = date('Y-m-d', strtotime($date));
            $criteria->compare('date', $date);
        }

        $criteria->compare('is_archive', 0);
        $criteria->order = 'date DESC, time DESC';
        $criteria->compare('active', '1');

        $newsCount = Pages::model()->count($criteria);

        $newsPages = new CPagination($newsCount);
       // $newsPages->pageSize = 12;
        $newsPages->pageSize = 8;
        $newsPages->applyLimit($criteria);

        $newsModels = Pages::model()->findAll($criteria);


        $this->pageTitle = $category->title;

        $categories = Categories::model()->findBySefnameObj('news')->children()->findAll();

        $this->render('allNews', array('newsModels' => $newsModels, 'newsPages' => $newsPages, 'category' => $category, 'categories' => $categories));
    }

    public function actionView($category, $sefname) {
        $this->layout = '//layouts/content';
        $model = Pages::model()->findBySefname($sefname);
        if ($model === null) {
            $this->redirect(array('allNews', 'sefname' => $category));
            //throw new CHttpException(404, 'Страница не найдена');
        }

        if ($category == 'press_about_us') {
            $this->layout = '//layouts/press';
        } elseif ($category == 'about_us_team') {
            $this->layout = '//layouts/team';
        }

        $model->views = $model->views + 1;
        $model->save();

        $pl = array();
        if (count($model->videos) > 1) {
            foreach ($model->videos as $video) {
                if ($video->upload_ext == 'flv') {
                    $filename = $video->upload_file;
                } else {
                    $filename = $video->filename . '_[240,320,480].mp4';
                }
                $pl['playlist'][] = array(
                    'file' => '/upload/video/pages/' . $video->page_id . '/' . $filename,
                    'comment' => $video->title,
                    'poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
                    'bigposter' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
                );
            }
        } elseif (count($model->videos) == 1) {
            $video = $model->videos[0];
            if ($video->upload_ext == 'flv') {
                $filename = $video->upload_file;
            } else {
                $filename = $video->filename . '_[240,320,480].mp4';
            }
            $params = array(
                'file' => '/upload/video/pages/' . $video->page_id . '/' . $filename,
                'comment' => $video->title,
                'poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
                'big_poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
            );
        }


        if (empty($params)) {
            $params = array('pl' => $pl, 'file' => '');
        }


        $this->pageTitle = $model->title;
        $this->metaTags = $model->meta_tags;
        $this->metaDescription = $model->meta_description;

        if ($model->category->parent->sefname == 'news') {
            $this->layout = '//layouts/news';
            $this->newsCategory = $model->category_id;
        }

        $this->render('view', array('model' => $model, 'params' => $params));
    }

    public function actionNewsAjax($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $pages = Pages::model()->findAllByAttributes(array('category_id' => (int) $id, 'active' => '1', 'is_archive' => 0), array('order' => 'date DESC, time DESC'));

            $this->renderPartial('_newsTabsToMain', array('pages' => $pages));

            Yii::app()->end();
        } else {
            $this->redirect('/');
        }
    }

    public function actionArchive($sefname = '', $date = 0) {
        $langId = Langs::getLangIdByCode(Yii::app()->language);
        if ($sefname == '') {
            $category = Categories::model()->findAll('lang_id=:lang_id AND active=1', array(':lang_id' => $langId));
            $categoryId = CHtml::listData($category, 'id', 'id');
        } else {
            $category = Categories::model()->findBySefname($sefname);
            $categoryId = $category['id'];
        }
        if ($category === null) {
            throw new CHttpException(404, 'Страница не найдена');
        }

        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $categoryId);
        if ($date != 0) {
            $date = date('Y-m-d', strtotime($date));
            $criteria->compare('date', $date);
        }

        $criteria->compare('is_archive', '1');
        $criteria->order = 'date DESC, time DESC';
        
        $newsCount = Pages::model()->count($criteria);
        
        $newsPages = new CPagination($newsCount);
        //$newsPages->pageSize = 12;
        $newsPages->pageSize = 8;
        $newsPages->applyLimit($criteria);

        $newsModels = Pages::model()->findAll($criteria);

        $this->pageTitle = (count($category) > 1) ? 'Архив' : $category['title'];

        $this->render('allNewsArchive', array('newsModels' => $newsModels, 'newsPages' => $newsPages, 'category' => $category, 'sefname' => $sefname));
    }

    public function actionRss($category_id = 0) {
        $pages = Pages::model()->getPagesForRSS($category_id);

        Yii::import('ext.feed.*');

        $feed = new EFeed();
        $feed->title = '';
        $feed->link = 'http://www.kazakh-tv.kz';
        $feed->description = 'Первый национальный спутниковый канал Республики Казахстан ';
        foreach ($pages as $page) {
            $item = $feed->createNewItem();

            $item->title = $page->title;
            $item->link = $this->createAbsoluteUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname));
            $item->date = strtotime($page->date . ' ' . $page->time);
            $item->description = $page->short_text;
            $item->addTag('category', $page->category->title);
            $feed->addItem($item);
        }

        $feed->generateFeed();
    }

    public function actionSiteMap() {
        $this->layout = '//layouts/content';

        $this->render('sitemap');
    }

}
