<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 21.02.13
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */

class mainNewsWidget extends CWidget {

    public function run() {
        $category = Categories::model()->findBySefnameObj('world_news');
        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $category->id);
        $criteria->compare('active', '1');
        $criteria->compare('is_archive', '0');
        $criteria->order = 'date DESC, time DESC';
        $criteria->limit = 4;
        $criteria->compare('is_main', '0');

        $pages = Pages::model()->findAll($criteria);

        $this->render('mainNewsWidget', array('pages' => $pages));
    }
}