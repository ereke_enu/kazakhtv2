<?php

class businessNewsWidget extends CWidget {

    public function run() {
        $category = Categories::model()->findBySefnameObj('business');

        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $category->id);
        $criteria->compare('active', '1');
        $criteria->compare('is_archive', '0');
        $criteria->order = 'date DESC, time DESC';
        $criteria->limit = 4;
        $criteria->compare('is_main', '0');


        $pages = Pages::model()->findAll($criteria);
        
        $this->render('businessNewsWidget', array('pages' => $pages));
    }
}