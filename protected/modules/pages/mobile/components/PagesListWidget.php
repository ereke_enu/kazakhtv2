<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 01.02.13
     * Time: 15:43
     * To change this template use File | Settings | File Templates.
     */

    class PagesListWidget extends CWidget {
        public $category;
        public $title = false;
        public $date = false;
        public $limit = 10;

        public function run() {
            $category = Categories::getChildCategories($this->category);
            if (empty($category)) {
                return false;
            }

            $curSefname = Yii::app()->request->getParam('sefname', false);

            if ($this->title === false) {
                $this->title = $category->title;
            }


            $criteria = new CDbCriteria;
            $criteria->compare('category_id', $category);
            if ($curSefname !== false) {
                $criteria->compare('sefname', '<>' . $curSefname);
            }
            $criteria->order = 'date DESC, time DESC';
            $criteria->compare('is_archive', '0');
            $criteria->compare('active', '1');
            if ($this->limit > 0) {
                $criteria->limit = $this->limit;
            }
            $pages = Pages::model()->findAll($criteria);

            $this->render('pagesListWidget', array('pages' => $pages, 'category' => $category, 'title' => $this->title, 'date' => $this->date));
        }

    }