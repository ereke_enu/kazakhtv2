<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 05.02.13
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */

class ArchivePagesListWidgetMob extends CWidget {
    public $categoryId = 'news';

    public function run() {
        $archiveCriteria = new CDbCriteria;
        $archiveCriteria->compare('is_archive', '1');
        $archiveCriteria->compare('category_id', $this->categoryId);
        $archiveCriteria->order = 'date DESC, time DESC';
        $archiveCriteria->limit = 7;
        
        $archiveNews = Pages::model()->findAll($archiveCriteria);
        $this->render('archivePagesListWidget', array('archiveNews' => $archiveNews));
    }

}