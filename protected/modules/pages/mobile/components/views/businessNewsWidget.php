<?php 
    $i = 1;
    foreach($pages as $page) {
        $url = Yii::app()->createUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname ));
?>
<?php if($i == 1) {
            $image = Pages::getAnounceImg($page->image, 'top_');
            $date = new DateTime($page->date.' '.$page->time, new DateTimeZone('Asia/Almaty'));
            $comments = Comments::model()->count('cid=:cid', array(':cid'=>$page->id));
?>
                <!--<style>
				@media (max-width: 480px){
				#lineNews{
				display:table;
				}
				
				#lineNewsA{
				width:50%;padding-right: 10px;
				}
				
				#line{
				width:50%;
				}
				}
				
				</style>-->
				
				<article class="clearfix" id="lineNews">
				 
				<table>
					<tr>
						<td class="imgtable">
						
						<a href="<?= $url ?>" id="lineNewsA"><img style="width:100%;" src="<?= $image ?>" class="img-responsive col-sm-6 no-padding" /></a>
                   <!-- <div id="line" >-->
						
						</td>
						
						<td class="texttable">
						
						<h3><a href="<?= $url ?>"><?= $page->title ?></a></h3>
						<!--<p><a href="<?//= $url ?>" class="arrow"><?= $page->short_text ?></a></p>-->
						<div class="sign">
                        <span class="time"><?= $date->format('G:i') ?></span>
                        <span class="date"><?= $date->format('j/m/Y') ?></span>
                        <span class="comments"><i class="glyphicon glyphicon-comment"></i> <?= $comments ?></span>
                        <span class="views"><i class="glyphicon glyphicon-user"></i> <?= $page->views ?></span>
						</div>
						
						</td>
						<!--</div>-->
					</tr>
				</table>
                </article>
                <hr />
<?php } else { ?>
                <article>
                    <h3 class="small bul"><a href="<?= $url ?>" class="arrow"><?= $page->title ?></a></h3>
                </article>
                <hr />
<?php
        }
        $i++;
    }
?>