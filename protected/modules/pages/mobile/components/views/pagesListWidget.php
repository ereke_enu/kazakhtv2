﻿<div class="row margin-bottom">
    <div class="col-sm-12">
        <hr class="bold" />
        <h2><?= $title ?></h2>
<?php
foreach($pages as $page) {
    $url = Yii::app()->createUrl('pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname));
?>
        <article>
            <h3 class="small bul"><a href="<?= $url ?>" class="arrow"><?= $page->title ?></a></h3>
        </article>
        <hr />
    
<?php } ?>
    </div>
</div>