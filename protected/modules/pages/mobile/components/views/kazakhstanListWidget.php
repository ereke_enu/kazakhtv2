                <ul>
<?php
$i = 1;
foreach($categories as $category) {
    $url = Yii::app()->createUrl('/pages/default/category', array('sefname' => $category->sefname));
?>
                    <li<?= ($i++ > $show) ? ' class="collapse"' : '' ?>>
                        <h3 class="grey bul"><a href="<?= $url ?>"><?= $category->title ?></a></h3>
                        <hr />
                    </li>
<?php } ?>
                </ul>