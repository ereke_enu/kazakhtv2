<div id="myCarousel" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>
<div class="carousel-inner" role="listbox">
<?php 
$i=0;
    foreach($pages as $page){
    $image = Pages::getAnounceImg($page->image, 'top_');
    $url = Yii::app()->createUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname ));
       
    $date = new DateTime($page->date.' '.$page->time, new DateTimeZone('Asia/Almaty'));
	    $comments = Comments::model()->count('cid=:cid', array(':cid'=>$page->id));
        if($i==0){$active_class="active";}else{ $active_class="none";}
        $i++;
?>
<div style="height:300px;" class="item <?=$active_class?>">
                <article>
                    <a href="<?= $url ?>"><img src="<?= $image ?>" class="img-responsive" /></a>
                    <h3><a href="<?= $url ?>"><?= $page->title ?></a></h3>
                    <p><a href="<?= $url ?>" class="arrow"><?= $page->short_text ?></a></p>
                    <div class="sign">
                        <span class="time"><?= $date->format('G:i') ?></span>
                        <span class="date"><?= $date->format('j/m/Y') ?></span>
                        <span class="comments"><i class="glyphicon glyphicon-comment"></i> <?= $comments ?></span>
                        <span class="views"><i class="glyphicon glyphicon-user"></i> <?= $page->views ?></span>
                    </div>
                </article>
</div>
<? } ?>
</div>
<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" style="background: none !important;">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" style="background: none !important;">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

</div>
