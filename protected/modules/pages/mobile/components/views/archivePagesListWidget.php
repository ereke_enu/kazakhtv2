﻿<div class="row margin-bottom">
    <div class="col-sm-12">
        <hr class="bold" />
        <h2><?=Yii::t('category', 'Архив');?></h2>
<?php
foreach($archiveNews as $item) {
    $url = Yii::app()->createUrl('/pages/default/view', array('category' => $item->category->sefname, 'sefname' => $item->sefname));
?>
        <article>
            <h3 class="small bul"><a href="<?= $url ?>" class="arrow"><?= $item->title ?></a></h3>
        </article>
        <hr />
    
<?php } ?>
    </div>
</div>