<?php

class sefnameNewsWidget extends CWidget {
    public $sefname;
    public function run() {
        //echo "sefname=".$this->sefname;
        $category = Categories::model()->findBySefnameObj($this->sefname);

        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $category->id);
        $criteria->compare('active', '1');
        $criteria->compare('is_archive', '0');
        $criteria->order = 'date DESC, time DESC';
        $criteria->limit = 4;
        $criteria->compare('is_main', '0');

        $pages = Pages::model()->findAll($criteria);

        $this->render('sefnameNewsWidget', array('pages' => $pages));
    }
}