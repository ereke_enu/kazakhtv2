<?php

class kazakhstanVideoWidget extends CWidget {
    
    public function run() {
        $category = Categories::model()->findBySefname('kazakhstan_overview');
        
        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $category['id']);
        $page = Pages::model()->with('videos')->find($criteria);
        
        $video = $page->videos[0];
                
        if ($video->upload_ext == 'flv') {
            $filename = $video->upload_file;
        } else {
            $filename = $video->filename . '_[240,320,480].mp4';
        }
        $params = array(
            'file'       => '/upload/video/pages/' . $video->page_id . '/' . $filename,
            'comment'    => $video->title,
            'poster'     => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
            'big_poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
        );
        if (empty($params)) {
            $params = array('pl' => $pl, 'file' => '');
        }
        
        $this->render('kazakhstanVideoWidget', array('page' => $page, 'params' => $params));
    }
    
}
