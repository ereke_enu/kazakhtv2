<?php

class kazakhstanListWidgetMob extends CWidget {
    
    public function run() {
        $categories = Categories::model()->getChildCategories('kazakhstan');
        
        $criteria = new CDbCriteria;
        $criteria->addInCondition('id', $categories);
        $criteria->compare('active', 1);

        $categories = Categories::model()->findAll($criteria);
        
        //print_r($categories);
        
        $this->render('kazakhstanListWidget', array('categories' => $categories, 'show' => 14));
    }
    
}
