<li class="<?=($index % 2) ? 'even' : 'odd'?>">
    <span class="date"><?=date('d.m.Y H:i', strtotime($data->date . ' ' . $data->time))?></span>
    <a href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname))?>">
        <?=$data->title?>
    </a>
</li>