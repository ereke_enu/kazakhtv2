<div class="col-sm-12">
    <hr class="bold" />
    <h1><?=Yii::t('pages', 'Карта сайта')?></h1>
    <hr class="margin-bottom" />
    
    <div id="text">
        <article class="sitemap">
            <?php $this->widget('MainMenuWidget', array('sectionId' => 1));?>
            <?php $this->widget('MainMenuWidget', array('sectionId' => 3));?>
        </article>
    </div>
</div>
