<? if (empty($pages)) { ?>

    <span class="empty"><?=Yii::t('pages', 'В данной категории еще нет новостей');?></span>
<? } else { ?>
    <?php
    $count = ceil(count($pages) / 8);
    $i = 0;
    ?>
    <div class="news_tab_items_content">
        <div class="slider">
            <ul class="slide">
                <?php
                $numPages = count($pages);
                foreach ($pages as $key => $page) {
                if ($page->image == '') {
                    $image = '/themes/front/images/noimage.jpg';
                } else {
                    $image = '/upload/anounces/' . $page->image;
                }
                ?>
                <li>
                    <a href="/<?=Yii::app()->language?>/view/<?=$page->category->sefname?>/<?=$page->sefname?>"><img
                            src="<?=$image?>">
                        <span class="items_title"><?= $page->title?></span>
                        <?php if (!empty($page->videos)) { ?>
                            <div class="play"><img src="/themes/front/images/play.png" alt=""></div>
                        <?php } ?>
                    </a>
                </li>
                <?php
                if ((($key + 1) % 8) == 0 || $key == $numPages - 1) {
                ?>
            </ul>
            <?php } ?>

            <?php if((($key + 1) % 8) == 0 && $key < $numPages) {?>
            <ul class="slide">
                <?php
                }
                }?>
        </div>
    </div>

    <div class="paginator">
        <span class="prev" href="#"></span>
        <span class="next" href="#"></span>

        <div><?=Yii::t('pages', 'Страниц');?> <span
                class="cur_page">1</span> <?=Yii::t('pages', 'из')?> <?=ceil($numPages / 8)?></div>
    </div>
<? } ?>
