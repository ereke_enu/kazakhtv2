﻿<div class="news_item team"
     data-href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname))?>">
    <a href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname))?>">
        <?php
        if (($image = Pages::getAnounceImg($data->image, '')) !== false) {
            ?>
            <div class="img">
                <img src="<?=$image?>" height="150" alt="">
            </div>
        <?php
        }
        ?>
        <div class="news_anounce">
            <h3><?=$data->title?></h3>

            <div class="pos"><?=$data->short_text?></div>
            <a class="more"
               href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname))?>">
                &mdash;&mdash;&mdash;&nbsp;&nbsp;  <?=Yii::t('category', 'Подробнее');?> &nbsp;&nbsp;&mdash;&mdash;&mdash;
            </a>
        </div>
    </a>
</div>
