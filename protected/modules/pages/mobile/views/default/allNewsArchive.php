<div class="col-sm-12">
    <hr class="bold" />
    <h1><?=Yii::t('pages', 'Архив новостей');?></h1>
    <hr class="" />
    
    <?php
    $menu_id = Categories::model()->findBySefname('news');
    $menu = Categories::model()->findByPk($menu_id['id']);
    ?>
    <ul class="news_menu">
        <?php
        foreach ($menu->children()->findAll() as $item) { ?>
            <li <?= ($item->sefname == $sefname) ? 'class="active"' : '' ?>><?= CHtml::link($item->title, Yii::app()->createUrl('pages/default/archive', array('sefname' => $item->sefname))) ?></li>
        <?php } ?>
    </ul>
    <hr class="margin-bottom" />
</div>

<?php $i = 1; foreach($newsModels as $data) { ?>

<?php
    $url = Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname));
    $date = new DateTime($data->date.' '.$data->time, new DateTimeZone('Asia/Almaty'));
    $comments = Comments::model()->count('cid=:cid', array(':cid'=>$data->id));
?>
<article class="col-xs-6 col-sm-3 element" data-href="<?= $url ?>">
    <?php if (($image = Pages::getAnounceImg($data->image, '')) !== false) { ?>
    <a href="<?= $url ?>"><img src="<?= $image ?>" class="img-responsive img-thumbnail" alt="<?= $data->title ?>" /></a>
    <?php } ?>
    <a href="<?= $url ?>"><h3><?= $data->title ?></h3></a>
    
    <div class="sign">
        <span class="time"><?= $date->format('G:i') ?></span>
        <span class="date"><?= $date->format('j/m/Y') ?></span><br />
        <span class="comments"><i class="glyphicon glyphicon-comment"></i> <?= $comments ?></span>
        <span class="views"><i class="glyphicon glyphicon-user"></i> <?= $data->views ?></span>
    </div>
    
    <?php if (!empty($data->videos)) { ?>
    <div class="play"><img src="/themes/mobile/img/play.png" alt=""></div>
    <?php } ?>
</article>
<?php if($i%4 == 0) {?>
<div class="clearfix hidden-xs element"></div>
<?php } ?>
<?php if($i%2 == 0) {?>
<div class="clearfix visible-xs element"></div>
<?php } ?>

<?php $i++; } ?>

<div class="clearfix element"></div>

<?php $this->widget('CLinkPager', array(
    'pages' => $newsPages,
    'header' => false,
    'nextPageLabel' => '&raquo;',
    'prevPageLabel' => '&laquo;',
    'maxButtonCount' => 0,
    'htmlOptions' => array(
        'id' => 'iasNews',
        'class' => 'pager'
    )
))?>

<?php
/*
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_allNews_item',
    'summaryText' => '',
    'id' => 'left_news',
    'pager' => array(
        'header' => '',
        'prevPageLabel' => '<img src="/themes/front/images/left_btn_partners.png">',
        'nextPageLabel' => '<img src="/themes/front/images/right_btn_partners.png">'
    ),
    'emptyText' => Yii::t('pages', 'В данной категории еще нет новостей')
));
 * 
 */
?>

