<?php
    $url = Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname));
    $date = new DateTime($data->date.' '.$data->time, new DateTimeZone('Asia/Almaty'));
?>
<article class="col-xs-6 col-sm-3" data-href="<?= $url ?>">
    <?php if (($image = Pages::getAnounceImg($data->image, '')) !== false) { ?>
    <a href="<?= $url ?>"><img src="<?= $image ?>" class="img-responsive" /></a>
    <?php } ?>
    <a href="<?= $url ?>"><h3><?= $data->title ?></h3></a>
    
    <div class="sign">
        <span class="time"><?= $date->format('G:i') ?></span>
        <span class="date"><?= $date->format('j/m/Y') ?></span>
        <span class="comments"><i class="glyphicon glyphicon-comment"></i> 12</span>
        <span class="views"><i class="glyphicon glyphicon-user"></i> 137</span>
    </div>
    
    <?php if (!empty($data->videos)) { ?>
    <div class="play"><img src="/themes/mobile/img/play.png" alt=""></div>
    <?php } ?>
</article>
