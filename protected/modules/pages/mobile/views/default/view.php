
<div class="col-sm-12">
    <hr class="bold" />
    <!--
    <h1><?= $model->category->title ?></h1>
    -->
    <h1><?= $model->title ?></h1>
    <!--<hr />-->
    
    <?php
    if ($model->category->isRoot()) {
        $this->breadcrumbs = array(
            $model->category->title => Yii::app()->createUrl('/pages/default/category', array('sefname' => $model->category->sefname)),
            $model->title
        );
    } else if($model->category->parent->sefname == 'news') {
        $this->breadcrumbs = array(
            $model->category->parent->title => Yii::app()->createUrl('/pages/default/category', array('sefname' => $model->category->parent->sefname)),
            $model->category->title => Yii::app()->createUrl('/pages/default/allNews', array('sefname' => $model->category->sefname)),
            $model->title
        );
    } else {
        $this->breadcrumbs = array(
            $model->category->parent->title => Yii::app()->createUrl('/pages/default/category', array('sefname' => $model->category->parent->sefname)),
            $model->category->title => Yii::app()->createUrl('/pages/default/category', array('sefname' => $model->category->sefname)),
            $model->title
        );
    }
    $this->widget('zii.widgets.CBreadcrumbs', array(
        'links'       => $this->breadcrumbs,
        'separator'   => ' / ',
        'htmlOptions' => array(
            'class' => 'breadcrumbs'
        )
    )); ?>
    <hr class="margin-bottom" />
    
    <span class="date">
        <?php if ($model->show_pub_date == 1 || ($model->show_pub_date == -1 && $model->category->show_pub_date == 1)) { ?>
            <?= date('G:i j/m/Y', strtotime($model->date . ' ' . $model->time)) ?>
        <?php } ?>
    </span>
    
    <?php if (!empty($model->videos)) { ?>
        <div id="videoplayer_content">
            <?php $this->widget('ext.uppod.LXUppodWidget', array('width' => '100%', 'height' => '100%', 'params' => CMap::mergeArray(Yii::app()->params['videoPlayer'], $params))) ?>
        </div>
    <?php } ?>
    
    <article class="article-text">
        <?= $model->text ?>
    </article>
    <hr />
    <div id="text">
        <?php $this->widget('shareWidget'); ?>
    </div>
    
    <?php //Pages::model()->getSimilar($model['id']); ?>
    
    <?php if ($model->show_comments == 1 || ($model->show_comments == -1 && $model->category->show_comments == 1)) { ?>

    <div class='comments'>
        <?php $this->widget('BlockComments', array(
                'type' => $this->getModule()->id,
                'id'   => $model->id)
        ); ?>
    </div>
    <?php } ?>
    
</div>
