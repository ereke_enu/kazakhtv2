<div class="col-sm-12">
    <hr class="bold" />
    <h1><?= $category->title ?></h1>
    <hr />
    
    <ul class="news_menu">
        <?php foreach ($categories as $item) { ?>
            <li <?= ($item->id == $category->id) ? 'class="active"' : '' ?>><?=CHtml::link($item->title, Yii::app()->createUrl('/pages/default/category', array('sefname' => $item->sefname)))?></li>
        <?php } ?>
    </ul>
    
    <?php /*
    if ($category->isRoot()) {
        $this->breadcrumbs = array(
            $category->title //=> Yii::app()->createUrl('/pages/default/category', array('sefname' => $category->sefname)),
            //$model->title
        );
    } else if($category->parent->sefname == 'news') {
        $this->breadcrumbs = array(
            $category->parent->title => Yii::app()->createUrl('/pages/default/category', array('sefname' => $category->parent->sefname)),
            $category->title // => Yii::app()->createUrl('/pages/default/allNews', array('sefname' => $category->sefname)),
            //$model->title
        );
    } else {
//        $parent = $category->parent;

        $this->breadcrumbs = array(
            $category->parent->title => $category->url,
            $category->title //=> Yii::app()->createUrl('/pages/default/category', array('sefname' => $category->sefname)),
            //$model->title
        );
    }
    $this->widget('zii.widgets.CBreadcrumbs', array(
        'links'       => $this->breadcrumbs,
        'separator'   => ' / ',
        'htmlOptions' => array(
            'class' => 'breadcrumbs'
        )
    ));  */?>
    
    <hr class="margin-bottom" />
    
    <?php if (!empty($pages)) { ?>

    <?php if(trim($pages->title) != '') { ?>
    <h2><?= $pages->title ?></h2>
    <?php } ?>

    <?php if ($pages->show_pub_date == 1 || ($pages->show_pub_date == -1 && $pages->category->show_pub_date == 1)) { ?>
    <span class="date">
        <?= date('d.m.Y H:i', strtotime($pages->date . ' ' . $pages->time)) ?>
    </span>
    <?php } ?>
            

    <?php if (!empty($pages->videos)) { ?>
    <div id="videoplayer_content">
        <?php $this->widget('ext.uppod.LXUppodWidget', array('width' => 688, 'params' => CMap::mergeArray(Yii::app()->params['videoPlayer'], $params)))?>
    </div>
    <?php } ?>

    <article class="article-text">
        <?=$pages->text?>
    </article>
<?php } ?>
</div>