<div class="col-sm-12">
    <hr class="bold" />
    <h1><?=$category->title?></h1>
    <hr />
    
    <ul class="news_menu">
        <?php foreach ($categories as $item) { ?>
            <li <?= ($item->id == $category->id) ? 'class="active"' : '' ?>><?=CHtml::link($item->title, Yii::app()->createUrl('/pages/default/category', array('sefname' => $item->sefname)))?></li>
        <?php } ?>
    </ul>
    <hr class="margin-bottom" />
</div>

<?php $i = 1; foreach($teamModels as $data) { ?>

<?php
    $url = Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname));
    //$date = new DateTime($data->date.' '.$data->time, new DateTimeZone('Asia/Almaty'));
?>
<div class="news_item team col-xs-6 col-sm-3 margin-bottom text-center" data-href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $data->category->sefname, 'sefname' => $data->sefname))?>">
    <a href="<?= $url ?>">
    <?php if (($image = Pages::getAnounceImg($data->image, '')) !== false) { ?>
        <div class="img" style="background-image: url(<?=$image?>); background-size: contain; background-position: center; background-repeat: no-repeat; height:150px;"></div>
    <?php } ?>
    <h3><?=$data->title?></h3>

    <p><?=$data->short_text?></p>
    </a>
</div>
<?php if($i%4 == 0) {?>
<div class="clearfix hidden-xs element"></div>
<?php } ?>
<?php if($i%2 == 0) {?>
<div class="clearfix visible-xs element"></div>
<?php } ?>

<?php $i++; } ?>

<div class="clearfix element"></div>

<?php
/*
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_team_item',
    'summaryText' => '',
    'id' => 'left_news',
    'pager' => array(
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
        'header' => ''
    ),
    'emptyText' => 'В данной категории еще нет статей'
));
 * 
 */
?>