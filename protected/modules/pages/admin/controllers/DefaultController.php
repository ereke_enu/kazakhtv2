<?php

class DefaultController extends LXAController {

    public $defaultAction = 'admin';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Pages;

        if (isset($_POST['Pages'])) {
            $model->attributes = $_POST['Pages'];
            $model->image = CUploadedFile::getInstance($model, 'image');

            if ($model->image !== null) {
                $filename = md5($model->image->name . time()) . '.' . $model->image->extensionName;
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/';

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                if ($model->image->saveAs($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/big_' . $filename)) {

                    Yii::import('application.extensions.image.Image');
                    $image = new Image($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/big_' . $filename);
                    $image->resize(540, 250, Image::WIDTH);
                    $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/top_' . $filename);
                    $image->resize(275, 200, Image::WIDTH);
                    $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/' . $filename);
                    $image->resize(104, 100, Image::WIDTH);
                    $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/104_' . $filename);

                } else {
                    $filename = '';
                }
            }

            $langIds = array();
            foreach ($_POST['Pages']['langs'] as $lang => $data) {
                $model = new Pages;
                $model->attributes = $data;
                $model->image = $filename;

                $model->background = CUploadedFile::getInstance($model, '[langs][' . $lang . ']background');

                if ($model->background !== null) {
                    $filename = md5($model->background->name . time()) . '.' . $model->background->extensionName;
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/branding/pages/';

                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }

                    if ($model->background->saveAs($path . $filename)) {
                        $model->background = $filename;
                    }
                }
                $model->user_id = Yii::app()->user->id;
                $model->lang_id = Langs::getLangIdByCode($lang);

                $enableValidate = true;
                if (trim($model->title) === '') {
                    $enableValidate = false;
                    $model->active = 0;
                }

                if ($model->save($enableValidate)) {
                    $langIds[$lang] = $model->id;

                    if ($enableValidate) {
                        $model->sefname = 'page_' . $model->id . '_' . UrlTransliterate::cleanString(substr($model->title, 0, 100));
                        $model->save();
                    }
                }
            }

            if (!empty($langIds)) {
                $criteria = new CDbCriteria;
                $criteria->addInCondition('id', $langIds);
                $langIds = CJSON::encode($langIds);
                Pages::model()->updateAll(array('other_id' => $langIds), $criteria);
            }
            //echo "Запись успешно добавлена!";
            $this->redirect(array('admin?archive=0'));
        } else {
            $model->date = date('Y-m-d', time());
            $model->time = date('H:i:s', time());
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $oldImage = $model->image;

        $langIds = CJSON::decode($model->other_id);
        $langIds = $this->clearArrayEmpty($langIds);
        $langModels = Pages::model()->with('lang')->findAllByPk($langIds);

        $models = array();
        foreach ($langModels as $item) {
            $models[$item->lang->code] = $item;
        }


        $filename = '';
        $langIds = array();
        if (isset($_POST['Pages'])) {
            $model->attributes = $_POST['Pages'];
            $model->image = CUploadedFile::getInstance($model, 'image');
            if ($model->image !== null) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/';

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $filename = md5($model->image->name . time()) . '.' . $model->image->extensionName;
                if (!$model->image->saveAs($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/big_' . $filename)) {
                    $filename = $oldImage;
                } else {
                    Yii::import('application.extensions.image.Image');
                    $image = new Image($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/big_' . $filename);
                    $image->resize(540, 250, Image::WIDTH);
                    $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/top_' . $filename);
                    $image->resize(275, 200, Image::WIDTH);
                    $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/' . $filename);
                    $image->resize(104, 100, Image::WIDTH);
                    $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/104_' . $filename);
                }
            } else {
                $filename = $oldImage;
            }
            foreach ($_POST['Pages']['langs'] as $lang => $data) {
                if (!empty($models[$lang])) {
                    $model = $models[$lang];
                    $oldBackground = $model->background;
                } else {
                    $model = new Pages();
                }

                $model->attributes = $data;

                $model->background = CUploadedFile::getInstance($model, '[langs][' . $lang . ']background');

                if ($model->background !== null) {
                    $filename = md5($model->background->name . time()) . '.' . $model->background->extensionName;
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/branding/pages/';

                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }

                    if ($model->background->saveAs($path . $filename)) {
                        $model->background = $filename;
                    } else {
                        $model->background = $oldBackground;
                    }
                }


                $model->image = $filename;
                $model->lang_id = Langs::getLangIdByCode($lang);

                if ($model->save()) {
                    $langIds[$lang] = $model->id;

                    if ($model->sefname == '') {
                        $model->sefname = 'page_' . $model->id . '_' . UrlTransliterate::cleanString(substr($model->title, 0, 100));
                        $model->save(false);
                    }
                }
            }

            if (!empty($langIds)) {
                $criteria = new CDbCriteria;
                $criteria->addInCondition('id', $langIds);
                $langIds = CJSON::encode($langIds);
                Pages::model()->updateAll(array('other_id' => $langIds), $criteria);
            }

            $this->redirect(array('admin?archive=0'));
        } else {
            foreach ($models as &$item) {
                $item->tags = implode(', ', CHtml::listData($item->listTags, 'tag', 'tag'));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'models' => $models
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin?archive=0'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($lang = 'ru') {
        $model = new Pages('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Pages']))
            $model->attributes = $_GET['Pages'];

        $model->lang_id = Langs::getLangIdByCode($lang);
        $model->is_archive = (int)$_GET['archive'];

        $this->render('admin', array(
            'model' => $model,
            'lang' => $lang
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Pages::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pages-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    private function clearArrayEmpty($array) {
        $ret_arr = array();
        foreach ($array as $val) {
            if (!empty($val)) {
                $ret_arr[] = trim($val);
            }
        }
        return $ret_arr;
    }

    public function actions() {
        return array(
            'fileManager' => array(
                'class' => 'application.extensions.elFinder.ElFinderAction',
            ),
        );
    }

    public function actionBrowse() {
        // специально отдельный пустой лейаут
        $this->layout = '//layouts/clear';
        $this->render('browser');
    }

    /**
     * Загрузка видео к статье
     * @param $id
     */
    public function actionUpload($id) {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, 'Bad request');
        }

        $page = Pages::model()->findByPk((int)$id);

        Yii::import("ext.eAjaxUpload.qqFileUploader");

        $allowedExtensions = array('avi', 'mpg', 'mp4', 'flv');
        $sizeLimit = 1000 * 1024 * 1024;

        $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/pages/' . $id . '/';
        if (!is_writable($path)) {
            mkdir($path, 0777, true);
        }
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($path);
        // to pass data through iframe you will need to encode all html tags
        $results = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        if (is_array($result) && empty($result['error'])) {
            $filename = 'video_' . $page->id . '_' . date('dmYHis', time());
            //Создание эскиза
            //640*480
            exec('/usr/bin/ffmpeg  -i "' . $path . $result['filename'] . '" -an -ss 00:00:' . rand(9, 12) . ' -an -r 1 -vframes 1 -s 640x480 -y -f mjpeg ' . $path . '/' . $filename . '.jpg');

            if ($result['ext'] != 'flv') {
                // конвертация видео
                //240
                exec("/usr/local/sbin/mpp4_240 " . $path . $result['filename'] . " " . $path . $filename . "_240.mp4");
                //320
                exec("/usr/local/sbin/mpp4_320 " . $path . $result['filename'] . " " . $path . $filename . "_320.mp4");
                //480
                exec("/usr/local/sbin/mpp4_480 " . $path . $result['filename'] . " " . $path . $filename . "_480.mp4");
            }

            $model = new Video;
            $model->module = 'pages';
            $model->page_id = $id;
            $model->filename = $filename;
            $model->video_lang_id = $page->lang_id;
            $model->title = $page->title;
            $model->upload_file = $result['filename'];
            $model->upload_ext = $result['ext'];
            if ($model->save()) {
                echo $results;
            } else {
                echo htmlspecialchars(json_encode(array('error' => 'Не удалось сохранить файл. DBERROR'.print_r($model->getErrors(), true))), ENT_NOQUOTES);
            }
        } else {
            echo $results;
        }
    }

    public function actionDeleteVideo($id) {
        $model = Video::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена');
        }

        $page_id = $model->page_id;
        $filename = $model->filename;
        if ($model->delete()) {
            @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/pages/' . $page_id . '/' . $filename . '_240.mp4');
            @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/pages/' . $page_id . '/' . $filename . '_320.mp4');
            @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/pages/' . $page_id . '/' . $filename . '_480.mp4');
            @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/pages/' . $page_id . '/' . $filename . '.jpg');
        }

        $this->redirect(array('/pages/default/update', 'id' => $page_id));

    }
}
