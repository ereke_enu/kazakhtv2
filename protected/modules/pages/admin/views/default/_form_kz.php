<?php echo $form->dropDownListRow($model, '[langs][kz]category_id', Categories::getTreeListItems(false, 'kz'), array('class' => 'span8')); ?>

<?php echo $form->textFieldRow($model, '[langs][kz]title', array('class' => 'span8')); ?>


<?php echo $form->textAreaRow($model, '[langs][kz]short_text', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->ckEditorRow($model, '[langs][kz]text', array('options' => CMap::mergeArray(array('fullpage' => 'js:true', 'width' => '640', 'resize_maxWidth' => '640', 'resize_minWidth' => '320'), Yii::app()->params['editorOptions']))); ?>

<?php echo $form->textFieldRow($model, '[langs][kz]date', array('class' => 'span8 datepicker')); ?>

<?php echo $form->textFieldRow($model, '[langs][kz]time', array('class' => 'span8')); ?>
<?php echo $form->textFieldRow($model, '[langs][kz]meta_tags', array('class' => 'span8')); ?>
<?php echo $form->textFieldRow($model, '[langs][kz]meta_description', array('class' => 'span8')); ?>
<?php echo $form->textFieldRow($model, '[langs][kz]tags', array('class' => 'span8')); ?>
<?php echo $form->checkBoxRow($model, '[langs][kz]is_main'); ?>
<?php echo $form->checkBoxRow($model, '[langs][kz]active'); ?>
<?php echo $form->checkBoxRow($model, '[langs][kz]is_archive'); ?>

<hr>
<h4>Настройки страницы</h4>
<p>Настройки категории не будут влиять на эту страницу, если они отличаются</p>
<?php if ($model->isNewRecord !== true && $model->background != '') { ?>
    <div class="controls">
        <div class="image">
            <img src="/upload/branding/<?=$model->background?>" alt="">
        </div>
    </div>
    <br>
    <?php echo $form->checkBoxRow($model, '[langs][kz]delete_background'); ?>
<?php } ?>
<div class="control-group">
    <label class="control-label" for="bgCoverkz">Бренд фон</label>

    <div class="controls">
        <div class="input-append">
            <input id="bgCoverkz" class="input-large" readonly="readonly" type="text">
            <a class="btn" onclick="$('input[id=Pages_Background_kz]').click();">Обзор</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('input[id=Pages_Background_kz]').change(function () {
            $('#bgCoverkz').val($(this).val());
        });
    });
</script>
<input type="file" id="Pages_Background_kz" name="Pages[langs][kz][background]" style="display:none;" maxlength="255"
       class="span8">

<?php echo $form->dropDownListRow($model, '[langs][kz]show_pub_date', array(-1 => 'Как у категории', 1 => 'Да', 0 => 'Нет'), array('class' => 'span8')) ?>
<?php echo $form->dropDownListRow($model, '[langs][kz]show_icons', array(-1 => 'Как у категории', 1 => 'Да', 0 => 'Нет'), array('class' => 'span8')) ?>
<?php echo $form->dropDownListRow($model, '[langs][kz]show_comments', array(-1 => 'Как у категории', 1 => 'Да', 0 => 'Нет'), array('class' => 'span8')) ?>
    <hr>
    <h4>Управление видео</h4>
<?php if (!empty($model->videos)) { ?>
    <h5>Загруженные видео</h5>
    <table class="table table-bordered table-hovered table-condensed">
        <tr>
            <th>Файл</th>
            <th>Действия</th>
        </tr>
        <?php foreach ($model->videos as $video) { ?>
            <tr>
                <td><?=$video->filename?></td>
                <td><a class="btn btn-small"  rel="tooltip" title="Удалить"
                       href="<?=$this->createUrl('/pages/default/deleteVideo', array('id' => $video->id))?>"><i
                            class="icon-remove"></i></a></td>
            </tr>
        <?php } ?>

    </table>
<?php } ?>
<?php
if (!$model->isNewRecord) {
    ?>
    <h5>Добавить новое видео</h5>
    <?php
    $this->widget('ext.eAjaxUpload.EAjaxUpload',
        array(
            'id' => 'uploadFilekz',
            'config' => array(
                'action' => Yii::app()->createUrl('/pages/default/upload', array('id' => $model->id)),
                'allowedExtensions' => array('flv', 'mp4', 'jpg', 'png', 'gif'),
                'sizeLimit' => 1000 * 1024 * 1024, // maximum file size in bytes
                'onComplete' => "js:function(id, fileName, responseJSON){ alert('Файл загружен'); }",
                'messages' => array(
                    'typeError' => "{file} запрещен к загрузке. Только файлы с типом {extensions} разрешены.",
//                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
//                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
//                  'emptyError'=>"{file} is empty, please select files again without it.",
//                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
                ),
//'showMessage'=>"js:function(message){ alert(message); }"
            )
        ));
} else {
    ?>
    <p>Для загрузки видео, нужно сохранить статью</p>
<?php } ?>