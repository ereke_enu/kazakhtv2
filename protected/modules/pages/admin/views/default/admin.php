<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Список страниц</span>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li><a href="<?= $this->createUrl('create'); ?>" class="">Создать страницу</a></li>
        </ul>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li>
                <a href="<?= $this->createUrl('admin', array('lang' => 'ru', 'archive'=>'0')); ?>" <?= ($lang == 'ru') ? 'style="font-weight:bold"' : ''; ?>>Русский</a>
            </li>
            <li>
                <a href="<?= $this->createUrl('admin', array('lang' => 'kz','archive'=>'0')); ?>" <?= ($lang == 'kz') ? 'style="font-weight:bold"' : ''; ?>>Казахский</a>
            </li>
            <li>
                <a href="<?= $this->createUrl('admin', array('lang' => 'en','archive'=>'0')); ?>" <?= ($lang == 'en') ? 'style="font-weight:bold"' : ''; ?>>Английский</a>
            </li>
        </ul>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => 'pages-grid',
    'type'         => 'bordered stripped condensed',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'ajaxUrl'      => Yii::app()->createUrl('/pages/default/admin', array('lang' => $lang)),
    'summaryText'  => '',
    'columns'      => array(
        array(
            'name'        => 'id',
            'htmlOptions' => array('width' => 50),
            'filter'      => false
        ),
        array(
            'name'  => 'title',
            'value' => '($data->title=="")?"<em>[нет заголовка]</em>":$data->title',
            'type'  => 'raw'
        ),
        'title',
        array(
            'name'   => 'category_id',
            'value'  => '$data->category->title',
            'filter' => Categories::getTreeListItems(false, $lang),

        ),
        array(
            'class'               => 'bootstrap.widgets.TbButtonColumn',
            'template'            => '{update} {delete}',
            'updateButtonOptions' => array(
                'class' => 'btn btn-small update'
            ),
            'deleteButtonOptions' => array(
                'class' => 'btn btn-small delete'
            ),
            'htmlOptions'         => array('width' => 100)
        ),
    ),
    'htmlOptions'  => array(
        'class' => 'grid-view table-hover'
    )
)); ?>
