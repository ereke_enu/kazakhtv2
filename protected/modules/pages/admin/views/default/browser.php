<?php
$this->widget('application.extensions.elFinder.ElFinderWidget', array(
    'lang' => 'ru',
    'url' => CHtml::normalizeUrl(array('default/fileManager')),
    'editorCallback' => 'js:function(url) {
        var funcNum = window.location.search.replace(/^.*CKEditorFuncNum=(\d+).*$/, "$1");
        window.opener.CKEDITOR.tools.callFunction(funcNum, url);
        window.close();
    }',
))?>