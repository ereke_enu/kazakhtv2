<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>

<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'pages-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbTabs', array(
        'type' => 'tabs',
        'tabs' => array(
            array('label' => 'Русский', 'content' => $this->renderPartial('_form_ru', array('model' => (empty($models['ru']) ? $model : $models['ru']), 'form' => $form), true), 'active' => true),
            array('label' => 'Казахский', 'content' => $this->renderPartial('_form_kz', array('model' => (empty($models['kz']) ? $model : $models['kz']), 'form' => $form), true)),
            array('label' => 'Английский', 'content' => $this->renderPartial('_form_en', array('model' => (empty($models['en']) ? $model : $models['en']), 'form' => $form), true)),
        ),
    )); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>


</div>