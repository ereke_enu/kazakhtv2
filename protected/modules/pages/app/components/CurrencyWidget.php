<?php

class CurrencyWidget extends CWidget {

	public function run() {
		$rates = array(
			'usd' => 'Доллар',
			'rub' => 'Рубль',
			'eur' => 'Евро',
			'cny' => 'Юань',
			'gbp' => 'Фунт',
			'kgs' => 'Сом',
			'aed' => '',
			'aud' => '',
			'byr' => '',
			'cad' => '',
			'chf' => '',
			'czk' => '',
			'dkk' => '',
//			'dru' => '',
			'eek' => '',
			'huf' => '',
			'jpy' => '',
			'krw' => '',
			'kwd' => '',
			'ltl' => '',
//			'lvl' => '',
			'mdl' => '',
			'nok' => '',
			'pln' => '',
			'sar' => '',
			'sek' => '',
			'sgd' => '',
//			'trl' => '',
			'try' => '',
			'uah' => '',
			'uzs' => '',
//			'xdr' => '',
			'zar' => ''
		);

		foreach ($rates as $table => $rate) {
			$data = Yii::app()->dbcurrency->cache(21600)->createCommand("SELECT value, shift FROM " . $table . " ORDER BY date DESC LIMIT 1")->query()->read();

			$shift = false;
			if ($data['shift'] > 0) {
				$shift = 'up';
			} elseif ($data['shift'] < 0) {
				$shift = 'down';
			}

			$rates_value[$table] = array(
				'rate' => (float)$data['value'],
				'shift' => $shift,
				'title' => $rate
			);
		}

		$this->render('currencyWidget', array('rates' => $rates_value));
	}

}