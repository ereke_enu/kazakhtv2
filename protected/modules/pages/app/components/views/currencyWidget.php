<div class="row">
	<div class="col-xs-12">
		<div class="bg-gradient currency-date text-center">
		<?= date('j.m.Y') ?> <?= Yii::t('app', 'Национальный Банк Казахстана') ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-8 col-sm-offset-3 col-md-offset-2">
		<div class="row">
			<?php $i=1; foreach ($rates as $name => $rate) { ?>
				<div class="col-xs-12 col-md-6<?= ($i++%2==0) ? '' : ' line' ?>">
					<div class="row">
						<div class="col-xs-9 col-md-9 text-uppercase currency">
							<img src="/themes/app/img/flags/<?= $name ?>.png"> <?= $name ?>
						</div>
						<div class="col-xs-2 currency">
							<?= $rate['rate']?>
						</div>
						<div class="col-xs-1 col-md-1 currency text-right"><i class="<?= $rate['shift']?>"></i>&nbsp;</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>