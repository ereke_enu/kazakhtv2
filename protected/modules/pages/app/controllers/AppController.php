<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 30.06.14
 * Time: 16:50
 */

class AppController extends LXController {

	public $layout = '//layouts/app';

	public function actionCategory($sefname) {
		if($sefname == 'about') $sefname = 'about_company';

		$category = Categories::model()->findBySefnameObj($sefname);

//		if ($category->sefname == '' || $category->parent_id == 11 || $category->sefname='press_about_us') {
//			$this->layout = '//layouts/company';
//		}

		if ($category === null) {
			throw new CHttpException(404, 'Категория не найдена');
		}

		$criteria = new CDbCriteria;
		$criteria->compare('category_id', $category->id);
		$criteria->compare('active', '1');
		$criteria->order = 'date DESC, time DESC';

		$page = Pages::model()->find($criteria);

//		$this->pageTitle = $category->title;
//		$this->metaTags = $page->meta_tags;
//		$this->metaDescription = $page->meta_description;
//		$this->pageTitle = $page->title;

		$pl = array();
		if (count($page->videos) > 1) {
			foreach ($page->videos as $video) {
				if ($video->upload_ext == 'flv') {
					$filename = $video->upload_file;
				} else {
					$filename = $video->filename . '_480.mp4';
				}
				$pl['playlist'][] = array(
					'file'      => '/upload/video/pages/' . $video->page_id . '/' . $filename,
					'comment'   => $video->title,
					'poster'    => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
					'bigposter' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
				);
			}
		} elseif (count($page->videos) == 1) {
			$video = $page->videos[0];
			if ($video->upload_ext == 'flv') {
				$filename = $video->upload_file;
			} else {
				$filename = $video->filename . '_[240,320,480].mp4';
			}
			$params = array(
				'file'       => '/upload/video/pages/' . $video->page_id . '/' . $filename,
				'comment'    => $video->title,
				'poster'     => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
				'big_poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
			);
		}

		if (empty($params))
			$params = array('pl' => $pl, 'file' => '');

		if($sefname == 'contacts')
			$this->render('contacts', array('category' => $category, 'page' => $page, 'params' => $params));
		else
			$this->render('category', array('category' => $category, 'page' => $page, 'params' => $params, 'sefname'=>$sefname));
	}

	public function actionNews($category='news', $date=false) {
		if($category == 'column') $view = 'column';
		else $view = 'news';

		$category = Categories::model()->findBySefnameObj($category);
		if ($category === null)
			throw new CHttpException(404, 'Страница не найдена');

		if ($category->isRoot()) {
			$categoryId = CHtml::listData($category->children()->findAll('active=1'), 'id', 'id');
		} else {
			$categoryId = $category->id;
		}

		$this->newsCategory = $categoryId;

		$criteria = new CDbCriteria;
		$criteria->compare('category_id', $categoryId);
		if ($date) {
			$date = date('Y-m-d', strtotime($date));
			$criteria->compare('date', $date);
		}

		$criteria->compare('is_archive', 0);
		$criteria->order = 'date DESC, time DESC';
		$criteria->compare('active', '1');

		$newsCount = Pages::model()->count($criteria);

		$pagination = new CPagination($newsCount);
		$pagination->pageSize = 10;
		$pagination->applyLimit($criteria);

		$news = Pages::model()->findAll($criteria);

		$this->render($view, array('news' => $news, 'pagination' => $pagination, 'category'=>$category));
	}

	public function actionView($category, $sefname) {
//		$this->layout = '//layouts/content';
		$model = Pages::model()->findBySefname($sefname);
		if ($model === null)
			throw new CHttpException(404, 'Страница не найдена');

//		if ($category == 'press_about_us') {
//			$this->layout = '//layouts/press';
//		} elseif ($category == 'about_us_team') {
//			$this->layout = '//layouts/team';
//		}

		$model->views = $model->views + 1;
		$model->save();

		$pl = array();
		if (count($model->videos) > 1) {
			foreach ($model->videos as $video) {
				if ($video->upload_ext == 'flv') {
					$filename = $video->upload_file;
				} else {
					$filename = $video->filename . '_480.mp4';
				}
				$pl['playlist'][] = array(
					'file' => '/upload/video/pages/' . $video->page_id . '/' . $filename,
					'comment' => $video->title,
					'poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
					'bigposter' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
				);
			}
		} elseif (count($model->videos) == 1) {
			$video = $model->videos[0];
			if ($video->upload_ext == 'flv') {
				$filename = $video->upload_file;
			} else {
				$filename = $video->filename . '_480.mp4';
			}
			$params = array(
				'file' => '/upload/video/pages/' . $video->page_id . '/' . $filename,
				'comment' => $video->title,
				'poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
				'big_poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
			);
		}


		if (empty($params)) {
			$params = array('pl' => $pl, 'file' => '');
		}


		$this->pageTitle = $model->title;
		$this->metaTags = $model->meta_tags;
		$this->metaDescription = $model->meta_description;

		if ($model->category->parent->sefname == 'news') {
//			$this->layout = '//layouts/news';
			$this->newsCategory = $model->category_id;
		}

		if($category == 'column')
			$this->render('view_column', array('model' => $model, 'params' => $params));
		else
			$this->render('view', array('model' => $model, 'params' => $params));
	}

	public function actionRates() {
		$this->layout = '//layouts/app_fluid';
		$this->render('rates');
	}
}