<div class="block-view">
	<h1><?= $page->title ?></h1>

	<?php if (!empty($page->videos)) { ?>
		<div id="videoplayer_content">
			<?php $this->widget('ext.uppod.LXUppodWidget', array('width' => '100%', 'height' => 200, 'params' => CMap::mergeArray(Yii::app()->params['videoPlayer'], $params)))?>
		</div>
	<?php } ?>

	<p class="lead"><?= $page->short_text ?></p>
	<p><?= $page->text ?></p>
</div>