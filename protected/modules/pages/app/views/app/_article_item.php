<?php $date = new DateTime($article->date.' '.$article->time, new DateTimeZone('Asia/Almaty')); ?>
<?php $url = Yii::app()->createUrl('pages/app/view', array('category'=>$article->category->sefname, 'sefname'=>$article->sefname)); ?>

<div class="col-xs-12 col-sm-6 news-item">
	<article>
		<div class="row">
			<div class="col-xs-6 col-image">
				<div class="img-16x9">
					<?php if (($image = Pages::getAnounceImg($article->image, '')) !== false) { ?>
					<a class="out" href="<?= $url ?>" style="background-image: url(<?= $image ?>)">
						<?php if (!empty($article->videos)) { ?>
							<span class="play"></span>
						<?php } ?>
					</a>
					<?php } else { ?>
						<a class="out" href="<?= $url ?>" style="background-image: url(/themes/front/images/noimage.jpg)">
							<?php if (!empty($article->videos)) { ?>
								<span class="play"></span>
							<?php } ?>
						</a>
					<?php } ?>
				</div>
			</div>
			<div class="col-xs-6 col-title">
				<h3><a href="<?= $url ?>"><?= $article->title ?></a></h3>
				<p><a href="<?= $url ?>"><?= $article->short_text ?></a></p>
			</div>
		</div>
		<div class="date"><?= $date->format('j.m.Y G:i') ?></div>
	</article>
</div>
<?php if($iterator % 2 == 0) { ?>
<div class="clearfix news-item"></div>
<?php } ?>