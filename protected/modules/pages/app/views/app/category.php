<?php $comments = Comments::model()->count('cid=:cid AND spam=0', array(':cid'=>$page->id)); ?>

<div class="row">
	<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
		<div class="block-view">
			<h1><?= $page->title ?></h1>

			<?php if (($image = Pages::getAnounceImg($page->image, '')) !== false) { ?>
				<div class="col-image">
					<?php if (!empty($page->videos)) { ?>
						<?php $video = (!empty($params['file']) ? $params['file'] : (isset($params['pl']['playlist'][0]['file']) ? $params['pl']['playlist'][0]['file'] : '')) ?>
						<div class="video-16x9">
							<video id="page_video" class="video-js vjs-default-skin vjs-big-play-centered" controls="controls" preload="none" poster="<?= $image ?>">
								<source src="<?= preg_replace('/(.*?)\[(.*?),(.*?),(.*?)\](.*?)/is', '$1$4$5', $video) ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
								<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
							</video>
						</div>
					<?php } ?>
				</div>
			<?php } ?>

			<ul class="pull-right stats">
				<li class="icon-user"><?= $page->views ?></li>
				<?php if($category->sefname != 'about_company') { ?>
				<li class="icon-comment"><?= $comments ?></li>
				<?php } ?>
			</ul>
			<div class="clearfix"></div>
			<hr>

			<p class="lead"><?= $page->short_text ?></p>
			<p><?= $page->text ?></p>

			<?php if($category->sefname != 'about_company') { ?>
				<a class="btn btn-grey pull-right btn-comments" href="<?= Yii::app()->createUrl('comments/app/category', array('sefname'=>Yii::app()->request->getParam('sefname', ''))) ?>"><?= Yii::t('comments', 'Комментарии') ?></a>
			<?php } ?>
		</div>
	</div>
</div>