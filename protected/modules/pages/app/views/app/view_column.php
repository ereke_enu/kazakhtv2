<?php $date = new DateTime($model->date.' '.$model->time, new DateTimeZone('Asia/Almaty')); ?>
<?php $comments = Comments::model()->count('cid=:cid AND spam=0', array(':cid'=>$model->id)); ?>

<div class="row">
	<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
		<div class="block-view">
			<h1><?= $model->title ?></h1>

			<p><?= $model->text ?></p>

			<div class="clearfix"></div>

			<hr>

			<div class="row">
				<div class="col-xs-6">
					<ul class="pull-left stats" style="margin-top: 7px">
						<li class="icon-user"><?= $model->views ?></li>
						<li class="icon-comment"><?= $comments ?></li>
					</ul>
				</div>
				<div class="col-xs-6">
					<a class="btn btn-grey pull-right visible-xs" href="<?= Yii::app()->createUrl('comments/app/news', array('category'=>$model->category->sefname, 'sefname'=>$model->sefname)) ?>"><?= Yii::t('comments', 'Комментарии') ?></a>
				</div>
			</div>

			<div class="hidden-xs">
				<?php $this->widget('application.modules.comments.app.components.BlockComments', array('type' => $this->getModule()->id, 'id' => $model->id)); ?>
			</div>
		</div>
	</div>
</div>