<div class="block-news">
	<div class="row">
		<?php
		$i = 1;
		foreach($news as $article) {
			$this->renderPartial('_column_item', array('article'=>$article, 'category'=>$category, 'iterator'=>$i++));
		}
		?>
	</div>
	<div class="clearfix"></div>
</div>

<?php $this->widget('CLinkPager', array(
	'pages' => $pagination,
	'header' => false,
	'nextPageLabel' => '&raquo;',
	'prevPageLabel' => '&laquo;',
	'maxButtonCount' => 0,
	'htmlOptions' => array(
		'id' => 'pagination',
		'class' => 'pager'
	)
))?>
