<?php $date = new DateTime($model->date.' '.$model->time, new DateTimeZone('Asia/Almaty')); ?>
<?php $comments = Comments::model()->count('cid=:cid AND spam=0', array(':cid'=>$model->id)); ?>

<div class="row">
	<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
		<div class="block-view">
			<h1><?= $model->title ?></h1>

			<?php if (($image = Pages::getAnounceImg($model->image, '')) !== false) { ?>
				<div class="col-image">
					<?php if (!empty($model->videos)) { ?>
						<div class="video-16x9">
							<video id="page_video" class="video-js vjs-default-skin vjs-big-play-centered" controls="controls" preload="none" poster="<?= $image ?>">
								<source src="<?= preg_replace('/(.*?)\[(.*?),(.*?),(.*?)\](.*?)/is', '$1$4$5', $params['file']) ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
								<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
							</video>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
			<div class="pull-left date"><?= $date->format('j.m.Y G:i') ?></div>
			<ul class="pull-right stats">
				<li class="icon-user"><?= $model->views ?></li>
				<li class="icon-comment"><?= $comments ?></li>
			</ul>
			<div class="clearfix"></div>
			<hr>

			<p><?= $model->text ?></p>

			<a class="btn btn-grey pull-right btn-comments visible-xs" href="<?= Yii::app()->createUrl('comments/app/news', array('category'=>$model->category->sefname, 'sefname'=>$model->sefname)) ?>"><?= Yii::t('comments', 'Комментарии') ?></a>

			<div class="hidden-xs">
				<?php $this->widget('application.modules.comments.app.components.BlockComments', array('type' => $this->getModule()->id, 'id' => $model->id)); ?>
			</div>
		</div>
	</div>
</div>