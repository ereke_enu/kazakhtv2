<?php

    /**
     * This is the model class for table "{{pages}}".
     *
     * The followings are the available columns in table '{{pages}}':
     * @property integer $id
     * @property integer $category_id
     * @property integer $user_id
     * @property integer $lang_id
     * @property string $title
     * @property string $short_text
     * @property string $text
     * @property string $sefname
     * @property string $date
     * @property string $time
     * @property integer $position
     * @property integer $views
     * @property integer $active
     * @property string $image
     * @property string $other_id
     * @property integer $is_archive
     */
    class Pages extends CActiveRecord {
        public $delete_image = 0;
        public $delete_background = 0;
        public $tags;

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return Pages the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{pages}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('category_id, lang_id', 'required'),
                array('is_main, is_archive, delete_background, delete_image, show_pub_date, show_icons, show_comments, category_id, user_id, lang_id, position, views, active', 'numerical', 'integerOnly' => true),
                array('sefname', 'length', 'max' => 200),
                array('other_id', 'length', 'max' => 255),
                array('tags, title, short_text, text, date, time, meta_tags, meta_description', 'safe'),
                array('background', 'file', 'types' => 'jpg,png,gif,jpeg', 'allowEmpty' => true),
                array('image', 'file', 'types' => 'jpg,png,gif,jpeg', 'allowEmpty' => true),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('is_archive, id, category_id, user_id, lang_id, title, short_text, text, sefname, date, time, position, views, active, image, other_id', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'lang'     => array(self::BELONGS_TO, 'Langs', 'lang_id'),
                'category' => array(self::BELONGS_TO, 'Categories', 'category_id'),
                'listTags' => array(self::MANY_MANY, 'Tags', '{{pages_tags}}(page_id, tag_id)'),
                'videos'   => array(self::HAS_MANY, 'Video', 'page_id', 'order' => 'videos.id DESC', 'condition' => 'module="pages"')
            );
        }

        public function beforeValidate() {

            if ($this->meta_description == '') {
                $this->meta_description = $this->title;
            }
            
            if ($this->meta_tags == '') {
                $this->meta_tags = $this->title;
                $this->meta_tags = str_replace(' ', ', ', str_replace(',', '', $this->meta_tags));
            }
            
            if ($this->delete_image == 1) {
                $this->image = '';
            }

            if ($this->delete_background == 1) {
                $this->background = '';
            }

            return parent::beforeValidate();
        }

        public function afterSave() {
            if (trim($this->tags) != '') {
                $tags = explode(',', $this->tags);
                if (!empty($tags)) {
                    foreach ($tags as $tag) {
                        $tag = Tags::model()->findByTagAndCreate($tag);
                        $tagId = $tag->id;

                        $tagModel = new PagesTags;
                        $tagModel->page_id = $this->id;
                        $tagModel->tag_id = $tagId;
                        $tagModel->save();
                    }
                }
            }


            return parent::afterSave();
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'                => 'ID',
                'category_id'       => 'Категория',
                'user_id'           => 'Пользователь',
                'lang_id'           => 'Язык',
                'title'             => 'Заголовок',
                'short_text'        => 'Анонс',
                'text'              => 'Текст',
                'sefname'           => 'ЧПУ',
                'date'              => 'Дата',
                'time'              => 'Время',
                'position'          => 'Позиция',
                'views'             => 'Просмотров',
                'active'            => 'Активность',
                'image'             => 'Изображение',
                'other_id'          => 'Связка',
                'background'        => 'Бренд фон',
                'sort_type'         => 'Тип сортировки',
                'show_pub_date'     => 'Показывать дату публикации',
                'show_icons'        => 'Показывать иконки соц. сетей',
                'show_comments'     => 'Включить комментарии',
                'delete_background' => 'Удалить бренд фон',
                'delete_image'      => 'Удалить изображение',
                'tags'              => 'Теги страницы через запятую',
                'is_archive'        => 'Страница в архиве',
                'is_main'           => 'Главная новость'
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;


            $criteria->compare('id', $this->id);
            $criteria->compare('category_id', $this->category_id);
            $criteria->compare('user_id', $this->user_id);
            $criteria->compare('lang_id', $this->lang_id);
            $criteria->compare('title', $this->title, true);
            $criteria->compare('short_text', $this->short_text, true);
            $criteria->compare('text', $this->text, true);
            $criteria->compare('sefname', $this->sefname, true);
            $criteria->compare('date', $this->date, true);
            $criteria->compare('time', $this->time, true);
            $criteria->compare('position', $this->position);
            $criteria->compare('views', $this->views);
            $criteria->compare('active', $this->active);
            $criteria->compare('image', $this->image, true);
            $criteria->compare('other_id', $this->other_id, true);
            $criteria->compare('is_archive', $this->is_archive);

          //  $criteria->addCondition("title<>'' AND text<>''");
            $criteria->order = 'id DESC';


            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }

        /**
         * Получение страниц в категории
         * @param $categoryId
         * @return array
         */
        public function getPagesInCategory($categoryId) {
            $sql = "SELECT `id`, `lang_id`, `title`, `short_text`, `text`, `sefname`, `DATE`, `TIME`, `views`, `image`, `other_id` FROM {{pages}} WHERE `category_id`=:category_id AND `active`='1' ORDER BY `DATE` DESC, `TIME` DESC";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':category_id', $categoryId, PDO::PARAM_INT);

            $result = $command->queryAll();


            return $result;
        }

        /**
         * Получение страницы по ЧПУ
         * @param $sefname
         * @return array|false
         */
        public function findBySefnameArr($sefname) {
            $sql = "SELECT `id`, `lang_id`, `category_id`, `title`, `short_text`, `text`, `sefname`, `DATE`, `TIME`, `views`, `image`, `other_id`, `background` FROM {{pages}} WHERE `sefname`=:sefname AND `active`='1' ORDER BY `DATE` DESC";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':sefname', $sefname, PDO::PARAM_STR);
            $result = $command->query();

            $result = $result->read();

            if ($result['lang_id'] != LXController::getLangId()) {
                $otherIds = CJSON::decode($result['other_id']);
                if (!empty($otherIds[LXController::getLangId()])) {
                    $sql = "SELECT `id`, `lang_id`, `category_id`, `title`, `short_text`, `text`, `sefname`, `DATE`, `TIME`, `views`, `image`, `other_id` FROM {{pages}} WHERE `id`=:id AND `active`='1' ORDER BY `DATE`";
                    $command = Yii::app()->db->createCommand($sql);
                    $command->bindParam(':id', $otherIds[LXController::getLangId()], PDO::PARAM_INT);
                    $result = $command->query();

                    $result = $result->read();
                }
            }

            return $result;
        }

        /**
         * Получение страницы по ЧПУ
         * @param $sefname
         * @return array|false
         */
        public function findBySefname($sefname) {
            $model = Pages::model()->find('sefname=:sefname AND active=1', array(':sefname' => $sefname));

	        if($model === null)
		        return null;

	        if ($model->lang_id != LXController::getLangId()) {
                $otherIds = CJSON::decode($model->other_id);
                if (!empty($otherIds[Yii::app()->language])) {
                    $model = Pages::model()->findByPk((int)$otherIds[Yii::app()->language], 'active=1');
                }
            }

            return $model;
        }

        /**
         * Получение списка статей из категории с $sefname в количестве $limit
         * @param $sefname
         * @param int $limit
         * @return mixed
         * @throws CDbException
         */
        public function getLimitPagesInCategorySefname($sefname, $limit = 1) {
            if ($limit <= 0) {
                throw new CDbException('LIMIT не может быть меньше или равен 0');
            }

            $category = Categories::model()->findBySefname($sefname);
            if (empty($category)) {
                throw new CHttpException(404, 'Категория с ЧПУ "' . $sefname . '" не найдена');
            }

            $sql = "SELECT t.`id`, t.`lang_id`, t.`title`, t.`short_text`, t.`text`, t.`sefname`, t.`date`, t.`time`, t.`views`, t.`image`, t.`other_id`, C.sefname AS category
                FROM {{pages}} AS t
                LEFT JOIN {{categories}} AS C ON C.id=t.category_id
                WHERE `category_id`=:category_id AND t.`active`='1'
                ORDER BY `DATE` DESC, `TIME` DESC
                LIMIT {$limit}";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':category_id', $category['id'], PDO::PARAM_INT);

            $result = $command->queryAll();


            return $result;

        }

        public  function GetSliderPages($categories)
        {
            $sql="select p1.sefname as category_sefname,(select concat(sefname,'+',title,'+',image,’+’,short_text) as datas from lx_pages p2 where p2.category_id = p1.Id order by Id desc limit 1) as page from lx_categories p1 where p1.id in ("+$categories+")";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            return $result;
        }

        public  function GetCategoryPages($category_sefname)
        {
            $sql="SELECT l.*, cat.sefname as category_sefname FROM lx_pages l inner join lx_categories cat on cat.id=l.category_id where l.is_archive=0 and l.lang_id=1 and l.active=1 and cat.sefname='"+$category_sefname+"' order by l.id desc limit 3";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            return $result;
        }

        public function GetSliderPages2()
        {
            //SELECT * FROM lx_pages l limit 5;
            $sql="SELECT l.*, cat.sefname as category_sefname FROM lx_pages l inner join lx_categories cat on cat.id=l.category_id where l.is_archive=0 and l.active=1 and l.lang_id=1  order by l.id desc limit 30";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            return $result;
        }

        /**
         * Получение пути картинки
         * @param $filename
         * @return bool|string
         */
        public static function getAnounceImg($filename, $prefix = '', $image='/themes/front/images/noimage.jpg') {
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/' . $prefix . $filename) && is_file($_SERVER['DOCUMENT_ROOT'] . '/upload/anounces/' . $prefix . $filename)) {
                return '/upload/anounces/' . $prefix . $filename;
            } else {
                if ($prefix == 'top_')
                    return '/themes/front/images/top_noimage.png';
                return $image;
            }
            return false;
        }

        public static function getSearchPages(CDbCriteria $criteria, $searchString) {
            $criteria->addCondition("CONCAT(date, time) <= STR_TO_DATE('" . date('Y-m-d H:i:s', time()) . "', '%Y-%m-%d %H:%i:%s')");

            $pages = self::model()->together()->findAll($criteria);

            foreach ($pages as &$page) {
                $_value = trim(strip_tags($page->text));
                $_temps = preg_split("'[,.?!:]'", $_value);

                $_value = '';
                $it = 0;
                foreach ($_temps as $_temp) {
                    $_temp = trim($_temp);
                    if (preg_match("'(^|[^\xD0-\xD3][^0-9a-zA-Z])(" . $searchString . ")'s", $_temp)) {
                        $str_pu = strpos($_temp, $searchString);
                        $it++;
                        $_value .= "..." . (substr($_temp, 0, $str_pu) . "<span style='color: #00202e; font-weight: bold; text-decoration:underline;'>" . $searchString . "</span>" . substr($_temp, $str_pu + strlen($searchString))) . "...<br>\n";

                        // $_value .= "..." . (preg_replace("'(^|\s|[^\xD0-\xD3][^0-9a-zA-Z])(" . $searchString . ")'s", "\\1<span style='color: #406E9C; font-weight: bold;'>\\2</span>", $_temp)) . "...<br>\n";
                    }
                    if ($it == 5) break;
                }
                $page->text = $_value;

                if (preg_match("'(^|[^\xD0-\xD3][^0-9a-zA-Z])(" . $searchString . ")'s", $page->title)) {
                    $page->title = preg_replace("'(^|\s|[^\xD0-\xD3][^0-9a-zA-Z])(" . $searchString . ")'s", "\\1<span style='color: #00202e; font-weight: bold;'>\\2</span>", $page->title);
                }
            }

            return $pages;
        }

        public function getSimilar($current_page) {
            
            foreach ($current_page->listTags as $tag) {
                $all_tags[] = $tag->id;
            }

            $pages_criteria = new CDbCriteria;
            $pages_criteria->addInCondition('tag_id', $all_tags);
            $pages = PagesTags::model()->findAll($pages_criteria);

            foreach ($pages as $tag) {
                $tags[] = $tag->page_id;
            }

            $criteria = new CDbCriteria;
            $criteria->addInCondition('id', $tags);
            $criteria->compare('id', '<>' . $id);
            $model = Pages::model()->findAll($criteria);


            return $model;
        }

        public function getPopular($id) {

            $criteria = new CDbCriteria;
            $criteria->compare('id', '<>' . $id);
            $criteria->order = 'view ASC';
            $criteria->limit = 5;

            $page = self::model()->findAll($criteria);
        }

        public function getPagesForRSS($category_id) {
            $criteria = new CDbCriteria;
            $criteria->compare('t.active', '1');
            $criteria->compare('t.lang_id', LXController::getLangId());
            //$criteria->compare('is_archive', '0');

            $criteria->with = array('category');
            if ($category_id > 0) {
                $criteria->compare('t.category_id', $category_id);
            } else {
                $category = Categories::model()->findBySefnameObj('news');
                $categoryId = CHtml::listData($category->children()->findAll('active=1'), 'id', 'id');
                $criteria->addInCondition('t.category_id', $categoryId);
            }
            $criteria->order = 'date DESC, time DESC';
            $criteria->limit = 100;

            $pages = Pages::model()->findAll($criteria);

            return $pages;

        }

		public function getBannerPages() {
            $categories = '28,24,248,245,25';
            if(mb_substr($_SERVER['REQUEST_URI'],0,4)=='/kz/')$categories='56,60,249,246,63';
            $sql = "SELECT cat.sefname as catsefname,(select title from lx_pages p where p.category_id=cat.id order by id desc limit 1)as title, (select image from lx_pages p where p.category_id=cat.id order by id desc limit 1)as image, (select sefname from lx_pages p where p.category_id=cat.id order by id desc limit 1) as sefname FROM lx_categories cat where cat.id in (".$categories.")";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            return $result;
        }

        public function findById($id) {
            $model = Pages::model()->find('id=:id AND active=1', array(':id' => $id));

            if($model === null)
                return null;

            if ($model->lang_id != LXController::getLangId()) {
                $otherIds = CJSON::decode($model->other_id);
                if (!empty($otherIds[Yii::app()->language])) {
                    $model = Pages::model()->findByPk((int)$otherIds[Yii::app()->language], 'active=1');
                }
            }

            return $model;
        }

    }