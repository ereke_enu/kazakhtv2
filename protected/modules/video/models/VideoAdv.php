<?php

/**
 * This is the model class for table "{{video_adv}}".
 *
 * The followings are the available columns in table '{{video_adv}}':
 * @property integer $id
 * @property string $title
 * @property string $filename
 * @property string $date_create
 */
class VideoAdv extends CActiveRecord {
    public static $_items;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VideoAdv the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{video_adv}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, filename', 'required'),
            array('title', 'length', 'max' => 255),
            array('is_old', 'numerical', 'integerOnly' => true),
            array('date_create', 'safe'),
            array('filename', 'file', 'types' => 'flv,mp4,avi', 'on' => 'insert'),
            array('filename', 'file', 'types' => 'flv,mp4,avi', 'on' => 'update', 'allowEmpty' => true),

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, filename, date_create', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Название',
            'filename' => 'Файл видео',
            'date_create' => 'Дата создания',
            'is_old' => 'Устаревшее'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('filename', $this->filename, true);
        $criteria->compare('date_create', $this->date_create, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getListItems() {

        if (empty(self::$_items)) {
            $model = self::model()->findAll('is_old=0');
            self::$_items = CHtml::listData($model, 'id', 'title');
        }

        return self::$_items;


    }
}