<?php

    /**
     * This is the model class for table "{{video}}".
     *
     * The followings are the available columns in table '{{video}}':
     * @property integer $id
     * @property string $module
     * @property integer $page_id
     * @property string $title
     * @property string $filename
     * @property integer $definition
     * @property string $subtitles
     * @property integer $video_lang_id
     * @property integer $subtatiles_lang_id
     * @property integer $adv_preroll_id
     * @property integer $adv_midroll_id
     * @property integer $adv_postroll_id
     * @property string $date_create
     */
    class Video extends CActiveRecord {
        public $category_id;
        public $lang_id;

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return Video the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{video}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('page_id, module,  filename', 'required'),
                array('page_id, definition, video_lang_id, subtatiles_lang_id, adv_preroll_id, adv_midroll_id, adv_postroll_id', 'numerical', 'integerOnly' => true),
                array('module', 'length', 'max' => 50),
                array('upload_file, upload_ext, title', 'length', 'max' => 255),
                array('date_create', 'safe'),
                array('subtitles', 'file', 'allowEmpty' => true),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, module, page_id, title, filename, definition, subtitles, video_lang_id, subtatiles_lang_id, adv_preroll_id, adv_midroll_id, adv_postroll_id, date_create', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'PagesPage'    => array(self::BELONGS_TO, 'Pages', 'page_id'),
                'ProgrammPage' => array(self::BELONGS_TO, 'ProgrammsContent', 'page_id'),
                'preroll'      => array(self::BELONGS_TO, 'VideoAdv', 'adv_preroll_id'),
                'midroll'      => array(self::BELONGS_TO, 'VideoAdv', 'adv_midroll_id'),
                'postroll'     => array(self::BELONGS_TO, 'VideoAdv', 'adv_postroll_id'),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'                 => 'ID',
                'module'             => 'Модуль',
                'page_id'            => 'Страница',
                'title'              => 'Наименование',
                'filename'           => 'Файл',
                'definition'         => 'Качество',
                'subtitles'          => 'Файл субтитров (SRT, SSA/ASS)',
                'video_lang_id'      => 'Язык видео',
                'subtatiles_lang_id' => 'Язык субтитров',
                'adv_preroll_id'     => 'Реклама преролл',
                'adv_midroll_id'     => 'Реклама мидлролл',
                'adv_postroll_id'    => 'Реклама постролл',
                'date_create'        => 'Дата создания',
                'lang_id'            => 'Язык',
                'category_id'        => 'Категория'
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('module', $this->module, true);
            $criteria->compare('page_id', $this->page_id);
            $criteria->compare('title', $this->title, true);
            $criteria->compare('filename', $this->filename, true);
            $criteria->compare('definition', $this->definition);
            $criteria->compare('subtitles', $this->subtitles, true);
            $criteria->compare('video_lang_id', $this->video_lang_id);
            $criteria->compare('subtatiles_lang_id', $this->subtatiles_lang_id);
            $criteria->compare('adv_preroll_id', $this->adv_preroll_id);
            $criteria->compare('adv_midroll_id', $this->adv_midroll_id);
            $criteria->compare('adv_postroll_id', $this->adv_postroll_id);
            $criteria->compare('date_create', $this->date_create, true);

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }

        public static function getAnounceImg($file, $id) {
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/video/archive/' . $id . '/' . $file) && is_file($_SERVER['DOCUMENT_ROOT'] . '/upload/video/archive/' . $id . '/' . $file)) {
                return '/upload/video/archive/' . $id . '/' . $file;
            }

            return false;
        }
    }