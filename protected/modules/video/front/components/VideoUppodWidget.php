<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 17.01.13
 * Time: 11:41
 * To change this template use File | Settings | File Templates.
 */
class VideoUppodWidget extends CWidget {
    public $module;
    public $pageId;
    public $width;
    public $height;

    public function run() {
        if (!in_array($this->module, array('programms', 'pages', 'program_archive'))) {
            throw new CException('Модуль ' . $this->module . ' не подключен к видео. Проверьте название модуля');
        }
        $video = Video::model()->find('module=:module AND page_id=:page_id', array(':module' => $this->module, ':page_id' => $this->pageId));
        $options = array();
        if (!empty($video)) {

            $options['file'] = $video->filename;
            $options['m'] = 'video';
            $options['comment'] = $video->title;
            if ($video->postroll !== null) {
                $options['postroll'] = Yii::app()->createAbsoluteUrl('/upload/video/' . $video->page_id . '/' . $video->postroll->filename);
            }

            if ($video->preroll !== null) {
                $options['preroll'] = Yii::app()->createAbsoluteUrl('/upload/video/' . $video->page_id . '/' . $video->preroll->filename);
            }

            if ($video->midroll !== null) {
                $options['midroll'] = Yii::app()->createAbsoluteUrl('/upload/video/' . $video->page_id . '/' . $video->midroll->filename);
            }

            if (Yii::app()->language != Langs::getLangCodeById($video->video_lang_id) && !empty($video->subtitles)) {
                $options['sub'] = Yii::app()->createAbsoluteUrl('/upload/video/' . $video->page_id . '/' . $video->subtitles);
            }
        }
    }
}
