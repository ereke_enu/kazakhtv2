<?php

    class VideoArchiveController extends LXAController {
        public $defaultAction = 'admin';
        public $listTimetable;

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate() {
            $model = new VideoArchive;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['VideoArchive'])) {
                $model->attributes = $_POST['VideoArchive'];
                if ($model->save())
                    $this->redirect(array('admin'));
            }

            $this->listTimetable = array();

            $this->render('create', array(
                'model' => $model,
            ));
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
            $model = $this->loadModel($id);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['VideoArchive'])) {
                $model->attributes = $_POST['VideoArchive'];
                if ($model->save())
                    $this->redirect(array('admin'));
            }

            $timetable = ProgrammsTimetable::model()->findByPk($model->timetable_id);
            if ($timetable === null) {
                $this->listTimetable = array();
            } else {
                $this->listTimetable = array($timetable->id => $timetable->title . ' ' . $timetable->date . ' ' . $timetable->time);
            }

            $this->render('update', array(
                'model' => $model,
            ));
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            } else
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }

        /**
         * Manages all models.
         */
        public function actionAdmin() {
            $model = new VideoArchive('search');
            $model->unsetAttributes(); // clear any default values
            if (isset($_GET['VideoArchive']))
                $model->attributes = $_GET['VideoArchive'];
            //$model = VideoArchive::model()->findAll();

            $this->render('admin', array(
                'model' => $model,
            ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer the ID of the model to be loaded
         */
        public function loadModel($id) {
            $model = VideoArchive::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');

            return $model;
        }

        /**
         * Performs the AJAX validation.
         * @param CModel the model to be validated
         */
        protected function performAjaxValidation($model) {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'video-archive-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }

        /**
         * Загрузка видео к статье
         * @param $id
         */
        public function actionUpload($id) {
            if (!Yii::app()->request->isAjaxRequest) {
                throw new CHttpException(400, 'Bad request');
            }


            $page = VideoArchive::model()->findByPk((int)$id);

            Yii::import("ext.eAjaxUpload.qqFileUploader");

            $allowedExtensions = array('mp4', 'flv');
            $sizeLimit = 1000 * 1024 * 1024;

            $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/archive/' . $id . '/';
            if (!is_writable($path)) {
                mkdir($path, 0777, true);
            }

            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload($path);
            // to pass data through iframe you will need to encode all html tags
            $results = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

            if (is_array($result) && empty($result['error'])) {
                $filename = 'video_' . $page->id . '_' . date('dmYHis', time());

                //Создание эскиза
                //640*480
                exec('/usr/bin/ffmpeg  -i "' . $path . $result['filename'] . '" -an -ss 00:00:' . rand(9, 12) . ' -an -r 1 -vframes 1 -s 640x480 -y -f mjpeg ' . $path . '/' . $filename . '.jpg');

                if ($result['ext'] != 'flv') {
                    // конвертация видео
                    //240
                    exec("/usr/local/sbin/mpp4_240 " . $path . $result['filename'] . " " . $path . $filename . "_240.mp4");
                    //320
                    exec("/usr/local/sbin/mpp4_320 " . $path . $result['filename'] . " " . $path . $filename . "_320.mp4");
                    //480
                    exec("/usr/local/sbin/mpp4_480 " . $path . $result['filename'] . " " . $path . $filename . "_480.mp4");
                }
                $model = new Video;
                $model->module = 'videoarchive';
                $model->page_id = $id;
                $model->filename = $filename;
                $model->video_lang_id = $page->timetable->lang_id;
                $model->title = $page->title;
                $model->upload_file = $result['filename'];
                $model->upload_ext = $result['ext'];

                if ($model->save()) {
                    $this->sendMail('kazakhtv success', $filename, 'e.dymov@softdeco.com', 'e.dymov@softdeco.com');
                } else {
                    $errors = print_r($model->errors, true);
                    $this->sendMail('kazakhtv error', $errors, 'e.dymov@softdeco.com', 'e.dymov@softdeco.com');
                }

                echo $results;


                /*if ($model->save()) {
                    echo $results;
                } else {
                    echo htmlspecialchars(json_encode(array('error' => 'Не удалось сохранить файл. DBERROR:' . implode(';', $model->errors))), ENT_NOQUOTES);
                } */
            } else {
                echo $results;
            }
        }

        public function actionUploadTest($id) {
            $page = VideoArchive::model()->findByPk((int)$id);


            $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/archive/' . $id . '/';
            if (!is_writable($path)) {
                mkdir($path, 0777, true);
            }

            $file = CUploadedFile::getInstanceByName('videofile');
            $filename = 'video_' . $page->id . '_' . date('dmYHis', time());
            $file->saveAs($path . $filename.'.'.$file->extensionName);

            exec('/usr/bin/ffmpeg  -i ' . $path . $filename.".".$file->extensionName . ' -an -ss 00:00:' . rand(9, 12) . ' -an -r 1 -vframes 1 -s 640x480 -y -f mjpeg ' . $path . '/' . $filename . '.jpg');

            if ($file->extensionName != 'flv') {
                // конвертация видео
                //240
                exec("/usr/local/sbin/mpp4_240 " . $path . $filename.".".$file->extensionName . " " . $path . $filename . "_240.mp4");
                //320
                exec("/usr/local/sbin/mpp4_320 " . $path . $filename.".".$file->extensionName . " " . $path . $filename . "_320.mp4");
                //480
                exec("/usr/local/sbin/mpp4_480 " . $path . $filename.".".$file->extensionName . " " . $path . $filename . "_480.mp4");
            }
            $model = new Video;
            $model->module = 'videoarchive';
            $model->page_id = $id;
            $model->filename = $filename;
            $model->title = $page->title;
            $model->video_lang_id = 1;
            $model->upload_file = $filename.'.'.$file->extensionName;
            $model->upload_ext = $file->extensionName;
            if ($model->save()) {
             //   $this->sendMail('success', $filename, 'e.dymov@softdeco.com', 'e.dymov@softdeco.com');

            } else {
                $errors = print_r($model->errors, true);
                //$this->sendMail('error', $errors, 'e.dymov@softdeco.com', 'e.dymov@softdeco.com');
            }

            $this->redirect(array('/video/videoArchive/update', 'id' => $id));

        }

        public function actionDeleteVideo($id) {
            $model = Video::model()->findByPk($id);
            if ($model === null) {
                throw new CHttpException(404, 'Страница не найдена');
            }

            $page_id = $model->page_id;
            $filename = $model->filename;
            if ($model->delete()) {
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/archive/' . $page_id . '/' . $filename . '_240.mp4');
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/archive/' . $page_id . '/' . $filename . '_320.mp4');
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/archive/' . $page_id . '/' . $filename . '_480.mp4');
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/archive/' . $page_id . '/' . $filename . '.jpg');
            }

            $this->redirect(array('/video/videoArchive/update', 'id' => $page_id));

        }
    }
