<?php

class DefaultController extends LXAController {

    public $defaultAction = 'admin';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Video;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Video'])) {
            $model->attributes = $_POST['Video'];

            $model->filename = CUploadedFile::getInstance($model, 'filename');

            if ($model->filename !== null) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/' . $model->page_id . '/';
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $filename = time() . UrlTransliterate::cleanString($model->filename->name, '_') . '.' . $model->filename->getExtensionName();

                //ffmpeg encode

                if ($model->saveAs($path . $filename)) {
                    $model->filename = $filename;
                }
            }

            $model->filename = CUploadedFile::getInstance($model, 'subtitles');

            if ($model->filename !== null) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/' . $model->page_id . '/subtitles/';
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $subtfilename = time() . UrlTransliterate::cleanString($model->subtitles->name, '_') . '.' . $model->subtitles->getExtensionName();

                if ($model->saveAs($path . $subtfilename)) {
                    $model->subtitles = $subtfilename;
                }

                if (empty($model->subtatiles_lang_id)) {
                    $model->subtatiles_lang_id = $model->lang_id;
                }
            }


            if ($model->save()) {
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $oldFilename = $model->filename;
        $oldsubtitles = $model->subtitles;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Video'])) {
            $model->attributes = $_POST['Video'];

            $model->filename = CUploadedFile::getInstance($model, 'filename');

            if ($model->filename !== null) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/' . $model->page_id . '/';
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $filename = time() . UrlTransliterate::cleanString($model->filename->name, '_') . '.' . $model->filename->getExtensionName();

                if ($model->saveAs($path . $filename)) {
                    $model->filename = $filename;
                }
            } else {
                $model->filename = $oldFilename;
            }

            $model->filename = CUploadedFile::getInstance($model, 'subtitles');

            if ($model->filename !== null) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/' . $model->page_id . '/subtitles/';
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $subtfilename = time() . UrlTransliterate::cleanString($model->subtitles->name, '_') . '.' . $model->subtitles->getExtensionName();

                if ($model->saveAs($path . $subtfilename)) {
                    $model->subtitles = $subtfilename;
                }

                if (empty($model->subtatiles_lang_id)) {
                    $model->subtatiles_lang_id = $model->lang_id;
                }
            } else {
                $model->subtitles = $oldsubtitles;
            }


            if ($model->save()) {
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Video('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Video']))
            $model->attributes = $_GET['Video'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Video::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'video-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetAjaxCategories() {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, 'Bad request');
        }

        if ($_POST['Video']['module'] == 'pages') {
            $data = Categories::getTreeListItems(false, Langs::getLangCodeById((int)$_POST['Video']['lang_id']));
        } elseif ($_POST['Video']['module'] == 'programms') {
            $data = ProgrammsCategories::getTreeListItems(false, Langs::getLangCodeById((int)$_POST['Video']['lang_id']));
        } else {
            $data = array();
        }

        //$data = CHtml::listData($data, 'id', 'title');
        echo CHtml::tag('option',
            array('value' => 0), CHtml::encode('Выберите категорию'), true);
        foreach ($data as $value => $name) {
            echo CHtml::tag('option',
                array('value' => $value), CHtml::encode($name), true);
        }
    }

    public function actionGetAjaxPages() {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, 'Bad request');
        }

        if ($_POST['Video']['module'] == 'pages') {
            $data = Pages::model()->findAll('category_id=:category_id', array(
                ':category_id' => (int)$_POST['Video']['category_id']
            ));
        } elseif ($_POST['Video']['module'] == 'programms') {
            $data = ProgrammsContent::model()->findAll('category_id=:category_id', array(
                ':category_id' => (int)$_POST['Video']['category_id']
            ));
        } else {
            $data = array();
        }

        //$data = CHtml::listData($data, 'id', 'title');
        echo CHtml::tag('option',
            array('value' => 0), CHtml::encode('Выберите страницу'), true);
        foreach ($data as $value => $name) {
            echo CHtml::tag('option',
                array('value' => $value), CHtml::encode($name), true);
        }
    }
}
