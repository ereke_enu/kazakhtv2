<?php

class VideoAdvController extends LXAController {

    public $defaultAction = 'admin';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new VideoAdv;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['VideoAdv'])) {
            $model->attributes = $_POST['VideoAdv'];

            $model->filename = CUploadedFile::getInstance($model, 'filename');

            if ($model->filename !== null) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/adv/';

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $filename = 'video_adv_' . date('dmYHis', time());
                $fullName = $filename . '.' . $model->filename->getExtensionName();

                if ($model->filename->saveAs($path . $fullName)) {
                    $model->filename = $filename;

                    //Создание эскиза
                    //640*480
                    /*   exec('/usr/bin/ffmpeg  -i ' . $path . $fullName . ' -an -ss 00:00:' . rand(9, 12) . ' -an -r 1 -vframes 1 -s 640x480 -y -f mjpeg ' . $path . '/' . $filename . '.jpg');

                       // конвертация видео
                       $PATH = getenv("PATH");
                       $LD_LIBRARY_PATH = getenv("LD_LIBRARY_PATH");
                       $XUGGLE_HOME = "/usr/local/xuggler";
                       putenv("XUGGLE_HOME=$XUGGLE_HOME");
                       putenv("LD_LIBRARY_PATH=$XUGGLE_HOME/lib:$LD_LIBRARY_PATH");
                       putenv("PATH=$XUGGLE_HOME/bin:$PATH");

                       //240
                       exec("/usr/local/xuggler/bin/ffmpeg -y -i ". $path . $fullName ." -ab 96k  -ar 48000 -f mp4 -b 480k -r 25  ". $path . $filename ."_240.mp4");
                       //320
                       exec("/usr/local/xuggler/bin/ffmpeg -y -i ". $path . $fullName ." -ab 96k  -ar 48000 -f mp4 -b 640k -r 25  ". $path . $filename ."_320.mp4");
                       //480
                       exec("/usr/local/xuggler/bin/ffmpeg -y -i ". $path . $fullName ." -ab 96k  -ar 48000 -f mp4 -b 960k -r 25  ". $path . $filename ."_480.mp4");
                      */

                }

            }

            if ($model->save()) {
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $oldVideo = $model->filename;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['VideoAdv'])) {
            $model->attributes = $_POST['VideoAdv'];
            $model->filename = CUploadedFile::getInstance($model, 'filename');

            if ($model->filename !== null) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/adv/';

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $filename = 'video_adv_' . date('dmYHis', time());
                $fullName = $filename . '.' . $model->filename->getExtensionName();

                if ($model->filename->saveAs($path . $fullName)) {
                    $model->filename = $filename;

                    //Создание эскиза
                    //640*480
                    /*   exec('/usr/bin/ffmpeg  -i ' . $path . $fullName . ' -an -ss 00:00:' . rand(9, 12) . ' -an -r 1 -vframes 1 -s 640x480 -y -f mjpeg ' . $path . '/' . $filename . '.jpg');

                       // конвертация видео
                       $PATH = getenv("PATH");
                       $LD_LIBRARY_PATH = getenv("LD_LIBRARY_PATH");
                       $XUGGLE_HOME = "/usr/local/xuggler";
                       putenv("XUGGLE_HOME=$XUGGLE_HOME");
                       putenv("LD_LIBRARY_PATH=$XUGGLE_HOME/lib:$LD_LIBRARY_PATH");
                       putenv("PATH=$XUGGLE_HOME/bin:$PATH");

                       //240
                       exec("/usr/local/xuggler/bin/ffmpeg -y -i ". $path . $fullName ." -ab 96k  -ar 48000 -f mp4 -b 480k -r 25  ". $path . $filename ."_240.mp4");
                       //320
                       exec("/usr/local/xuggler/bin/ffmpeg -y -i ". $path . $fullName ." -ab 96k  -ar 48000 -f mp4 -b 640k -r 25  ". $path . $filename ."_320.mp4");
                       //480
                       exec("/usr/local/xuggler/bin/ffmpeg -y -i ". $path . $fullName ." -ab 96k  -ar 48000 -f mp4 -b 960k -r 25  ". $path . $filename ."_480.mp4");
                      */

                }

            } else {
                $model->filename = $oldVideo;
            }

            if ($model->save()) {
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new VideoAdv('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['VideoAdv']))
            $model->attributes = $_GET['VideoAdv'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = VideoAdv::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'video-adv-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
