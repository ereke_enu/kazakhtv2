<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>
<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'video-adv-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    )); ?>

    <?php echo $form->textFieldRow($model, 'title', array('class' => 'span8', 'maxlength' => 255)); ?>

    <div class="control-group">
        <label class="control-label required" for="videofile">Видео файл рекламы <span class="required">*</span></label>

        <div class="controls">
            <div class="input-append">
                <input id="videofile" class="input-large" readonly="readonly" type="text">
                <a class="btn" onclick="$('input[id=VideoAdv_filename]').click();">Обзор</a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('input[id=VideoAdv_filename]').change(function () {
                $('#videofile').val($(this).val());
            });
        });
    </script>
    <input type="file" id="VideoAdv_filename" name="VideoAdv[filename]" style="display:none;"
           maxlength="255" class="span8">

    <?php echo $form->checkBoxRow($model, 'is_old'); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>