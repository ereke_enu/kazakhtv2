<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Список видео архивов</span>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li><a href="<?=$this->createUrl('create');?>" class="">Создать видео архив</a></li>
        </ul>
       
    </div>
</div>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'bordered condensed stripped hover',
    'id' => 'video-archive-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
       array(
            'name' => 'timetable_id',
            'value' => '$data->timetable->title."(".$data->timetable->date." ".$data->timetable->time.")"'
        ),
        'title',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>
