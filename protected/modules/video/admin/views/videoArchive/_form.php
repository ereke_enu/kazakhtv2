<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>
<div class="well">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'video-archive-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
    )); ?>


    <?php echo $form->dropDownListRow($model, 'program_id', ProgrammsContent::getListActivePrograms(), array('class' => 'span8', 'prompt' => 'Выберите программу', 'ajax' => array(
        'url' => Yii::app()->createUrl('/programms/default/getAjaxListTimetable'),
        'type' => 'post',
        'update' => '#VideoArchive_timetable_id'
    ))); ?>

    <?php echo $form->dropDownListRow($model, 'timetable_id', $this->listTimetable, array('class' => 'span8', 'prompt' => 'Чтобы выбрать расписание, выберите программу')); ?>

    <?php echo $form->textFieldRow($model, 'title', array('class' => 'span8', 'maxlength' => 255)); ?>

    <?php echo $form->ckEditorRow($model, 'text', array('options' => CMap::mergeArray(array('fullpage' => 'js:true', 'width' => '640', 'resize_maxWidth' => '640', 'resize_minWidth' => '320'), Yii::app()->params['editorOptions']))); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>
    <?php $this->endWidget(); ?>
    <hr>
    <h4>Управление видео</h4>
    <?php if (!empty($model->videos)) { ?>
        <h5>Загруженные видео</h5>
        <table class="table table-bordered table-hovered table-condensed">
            <tr>
                <th>Файл</th>
                <th>Действия</th>
            </tr>
            <?php foreach ($model->videos as $video) { ?>
                <tr>
                    <td><?=$video->filename?></td>
                    <td><a class="btn btn-small" rel="tooltip" title="Удалить"
                           href="<?= $this->createUrl('/video/videoArchive/deleteVideo', array('id' => $video->id)) ?>"><i
                                class="icon-remove"></i></a></td>
                </tr>
            <?php } ?>

        </table>
    <?php } ?>


    <?php
    if (!$model->isNewRecord) {
        ?>
        <h5>Добавить новое видео</h5>
        <form method="post" action="<?= Yii::app()->createUrl('/video/videoArchive/uploadTest', array('id' => $model->id)); ?>" enctype="multipart/form-data">
            <input type="file" name="videofile" id="videofile"/>
            <button class="btn" type="submit">Загрузить</button>
        </form>
        <?php
//        $this->widget('ext.eAjaxUpload.EAjaxUpload',
//            array(
//                'id' => 'uploadFile',
//                'config' => array(
//                    'action' => Yii::app()->createUrl('/video/videoArchive/upload', array('id' => $model->id)),
//                    'allowedExtensions' => array('flv', 'mp4', 'avi', 'mpg'),
//                    'sizeLimit' => 1000 * 1024 * 1024, // maximum file size in bytes
//                    'onComplete' => "js:function(id, fileName, responseJSON){
//                     var resp = '';
//                     $.each(responseJSON, function(i, val) {
//                        resp = resp+i+':'+val;
//                     });
//                    alert('Файл загружен! '+resp);
//                    }",
//                    'messages' => array(
//                        'typeError' => "{file} запрещен к загрузке. Только файлы с типом {extensions} разрешены.",
//                        'sizeError' => "{file} слишком большой, максимальный размер {sizeLimit}.",
////                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
////                  'emptyError'=>"{file} is empty, please select files again without it.",
////                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
//                    ),
////'showMessage'=>"js:function(message){ alert(message); }"
//                )
//            ));
    } else {
        ?>
        <p>Для загрузки видео, нужно сохранить статью</p>
    <?php } ?>

</div>

