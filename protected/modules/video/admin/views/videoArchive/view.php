<?php
$this->breadcrumbs=array(
	'Video Archives'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List VideoArchive','url'=>array('index')),
	array('label'=>'Create VideoArchive','url'=>array('create')),
	array('label'=>'Update VideoArchive','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete VideoArchive','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage VideoArchive','url'=>array('admin')),
);
?>

<h1>View VideoArchive #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'timetable_id',
		'title',
		'text',
	),
)); ?>
