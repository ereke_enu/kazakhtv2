<?php
$this->breadcrumbs=array(
	'Video Archives',
);

$this->menu=array(
	array('label'=>'Create VideoArchive','url'=>array('create')),
	array('label'=>'Manage VideoArchive','url'=>array('admin')),
);
?>

<h1>Video Archives</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
