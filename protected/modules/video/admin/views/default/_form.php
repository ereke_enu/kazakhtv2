<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>
<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'video-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    )); ?>

    <div class="control-group">
        <?=$form->labelEx($model, 'module', array('class' => 'control-label'))?>
        <div class="controls">
            <div class="input-append">
                <?=$model->module?>
            </div>
        </div>
    </div>

    <div class="control-group">
        <?=$form->labelEx($model, 'page_id', array('class' => 'control-label'))?>
        <div class="controls">
            <div class="input-append">
                <?php if ($model->module == 'programms') {
                    echo $model->ProgrammPage->title;
                } else {
                    echo $model->PagesPage->title;

                }                 ?>
            </div>
        </div>
    </div>

    <?php echo $form->textFieldRow($model, 'title', array('class' => 'span8', 'maxlength' => 255)); ?>

    <div class="control-group">
        <label class="control-label" for="videofile">Файл субтитров (SRT, SSA/ASS)</label>

        <div class="controls">
            <div class="input-append">
                <input id="subtitle" class="input-large" readonly="readonly" type="text">
                <a class="btn" onclick="$('input[id=Video_subtitles]').click();">Обзор</a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('input[id=Video_subtitles]').change(function () {
                $('#subtitle').val($(this).val());
            });
        });
    </script>
    <input type="file" id="Video_subtitles" name="Video[subtitles]" style="display:none;"
           maxlength="255"
           class="span8">

    <?php echo $form->dropDownListRow($model, 'video_lang_id', Langs::getListItems(), array('class' => 'span8', 'prompt' => 'Выберите язык видео')); ?>

    <?php echo $form->dropDownListRow($model, 'subtatiles_lang_id', Langs::getListItems(), array('class' => 'span8', 'prompt' => 'Выберите язык субтитров')); ?>

    <?php echo $form->dropDownListRow($model, 'adv_preroll_id', array(), array('class' => 'span8')); ?>

    <?php echo $form->dropDownListRow($model, 'adv_midroll_id', array(), array('class' => 'span8')); ?>

    <?php echo $form->dropDownListRow($model, 'adv_postroll_id', array(), array('class' => 'span8')); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>