
<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Настройки видео</span>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'video-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'module',
		'page.title',
		'title',
        'videolang.title',
        /*
        'subtitles',
        'video_lang_id',
        'subtatiles_lang_id',
        'adv_preroll_id',
        'adv_midroll_id',
        'adv_postroll_id',
        'date_create',
        */
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
