<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Список разделов</span>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li><a href="<?=$this->createUrl('create');?>" class="">Создать раздел</a></li>
        </ul>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'sections-grid',
    'type' => 'bordered condensed stripped',
    'dataProvider' => $model->search(),
    'enableSorting' => false,
    'htmlOptions' => array(
        'class' => 'table-hover'
    ),
    'columns' => array(
        'id',
        'title',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}',
            'updateButtonOptions' => array(
                'class' => 'btn btn-small update'
            ),
            'deleteButtonOptions' => array(
                'class' => 'btn btn-small delete'
            )
        ),
    ),
    'htmlOptions' => array(
        'class' => 'table-hover'
    )

)); ?>
