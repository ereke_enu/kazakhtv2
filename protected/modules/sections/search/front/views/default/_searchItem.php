<tr>
    <td>
        <?php if ($data instanceof ProgrammsContent) { ?>
            <strong><a
                    href="<?=$this->createUrl('/programms/default/program', array('#' => 'page', 'sefname' => $data->sefname))?>">
                    <?php echo $data->title?>
                </a></strong>
            <br/><?php echo $data->text ?>
        <?php } else { ?>
            <strong><a
                    href="<?=$this->createUrl('/pages/default/view', array('#' => 'page', 'sefname' => $data->sefname, 'category' => $data->category->sefname))?>">
                    <?php echo $data->title?>
                </a></strong>
            <br/><?php echo $data->text ?>
        <?php } ?>
    </td>
</tr>
<tr>
    <td>
        <hr>
    </td>
</tr>