<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 02.07.14
 * Time: 18:46
 */

class AppController extends LXController {

	public $layout = '//layouts/search';

	public function actionIndex($query = '', $type = 1) {
		$query = strip_tags($query);

		if ($query != '' || strlen($query) > 3) {
			if ($type == 1) {
				$criteria = new CDbCriteria;
				$criteria->addCondition('`text` LIKE :query OR `title` LIKE :query1 OR short_text LIKE :query2');
				$criteria->params[':query'] = '%' . $query . '%';
				$criteria->params[':query1'] = '%' . $query . '%';
				$criteria->params[':query2'] = '%' . $query . '%';
				$criteria->compare('lang_id', $this->getLangId());
				$criteria->compare('active', '1');

//				$criteria->order = 'date DESC, time DESC';
//				$programModel = ProgrammsContent::getSearchPages($criteria, $query);

				$criteria->order = 'date DESC, time DESC';

				$searchCount = count(Pages::getSearchPages($criteria, $query));

				$pagination = new CPagination($searchCount);
				$pagination->pageSize = 10;
				$pagination->applyLimit($criteria, $query);

				$model = Pages::getSearchPages($criteria, $query);

//				$searchDataProvider = new CArrayDataProvider(CMap::mergeArray($model, $programModel), array(
//					'pagination' => array('pageSize' => 10)
//				));

				$query = htmlspecialchars($query, ENT_QUOTES);

				if (empty($model)) {
					$this->render('noResults', array('query' => $query));
				} else {
					$this->render('results', array('query' => $query, 'results' => $model, 'pagination'=>$pagination));
				}
			} else {
				$this->redirect($this->createUrl('/shop/default/search/', array('ShopSearchForm[title]' => $query)));
			}
		} else {
			$this->render('searchForm', array('query' => $query));
		}
	}
} 