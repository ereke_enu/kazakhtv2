<div id="searchForm">
	<form action="<?=$this->createUrl('/search/app/index')?>">
		<div class="input-group">
			<span class="input-group-btn">
				<button type="submit" class="btn"><i class="glyphicon glyphicon-search"></i></button>
			</span>
			<input type="search" name="query" class="form-control" value="<?=$query?>">
		</div>
	</form>
</div>

<article id="text" class="searchResult">
	<ul>
		<?php $date = 0; foreach($results as $data) { ?>
			<?php if($date != $data->date) { $date = $data->date ?>
			<li class="date"><?= date ( 'j.m.Y', strtotime($data->date  ) ) ?></li>
			<?php } ?>
			<?php $this->renderPartial('_searchItem', array('data'=>$data)); ?>
		<?php } ?>
	</ul>
</article>

<?php $this->widget('CLinkPager', array(
	'pages' => $pagination,
	'header' => false,
	'nextPageLabel' => '&raquo;',
	'prevPageLabel' => '&laquo;',
	'maxButtonCount' => 0,
	'htmlOptions' => array(
		'id' => 'pagination',
		'class' => 'pager'
	)
))?>