<div id="searchForm">
	<form action="<?=$this->createUrl('/search/app/index')?>">
		<div class="input-group">
			<span class="input-group-btn">
				<button type="submit" class="btn"><i class="glyphicon glyphicon-search"></i></button>
			</span>
			<input type="search" name="query" class="form-control" value="<?=$query?>">
		</div>
	</form>
</div>

<div id="noResults">
	<?= Yii::t('search', 'Нет результатов'); ?>
</div>