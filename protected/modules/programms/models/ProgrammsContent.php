<?php

    /**
     * This is the model class for table "{{programms_content}}".
     *
     * The followings are the available columns in table '{{programms_content}}':
     * @property integer $id
     * @property integer $category_id
     * @property integer $lang_id
     * @property string $title
     * @property string $short_text
     * @property string $text
     * @property string $meta_tags
     * @property string $meta_description
     * @property string $sefname
     * @property integer $views
     * @property integer $active
     * @property string $background
     * @property integer $show_comments
     * @property integer $show_icons
     * @property string $date_create
     */
    class ProgrammsContent extends CActiveRecord {
        public $tags;
        public $delete_background;
        public $delete_image = 0;
        public $videofile;

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return ProgrammsContent the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{programms_content}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('title, category_id', 'required'),
                array('user_id, delete_background, category_id, lang_id, views, active, show_comments, show_icons', 'numerical', 'integerOnly' => true),
                array('tags, other_id, title, meta_tags, meta_description, sefname, background', 'length', 'max' => 255),
                array('short_text, text, date_create', 'safe'),
                array('image', 'file', 'types' => 'jpg,png,gif,jpeg', 'allowEmpty' => true),
                array('main_image', 'file', 'types' => 'jpg,png,gif,jpeg', 'allowEmpty' => true),
                array('videofile', 'file', 'types' => 'mp4,avi,flv', 'allowEmpty' => true),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, category_id, lang_id, title, short_text, text, meta_tags, meta_description, sefname, views, active, background, show_comments, show_icons, date_create', 'safe', 'on' => 'search'),
            );
        }

        public function beforeValidate() {

            if ($this->meta_description == '') {
                $this->meta_description = $this->title;
            }

            if ($this->meta_tags == '') {
                $this->meta_tags = $this->title;
                $this->meta_tags = str_replace(' ', ', ', str_replace(',', '', $this->meta_tags));
            }

            if ($this->delete_background == 1) {
                $this->background = '';
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/branding/programms/' . $this->background)) {
                    @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/branding/programms/' . $this->background);
                }
            }

            return parent::beforeValidate();
        }

        public function afterSave() {
            $criteria = new CDbCriteria;
            $criteria->compare('program_id', $this->id);
            ProgrammsTags::model()->deleteAll($criteria);
            if (trim($this->tags) != '') {
                $tags = explode(',', $this->tags);
                if (!empty($tags)) {
                    foreach ($tags as $tag) {
                        $tag = Tags::model()->findByTagAndCreate($tag);
                        $tagId = $tag->id;

                        $tagModel = new ProgrammsTags();
                        $tagModel->program_id = $this->id;
                        $tagModel->tag_id = $tagId;
                        $tagModel->save();
                    }
                }
            }


            return parent::afterSave();
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'category' => array(self::BELONGS_TO, 'ProgrammsCategories', 'category_id'),
                'lang'     => array(self::BELONGS_TO, 'Langs', 'lang_id'),
                'listTags' => array(self::MANY_MANY, 'ProgrammsTags', '{{programms_tags}}(program_id, tag_id)'),
                'videos'   => array(self::HAS_MANY, 'Video', 'page_id', 'condition' => 'module="programms"', 'order' => 'videos.id DESC')
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'                => 'ID',
                'category_id'       => 'Категория',
                'lang_id'           => 'Язык',
                'title'             => 'Заголовок',
                'short_text'        => 'Краткое описание',
                'text'              => 'Текст',
                'meta_tags'         => 'Meta Tags',
                'meta_description'  => 'Meta Description',
                'sefname'           => 'ЧПУ',
                'views'             => 'Views',
                'active'            => 'Активность',
                'background'        => 'Бренд фон',
                'show_comments'     => 'Показывать комментарии',
                'show_icons'        => 'Показывать иконки соц. сетей',
                'date_create'       => 'Дата',
                'tags'              => 'Теги, через запятую',
                'delete_background' => 'Удалить фон',
                'image'             => 'Изображение',
                'main_image'        => 'Изображение для главной',
                'videofile'         => 'Видео файл',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('category_id', $this->category_id);
            $criteria->compare('lang_id', $this->lang_id);
            $criteria->compare('title', $this->title, true);
            $criteria->compare('short_text', $this->short_text, true);
            $criteria->compare('text', $this->text, true);
            $criteria->compare('meta_tags', $this->meta_tags, true);
            $criteria->compare('meta_description', $this->meta_description, true);
            $criteria->compare('sefname', $this->sefname, true);
            $criteria->compare('views', $this->views);
            $criteria->compare('active', $this->active);
            $criteria->compare('background', $this->background, true);
            $criteria->compare('show_comments', $this->show_comments);
            $criteria->compare('show_icons', $this->show_icons);
            $criteria->compare('date_create', $this->date_create, true);

            $criteria->order = 'id DESC';

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }

        public static function getListItems() {
            $models = ProgrammsContent::model()->findAll('active=1');
            $items = array();
            foreach ($models as $item) {
                $items[$item->id] = '(' . $item->lang->code . ') ' . $item->title;
            }

            return $items;
        }

        public function findBySefname($sefname) {
            $criteria = new CDbCriteria;
            $criteria->compare('sefname', $sefname);
            $criteria->compare('active', 1);

            $model = ProgrammsContent::model()->find($criteria);

            return $model;
        }

        public static function getAnounceImg($filename, $prefix='', $default='/themes/front/images/noimage2.jpg') {

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/' . $prefix . $filename) && is_file($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/' . $prefix . $filename)) {
                return '/upload/programms/' . $prefix . $filename;
            } else {
                return $default;
            }
        }

        public static function getMainImg($filename) {

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/main/' . $filename) && is_file($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/main/' . $filename)) {
                return '/upload/programms/main/' . $filename;
            } else {
                return '/themes/front/images/noimage2.jpg';
            }
        }

        public function active($val = true) {
            /*  $val = (int)$val;
              $owner = $this->getOwner();
              $criteria = $owner->getDbCriteria();
              $alias = $owner->getTableAlias();

              $criteria->mergeWith(array(
                  'condition' => $alias . '.active=' . $val,
              ));   */

            return $this;
        }

        public static function getSearchPages(CDbCriteria $criteria, $searchString) {

            $pages = self::model()->together()->findAll($criteria);

            foreach ($pages as &$page) {
                $_value = trim(strip_tags($page->text));
                $_temps = preg_split("'[,.?!:]'", $_value);

                $_value = '';
                $it = 0;
                foreach ($_temps as $_temp) {
                    $_temp = trim($_temp);
                    if (preg_match("'(^|[^\xD0-\xD3][^0-9a-zA-Z])(" . $searchString . ")'s", $_temp)) {
                        $str_pu = strpos($_temp, $searchString);
                        $it++;
                        $_value .= "..." . (substr($_temp, 0, $str_pu) . "<span style='color: #00202e; font-weight: bold;'>" . $searchString . "</span>" . substr($_temp, $str_pu + strlen($searchString))) . "...<br>\n";

                        // $_value .= "..." . (preg_replace("'(^|\s|[^\xD0-\xD3][^0-9a-zA-Z])(" . $searchString . ")'s", "\\1<span style='color: #406E9C; font-weight: bold;'>\\2</span>", $_temp)) . "...<br>\n";
                    }
                    if ($it == 5) break;
                }
                $page->text = $_value;

                if (preg_match("'(^|[^\xD0-\xD3][^0-9a-zA-Z])(" . $searchString . ")'s", $page->title)) {
                    $page->title = preg_replace("'(^|\s|[^\xD0-\xD3][^0-9a-zA-Z])(" . $searchString . ")'s", "\\1<span style='color: #00202e; font-weight: bold;'>\\2</span>", $page->title);
                }
            }

            return $pages;
        }

        public static function getListActivePrograms() {
            $model = self::model()->with('lang')->cache(300)->findAll('active=1');

            return CHtml::listData($model, 'id', 'title', 'lang.title');

        }
    }