<?php

    /**
     * This is the model class for table "{{programms_timetable}}".
     *
     * The followings are the available columns in table '{{programms_timetable}}':
     * @property integer $id
     * @property integer $programm_id
     * @property string $title
     * @property string $description
     * @property string $date
     * @property string $time
     * @property integer $active
     * @property string $trans
     * @property integer $is_show
     */
    class ProgrammsTimetable extends CActiveRecord {
        private static $_onAirProgram = null;

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return ProgrammsTimetable the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{programms_timetable}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('title, date, time', 'required'),
                array('date+time', 'application.extensions.uniqueMultiColumnValidator', 'caseSensitive' => false, 'message' => 'Данное время уже занято'),
                array('in_anounce, lang_id, programm_id, active, is_show', 'numerical', 'integerOnly' => true),
                array('title', 'length', 'max' => 255),
                array('trans', 'length', 'max' => 1),
                array('description, date, time', 'safe'),
                array('image', 'file', 'types' => 'jpg,png,gif', 'allowEmpty' => true),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, programm_id, title, description, date, time, active, trans, is_show', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'content' => array(self::BELONGS_TO, 'ProgrammsContent', 'programm_id')
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'          => 'ID',
                'programm_id' => 'Привязка к программе',
                'title'       => 'Заголовок',
                'description' => 'Описание',
                'date'        => 'Дата',
                'time'        => 'Время',
                'active'      => 'Активность',
                'trans'       => 'Трансляция',
                'is_show'     => 'Сейчас в эфире',
                'lang_id'     => 'Язык',
                'in_anounce'  => 'Вывести программу в блок анонса',
                'image'       => 'Изображение для анонса'
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('programm_id', $this->programm_id);
            $criteria->compare('title', $this->title, true);
            $criteria->compare('description', $this->description, true);
            $criteria->compare('date', $this->date, true);
            $criteria->compare('time', $this->time, true);
            $criteria->compare('active', $this->active);
            $criteria->compare('trans', $this->trans, true);
            $criteria->compare('is_show', $this->is_show);

            $criteria->order = 'date DESC, time DESC';

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }

        public static function getOnAirProgram() {
            if (self::$_onAirProgram === null) {
                $criteria = new CDbCriteria;
                $criteria->compare('active', '1');
                $criteria->compare('date', date('Y-m-d', time()));
                $criteria->compare('time', '<=' . date('H:i:00', time()));
                $criteria->order = 'time DESC';

                $onAirProgram = ProgrammsTimetable::model()->find($criteria);
                if ($onAirProgram !== null) {
                    ProgrammsTimetable::model()->updateAll(array('is_show' => 0));
                    $onAirProgram->is_show = 1;
                    $onAirProgram->save();
                }

                self::$_onAirProgram = $onAirProgram;
            }

            return self::$_onAirProgram;
        }

        public static function getImage($image, $pid) {
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/upload/timetable/' . $pid . '/' . $image) && is_file($_SERVER['DOCUMENT_ROOT'] . '/upload/timetable/' . $pid . '/' . $image)) {
                return '/upload/timetable/' . $pid . '/' . $image;
            }

            return false;
        }
    }