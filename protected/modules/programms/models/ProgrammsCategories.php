<?php

/**
 * This is the model class for table "{{programms_categories}}".
 *
 * The followings are the available columns in table '{{programms_categories}}':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 * @property string $sefname
 * @property integer $active
 * @property string $background
 * @property integer $show_icons
 * @property integer $show_comments
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 */
class ProgrammsCategories extends CActiveRecord {
    public $delete_image;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ProgrammsCategories the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{programms_categories}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, sefname, lang_id', 'required'),
            array('parent_id, lang_id, active, show_icons, show_comments', 'numerical', 'integerOnly' => true),
            array('title, sefname, background', 'length', 'max' => 255),
            array('description', 'safe'),
            array('background', 'file', 'types' => 'jpg,png,gif,jpeg', 'allowEmpty' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, parent_id, lang_id, title, description, sefname, active, background, show_icons, show_comments, root, lft, rgt, level', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lang' => array(self::BELONGS_TO, 'Langs', 'lang_id'),
            'child' => array(self::HAS_MANY, 'ProgrammsCategories', 'parent_id'),
            'parent' => array(self::BELONGS_TO, 'ProgrammsCategories', 'parent_id'),
	        'programs' => array(self::HAS_MANY, 'ProgrammsContent', 'category_id', 'condition'=>'active=1', 'order'=>'date_create DESC'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parent_id' => 'Родитель',
            'lang_id' => 'Язык',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'sefname' => 'ЧПУ',
            'active' => 'Активность',
            'background' => 'Бренд фон',
            'show_icons' => 'Показывать иконки соц. сетей',
            'show_comments' => 'Включить комментарии',
            'root' => 'Root',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'level' => 'Level',
            'delete_image' => 'Удалить бренд фон',

        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('lang_id', $this->lang_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('sefname', $this->sefname, true);
        $criteria->compare('active', $this->active);
        $criteria->compare('background', $this->background, true);
        $criteria->compare('show_icons', $this->show_icons);
        $criteria->compare('show_comments', $this->show_comments);
        $criteria->compare('root', $this->root);
        $criteria->compare('lft', $this->lft);
        $criteria->compare('rgt', $this->rgt);
        $criteria->compare('level', $this->level);

        $criteria->order = $this->tree->hasManyRoots
            ? $this->tree->rootAttribute . ', ' . $this->tree->leftAttribute
            : $this->tree->leftAttribute;

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 99999
            )
        ));
    }

    public function behaviors() {
        return array(
            'tree' => array(
                'class' => 'ext.nestedset.NestedSetBehavior',
                // хранить ли множество деревьев в одной таблице
                'hasManyRoots' => true,
                // поле для хранения идентификатора дерева при $hasManyRoots=false; не используется
                'rootAttribute' => 'root',
                // обязательные поля для NS
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
            ),
            /* 'treeView' => array(
                 'class' => 'ext.nestedset.TreeViewTreebehavior'
             ),*/
            'treeArray' => array(
                'class' => 'ext.nestedset.ArrayTreeBehavior'
            )

        );
    }

    public static function getTreeListItems($withRoot = false, $lang = false) {
        $criteria = new CDbCriteria;
        if ($lang !== false) {
            $criteria->compare('lang_id', Langs::getLangIdByCode($lang));
        }
        $categoryData = self::model()->findAll($criteria);
        $categoryDataTree = self::model()->dbResultToForest($categoryData, 'id', 'parent_id', 'title');
        $categoryDataSelect = self::model()->converTreeArrayToSelect($categoryDataTree, 0, $withRoot);
        return CHtml::listData($categoryDataSelect, 'id', 'name');
    }

    /**
     * Build heriarhal result from DB Query result.
     * db result must conist id, parent_id, value
     *
     * @param Object $rows
     * @param string $idName name of id key in result query
     * @param string $pidName name of parent id for query result
     * @param string $labelName name of value field in query result
     * @return array heriarhical tree
     */
    public function dbResultToForest($rows, $idName, $pidName, $labelName = 'label') {
        $totalArray = array();
        $children = array(); // children of each ID
        $ids = array();
        $k = 0;
        // Collect who are children of whom.
        foreach ($rows as $i => $r) {
            $element = array('id' => $rows[$i][$idName], 'parent_id' => $rows[$i][$pidName], 'value' => $rows[$i][$labelName]);
            $totalArray[$k++] = $element;
            $row = & $totalArray[$k - 1];
            $id = $row['id'];
            if ($id === null) {
                // Rows without an ID are totally invalid and makes the result tree to
                // be empty (because PARENT_ID = null means "a root of the tree"). So
                // skip them totally.
                continue;
            }
            $pid = $row['parent_id'];
            if ($id == $pid) {
                $pid = null;
            }
            $children[$pid][$id] =& $row;
            if (!isset($children[$id])) {
                $children[$id] = array();
            }
            $row['childNodes'] = & $children[$id];
            $ids[$id] = true;
        }

        // Root elements are elements with non-found PIDs.
        $forest = array();
        foreach ($totalArray as $i => $r) {
            $row = & $totalArray[$i];
            $id = $row['id'];
            $pid = $row['parent_id'];
            if ($pid == $id) $pid = null;
            if (!isset($ids[$pid])) {
                $forest[$row[$idName]] =& $row;
            }
        }
        return $forest;
    }

    /**
     * Recursive function converting tree like array to single array with
     * delimiter. Such type of array used for generate drop down box
     *
     * @param array $data data of tree like
     * @param int $level current level of recursive function
     * @return array converted array
     */
    public function converTreeArrayToSelect($data, $level = 0, $withRoot = false) {
        if ($withRoot) {
            $returnArray[0] = array('name' => 'Корневая категория', 'id' => '0');
        }

        foreach ($data as $item) {
            $subitems = array();
            $elementName = "|" . str_repeat("--", $level * 2) . " " . $item['value'];
            $returnItem = array('name' => $elementName, 'id' => $item['id']);
            if ($item['childNodes']) {
                $subitems = $this->converTreeArrayToSelect($item['childNodes'], $level + 1);
            }

            $returnArray[] = $returnItem;

            if ($subitems != array()) {
                $returnArray = array_merge($returnArray, $subitems);
            }

        }
        return empty($returnArray) ? array() : $returnArray;
    }

    public function findBySefname($sefname) {

        $lang_id = Langs::getLangIdByCode(Yii::app()->language);
        $model = ProgrammsCategories::model()->find('sefname=:sefname AND lang_id=:lang_id', array(':sefname' => $sefname, ':lang_id' => $lang_id));

        return $model;
    }

    public function findById($id){
        $model = ProgrammsCategories::model()->find('id=:id', array(':id' => $id ));
        return $model;
    }
}