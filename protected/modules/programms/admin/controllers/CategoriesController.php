<?php

class CategoriesController extends LXAController {

    public $defaultAction = 'admin';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new ProgrammsCategories;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ProgrammsCategories'])) {
            $model->attributes = $_POST['ProgrammsCategories'];
            $model->background = CUploadedFile::getInstance($model, 'background');
            if ($model->background !== null) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/branding/programms/';
                $filename = md5($model->background->getName() . time()) . '.' . $model->background->getExtensionName();

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                if ($model->background->saveAs($path . $filename)) {
                    $model->background = $filename;
                }
            } else {
                $model->background = '';
            }

            if ($model->parent_id != 0) {
                $rootModel = ProgrammsCategories::model()->findByPk($model->parent_id);
                $model->lang_id = $rootModel->lang_id;
                if ($model->appendTo($rootModel)) {
                    $this->redirect(array('admin'));
                }
            } else {
                if ($model->saveNode()) {
                    $this->redirect(array('admin'));
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $oldParentId = $model->parent_id;
        $oldBackground = $model->background;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ProgrammsCategories'])) {
            $model->attributes = $_POST['ProgrammsCategories'];
            $model->background = CUploadedFile::getInstance($model, 'background');
            if ($model->background !== null) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/branding/';
                $filename = md5($model->background->getName() . time()) . '.' . $model->background->getExtensionName();

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                if ($model->background->saveAs($path . $filename)) {
                    $model->background = $filename;
                } else {
                    $model->background = $oldBackground;
                }
            }

            if ($model->delete_image == 1) {
                $model->background = '';
            }

            $parentCategory = ProgrammsCategories::model()->findByPk($model->parent_id);

            if ($model->parent_id != 0) {
                $parentLanguageId = $parentCategory->lang_id;
                $model->lang_id = $parentLanguageId;
            }

            if ($model->saveNode()) {
                if ($oldParentId != $model->parent_id && $model->parent_id != 0) {
                    $model->moveAsLast($parentCategory);
                } else if ($oldParentId != $model->parent_id && $model->parent_id == 0) {
                    $model->moveAsRoot();
                }
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->deleteNode();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($lang = 'ru') {
        $model = new ProgrammsCategories('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['ProgrammsCategories']))
            $model->attributes = $_GET['ProgrammsCategories'];
        $model->lang_id = Langs::getLangIdByCode($lang);

        $this->render('admin', array(
            'model' => $model,
            'lang' => $lang
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = ProgrammsCategories::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'programms-categories-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionMove($id, $type) {
        $model = ProgrammsCategories::model()->findByPk((int)$id);

        if ($model === null) {
            throw new CHttpException(404, 'Страница не найдена');
        }

        if (!$model->isRoot()) {
            if ($type == 'up') {
                $prev = $model->prev()->find();
                if ($prev !== null) {
                    $model->moveBefore($prev);
                }
            } else {
                $next = $model->next()->find();
                if ($next !== null) {
                    $model->moveAfter($next);
                }
            }
        } else {
            $curPos = $model->root;
            if ($type == 'up') {
                $command = Yii::app()->db->createCommand("SELECT id, root FROM {{programms_categories}} WHERE lang_id=:lang AND root<:root ORDER BY root ASC");
                $command->bindParam(':lang', $model->lang_id, PDO::PARAM_INT);
                $command->bindParam(':root', $model->root, PDO::PARAM_INT);
                $command->bindParam(':section_id', $model->section_id, PDO::PARAM_INT);

                $data = $command->query();
                $data = $data->read();

                if ($data !== false) {
                    $allDescendantsCurModel = $model->descendants()->findAll();

                    foreach ($allDescendantsCurModel as $curItem) {
                        $curItem->root = $data['root'];
                        $curItem->saveNode();
                    }

                    $model->root = $data['root'];
                    $model->saveNode();

                    $prevModel = ProgrammsCategories::model()->findByPk($data['id']);
                    $allDescendantsPrevModel = $prevModel->descendants()->findAll();

                    foreach ($allDescendantsPrevModel as $prevItem) {
                        $prevItem->root = $curPos;
                        $prevItem->saveNode();
                        echo $prevItem->id . '<br>';
                    }

                    $prevModel->root = $curPos;
                    $prevModel->saveNode();
                }

            } else {
                $command = Yii::app()->db->createCommand("SELECT id, root FROM {{programms_categories}} WHERE lang_id=:lang AND root>:root ORDER BY root ASC");
                $command->bindParam(':lang', $model->lang_id, PDO::PARAM_INT);
                $command->bindParam(':root', $model->root, PDO::PARAM_INT);
                $command->bindParam(':section_id', $model->section_id, PDO::PARAM_INT);

                $data = $command->query();
                $data = $data->read();

                if ($data !== false) {
                    $allDescendantsCurModel = $model->descendants()->findAll();

                    foreach ($allDescendantsCurModel as $curItem) {
                        $curItem->root = $data['root'];
                        $curItem->saveNode();
                    }

                    $model->root = $data['root'];
                    $model->saveNode();

                    $prevModel = ProgrammsCategories::model()->findByPk($data['id']);
                    $allDescendantsPrevModel = $prevModel->descendants()->findAll();

                    foreach ($allDescendantsPrevModel as $prevItem) {
                        $prevItem->root = $curPos;
                        $prevItem->saveNode();
                        echo $prevItem->id . '<br>';
                    }

                    $prevModel->root = $curPos;
                    $prevModel->saveNode();
                }
            }
        }
        $this->redirect(array('admin', 'lang' => $model->lang->code));
    }
}
