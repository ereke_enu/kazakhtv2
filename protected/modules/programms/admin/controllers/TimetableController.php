<?php

    class TimetableController extends LXAController {

        public $defaultAction = 'admin';

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate() {
            $model = new ProgrammsTimetable;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['ProgrammsTimetable'])) {
                foreach ($_POST['ProgrammsTimetable']['date'] as $i => $item) {
                    $model = new ProgrammsTimetable;
                    $model->attributes = $_POST['ProgrammsTimetable'];
                    $model->date = $item;
                    $model->time = $_POST['ProgrammsTimetable']['time'][$i];
                    $model->save();
                }
                $this->redirect(array('admin'));
            }

            $this->render('create', array(
                'model' => $model,
            ));
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
            $model = $this->loadModel($id);
            $oldImage = $model->image;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['ProgrammsTimetable'])) {
                $model->attributes = $_POST['ProgrammsTimetable'];

                $model->image = CUploadedFile::getInstance($model, 'image');
                if ($model->image !== null) {
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/timetable/' . $model->programm_id . '/';

                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }

                    $filename = md5($model->image->name . time()) . '.' . $model->image->extensionName;

                    if ($model->image->saveAs($path . $filename)) {
                        $model->image = $filename;
                    }
                } else {
                    $model->image = $oldImage;
                }

                if ($model->save()) {
                    $this->redirect(array('admin'));
                }
            }

            $this->render('update', array(
                'model' => $model,
            ));
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            } else
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }

        /**
         * Manages all models.
         */
        public function actionAdmin() {
            $model = new ProgrammsTimetable('search');
            $model->unsetAttributes(); // clear any default values
            if (isset($_GET['ProgrammsTimetable']))
                $model->attributes = $_GET['ProgrammsTimetable'];

            $this->render('admin', array(
                'model' => $model,
            ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer the ID of the model to be loaded
         */
        public function loadModel($id) {
            $model = ProgrammsTimetable::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');

            return $model;
        }

        /**
         * Performs the AJAX validation.
         * @param CModel the model to be validated
         */
        protected function performAjaxValidation($model) {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'programms-timetable-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }

        public function actionImport() {

            if (Yii::app()->request->isPostRequest) {
                $file = CUploadedFile::getInstanceByName('excelimport');
                if ($file !== null) {
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/import/xlsx/';
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $filename = date('Y-m-d', time()) . '_excel.xlsx';
                    if ($file->saveAs($path . $filename)) {
                        Yii::import('ext.SimpleXLSX');

                        $excel = new SimpleXLSX($path . $filename);
                        $rows = $excel->rows();

                        foreach ($rows as $key => $cell) {
                            if ($key == 0) {
                                continue;
                            }

                            $datetime = $cell[0] + $cell[1];
                            $date = date('Y-m-d', $this->xls2tstamp($datetime));
                            $time = date('H:i:00', $this->xls2tstamp($datetime));

                            try {
                                Yii::app()->db->createCommand("INSERT INTO {{programms_timetable}} SET programm_id=:programm_id, title=:title, `date`=:date, `time`=:time, active='1', trans=:trans")
                                    ->execute(array(
                                        ':programm_id' => (int)$cell[3],
                                        ':title'       => $cell[2],
                                        ':date'        => $date,
                                        ':time'        => $time,
                                        ':trans'       => (int)$cell[5]
                                    ));
                            } catch (CDbException $e) {
                                echo $e;
                                continue;
                            }

                        }


                    }

                }
                $this->redirect(array('admin'));
            } else {
                $this->render('import');
            }
        }

        private function xls2tstamp($date) {
            return ((($date > 25568) ? $date : 25569) * 86400) - ((70 * 365 + 19) * 86400) - 21600 /*Hour line +6*/
                ;
        }
    }
