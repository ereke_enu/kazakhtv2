<?php

    class DefaultController extends LXAController {

        public $defaultAction = 'admin';

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate() {
            $model = new ProgrammsContent;

            $error = false;

            if (isset($_POST['ProgrammsContent'])) {
                $model->attributes = $_POST['ProgrammsContent'];
                $model->image = CUploadedFile::getInstance($model, 'image');

                if ($model->image !== null) {
                    $imgfilename = md5($model->image->name) . '.' . $model->image->extensionName;
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/programms/';

                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }

                    if ($model->image->saveAs($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/' . $imgfilename)) {
                        Yii::import('application.extensions.image.Image');
                        $image = new Image($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/' . $imgfilename);
                        $image->resize(275, 200, Image::WIDTH);
                        $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/' . $imgfilename);
                        $image->resize(145, 200, Image::WIDTH);
                        $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/145_' . $imgfilename);
                        $image->resize(104, 100, Image::WIDTH);
                        $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/104_' . $imgfilename);
                    } else {
                        $imgfilename = '';
                    }
                }


                $langIds = array();
                foreach ($_POST['ProgrammsContent']['langs'] as $lang => $data) {
                    $model = new ProgrammsContent;
                    $model->attributes = $data;

                    $model->background = CUploadedFile::getInstance($model, '[langs][' . $lang . ']background');
                    $model->main_image = CUploadedFile::getInstance($model, '[langs][' . $lang . ']main_image');


                    if ($model->background !== null) {
                        $filename = md5($model->background->name . time()) . '.' . $model->background->extensionName;
                        $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/branding/programms/';

                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }

                        if ($model->background->saveAs($path . $filename)) {
                            $model->background = $filename;
                        }
                    }

                    if ($model->main_image !== null) {
                        $filename = md5($model->main_image->name . time()) . '.' . $model->main_image->extensionName;
                        $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/programms/main/';

                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }

                        if ($model->main_image->saveAs($path . $filename)) {
                            $model->main_image = $filename;
                        }
                    }
                    $model->user_id = Yii::app()->user->id;
                    $model->lang_id = Langs::getLangIdByCode($lang);
                    $model->image = $imgfilename;
                    $model->date_create = new CDbExpression('NOW()');

                    $enableValidate = true;
                    if (trim($model->title) === '') {
                        $enableValidate = false;
                        $model->active = 0;
                    }

                    if ($model->save($enableValidate)) {
                        $langIds[$lang] = $model->id;

                        if ($enableValidate) {
                            $model->sefname = 'program_' . $model->id . '_' . UrlTransliterate::cleanString(substr($model->title, 0, 100));
                            $model->save();
                        }
                    } else {
                        $error = true;
                    }
                }

                if (!empty($langIds)) {
                    $criteria = new CDbCriteria;
                    $criteria->addInCondition('id', $langIds);
                    $langIds = CJSON::encode($langIds);
                    ProgrammsContent::model()->updateAll(array('other_id' => $langIds), $criteria);
                }


                if (!$error) {
                    $this->redirect(array('admin'));
                }
            }

            $this->render('create', array(
                'model' => $model,
            ));
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
            $model = $this->loadModel($id);
            $oldImage = $model->image;


            $langIds = CJSON::decode($model->other_id);
            $langIds = $this->clearArrayEmpty($langIds);
            $langModels = ProgrammsContent::model()->with('lang')->findAllByPk($langIds);

            $models = array();
            foreach ($langModels as $item) {
                $models[$item->lang->code] = $item;
            }


            $imgfilename = '';
            $langIds = array();
            if (isset($_POST['ProgrammsContent'])) {
                $model->attributes = $_POST['ProgrammsContent'];
                $model->image = CUploadedFile::getInstance($model, 'image');

                if ($model->image !== null) {
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/programms/';

                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $imgfilename = md5($model->image->name) . '.' . $model->image->extensionName;
                    if ($model->image->saveAs($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/' . $imgfilename)) {
                        Yii::import('application.extensions.image.Image');
                        $image = new Image($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/' . $imgfilename);
                        $image->resize(275, 200, Image::WIDTH);
                        $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/' . $imgfilename);
                        $image->resize(145, 200, Image::WIDTH);
                        $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/145_' . $imgfilename);
                        $image->resize(104, 100, Image::WIDTH);
                        $image->save($_SERVER['DOCUMENT_ROOT'] . '/upload/programms/104_' . $imgfilename);
                    }
                } else {
                    $imgfilename = $oldImage;
                }


                foreach ($_POST['ProgrammsContent']['langs'] as $lang => $data) {
                    if (!empty($models[$lang])) {
                        $model = $models[$lang];
                        $oldBackground = $model->background;
                        $oldMainImage = $model->main_image;
                    } else {
                        $model = new ProgrammsContent;
                    }

                    $model->attributes = $data;

                    $model->background = CUploadedFile::getInstance($model, '[langs][' . $lang . ']background');
                    $model->main_image = CUploadedFile::getInstance($model, '[langs][' . $lang . ']main_image');

                    if ($model->background !== null) {
                        $filename = md5($model->background->name . time()) . '.' . $model->background->extensionName;
                        $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/branding/programms/';

                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }

                        if ($model->background->saveAs($path . $filename)) {
                            $model->background = $filename;
                        } else {
                            $model->background = $oldBackground;
                        }
                    } else {
                        $model->background = $oldBackground;
                    }

                    $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/programms/' . $id . '/';

                    if (!is_writable($path)) {
                        mkdir($path, 0777, true);
                    }

                    $file = CUploadedFile::getInstance($model, '[langs][' . $lang . ']videofile');
                    if ($file !== null) {
                        $filename = 'video_' . $model->id . '_' . date('dmYHis', time());
                        $file->saveAs($path . $filename . '.' . $file->extensionName);

                        exec('/usr/bin/ffmpeg  -i ' . $path . $filename . "." . $file->extensionName . ' -an -ss 00:00:' . rand(9, 12) . ' -an -r 1 -vframes 1 -s 640x480 -y -f mjpeg ' . $path . '/' . $filename . '.jpg');

                        if ($file->extensionName != 'flv') {
                            // конвертация видео
                            //240
                            exec("/usr/local/sbin/mpp4_240 " . $path . $filename . "." . $file->extensionName . " " . $path . $filename . "_240.mp4");
                            //320
                            exec("/usr/local/sbin/mpp4_320 " . $path . $filename . "." . $file->extensionName . " " . $path . $filename . "_320.mp4");
                            //480
                            exec("/usr/local/sbin/mpp4_480 " . $path . $filename . "." . $file->extensionName . " " . $path . $filename . "_480.mp4");
                        }
                        $vModel = new Video;
                        $vModel->module = 'programms';
                        $vModel->page_id = $id;
                        $vModel->filename = $filename;
                        $vModel->video_lang_id = $model->lang_id;
                        $vModel->title = $model->title;
                        $vModel->upload_file = $filename;
                        $vModel->upload_ext = $file->extensionName;
                        $vModel->save();
                    }

                    if ($model->main_image !== null) {
                        $filename = md5($model->main_image->name . time()) . '.' . $model->main_image->extensionName;
                        $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/programms/main/';

                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }

                        if ($model->main_image->saveAs($path . $filename)) {
                            $model->main_image = $filename;
                        } else {
                            $model->main_image = $oldMainImage;
                        }
                    } else {
                        $model->main_image = $oldMainImage;
                    }

                    $model->image = $imgfilename;
                    $model->lang_id = Langs::getLangIdByCode($lang);

                    $enableValidate = true;
                    if (trim($model->title) === '') {
                        $enableValidate = false;
                        $model->active = 0;
                    }

                    if ($model->save($enableValidate)) {
                        $langIds[$lang] = $model->id;

                        if ($enableValidate) {
                            $model->sefname = 'program_' . $model->id . '_' . UrlTransliterate::cleanString(substr($model->title, 0, 100));
                            $model->save();
                        }
                    }
                }

                if (!empty($langIds)) {
                    $criteria = new CDbCriteria;
                    $criteria->addInCondition('id', $langIds);
                    $langIds = CJSON::encode($langIds);
                    ProgrammsContent::model()->updateAll(array('other_id' => $langIds), $criteria);
                }

                $this->redirect(array('admin'));
            } else {
                foreach ($models as &$item) {
                    $item->tags = implode(', ', CHtml::listData($item->listTags, 'tag', 'tag'));
                }
            }


            $this->render('update', array(
                'model'  => $model,
                'models' => $models
            ));
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            } else
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }

        /**
         * Manages all models.
         */
        public function actionAdmin($lang = 'ru') {
           $model = new ProgrammsContent('search');
            $model->unsetAttributes(); // clear any default values
            if (isset($_GET['ProgrammsContent']))
                $model->attributes = $_GET['ProgrammsContent'];

            $model->lang_id = Langs::getLangIdByCode($lang);

            $this->render('admin', array(
                'model' => $model,
                'lang'  => $lang
            ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer the ID of the model to be loaded
         */
        public function loadModel($id) {
            $model = ProgrammsContent::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');

            return $model;
        }

        /**
         * Performs the AJAX validation.
         * @param CModel the model to be validated
         */
        protected function performAjaxValidation($model) {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'programms-content-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }

        private function clearArrayEmpty($array) {
            $ret_arr = array();
            foreach ($array as $val) {
                if (!empty($val)) {
                    $ret_arr[] = trim($val);
                }
            }

            return $ret_arr;
        }

        /**
         * Загрузка видео к статье
         * @param $id
         */
        public function actionUpload($id) {
            if (!Yii::app()->request->isAjaxRequest) {
                throw new CHttpException(400, 'Bad request');
            }

            $page = ProgrammsContent::model()->findByPk((int)$id);

            Yii::import("ext.eAjaxUpload.qqFileUploader");

            $allowedExtensions = array('avi', 'mpg', 'mp4', 'flv');
            $sizeLimit = 1000 * 1024 * 1024;

            $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/video/programms/' . $id . '/';
            if (!is_writable($path)) {
                mkdir($path, 0777, true);
            }

            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload($path);
            // to pass data through iframe you will need to encode all html tags
            $results = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

            if (is_array($result) && empty($result['error'])) {
                $filename = 'video_' . $page->id . '_' . date('dmYHis', time());

                //Создание эскиза
                //640*480
                exec('/usr/bin/ffmpeg  -i "' . $path . $result['filename'] . '" -an -ss 00:00:' . rand(9, 12) . ' -an -r 1 -vframes 1 -s 640x480 -y -f mjpeg ' . $path . '/' . $filename . '.jpg');

                if ($result['ext'] != 'flv') {
                    // конвертация видео
                    //240
                    exec("/usr/local/sbin/mpp4_240 " . $path . $result['filename'] . " " . $path . $filename . "_240.mp4");
                    //320
                    exec("/usr/local/sbin/mpp4_320 " . $path . $result['filename'] . " " . $path . $filename . "_320.mp4");
                    //480
                    exec("/usr/local/sbin/mpp4_480 " . $path . $result['filename'] . " " . $path . $filename . "_480.mp4");
                }

                $model = new Video;
                $model->module = 'programms';
                $model->page_id = $id;
                $model->filename = $filename;
                $model->video_lang_id = $page->lang_id;
                $model->title = $page->title;
                $model->upload_file = $result['filename'];
                $model->upload_ext = $result['ext'];
                if ($model->save()) {
                    echo $results;
                } else {
                    htmlspecialchars(json_encode(array('error' => 'Не удалось сохранить файл. DBERROR')), ENT_NOQUOTES);
                }
            } else {
                echo $results;
            }
        }

        public function actionDeleteVideo($id) {
            $model = Video::model()->findByPk($id);
            if ($model === null) {
                throw new CHttpException(404, 'Страница не найдена');
            }

            $page_id = $model->page_id;
            $filename = $model->filename;
            if ($model->delete()) {
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/programms/' . $page_id . '/' . $filename . '_240.mp4');
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/programms/' . $page_id . '/' . $filename . '_320.mp4');
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/programms/' . $page_id . '/' . $filename . '_480.mp4');
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/video/programms/' . $page_id . '/' . $filename . '.jpg');
            }

            $this->redirect(array('/programms/default/update', 'id' => $page_id));

        }

        public function actionGetAjaxListTimetable() {

            if (!Yii::app()->request->isAjaxRequest) {
                throw new CHttpException(400, 'Bad request');
            }

            $program_id = (int)$_POST['VideoArchive']['program_id'];

            $model = ProgrammsTimetable::model()->cache(300)->findAll('active=1 AND programm_id=:program_id', array(':program_id' => $program_id));


            echo CHtml::tag('option',
                array('value' => 0), CHtml::encode('Выберите расписание'), true);
            foreach ($model as $item) {
                echo CHtml::tag('option',
                    array('value' => $item->id), CHtml::encode($item->title . ' ' . date('d.m.Y H:i', strtotime($item->date . ' ' . $item->time))), true);
            }
        }
    }
