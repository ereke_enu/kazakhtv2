<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>
<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'programms-timetable-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal'
    )); ?>

    <?php echo $form->dropDownListRow($model, 'programm_id', array(0 => 'Без привзяки')+ProgrammsContent::getListItems(), array('class' => 'span8')); ?>
    <?php echo $form->dropDownListRow($model, 'lang_id', Langs::getListItems(), array('class' => 'span8')); ?>


    <?php echo $form->textFieldRow($model, 'title', array('class' => 'span8', 'maxlength' => 255)); ?>

    <?php echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>
    <div class="copy_timetable">
        <div class="control-group">
            <label for="ProgrammsTimetable_date" class="required control-label">
                Дата и время начала <span class="required">*</span>
            </label>

            <div class="controls">
                <?=$form->textField($model, 'date[]', array('class' => 'span4'))?>
                <?=$form->textField($model, 'time[]', array('class' => 'span4'))?><br><br>
            </div>
        </div>
    </div>
    <a class="btn addMore" href="#">Добавить время</a>


    <?php echo $form->checkBoxRow($model, 'in_anounce'); ?>
    <?php echo $form->checkBoxRow($model, 'active'); ?>

    <?php echo $form->dropDownListRow($model, 'trans', array(0 => 'Не ретранслировать', 1 => 'Везде', 2 => 'Только Казахстан'), array('class' => 'span8', 'maxlength' => 1)); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    $(function () {

        $('.addMore').click(function () {
            var tt = $('.copy_timetable');
           $(tt.html()).insertAfter(tt);
            return false;

        });
    });
</script>