<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Импорт сетки передач</span>
    </div>
</div>

<div class="well">
    <form action="<?=Yii::app()->createUrl('/programms/timetable/import')?>" method="POST"
          enctype="multipart/form-data" class="form-horizontal">
        <div class="control-group">
            <label class="control-label" for="excelimport_field">Excel файл</label>

            <div class="controls">
                <div class="input-append">
                    <input id="excelimport_field" class="input-large" readonly="readonly" type="text">
                    <a class="btn" onclick="$('input[id=excelimport]').click();">Обзор</a>
                </div>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('input[id=excelimport]').change(function () {
                        $('#excelimport_field').val($(this).val());
                    });
                });
            </script>
            <input type="file" id="excelimport" name="excelimport"
                   style="display:none;"
                   maxlength="255"
                   class="span8">
        </div>
        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => 'Импорт'
            )); ?>
        </div>
    </form>
</div>