<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>
<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'programms-categories-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal'   ,
        'htmlOptions' => array('enctype' => 'multipart/fotm-data')
    )); ?>

    <?php echo $form->dropDownListRow($model, 'parent_id', ProgrammsCategories::getTreeListItems(true), array('class' => 'span8')); ?>

    <?php echo $form->dropDownListRow($model, 'lang_id', Langs::getListItems(), array('class' => 'span8')); ?>

    <?php echo $form->textFieldRow($model, 'title', array('class' => 'span8', 'maxlength' => 255)); ?>

    <?php echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

    <?php echo $form->textFieldRow($model, 'sefname', array('class' => 'span8', 'maxlength' => 255)); ?>

    <?php echo $form->checkBoxRow($model, 'active'); ?>



    <h4>Настройки категории</h4>

    <p>Настройки будут влиять на статьи, вложенные в категории</p>

    <?php if ($model->isNewRecord !== true && $model->background != '') { ?>
        <div class="controls">
            <div class="image">
                <img src="/upload/branding/<?=$model->background?>" alt="">
            </div>
        </div>
        <br>
        <?php echo $form->checkBoxRow($model, 'delete_image'); ?>
    <?php } ?>
    <div class="control-group">
        <label class="control-label" for="photoCover">Бренд фон</label>

        <div class="controls">
            <div class="input-append">
                <input id="photoCover" class="input-large" readonly="readonly" type="text">
                <a class="btn" onclick="$('input[id=ProgrammsCategories_Background]').click();">Обзор</a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('input[id=ProgrammsCategories_Background]').change(function () {
                $('#photoCover').val($(this).val());
            });
        });

    </script>
    <input type="file" id="ProgrammsCategories_Background" name="ProgrammsCategories[background]" style="display:none;" maxlength="255"
           class="span8">

    <?php echo $form->checkBoxRow($model, 'show_icons'); ?>

    <?php echo $form->checkBoxRow($model, 'show_comments'); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>