<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Список категорий</span>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li><a href="<?=$this->createUrl('create');?>" class="">Создать категорию</a></li>
        </ul>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li><a href="<?=$this->createUrl('admin', array('lang' => 'ru'));?>" <?=($lang=='ru')?'style="font-weight:bold"':'';?>>Русский</a></li>
            <li><a href="<?=$this->createUrl('admin', array('lang' => 'kz'));?>" <?=($lang=='kz')?'style="font-weight:bold"':'';?>>Казахский</a></li>
            <li><a href="<?=$this->createUrl('admin', array('lang' => 'en'));?>" <?=($lang=='en')?'style="font-weight:bold"':'';?>>Английский</a></li>
        </ul>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'categories-grid',
    'type' => 'condensed bordered striped',
    'dataProvider' => $model->search(),
    'enableSorting' => false,
    'summaryText' => '',
    'ajaxUpdate' => false,
    'columns' => array(
        array(
            'name' => 'title',
            'header' => 'Наименование',
            'value' => '"<div class=\"span".($data->level-1)."\"></div><div class=\"span".(8-$data->level)."\">".$data->title."</div>"',
            'type' => 'raw',
        ),
        array(
            'value' => 'CHtml::link("<i class=\"icon-arrow-up\"></i>", Yii::app()->createUrl("/programms/categories/move", array("id" => $data->id, "type" => "up")), array("class" => "btn btn-small"))."&nbsp".CHtml::link("<i class=\"icon-arrow-down\"></i>", Yii::app()->createUrl("/programms/categories/move", array("id" => $data->id, "type" => "down")), array("class" => "btn btn-small"))',
            'type' => 'raw',
            'htmlOptions' => array('align' => 'center')
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}',
            'updateButtonOptions' => array(
                'class' => 'btn btn-small update'
            ),
            'deleteButtonOptions' => array(
                'class' => 'btn btn-small delete'
            )
        ),
    ),
    'htmlOptions' => array(
        'class' => 'table-hover'
    )
)); ?>
