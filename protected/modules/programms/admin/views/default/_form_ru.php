<?php echo $form->dropDownListRow($model, '[langs][ru]category_id', ProgrammsCategories::getTreeListItems(false, 'ru'), array('class' => 'span8', 'prompt' => 'Выберите категорию')); ?>

<?php echo $form->textFieldRow($model, '[langs][ru]title', array('class' => 'span8', 'maxlength' => 255)); ?>

<?php echo $form->textAreaRow($model, '[langs][ru]short_text', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->ckEditorRow($model, '[langs][ru]text', array('options' => CMap::mergeArray(array('fullpage' => 'js:true', 'width' => '640', 'resize_maxWidth' => '640', 'resize_minWidth' => '320'), Yii::app()->params['editorOptions']))); ?>

<?php if ($model->isNewRecord !== true && $model->image != '') { ?>
    <div class="controls">
        <div class="image">
            <img src="/upload/programms/104_<?= $model->image ?>" alt="">
        </div>
    </div>
    <br>
    <?php echo $form->checkBoxRow($model, 'delete_image'); ?>
<?php } ?>
    <div class="control-group">
        <label class="control-label" for="photoCover">Изображение</label>

        <div class="controls">
            <div class="input-append">
                <input id="photoCover" class="input-large" readonly="readonly" type="text">
                <a class="btn" onclick="$('input[id=Pages_Image]').click();">Обзор</a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('input[id=Pages_Image]').change(function () {
                $('#photoCover').val($(this).val());
            });
        });

    </script>
    <input type="file" id="Pages_Image" name="ProgrammsContent[image]" style="display:none;" maxlength="255"
           class="span8">

<?php echo $form->fileFieldRow($model, '[langs][ru]main_image', array('class' => 'span8')); ?>
<?php echo $form->textFieldRow($model, '[langs][ru]tags', array('class' => 'span8', 'maxlength' => 255)); ?>
<?php echo $form->textFieldRow($model, '[langs][ru]meta_tags', array('class' => 'span8', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, '[langs][ru]meta_description', array('class' => 'span8', 'maxlength' => 255)); ?>
<?php echo $form->textFieldRow($model, '[langs][ru]date_create', array('class' => 'span8', 'maxlength' => 255)); ?>

<?php echo $form->checkBoxRow($model, '[langs][ru]active'); ?>

    <hr>
    <h4>Настройки страницы</h4>

    <p>Настройки категории не будут влиять на эту страницу, если они отличаются</p>

<?php if ($model->isNewRecord !== true && $model->background != '') { ?>
    <div class="controls">
        <div class="image">
            <img src="/upload/branding/programms/<?= $model->background ?>" alt="">
        </div>
    </div>
    <br>
    <?php echo $form->checkBoxRow($model, '[langs][ru]delete_background'); ?>
<?php } ?>
    <div class="control-group">
        <label class="control-label" for="bgCover">Бренд фон</label>

        <div class="controls">
            <div class="input-append">
                <input id="bgCover" class="input-large" readonly="readonly" type="text">
                <a class="btn" onclick="$('input[id=ProgrammsContent_Background]').click();">Обзор</a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('input[id=ProgrammsContent_Background]').change(function () {
                $('#bgCover').val($(this).val());
            });
        });
    </script>
    <input type="file" id="ProgrammsContent_Background" name="ProgrammsContent[langs][ru][background]"
           style="display:none;" maxlength="255" class="span8">

<?php echo $form->dropDownListRow($model, '[langs][ru]show_icons', array(-1 => 'Как у категории', 1 => 'Да', 0 => 'Нет'), array('class' => 'span8')) ?>
<?php echo $form->dropDownListRow($model, '[langs][ru]show_comments', array(-1 => 'Как у категории', 1 => 'Да', 0 => 'Нет'), array('class' => 'span8')) ?>

    <hr>
    <h4>Управление видео</h4>
<?php if (!empty($model->videos)) { ?>
    <h5>Загруженные видео</h5>
    <table class="table table-bordered table-hovered table-condensed">
        <tr>
            <th>Файл</th>
            <th>Действия</th>
        </tr>
        <?php foreach ($model->videos as $video) { ?>
            <tr>
                <td><?= $video->filename ?></td>
                <td><a class="btn btn-small" rel="tooltip" title="Удалить"
                       href="<?= $this->createUrl('/programms/default/deleteVideo', array('id' => $video->id)) ?>"><i
                            class="icon-remove"></i></a></td>
            </tr>
        <?php } ?>

    </table>
<?php } ?>
<?php
    if (!$model->isNewRecord) {
        ?>
        <h5>Добавить новое видео</h5>
<?php
//    $this->widget('ext.eAjaxUpload.EAjaxUpload',
//        array(
//            'id' => 'uploadFile',
//            'config' => array(
//                'action' => Yii::app()->createUrl('/programms/default/upload', array('id' => $model->id)),
//                'allowedExtensions' => array('flv', 'mp4', 'avi', 'mpg'),
//                'sizeLimit' => 1000 * 1024 * 1024, // maximum file size in bytes
//                'onComplete' => "js:function(id, fileName, responseJSON){ alert('Файл загружен'); }",
//                'messages' => array(
//                    'typeError' => "{file} запрещен к загрузке. Только файлы с типом {extensions} разрешены.",
//                    'sizeError'=>"{file} слишком большой, максимальный размер {sizeLimit}.",
////                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
////                  'emptyError'=>"{file} is empty, please select files again without it.",
////                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
//                ),
////'showMessage'=>"js:function(message){ alert(message); }"
//            )
//        ));
        echo $form->fileFieldRow($model, '[langs][ru]videofile');
    } else {
        ?>
        <p>Для загрузки видео, нужно сохранить статью</p>
    <?php } ?>