<div class="col-sm-12">
    <?php if ($anounce !== null) { ?>
        <div class="anounce_program">
            <div class="anounce_header"><?= Yii::t('programms', 'Смотрите') ?></div>
            <div class="anounce_program_data">
                <div class="img">
                    <?php

                        $image = ProgrammsTimetable::getImage($anounce->image, $anounce->programm_id);

                        if ($image === false) {
                            $image = ProgrammsContent::getAnounceImg($anounce->content->image);
                        }
                    ?>
                    <a href="<?= Yii::app()->createUrl('/programms/default/program', array('sefname' => $anounce->content->sefname)) ?>">
                        <img src="<?= $image ?>" alt="" width="310"/>
                    </a>
                </div>
            </div>
            <div class="anounce_text">
                <h2>
                    <a href="<?= Yii::app()->createUrl('/programms/default/program', array('sefname' => $anounce->content->sefname)) ?>">
                        <?= $anounce->title ?></a></h2>
                <?= $anounce->description ?>
            </div>
        </div>

    <?php } ?>


    <hr class="bold" />
    <h1><?= ($isSecond) ? $category->title : Yii::t('programms', 'Программы'); ?></h1>
    <hr />
    <?php
    if($category->parent) {
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'       => array(
                Yii::t('programms', 'Программы') => Yii::app()->createUrl('/pages/default/category', array('sefname' => 'programms')),
                $category->parent->title => Yii::app()->createUrl('/programms/default/index', array('id' => $category->parent->sefname)),
                $category->title),

            'separator'   => ' / ',
            'htmlOptions' => array(
                'class' => 'breadcrumbs'
            )
        ));
    } else {
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'       => array(
                Yii::t('programms', 'Программы')    => Yii::app()->createUrl('/pages/default/category', array('sefname' => 'programms')),
                $category->title),

            'separator'   => ' / ',
            'htmlOptions' => array(
                'class' => 'breadcrumbs'
            )
        ));
    }
?>
    
    <?php if (!empty($categories)) { ?>
    <hr class="" />
    
    
    
    <ul class="news_menu">
        <?php foreach ($categories as $item) { ?>
        <li <?= ($category->id == $item->id) ? 'class="active"' : '' ?>><a href="<?= $this->createUrl('/programms/default/index', array('id' => $item->sefname)) ?>"><?= $item->title ?></a></li>
        <?php } ?>
    </ul>
    <?php } ?>

    <hr class="margin-bottom" />
</div>

<?php $i = 1; foreach($programmsModels as $data) { ?>

<?php
    $url = Yii::app()->createUrl('/programms/default/program', array('sefname' => $data->sefname));
?>

<div class="col-xs-6 col-sm-4 element" style="margin-bottom:15px;" data-href="<?= $url ?>">
    <a href="<?=Yii::app()->createUrl('/programms/default/program', array('sefname' => $data->sefname))?>">
        <?php if (($image = ProgrammsContent::getAnounceImg($data->image, '')) !== false) { ?>
                <img src="<?= $image ?>" alt="<?=$data->title?>" class="img-responsive img-thumbnail">
        <?php } ?>
        <div class="news_anounce">
            <h3><?=$data->title?></h3>
        </div>
    </a>
</div>
<?php if($i%3 == 0) {?>
<div class="clearfix hidden-xs element"></div>
<?php } ?>
<?php if($i%2 == 0) {?>
<div class="clearfix visible-xs element"></div>
<?php } ?>

<?php $i++; } ?>
<div class="clearfix element"></div>

<?php $this->widget('CLinkPager', array(
    'pages' => $programmsPages,
    'header' => false,
    'nextPageLabel' => '&raquo;',
    'prevPageLabel' => '&laquo;',
    'maxButtonCount' => 0,
    'htmlOptions' => array(
        'id' => 'iasNews',
        'class' => 'pager'
    )
))?>


    <?php
    /*
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView'     => '_itemIndex',
            'emptyText'    => '',
            'summaryText'  => '',
            'id'           => 'left_news',
            'pager'        => array(
                'header'        => '',
                'prevPageLabel' => '<img src="/themes/front/images/left_btn_partners.png">',
                'nextPageLabel' => '<img src="/themes/front/images/right_btn_partners.png">'
            )
        ));
     * 
     */
    ?>

