<script type="text/javascript">

    $(document).ready(function () {
        $("#showcase").awShowcase(
                {
                    content_width:528,
                    content_height:390,
                    fit_to_parent:false,
                    auto:false,
                    interval:3000,
                    continuous:false,
                    loading:true,
                    tooltip_width:200,
                    tooltip_icon_width:32,
                    tooltip_icon_height:32,
                    tooltip_offsetx:18,
                    tooltip_offsety:0,
                    arrows:true,
                    buttons:false,
                    btn_numbers:false,
                    keybord_keys:true,
                    mousetrace:false, /* Trace x and y coordinates for the mouse */
                    pauseonover:true,
                    stoponclick:true,
                    transition:'vslide', /* hslide/vslide/fade */
                    transition_delay:300,
                    transition_speed:500,
                    show_caption:'onhover', /* onload/onhover/show */
                    thumbnails:true,
                    thumbnails_position:'outside-last', /* outside-last/outside-first/inside-last/inside-first */
                    thumbnails_direction:'vertical', /* vertical/horizontal */
                    thumbnails_slidex:0, /* 0 = auto / 1 = slide one thumbnail / 2 = slide two thumbnails / etc. */
                    dynamic_height:false, /* For dynamic height to work in webkit you need to set the width and height of images in the source. Usually works to only set the dimension of the first slide in the showcase. */
                    speed_change:true, /* Set to true to prevent users from swithing more then one slide at once. */
                    viewline:false /* If set to true content_width, thumbnails, transition and dynamic_height will be disabled. As for dynamic height you need to set the width and height of images in the source. */
                });
    });

</script>

<h1><?=$model_category['title']?></h1>
<?
if (!empty($model['background'])) {
    ?>
<style>
    body {
        background-image: url("/upload/branding/<?=$model['background']?>") !important;
    }
</style>
<?
} elseif ($model_category['background'] !== 0) {
    ?>
<style>
    body {
        background-image: url("/upload/branding/<?=$model_category['background']?>") !important;
    }
</style>
<?
}
?>
<div id="text">
    <h3><?=$model['title']?></h3>

    <span class="date"><?=$model['DATE'] . ' ' . $model['TIME']?></span>

    <?
    if (!empty($video)) {

        ?>
        <div id="showcase" class="showcase">

            <?foreach ($video as $item) {
            //  var_dump($item);
            ?>
            <div class="showcase-slide">
                <div class="showcase-content">
                    <object id="идентификатор" type="application/x-shockwave-flash" data="/themes/front/player/uppod.swf"
                            width="528" height="390">
                        <param name="bgcolor" value="#ffffff"/>
                        <param name="allowFullScreen" value="true"/>
                        <param name="allowScriptAccess" value="always"/>
                        <param name="wmode" value="window"/>
                        <param name="movie" value="/themes/front/player/uppod.swf"/>
                        <param name="flashvars" value="comment=testg&amp;st=/themes/front/player/video153-819.txt&amp;file=/upload/pages/video/<?=$model['id']?>/<?=$item->filename?>"/>
                    </object>
                </div>
                <div class="showcase-thumbnail">
                    <img src="images/03.jpg" alt="01" width="140px"/>

                    <div class="showcase-thumbnail-caption"><?=$item->filename?></div>
                    <div class="showcase-thumbnail-cover"></div>
                </div>
            </div>
            <? }?>
        </div>
        <? }?>

    <article>
        <?=$model['text']?>
    </article>
</div>


<!--                 <div id="video_<?=$item->id?>">Loading the player...</div>

                <script type="text/javascript">
                    jwplayer("video_<?=$item->id?>").setup({
                        file: "/upload/pages/video/<?=$model['id']?>/<?=$item->filename?>",
                        image: "/uploads/example.jpg"
                    });
                </script>




                <object id="videoplayer445" type="application/x-shockwave-flash" data="/uppod.swf" width="528" height="390">
                <param name="bgcolor" value="#ffffff"/>
                <param name="allowFullScreen" value="true"/>
                <param name="allowScriptAccess" value="always"/>
                <param name="movie" value="/uppod.swf"/>
                <param name="flashvars" value="comment=testg&amp;st=/video153-819.txt&amp;file=http://test"/>
            </object>



                -->