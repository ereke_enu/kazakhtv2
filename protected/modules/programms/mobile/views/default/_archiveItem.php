<div class="news_item program parchive" data-href="<?=Yii::app()->createUrl('/programms/default/viewArchive', array('if' => $data->id))?>">
    <a href="<?=Yii::app()->createUrl('/programms/default/viewArchive', array('id' => $data->id))?>">
        <?php

        if (($image = ProgrammsContent::getAnounceImg($data->timetable->content->image, '')) !== false) {
            ?>
            <div class="img">
                <img src="<?=$image?>" width="205" alt="">
            </div>
        <?php
        }
        ?>
        <div class="news_anounce">
            <div class="date"><?=date('d.m.Y H:i', strtotime($data->timetable->date.' '.$data->timetable->time))?></div>

            <h3><?=$data->title?></h3>
        </div>
    </a>
</div>