<div class="col-sm-12">
    <hr class="bold" />
    <h1><?=$model->title?></h1>
    <!--<hr />-->    
    <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
                'links'       => array(
                    Yii::t('programms', 'Программы')    => Yii::app()->createUrl('/pages/default/category', array('sefname' => 'programms')),
                    //$model->timetable->content->title   => Yii::app()->createUrl('/programms/default/program', array('sefname' => $model->timetable->content->sefname)),
                    $category->title                    => Yii::app()->createUrl('/programms/default/index', array('id' => $category->sefname)),
                    $model->timetable->content->title   => Yii::app()->createUrl('/programms/default/program', array('sefname' => $model->timetable->content->sefname)),
                    $model->title),

                'separator'   => ' / ',
                'htmlOptions' => array(
                    'class' => 'breadcrumbs'
                )
            )
        ); ?>
    <hr class="margin-bottom" />
    
    
    <span class="date">
        <?= date('d.m.Y H:i', strtotime($model->timetable->date . ' ' . $model->timetable->time)) ?>
    </span>
    
    <?php if (!empty($model->videos)) { ?>
    <div id="videoplayer_content ">
		<!--<script src='/uppod.js' type='text/javascript'></script>
		<script src='/swfobject.js' type='text/javascript'></script>
		<div id="myplayer" style="width:500px;height:281px"></div>
		 <?php //foreach ($model->videos as $video) { ?>
            
		<SCRIPT type=text/javascript>this.videoplayer = new Uppod({m:"video",comment:"<?//=$model->title ?>",uid:"myplayer",file:"http://html5player.ru/html5.mp4"});</script>
		
		
		
		<!--WORKING VERSION-->
		<!--<SCRIPT type=text/javascript>this.videoplayer = new Uppod({m:"video",comment:"<?//=$model->title ?>",uid:"myplayer",file:"/upload/video/archive/<?//=$model->id?>/<?//= $video->filename ?>.flv"});</script>-->
		
		
		
		
        <?php $this->widget('ext.uppod.LXUppodWidget', array('width' => '100%', 'params' => CMap::mergeArray(Yii::app()->params['videoPlayer'], $params)))?>
    </div>
    <?php }?>

    <article class="article-text"> 
        <?=$model->text?>
    </article>
    <hr />
    
    <?php $this->widget('shareWidget');?>

    <?php //Pages::model()->getSimilar($model['id']); ?>
    <?php if ($model->timetable->content->show_comments == 1 || ($model->timetable->content->show_comments == -1 && $model->timetable->content->category->show_comments == 1)) { ?>
    <div class='comments'>
        <?php $this->widget('BlockComments', array(
            'type' => 'videoArchive',
            'id' => $model->id)
        ); ?>

    </div>
    <?php } ?>
    
</div>



