<div class="news_item program" data-href="<?=Yii::app()->createUrl('/programms/default/program', array('sefname' => $data->sefname))?>">
    <a href="<?=Yii::app()->createUrl('/programms/default/program', array('sefname' => $data->sefname))?>">
        <?php

        if (($image = ProgrammsContent::getAnounceImg($data->image, '')) !== false) {
            ?>
            <div class="img">
                <img src="<?=$image?>" width="215" alt="">
            </div>
            <?php
        }
        ?>
        <div class="news_anounce">
            <h3><?=$data->title?></h3>
        </div>
    </a>
</div>