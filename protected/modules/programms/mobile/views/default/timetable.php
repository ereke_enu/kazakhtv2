<div class="col-sm-12">
    <hr class="bold" />
    <h1><?=Yii::t('programms', 'Программа передач')?></h1>
    <hr />
    
    <a class="pull-right next-week" data-date="<?=date('Y-m-d', $next_monday)?>" href="<?= Yii::app()->createUrl('/programms/default/timetable', array('date' => date('Y-m-d', $next_monday))) ?>"><?=Yii::t('programms', 'Следующая неделя')?></a>
    <?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'       => array(
                Yii::t('programms', 'Программы')    => Yii::app()->createUrl('/pages/default/category', array('sefname' => 'programms')),
                Yii::t('programms', 'Программа передач')),
            'separator'   => ' / ',
            'htmlOptions' => array(
                'class' => 'breadcrumbs'
            )
        )
    ); ?>
    <div class="clearfix"></div>
    
    <section class="timetable_all table-responsive">
        <table class="table">
            <tr>
                <th class="timetable_date">
                    CET <br>
                    GMT +0
                </th>
                <th class="timetable_date">
                    Moscow <br>
                    GMT +2
                </th>
                <th class="timetable_date">
                    Astana <br>
                    GMT +5
                </th>
                <?php

                for ($mon_num = 1; $mon_num < 8; $mon_num++) {
                    $active_day = '';

                    if (date('Y-m-d', $monday) == $date) {
                        $active_day = 'class="active"';
                    }
                    ?>
                    <th <?=$active_day?>>
                        <a class="getTimeTable" href="<?= Yii::app()->createUrl('/programms/default/timetable', array('date' => date('Y-m-d', $monday))) ?>" data-date="<?=date('Y-m-d', $monday);?>"><?=Yii::app()->dateFormatter->format('EE', $monday)?>
                            <div class="select_date">
                                <?=Yii::app()->dateFormatter->format('d', $monday)?> <?=Yii::app()->dateFormatter->format('MMMM', $monday)?>
                            </div>
                        </a>
                    </th>
                    <?php
                    $monday += 86400;
                }
                ?>
            </tr>

            <?php foreach ($programs as $program) {
            $onAirClass = '';
            if ($program->id == $onAirProgram->id) {
                $onAirClass = 'active';
            }

            $beforeClass = '';
            if (strtotime($program->date.' '.$program->time) < strtotime($onAirProgram->date.' '.$onAirProgram->time)) {
                $beforeClass = "beforeNow";
            }
            ?>
            <tr>
                <td class="time_value"><?=Yii::app()->dateFormatter->format('H:mm', (strtotime($program->time) - 3600 * 4))?></td>
                <td class="time_value"><?=Yii::app()->dateFormatter->format('H:mm', (strtotime($program->time) - 3600 * 3))?></td>
                <td class="time_value"><?=Yii::app()->dateFormatter->format('H:mm', strtotime($program->time))?></td>
                <td class="timetable_title <?=$onAirClass?> <?=$beforeClass?>" colspan="7">
                    <?php if ($program->content !== null) { ?>
                    <?php $image = ProgrammsContent::getAnounceImg($program->content->image, '104_'); ?>
                        <div class="program_content">
                            <h3>
                                <a href="<?=Yii::app()->createUrl('/programms/default/program', array('sefname' => $program->content->sefname))?>"><?=$program->title?></a>
                            </h3>

                            <div class="description">
                                <div class="img pull-left" style="margin-right: 10px;">
                                    <img src="<?=$image?>" width="130" alt="">
                                </div>
                                <div class="short_text"><?=$program->content->short_text?></div>
                            </div>

                        </div>
                    <?php
                    } else {
                        echo $program->title;
                    }
                    ?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </section>
</div>