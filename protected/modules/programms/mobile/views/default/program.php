<div class="col-sm-12">
    <hr class="bold" />
    <h1><?= $model->title ?></h1>
   <!-- <hr />-->
    <?php
        if(!$model->category->parent) {
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'links' => array(
                    Yii::t('programms', 'Программы') => Yii::app()->createUrl('/pages/default/category', array('sefname' => 'programms')),
                    $model->category->title          => Yii::app()->createUrl('/programms/default/index', array('id' => $model->category->sefname)),
                    $model->title),
                'separator' => ' / ',
                'htmlOptions' => array(
                    'class' => 'breadcrumbs'
                )
            ));
        } else {
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'links' => array(
                    Yii::t('programms', 'Программы')    => Yii::app()->createUrl('/pages/default/category', array('sefname' => 'programms')),
                    $model->category->parent->title     => Yii::app()->createUrl('/programms/default/index', array('id' => $model->category->parent->sefname)),
                    $model->category->title             => Yii::app()->createUrl('/programms/default/index', array('id' => $model->category->sefname)),
                    $model->title),
                'separator' => ' / ',
                'htmlOptions' => array(
                    'class' => 'breadcrumbs'
                )
            ));
        }
    ?>
    <hr class="margin-bottom" />
    
    <?php if (!empty($model->videos)) { ?>
    <div id="videoplayer_content">
        <?php $this->widget('ext.uppod.LXUppodWidget', array('width' => '100%', 'height' => '100%', 'params' => CMap::mergeArray(Yii::app()->params['videoPlayer'], $params))) ?>
    </div>
    <?php } ?>
    
    <article class="article-text">
    <?= $model->text ?>
    </article>
    <div class="clearfix margin-bottom"></div>
    <hr />
    
    <?php $this->widget('shareWidget'); ?>
    
    <?php if (!empty($archiveProgramms)) { ?>
    <section style="margin-top:20px;">
        <hr class="bold" />
        <h2><?= Yii::t('pages', 'Последние передачи') ?></h2>
        
        <div class="row">
            <?php $i = 1; foreach($archiveProgramms as $item) { ?>

            <?php
                $url = Yii::app()->createUrl('/programms/default/viewArchive', array('id' => $item->id));
                $date = new DateTime($item->timetable->date.' '.$item->timetable->time, new DateTimeZone('Asia/Almaty'));
            ?>

            <div class="col-xs-6 col-sm-4 element margin-bottom" data-href="<?= $url ?>">
                <?php if (!empty($item->videos[0]) && ($image = Video::getAnounceImg($item->videos[0]->filename . '.jpg', $item->id)) !== false) { ?>
                <a href="<?= $url ?>"><img src="<?= $image ?>" height="110" alt="" /></a>
                <?php } ?>
                <div class="date"><?= $date->format('j.m.Y G:i') ?></div>
                <h3><a href="<?= $url ?>"><?= $item->timetable->content->title ?> &mdash; <?= $item->title ?></a></h3>
            </div>
            <?php if($i%3 == 0) {?>
            <div class="clearfix hidden-xs element"></div>
            <?php } ?>
            <?php if($i%2 == 0) {?>
            <div class="clearfix visible-xs element"></div>
            <?php } ?>

            <?php $i++; } ?>

            <div class="clearfix element"></div>
        </div>
        
        <?php $this->widget('CLinkPager', array(
            'pages' => $archivePages,
            'header' => false,
            'nextPageLabel' => '&raquo;',
            'prevPageLabel' => '&laquo;',
            'maxButtonCount' => 0,
            'htmlOptions' => array(
                'id' => 'iasNews',
                'class' => 'pager'
            )
        ))?>

    <?php } ?>
    
    <?php if ($model->show_comments == 1 || ($model->show_comments == -1 && $model->category->show_comments == 1)) { ?>

    <div class='comments'>
        <?php $this->widget('BlockComments', array(
                'type' => $this->getModule()->id,
                'id'   => $model->id)
        ); ?>
    </div>
    
    <?php } ?>
</div>