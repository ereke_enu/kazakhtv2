<div class="col-sm-12">
<style>
		@media (max-width: 480px){
		.news_menu li{
		display:block;
		border:none;}
		
		.news_menu li:last-child{
		border:none;}
		}
        </style>
    <hr class="bold" />
    <h1><?=$model->title?></h1>
  <!--  <hr />-->
    <?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'       => array(
                $model->title),

            'separator'   => ' / ',
            'htmlOptions' => array(
                'class' => 'breadcrumbs'
            )
        )
    ); ?>
    <hr class="margin-bottom" />
    
    <?php foreach($model->child as $item) { ?>
    <h2><a href="<?= ($item->url) ? Yii::app()->params['index'].$item->url : Yii::app()->createUrl('/pages/default/category', array('sefname' => $item->sefname)) ?>"><?= $item->title ?></a></h2>
    <?php } ?>
    
</div>
