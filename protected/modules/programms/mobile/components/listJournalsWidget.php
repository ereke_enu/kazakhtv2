<?php
/**
 * Description of listJournalsWidget
 *
 * @author Алексей
 */
class listJournalsWidget extends CWidget {
    
    public function run() {
        $programCategories = ProgrammsCategories::model()->findBySefname('features');
        
        $criteria = new CDbCriteria;
        $criteria->limit = '6';
        $criteria->compare('category_id', $programCategories->id);
        $criteria->order = 'id ASC';
        $criteria->compare('active', 1);

        $programms = ProgrammsContent::model()->findAll($criteria);
        
        $this->render('listJournalsWidget', array('programms' => $programms, 'showXS' => 4));
    }
    
}
