﻿<style>
	.news_menu li{
		display:block;
		border:none;
		text-align:left;
		line-height: 27px;
border: none;
border-bottom: 1px solid #DCDBDB;}
		
		.news_menu li:last-child{
		border:none;}
</style>

<div class="row margin-bottom" style="margin-top: -14px;">
    <div class="col-sm-12">
        <hr class="bold" />
        <h2><?=Yii::t('pages', 'Последние выпуски');?></h2>
<?php
foreach($archiveProgrammsList as $data) {
    $url = Yii::app()->createUrl('/programms/default/viewArchive', array('id' => $data->id));
?>
        <article>
            <h3 class="small bul"><a href="<?= $url ?>" class="arrow"><?=$data->timetable->content->title?> &mdash; <?=$data->title?></a></h3>
        </article>
        <hr />
<?php } ?>
    </div>
</div>
