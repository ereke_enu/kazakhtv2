<?php
$i = 1;
foreach($programms as $program) {
    $image = ProgrammsContent::getAnounceImg($program->image);
    $url = Yii::app()->createUrl('/programms/default/program', array('sefname' => $program->sefname ));
?>
            <div class="col-xs-6 col-sm-4 clearfix<?=($i>$showXS) ? ' hidden-xs' : ''?>">
                
                <h3><a href="<?= $url ?>"><?= $program->title ?></a></h3>
				<a href="<?= $url ?>"><img src="<?= $image ?>" class="img-responsive" /></a>
                <!--<p><a href="<?//=$url ?>"><?//=$program->short_text ?></a></p>-->
            </div>
<?php
    $i++;
}
?>