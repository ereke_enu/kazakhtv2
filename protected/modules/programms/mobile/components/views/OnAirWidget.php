
<div class="block_onair">    <a href="<?=Yii::app()->createUrl('/pages/default/category', array('sefname' => 'live_now'))?>">
    <strong><?=Yii::t('programms', 'Сейчас в эфире');?></strong> &mdash; <span
        title="<?=$onAir->title?>"><?=$onAir->title?></span> </a>

</div>

<div class="block_time">
    <span><?=date('H:i', strtotime($onAir->time));?></span>
</div>
<?php if ($this->full) { ?>
    <div style="clear: both"></div>
    <div class="block_of_lists">
        <ul>
            <?php foreach ($nextProgramms as $program) {
                $title = explode('.', $program->title);
                $title=$title[0];
                ?>
                <li>
                    <div>
                        <?php if ($program->content !== null) {
                            $link = Yii::app()->createUrl('/programms/default/program', array('sefname' => $program->content->sefname));
                        } else {
                            $link = '#';
                        }?>
                        <a href="<?=$link?>">
                            <span class="list_time"><?=date('H:i', strtotime($program->time));?></span>
                            <span class="list_title"><?=$title?></span>

                            <?php
                            if(isset($program->content->image))
                                $image = ProgrammsContent::getAnounceImg($program->content->image, '');
                            else
                                $image = ProgrammsContent::getAnounceImg('', '');
                            ?>
                            <img src="<?=$image?>" width="160" alt="">

                        </a>
                    </div>
                </li>
            <?php }?>
        </ul>
        <div class="next"></div>
    </div>
<?php } ?>
