<div>
    <hr class="bold" />
    <h2><?=$title?></h2>

    <?php foreach ($programms as $program) { ?>
    <article>
        <h3 class="bul"><a href="<?= Yii::app()->createUrl('/programms/default/program', array('sefname' => $program->sefname)) ?>"><?=$program->title?></a></h3>
    </article>
    <hr />
    <?php } ?>
</div>