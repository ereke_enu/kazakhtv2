<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 27.03.13
 * Time: 15:09
 */

class listCategoriesWidget extends CWidget {

    public function run() {

        $category = ProgrammsCategories::model()->cache(3600)->findBySefname('tv_shows_spisok');
        $categories = $category->children()->findAll('active=1');
        
        $this->render('listCategoriesWidget', array('categories' => $categories));
    }

}
