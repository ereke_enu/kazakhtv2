<?php
/**
 * Description of listJournalsWidget
 *
 * @author �������
 */
    class FilmListWidget extends CWidget {

        public function run() {

            $category = ProgrammsCategories::model()->cache(3600)->find('sefname=:sefname AND active=1 AND lang_id=:lang', array(':sefname' => 'films_tv', ':lang' => LXController::getLangId()));
           // $categories = $category->children()->findAll('active=1');


            $this->render('FilmListWidget', array('categories' => $category));
        }

    }
 
 