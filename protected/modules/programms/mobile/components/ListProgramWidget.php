<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 17.01.13
     * Time: 16:43
     * To change this template use File | Settings | File Templates.
     */
    class ListProgramWidget extends CWidget {

        public function run() {
            $criteria = new CDbCriteria;
            $criteria->compare('t.active', '1');
            $criteria->compare('t.lang_id', Langs::getLangIdByCode(Yii::app()->language));
            $criteria->addNotInCondition('category.sefname', array('sundayprogrammes', 'films_tv', 'news_dop', 'teleserial'));
            $criteria->with = array('category');
            $models = ProgrammsContent::model()->findAll($criteria);

            $this->render('ListProgramWidget', array('models' => $models));
        }

    }
