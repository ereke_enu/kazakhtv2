<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 18.01.13
 * Time: 14:12
 * To change this template use File | Settings | File Templates.
 */
class OnAirWidgetNew extends CWidget {
    public $full = true;

    public function run() {
        $onAirProgram = ProgrammsTimetable::getOnAirProgram();

        if ($this->full) {
            $criteria = new CDbCriteria;
            $criteria->compare('active', '1');
            $criteria->compare('date', date('Y-m-d', time()));
            $criteria->compare('time', '>=' . date('H:i:00', time()));
            $criteria->order = 'time ASC';
            $criteria->limit = 3;
            if ($onAirProgram !== null) {
                $criteria->compare('id', '<>' . $onAirProgram->id);
            }

            $nextProgramms = ProgrammsTimetable::model()->findAll($criteria);
        } else {
            $nextProgramms = array();
        }
        $this->render('OnAirWidgetNew', array('onAir' => $onAirProgram, 'nextProgramms' => $nextProgramms));

    }
}
