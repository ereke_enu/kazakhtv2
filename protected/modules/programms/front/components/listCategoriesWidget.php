<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 27.03.13
     * Time: 15:09
     * To change this template use File | Settings | File Templates.
     */

    class listCategoriesWidget extends CWidget {

        public function run() {

            $category = ProgrammsCategories::model()->cache(3600)->find('sefname=:sefname AND active=1 AND lang_id=:lang', array(':sefname' => 'tv_shows_spisok', ':lang' => LXController::getLangId()));
            $categories = $category->children()->findAll('active=1');


            $this->render('listCategoriesWidget', array('categories' => $categories));
        }

    }