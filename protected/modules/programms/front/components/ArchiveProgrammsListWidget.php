<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 05.02.13
     * Time: 14:46
     * To change this template use File | Settings | File Templates.
     */

    class ArchiveProgrammsListWidget extends CWidget {
        public $categoryId = 0;
        public $program = 0;
        public $subCategories = false;
        public $title = '';

        public function run() {
            $category = ProgrammsCategories::model()->findBySefname($this->categoryId);

            $archiveCriteria = new CDbCriteria;

            if ($this->subCategories===true) {
                $categories = CHtml::listData($category->children()->findAll('active=1'), 'id', 'id');

                if (!empty($categories)) {
                    $archiveCriteria->addInCondition('content.category_id', $categories);
                }
            } else {
                if ($this->categoryId !== 0) {
                    $archiveCriteria->compare('content.category_id', $category->id);
                }
            }

            if ($this->program !== 0) {
                $archiveCriteria->compare('timetable.programm_id', $this->program);
            }

            $archiveCriteria->with = array('timetable', 'timetable.content');
            $archiveCriteria->order = 'timetable.date DESC, timetable.time DESC';
            $archiveCriteria->together = true;


            $archiveDataProvider = new CActiveDataProvider('VideoArchive', array(
                'criteria'   => $archiveCriteria,
                'pagination' => array(
                    'pageSize' => 10
                )
            ));

            $this->render('archiveProgrammsListWidget', array('dataProvider' => $archiveDataProvider, 'title' => $this->title));
        }

    }