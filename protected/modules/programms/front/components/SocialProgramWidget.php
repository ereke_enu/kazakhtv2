<?php

class SocialProgramWidget extends CWidget {

    public $category_sefname='none';
    
    public function run() {

        $langId = Langs::getLangIdByCode(Yii::app()->language);

        $criteria = new CDbCriteria;
        $category = ProgrammsCategories::findBySefname($this->category_sefname);

        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $category->id);
        
        $criteria->compare('active', 1);
        $criteria->compare('lang_id', $langId);
        $criteria->limit=2;
        $criteria->order = 'id DESC';

        $model = ProgrammsContent::model()->findAll($criteria);
        
        $this->render('SocialProgramWidget', array('model' => $model, 'category'=>$category));
    }
}