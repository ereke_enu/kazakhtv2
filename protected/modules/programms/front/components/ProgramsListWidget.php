<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 27.03.13
     * Time: 14:50
     * To change this template use File | Settings | File Templates.
     */

    class ProgramsListWidget extends CWidget {
        public $program;
        public $title = '';
        public $category;

        public function run() {
            $category = ProgrammsCategories::model()->findBySefname($this->category);
            if ($category === null) {
                return false;
            }

            $mtop=true;
            if ($category!='features') {
                $mtop=false;
            }


            $criteria = new CDbCriteria;
            $criteria->compare('category_id', $category->id);
            $criteria->order = 'date_create DESC';
            $criteria->compare('active', '1');
            $criteria->compare('id', '<>' . (int)$this->program);

            $programms = ProgrammsContent::model()->findAll($criteria);


            $this->render('programmsListWidget', array('title' => $this->title, 'programms' => $programms, 'mtop' => $mtop));

        }


    }