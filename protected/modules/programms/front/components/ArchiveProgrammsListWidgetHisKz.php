<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 05.02.13
     * Time: 14:46
     * To change this template use File | Settings | File Templates.
     */

    class ArchiveProgrammsListWidgetHisKz extends CWidget {
        public $categoryId = 0;
        public $program = 44;
        public $subCategories = false;
        public $title = '';

        public function run() {
            $category = 486;

            $archiveCriteria = new CDbCriteria;

            if ($this->subCategories===true) {
                $categories = 486;

                if (!empty($categories)) {
                    $archiveCriteria->addInCondition('content.category_id', $categories);
                }
            } else {
                if ($this->categoryId !== 0) {
                    $archiveCriteria->compare('content.category_id', $category->id);
                }
            }

            if ($this->program !== 0) {
                $archiveCriteria->compare('timetable.programm_id', $this->program);
            }

            $archiveCriteria->with = array('timetable', 'timetable.content');
            $archiveCriteria->order = 'timetable.date DESC, timetable.time DESC';
            $archiveCriteria->together = true;
            $archiveCriteria->limit = 3;
            
            $archiveProgrammsList = VideoArchive::model()->findAll($archiveCriteria);

/*
            $archiveDataProvider = new CActiveDataProvider('VideoArchive', array(
                'criteria'   => $archiveCriteria,
                'pagination' => array(
                    'pageSize' => 6
                )
            ));
 * 
 */

            $this->render('archiveProgrammsListWidgetNew', array('archiveProgrammsList' => $archiveProgrammsList, 'title' => $this->title));
        }

    }