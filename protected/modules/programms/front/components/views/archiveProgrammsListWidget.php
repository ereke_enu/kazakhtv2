﻿<div class="lastComment" >
    <div  class="anounce" >
        <div class="anounce-h"  ><h2><?=$title ?></h2></div>
    </div>
    <div class="comments_content">
        <div class="archive_content">
            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_archiveItem',
                'summaryText' => '',
                'itemsTagName' => 'ul',
                'pager' => array(
                    'header' => '',
                    'prevPageLabel' => '<img src="/themes/front/images/left_btn_partners.png">',
                    'nextPageLabel' => '<img src="/themes/front/images/right_btn_partners.png">',
                    'maxButtonCount' => 4
                ),
            ));
            ?>
        </div>
    </div>
</div>