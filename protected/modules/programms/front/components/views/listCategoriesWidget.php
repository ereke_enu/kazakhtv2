<div class="lastComment" style="margin-top:20px;">
    <div class="anounce">
        <div class="anounce-h"><h2><?=Yii::t('prgramms', 'Другие разделы')?></h2></div>
    </div>
    <div class="comments_content">
        <ul>
            <?php foreach ($categories as $category) {
                ?>
                <li>
                    <a href="<?= Yii::app()->createUrl('/programms/default/index', array('id' => $category->sefname)) ?>">
                        <?=$category->title?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>