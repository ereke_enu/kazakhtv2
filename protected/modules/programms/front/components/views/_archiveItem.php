<li class="<?=($index % 2) ? 'even' : 'odd'?>">
    <span class="date"><?=date('d.m.Y H:i', strtotime($data->timetable->date . ' ' . $data->timetable->time))?></span>
    <a href="<?=Yii::app()->createUrl('/programms/default/viewArchive', array('id' => $data->id))?>">
        <?=$data->timetable->content->title?> &mdash; <?=$data->title?>
    </a>
</li>