<div class="lastComment" style="margin-top:20px;">
    <div class="anounce">
        <div class="anounce-h"><h2><?=$title?></h2></div>
    </div>
    <div class="comments_content">
        <ul>
            <?php foreach ($programms as $program) {
                ?>
                <li>
                    <a href="<?= Yii::app()->createUrl('/programms/default/program', array('sefname' => $program->sefname)) ?>">
                        <?=$program->title?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>