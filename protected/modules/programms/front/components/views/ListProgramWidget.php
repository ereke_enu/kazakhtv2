<div class="programSliderC">
    <div class="programSlider">
        <div class="slider">
            <?php
            foreach ($models as $key => $item) {
            ?>
            <div class="slide">
                <div class="program">

                            <img src="<?=ProgrammsContent::getMainImg($item->main_image)?>" width="230" alt="">

                    <span><a href="<?=Yii::app()->createUrl('/programms/default/program', array('sefname' => $item->sefname))?>"><?=$item->title?></a></span>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <ul class="slideSelectors">
        <li><span class="prev"><img src="/themes/front/images/programms_prev.png" alt=""/></span></li>
        <li><span class="next"><img src="/themes/front/images/programms_next.png" alt=""/></span></li>
    </ul>
</div>