<?php
    include "./protected/components/Translator.php";
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 15.01.13
     * Time: 10:16
     * To change this template use File | Settings | File Templates.
     */
    class DefaultController extends LXController {
        public $layout = '//layouts/second';


        /**
         * Вывод программ, если указан ID, то вывод из указанной категории по ID
         * @param string $id
         */
        public function actionIndex($id = '') {
            $this->layout = '//layouts/live';
            
            $lang=Yii::app()->language;
            switch ($lang) {
              case 'kz':
                $this->metaTags="Бағдарлама кестесі, бағдарламалар, журналдар, жексенбілік бағдарламалар, фильмдер, телехикаялар, Қазақстан, әлем, спорт, мәдениет, трейлер, жарнама, Астана, Алматы, Еуразия, Азия, Еуропа, Америка, эфир кестесі, аналитикалық, танымдық, Саяхат, туризм, арнайы жобалар, Қазақстан, стартаптар, ТМД елдері, экономика,ЭКСПО-2017";
                $this->metaDescription="Kazakh-tv.kz – тәулік бойы жаңартылатын ақпараттар легі. Қазақстан, Ресей және әлемдегі бизнес экономика, оқиға, спорт туралы соңғы жаңалықтар";
                break;
              case 'ru':
                $this->metaTags="Программа передач, Программы, Журналы, Воскресные программы, Фильмы, Телесериалы, Казахстан, Мир, Спорт, Культура, трейлер, реклама, Астана, Алматы, Евразия, Азия, Европа, Америка, Сетка вещания, Аналитика, Познавательные, Публицистика, Туризм,Специальные проекты, EXPO 2017, маркетинг, индустрия, успешные, стартапы, СНГ";
                $this->metaDescription="Kazakh-tv.kz - круглосуточно обновляемая лента новостей. Последние новости Казахстана, России и мира о бизнесе, экономике, происшествиях, спорте";
                break;
                case 'en':
                $this->metaTags="Programme schedule, programmes, features, Sunday programmes, Movies, Films, TV Series, Kazakhstan, World, Sport, Culture, trailer, advertising, Astana, Almaty, Eurasia, Asia, Europe, America, broadcasting schedule, Research, Learning, Reading, Travel, Special projects, EXPO 2017, marketing, industry, successful peoples, startups, CIS";
                $this->metaDescription="Kazakh-tv.kz - 24 hours updated news. The latest business, economics, sports and accedent news from Kazakhstan, Russia and the world";
                break;
              default:
                $this->metaTags="Бағдарлама кестесі, бағдарламалар, журналдар, жексенбілік бағдарламалар, фильмдер, телехикаялар, Қазақстан, әлем, спорт, мәдениет, трейлер, жарнама, Астана, Алматы, Еуразия, Азия, Еуропа, Америка, эфир кестесі, аналитикалық, танымдық, Саяхат, туризм, арнайы жобалар, Қазақстан, стартаптар, ТМД елдері, экономика,ЭКСПО-2017";
                $this->metaDescription="Kazakh-tv.kz – тәулік бойы жаңартылатын ақпараттар легі. Қазақстан, Ресей және әлемдегі бизнес экономика, оқиға, спорт туралы соңғы жаңалықтар";
                break;
            }

            $langId = Langs::getLangIdByCode($lang);
            $category = null;

            $criteria = new CDbCriteria;
            $criteria->compare('t.lang_id', $langId);
            $isSecond = true;
            if ($id == '') {
                $category = ProgrammsCategories::model()->find('lang_id=:lang_id AND parent_id=:parent_id', array(':lang_id' => $langId, ':parent_id' => 0));
                $isSecond = false;
                //$this->pageTitle = Yii::t('programms', 'Программы');
                switch ($lang) {
                  case 'kz':
                        $this->pageTitle="Бағдарламалар - ҚАЗАҚСТАН РЕСПУБЛИКАСЫНЫҢ АЛҒАШҚЫ ҰЛТТЫҚ СПУТНИКТІК АРНАСЫ";
                    break;
                  case 'ru':
                        $this->pageTitle="Программы - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН";
                    break;
                    case 'en':
                        $this->pageTitle="Programms - THE FIRST NATIONAL SATELLITE CHANNEL OF THE REPUBLIC OF KAZAKHSTAN";
                    break;
                  default:
                        $this->pageTitle="Программы - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН";
                    break;
                }
            } else {
                $category = ProgrammsCategories::model()->findBySefname($id);
                $this->pageTitle = $category->title;
            }
            if ($category === null) {
                //    $this->redirect(array('mainpage/default/error'));
                throw new CHttpException(404, 'Страница не найдена');
            }

            $criteria->compare('active', 1);

            if ($category->isRoot()) {
                $categories = $category->children()->findAll('active=1');
                $childrenCategories = CHtml::listData($categories, 'id', 'id');
                if (!empty($childrenCategories)) {
                    $criteria->addInCondition('category_id', $childrenCategories);
                } else {
                    $criteria->compare('category_id', $category->id);
                }

            } else {
                $criteria->compare('category_id', $category->id);
                $categories = $category->parent()->find()->children()->findAll('active=1');
            }

            if ($category->sefname == 'features') {
                $pageSize = 9;
                $this->layout = '//layouts/features';
            } else {
                
                $pageSize = 32;
                $this->layout = '//layouts/listProgramms';
                if($category->sefname=="films_tv"||$category->sefname=="teleserial")
                    $this->layout = '//layouts/listProgrammsNoSidebar';
            }

            $anounceCriteria = new CDbCriteria;
            $anounceCriteria->compare('in_anounce', '1');
            $anounceCriteria->order = 'DATE DESC, time ASC';
            $anounceCriteria->compare('lang_id', LXController::getLangId());

            $anounceProgramms = ProgrammsTimetable::model()->find($anounceCriteria);

            $criteria->order = 'date_create DESC';

            $dataProvider = new CActiveDataProvider('ProgrammsContent', array(
                'criteria'   => $criteria,
                'pagination' => false,
            ));

            $this->render('index', array('anounce' => $anounceProgramms, 'categories' => $categories, 'category' => $category, 'dataProvider' => $dataProvider, 'isSecond' => $isSecond));

        }

        public function actionProgram($sefname) {
            $lang = Langs::getLangIdByCode(Yii::app()->language);

            $program = ProgrammsContent::model()->active()->findBySefname($sefname);

            if ($program->category->sefname == 'features') {
                $this->layout = '//layouts/journals';
            } else {
                $this->layout = '//layouts/viewProgram';
                $this->program_category_id = $program->category->sefname;
                $this->program_category_title = $program->category->title;
            }

            $this->program_id = $program->id;

            if ($program === null) {
                throw new CHttpException(404, 'Страница не найдена');
            }

            $category = $program->category->sefname;

            if ($program->lang_id != $lang) {
                $otherIds = CJSON::decode($program->other_id);

                if (is_array($otherIds) && !empty($otherIds[Yii::app()->language])) {
                    $program = ProgrammsContent::model()->active()->findByPk($otherIds[Yii::app()->language]);

                    if ($program === null) {
                        $this->redirect(array('index', 'id' => $category));
                        throw new CHttpException(404, 'Страница не найдена');
                        }
                }
            }
            /*
            $pl = array();
            if (count($program->videos) > 1) {
                foreach ($program->videos as $video) {
                    if ($video->upload_ext == 'flv') {
                        $filename = $video->upload_file;
                    } else {
                        $filename = $video->filename . '_[240,320,480].mp4';
                    }
                    $pl['playlist'][] = array(
                        'file' => '/upload/video/programms/' . $video->page_id . '/' . $filename,
                        'comment' => $video->title,
                        'poster' => '/upload/video/programms/' . $video->page_id . '/' . $video->filename . '.jpg',
                        'bigposter' => '/upload/video/programms/' . $video->page_id . '/' . $video->filename . '.jpg'
                    );
                }
            } elseif (count($program->videos) == 1) {
                $video = $program->videos[0];
                if ($video->upload_ext == 'flv') {
                    $filename = $video->upload_file;
                } else {
                    $filename = $video->filename . '_[240,320,480].mp4';
                }
                
                $params = array(
                'file' => '/upload/video/programms/' . $video->page_id . '/' . $filename,
                'comment' => $video->title,
                'poster' => '/upload/video/programms/' . $video->page_id . '/' . $video->filename . '.jpg',
                'big_poster' => '/upload/video/programms/' . $video->page_id . '/' . $video->filename . '.jpg'
                );
            }

            if (empty($params)) {
                $params = array('pl' => $pl, 'file' => '');
            }
            */
            $archiveCriteria = new CDbCriteria;
            $archiveCriteria->order = 'views DESC';
            $archiveCriteria->limit = 8 * 3;
            $archiveCriteria->with = array('timetable');
            $archiveCriteria->compare('timetable.programm_id', $program->id);
            $archiveCriteria->order = 'timetable.date DESC, timetable.time DESC';

            $dataProvider = new CActiveDataProvider('VideoArchive', array(
            'criteria' => $archiveCriteria,
            'pagination' => array(
            'pageSize' => 9
            )
            ));

            $this->pageTitle = $program->title;
            $this->metaTags = $program->meta_tags;
            $this->metaDescription = $program->meta_description;

            $last_program;
            foreach ($dataProvider->getData() as $item) { 
            $last_program = $item;
            break;
            }
            /*
            $pl2 = array();
             if (count($model->videos) > 1) { 
            foreach ($last_program->videos as $video) {
            if ($video->upload_ext == 'flv') {
            $filename = $video->upload_file;
            } else {
            $filename = $video->filename . '_[240,320,480].mp4';
            }
            $pl2['playlist'][] = array(
            'file' => '/upload/video/archive/' . $video->page_id . '/' . $filename,
            'comment' => $video->title,
            'poster' => '/upload/video/archive/' . $video->page_id . '/' . $video->filename . '.jpg',
            'bigposter' => '/upload/video/archive/' . $video->page_id . '/' . $video->filename . '.jpg'
            );
            }
            $params2 = array('pl' => $pl2, 'file' => '');
            */

            $this->render('program', array('model' => $program, 'dataProvider' => $dataProvider, 'last_program'=>$last_program));
        }

        private function get_first_day_of_week($date = 0) {
            if ($date <= 0) {
                $date = time();
            }

            $dayweek = date('w', $date);

            if ($dayweek == 1) {
                return $date;
            } elseif ($dayweek == 0) {
                return $date - (6 * 86400);
            } else {
                return $date - (($dayweek - 1) * 86400);
            }
        }

        public function actionTimetable($date = 0) {
            $this->layout = '//layouts/timetable';
            if(isset($_GET['date'])&&$_GET['date']!=''){
                $date = date('Y-m-d',strtotime($_GET['date']));
            }elseif ($date == 0) {
                $date = date('Y-m-d', time());
                $timest_fwd = 0;
            } elseif (strtotime($date) < strtotime(date('Y-m-d', time()))) {
                $date = $date;
                $timest_fwd = 0;
            } else {
                $date = $date;
                $timest_fwd = strtotime($date);
            }


            $selDate = $date;
            $criteria = new CDbCriteria;
            $criteria->select = 't.*, content.*';
            $criteria->with = array('content');
            $criteria->order = 'time ASC';
            $criteria->compare('t.active', '1');
            $criteria->compare('date', $selDate);

            $programs = ProgrammsTimetable::model()->cache(7200)->findAll($criteria);

            $monday = $this->get_first_day_of_week($timest_fwd);

            if (!empty($timest_fwd)) {
                $next_monday = $monday - 86400 * 7;
            } else {
                $next_monday = $monday + 86400 * 7;
            }

            //$onAirProgram = ProgrammsTimetable::getOnAirProgram();

            $this->pageTitle = Yii::t('programms', 'Программа передач');

            if (Yii::app()->request->isAjaxRequest) {
                $this->renderPartial('timetable', array('next_monday' => $next_monday, 'monday' => $monday, 'timest_fwd' => $timest_fwd, 'date' => $selDate, 'programs' => $programs, ));
            } else {
                $this->render('timetable', array('next_monday' => $next_monday, 'monday' => $monday, 'timest_fwd' => $timest_fwd, 'date' => $selDate, 'programs' => $programs, ));
            }


        }

        public function actionLive() {

            $this->layout = '//layouts/live';
            $criteria = new CDbCriteria;
            $criteria->compare('date', date('Y-m-d', time()));
            $criteria->order = 'time ASC';
            $criteria->compare('active', '1');
            $programms = ProgrammsTimetable::model()->findAll($criteria);
            $this->pageTitle = 'Прямой эфир';
               $lang=Yii::app()->language;
               switch ($lang) {
                  case 'kz':
                        $this->pageTitle="Тікелей эфир - ҚАЗАҚСТАН РЕСПУБЛИКАСЫНЫҢ АЛҒАШҚЫ ҰЛТТЫҚ СПУТНИКТІК АРНАСЫ Kazakh TV";
                        break;
                  case 'ru':
                        $this->pageTitle="Прямой эфир - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                        break;
                    case 'en':
                        $this->pageTitle="Live now - THE FIRST NATIONAL SATELLITE CHANNEL OF THE REPUBLIC OF KAZAKHSTAN Kazakh TV";
                        break;
                  default:
                        $this->pageTitle="Прямой эфир - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
                        break;
                }
            

            $page = Pages::model()->with('category')->find('category.sefname="live_now" AND t.lang_id=:lang', array(':lang' => LXController::getLangId()));


            $this->render('live', array('programs' => $programms, 'page' => $page));
        }

        public function actionArchive($sefname = 'tv_shows_spisok', $program_id = 0, $day = 0, $month = 0, $year = 0) {
            $this->layout = '//layouts/program_archive';

            $category = ProgrammsCategories::model()->findBySefname($sefname);
            if ($category === null) {
                throw new CHttpException(404, 'Страница не найдена');
            }

            $this->pageTitle = $category->title;

            $childCategories = $category->children()->findAll('active=1');
            if (!empty($childCategories)) {
                $categories = CHtml::listData($childCategories, 'id', 'id');
            } else {
                $categories = array($category->id);
            }


            $criteria = new CDbCriteria;
            $criteria->compare('timetable_id', '<>0');
            $criteria->order = 'timetable.date DESC, timetable.time DESC';

            if ($day != 0 && $month != 0 && $year != 0) {
                $date = $year . '-' . $month . '-' . $day;
                if ($date != 0) {
                    $criteria->compare('timetable.date', $date);
                }
            }

            if ($program_id != 0) {
                $criteria->compare('timetable.programm_id', $program_id);
            }

            $criteria->addInCondition('content.category_id', $categories);
            $criteria->with = array('timetable', 'timetable.content');
            $criteria->together = true;


            $dataProvider = new CActiveDataProvider('VideoArchive', array(
                'criteria'   => $criteria,
                'pagination' => array(
                    'pageSize' => 6
                )));

            $this->program_category_id = $sefname;

            $this->render('archive', array('dataProvider' => $dataProvider));
        }

        public function actionViewArchive($id) {
            $this->layout = '//layouts/programms';
            $model = VideoArchive::model()->findByPk((int)$id);
            if ($model === null) {
                throw new CHttpException(404, 'Страница не найдена');
            }

            /*
            $pl = array();
             if (count($model->videos) > 1) {     
            foreach ($model->videos as $video) {
                if ($video->upload_ext == 'flv') {
                    $filename = $video->upload_file;
                } else {
                    $filename = $video->filename . '_[240,320,480].mp4';
                }
                $pl['playlist'][] = array(
                    'file'      => '/upload/video/archive/' . $video->page_id . '/' . $filename,
                    'comment'   => $video->title,
                    'poster'    => '/upload/video/archive/' . $video->page_id . '/' . $video->filename . '.jpg',
                    'bigposter' => '/upload/video/archive/' . $video->page_id . '/' . $video->filename . '.jpg'
                );
            }*/
            /*  } elseif (count($model->videos) == 1) {
                  $video = $model->videos[0];
                  if ($video->upload_ext == 'flv') {
                      $filename = $video->upload_file;
                  } else {
                      $filename = $video->filename . '_[240,320,480].mp4';
                  }
                  $pl = array(
                      'file' => '/upload/video/pages/' . $video->page_id . '/' . $filename,
                      'comment' => $video->title,
                      'poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg',
                      'big_poster' => '/upload/video/pages/' . $video->page_id . '/' . $video->filename . '.jpg'
                  );
              } */

            $this->program_id = $model->timetable->programm_id;


            //$params = array('pl' => $pl, 'file' => '');


            $this->pageTitle = $model->title;

            $archiveCriteria = new CDbCriteria;
            $archiveCriteria->order = 'views DESC';
            $archiveCriteria->limit = 8 * 3;
            $archiveCriteria->with = array('timetable');
            $archiveCriteria->compare('timetable.programm_id', $this->program_id);
            $archiveCriteria->order = 'timetable.date DESC, timetable.time DESC';

             $dataProvider = new CActiveDataProvider('VideoArchive', array(
                'criteria'   => $archiveCriteria,
                'pagination' => array(
                    'pageSize' => 9
                )
            ));

            $this->render('archiveView', array('model' => $model,'dataProvider'=>$dataProvider));
        }

        public function actionDontmiss(){
            $this->layout = '//layouts/live';

            $criteria = new CDbCriteria;
            $criteria->compare('date', date('Y-m-d', time()));
            $criteria->order = 'time ASC';
            $criteria->compare('active', '1');
            $programms = ProgrammsTimetable::model()->findAll($criteria);

            $this->render('dontmiss', array('programs' => $programms));
            
        }
    }
