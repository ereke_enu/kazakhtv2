<? 
    $parent = $model->timetable->content->category->sefname;
    if($parent!="features"){ $parent="programms"; };
    $program =$model->timetable->content;  
 ?>
<script type="text/javascript">
    menu_category='<?= $parent ?>';
</script>
<div class="content second program">
      <div id="program_block">
        <div class="section_title">
            <?=$program->title?>
        </div>
        
        <div class="text">
            <?=$program->text?>
        </div>
    </div>
<div style="width:100%;border-bottom:1px solid #d7d7d7;margin-bottom:15px;"></div>

    <div id="text">
        <h3><?=$model->title?></h3>
            <span class="date">
                    <?= date('d.m.Y H:i', strtotime($model->timetable->date . ' ' . $model->timetable->time)) ?>
            </span>

        <article>
            <?=$model->text?>
        </article>
        <?php $this->widget('shareWidget');?>

    </div>


<?php //Pages::model()->getSimilar($model['id']); ?>
<?php if ($model->timetable->content->show_comments == 1 || ($model->timetable->content->show_comments == -1 && $model->timetable->content->category->show_comments == 1)) {
    ?>

    <div class='comments'>
        <?php $this->widget('BlockComments', array(
                'type' => 'videoArchive',
                'id' => $model->id)
        ); ?>

    </div>
<?php } ?>


<div class="section_title">
            <?=ttt('Последние передачи') ?>
        </div>
<div id="video_archives">
        <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_archive_item',
                'summaryText' => '',
                'id' => 'left_news',
                'pager' => array(
                    'prevPageLabel' => '<',
                    'nextPageLabel' => '>',
                    'header' => ''
                )
            ));
        ?>

    </div>
    </div>