<h1 class="ib">Новости</h1>
<?
 $menu_id = Categories::model()->findBySefname('news');
//var_dump($menu_id['id']);

$menu = Categories::model()->findByPk($menu_id['id']);

?>
<ul class="news_menu">
<?
    foreach($menu->child as $item){
        echo '<li>'.CHtml::link($item->title, '/'.Yii::app()->language.'/allNews/'.$item->sefname).'</li>';
    }
?>
</ul>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_allNews_item',
    'summaryText' => '',
    'id' => 'left_news',
    'pager' => array(
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
        'header' => ''
    )
));
?>