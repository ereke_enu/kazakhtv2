 <div class="section_title">
        <?=Yii::t('programms', 'Программа передач')?>
    </div>
<script type="text/javascript">

function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var d = getParameterByName('date');
    if(d==null){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 
        d = yyyy+'-'+mm+'-'+dd;
    }
    $(document).ready(function(){
        $('.tptimetable td').each(function(){
            if($(this).children("a:first").attr('data-date')==d){
                $(this).css('background','#00506e url(../img/bg-navbar.png) bottom repeat-x');
                $(this).children("a:first").css('color','white');
                $(this).find('.select_date').css('color','white');
            }
        });

    })
    

</script>

<div id="timetable_data">
    <section class="timetable_all">
        <div class="nextWeek"><a data-date="<?=date('Y-m-d', $next_monday)?>" href="<?= Yii::app()->createUrl('/programms/default/timetable', array('date' =>date('Y-m-d', $next_monday))) ?>&next=1"><?=Yii::t('programms', 'Следующая неделя')?></a>
        </div>
        <table>
            <tr class="tptimetable">
                <td class="timetable_date">
                    CET <br>
                    GMT +0
                </td>
                <td class="timetable_date">
                    Moscow <br>
                    GMT +2
                </td>
                <td class="timetable_date">
                    Astana <br>
                    GMT +5
                </td>
                <?php
                $active_day = '';

                for ($mon_num = 1; $mon_num < 8; $mon_num++) {
                    
                    if (date('Y-m-d', $monday) == $date) {
                        $active_day = "class='active'";
                    }
                    ?>
                    <td <?=$active_day?>>
                        <a class="getTimeTable" href="?date=<?=date('Y-m-d', $monday);?>"
                           data-date="<?=date('Y-m-d', $monday);?>"><?=Yii::app()->dateFormatter->format('EE', $monday)?>
                            <div class="select_date">
                                <?=Yii::app()->dateFormatter->format('d', $monday)?> <?=Yii::app()->dateFormatter->format('MMMM', $monday)?></div>
                        </a>
                    </td>
                    <?php
                    $monday += 86400;
                }
                ?>
            </tr>


<!--            <tr>-->
<!--                <th class="time_0">GMT +0</th>-->
<!--                <th class="time_2">GMT +2</th>-->
<!--                <th class="time_5">GMT +5</th>-->
<!--                <th class="noborder" colspan="7">&nbsp;</th>-->
<!--            </tr>-->

            <?php foreach ($programs as $program) {
            $onAirClass = '';
            if ($program->id == $onAirProgram->id) {
                $onAirClass = 'active';
            }

            $beforeClass = '';
            if (($program->time < $onAirProgram->time && $program->date <= $onAirProgram->date)) {
                $beforeClass = "beforeNow";
            }
            ?>
            <tr class="row">
                <td
                    class="time_value"><?=Yii::app()->dateFormatter->format('H:mm', (strtotime($program->time) - 3600 * 4))?></td>
                <td
                    class="time_value"><?=Yii::app()->dateFormatter->format('H:mm', (strtotime($program->time) - 3600 * 3))?></td>
                <td class="time_value"><?=Yii::app()->dateFormatter->format('H:mm', strtotime($program->time))?></td>
                <td class="timetable_title <?=$onAirClass?> <?=$beforeClass?>" colspan="7">
                    <?php if ($program->content !== null) {
                        ?>
                        <div class="program_content">
                            <h2>
                                <a href="<?=Yii::app()->createUrl('/programms/default/program', array('sefname' => $program->content->sefname))?>"><?=$program->title?></a>
                            </h2>

                            <div class="description">
                                <div class="img"> <?php
                                    $image = ProgrammsContent::getAnounceImg($program->content->image, '104_');
                                    ?>
                                    <img src="<?=$image?>" width="130" alt="">
                                </div>
                                <div class="short_text"><?=$program->content->short_text?></div>
                            </div>

                        </div>
                    <?php
                    } else {
                        echo $program->title;
                    }
                    ?>
                </td>
                <?php } ?>
            </tr>
        </table>
    </section>
</div>