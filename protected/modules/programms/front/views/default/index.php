<div class="content second program" <? if($category->sefname=="teleserial"||$category->sefname=="films_tv") { ?> style="width:100%;" <? } ?> >
    <?php if ($anounce !== null) { ?>
        <div class="anounce_program">
            <div class="anounce_header"><?= Yii::t('programms', 'Смотрите') ?></div>
            <div class="anounce_program_data">
                <div class="img">
                    <?php

                        $image = ProgrammsTimetable::getImage($anounce->image, $anounce->programm_id);

                        if ($image === false) {
                            $image = ProgrammsContent::getAnounceImg($anounce->content->image);
                        }
                    ?>
                    <a href="<?= Yii::app()->createUrl('/programms/default/program', array('sefname' => $anounce->content->sefname)) ?>">
                        <img src="<?= $image ?>" alt="" width="310"/>
                    </a>
                </div>
            </div>
            <div class="anounce_text">
                <h2>
                    <a href="<?= Yii::app()->createUrl('/programms/default/program', array('sefname' => $anounce->content->sefname)) ?>">
                        <?= $anounce->title ?></a></h2>
                <?= $anounce->description ?>
            </div>
        </div>

    <?php } ?>

    <div class="section_title"><?= ($isSecond) ? $category->title : Yii::t('programms', 'Программы'); ?></div>
    <?php //if (!empty($categories)) { ?>
    <?php if (1==2) { ?>
        <ul class="news_menu">
            <?php
                foreach ($categories as $item) {
                    $class = '';
                    if ($category->id == $item->id) {
                        $class = 'active';
                    }
                    ?>
                    <li class="<?= $class ?>">
                        <a href="<?= $this->createUrl('/programms/default/index', array('id' => $item->sefname)) ?>"><?= $item->title ?></a>
                    </li>
                <?php } ?>
        </ul>
    <?php } ?>
    <div>
        <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView'     => '_itemIndex',
                'emptyText'    => '',
                'summaryText'  => '',
                'id'           => 'left_news',
                'pager'        => array(
                    'header'        => '',
                    'prevPageLabel' => '<img src="/themes/front/images/left_btn_partners.png">',
                    'nextPageLabel' => '<img src="/themes/front/images/right_btn_partners.png">'
                )
            ));
        ?>
    </div>
</div>