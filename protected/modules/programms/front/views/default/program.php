<? 
$parent = $model->category->sefname;
if($parent!="features"&&$parent!="sundayprogrammes"&&$parent!="teleserial"&&$parent!="films_tv"){ $parent="programms"; }
?>
<script type="text/javascript">
menu_category='<?= $parent ?>';
</script>
<div class="content second program">
<div id="program_block">
<div class="section_title">
<?=$model->title?>
</div>

<div class="text">

<?=$model->text?>

</div>
</div>
<div style="width:100%;border-bottom:1px solid #d7d7d7;margin-bottom:15px;"></div>

<?php if($last_program->title!=""){ ?>
<div id="text">
<h3><?=$last_program->title?></h3>
<span class="date">
<?= date('d.m.Y H:i', strtotime($last_program->timetable->date . ' ' . $last_program->timetable->time)) ?>
</span>

<?php
if (!empty($last_program->videos)) {

?>
<div id="videoplayer_content">
<?php //$this->widget('ext.uppod.LXUppodWidget', array('width' => 688, 'params' => CMap::mergeArray(Yii::app()->params['videoPlayer'], $params2)))?>

</div>
<? }?>

<article>
<?=$last_program->text?>
</article>
<?php $this->widget('shareWidget');?>

</div>
<? } ?>
<?php if ($last_program->timetable->content->show_comments == 1 || ($last_program->timetable->content->show_comments == -1 && $last_program->timetable->content->category->show_comments == 1)) {
?>

<div class='comments'>
<?php $this->widget('BlockComments', array(
'type' => 'videoArchive',
'id' => $last_program->id)
); ?>

</div>
<?php } ?>
<? if($model->category->sefname!="films_tv"&&$model->category->sefname!="teleserial"){ ?>
<div class="section_title">
<?=ttt('Последние передачи') ?>
</div>
<div id="video_archives">
<?php $this->widget('zii.widgets.CListView', array(
'dataProvider' => $dataProvider,
'itemView' => '_archive_item',
'summaryText' => '',
'id' => 'left_news',
'pager' => array(
'prevPageLabel' => '<',
'nextPageLabel' => '>',
'header' => ''
)
));
?>
</div>
<? } ?>
</div>