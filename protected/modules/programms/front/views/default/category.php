<h1><?=$category['title']?></h1>
<?php if (!empty($pages)) {
    $page = $pages[0];
    ?>
    <div id="text">

    <h2><?=$page['title']?></h2>
    <article>
    <div class="date"><?=Yii::app()->dateFormatter->formatDateTime(strtotime($page['date']), 'medium', false)?></div>
    <div class="text">
        <?=$page['text']?>
    </div>
</article>
    </div>

<?php } ?>
