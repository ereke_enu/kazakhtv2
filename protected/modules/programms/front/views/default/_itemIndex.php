<div class="news_item program" style="text-decoration:none !important;" data-href="<?=Yii::app()->createUrl('/programms/default/program', array('sefname' => $data->sefname))?>">
    <a style="text-decoration:none;" href="<?=Yii::app()->createUrl('/programms/default/program', array('sefname' => $data->sefname))?>">
        <div class="news_anounce">
            
            <span class="program_title"><?=$data->title?></span>
            <?php if($data->category->sefname!="features"&&$data->category->sefname!="sundayprogrammes"&&$data->category->sefname!="films_tv"&&$data->category->sefname!="teleserial"){ ?>
            <div style="font-size:12px;font-style:italic;">
                <?=$data->category->title?>
            </div>
            <?php } ?>
        </div>


        <?php

        if (($image = ProgrammsContent::getAnounceImg($data->image, '')) !== false) {
            ?>
            <div class="programm_img">
                <img src="<?=$image?>" width="200" alt="">
            </div>
            <?php
        }
        ?>
        <?php if($data->category->sefname!="features"&&$data->category->sefname!="sundayprogrammes"){ ?>
        <p style="text-decoration:none !important;">
        
        <?=mb_substr($data->short_text, 0, 100, 'utf8')?>...
        </p>
        <?php } ?>
        </a>
</div>