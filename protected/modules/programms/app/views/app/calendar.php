<section class="timetable-calendar">
	<div class="container">
		<div class="years">
			<ul class="wrapper">
				<?php foreach($years as $year) { ?>
					<li data-value="<?= $year['year'] ?>" class="slide<?= ($year['year'] == $date->format('Y')) ? ' active' : '' ?>"><?= $year['year'] ?></li>
				<?php } ?>
			</ul>
		</div>

		<div class="clearfix"></div>
		<hr>

		<div class="monthes">
			<ul class="wrapper">
				<?php
				$dates = new DateTime();
				$dates->setDate($date->format('Y'), 1, 1);
				?>
				<?php for($i=1; $i <= 12; $i++) { ?>
					<li data-value="<?= $i ?>" class="slide<?= ($dates->format('m') == $date->format('m')) ? ' active' : '' ?>"><?= Yii::app()->dateFormatter->format('LLLL', strtotime($dates->format('Y-m-d'))); ?></li>
					<?php $dates->modify('+1 month') ?>
				<?php } ?>
			</ul>
		</div>

		<div class="clearfix"></div>
		<hr>

		<div class="days">
			<ul class="wrapper">
				<?php for($i=1; $i<=(int)$date->format('t'); $i++) { ?>
					<li data-value="<?= $i ?>" class="slide<?= ($i == (int)$date->format('j')) ? ' active' : '' ?>"><?= $i ?></li>
				<?php } ?>
			</ul>
		</div>

		<div class="clearfix"></div>

		<div class="text-center" style="margin-top: 10px;">
			<a id="openTimetable" class="btn btn-white" href="<?= Yii::app()->createUrl('programms/app/timetable') ?>"><strong><?= Yii::t('app', 'Открыть') ?></strong></a>
		</div>

	</div>
</section>