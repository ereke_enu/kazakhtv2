<div class="block-news">
	<div class="row">
		<?php
		$i = 1;
		foreach($archiveProgramms as $series) {
			$this->renderPartial('_program_item', array('program'=>$program, 'series'=>$series, 'iterator'=>$i++));
		}
		?>
	</div>
	<div class="clearfix"></div>
</div>

<?php $this->widget('CLinkPager', array(
	'pages' => $pagination,
	'header' => false,
	'nextPageLabel' => '&raquo;',
	'prevPageLabel' => '&laquo;',
	'maxButtonCount' => 0,
	'htmlOptions' => array(
		'id' => 'pagination',
		'class' => 'pager'
	)
))?>