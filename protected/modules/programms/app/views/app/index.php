<?php foreach ($categories as $item) { ?>

	<h2><?= $item->title ?></h2>

	<?php $programs = $item->programs; ?>

	<div class="row">
	<?php foreach($programs as $program) { ?>
		<?php $url = Yii::app()->createUrl('programms/app/program', array('sefname'=>$program->sefname)); ?>

		<div class="col-xs-6 col-sm-4 col-programm">
			<article class="program">
				<div class="img-16x9">
					<?php
						$image = ProgrammsContent::getAnounceImg($program->image, '', '/themes/front/images/noimage.jpg');
						if(!$image) $image = '/themes/front/images/noimage.jpg';
					?>
					<a href="<?= $url ?>" class="out" style="background-image: url(<?= $image ?>)"></a>
				</div>
				<h3><a href="<?= $url ?>"><?= $program->title ?></a></h3>
			</article>
		</div>
	<?php } ?>
	</div>
	<div class="clearfix"></div>
<?php } ?>
