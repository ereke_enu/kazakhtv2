<?php $date = new DateTime($series->timetable->date.' '.$series->timetable->time, new DateTimeZone('Asia/Almaty')); ?>
<?php $url = Yii::app()->createUrl('programms/app/view', array('sefname'=>$program->sefname, 'id'=>$series->id)); ?>

<div class="col-xs-12 col-sm-6 col-md-4 news-item">
	<article>
		<div class="row ">
			<div class="col-xs-6 col-image">
				<div class="img-16x9">
					<?php if (!empty($series->videos[0]) && ($image = Videos::getAnounceImg($series->videos[0]->filename . '.jpg', $series->id)) !== false) { ?>
						<a class="out" href="<?= $url ?>" style="background-image: url(<?= $image ?>)">
							<span class="play"></span>
						</a>
					<?php } else { ?>
						<a class="out" href="<?= $url ?>" style="background-image: url(/themes/front/images/noimage.jpg)"></a>
					<?php } ?>
				</div>
			</div>
			<div class="col-xs-6 col-title">
				<h3><a href="<?= $url ?>"><?= $series->title ?></a></h3>
			</div>
		</div>
		<div class="date"><?= $date->format('j.m.Y G:i') ?></div>
	</article>
</div>