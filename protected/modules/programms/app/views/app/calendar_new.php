<section class="timetable-calendar-new">
	<div class="container">
		<div class="years">
			<ul class="wrapper"></ul>
		</div>

		<div class="clearfix"></div>
		<hr>

		<div class="monthes">
			<ul class="wrapper"></ul>
		</div>

		<div class="clearfix"></div>
		<hr>

		<div class="days">
			<ul class="wrapper">
				<?php foreach($days as $i => $day) { ?>
				<li class="slide<?= ($day['date'] == $date->format('Y-m-d') ? ' active' : '') ?>" data-value="<?= $day['date'] ?>">
					<span class="weekday"></span>
					<?= $day['day'] ?>
				</li>
				<?php } ?>
			</ul>
		</div>

		<div class="clearfix"></div>

		<div class="text-center" style="margin-top: 10px;">
			<a id="openTimetable" class="btn btn-white" href="<?= Yii::app()->createUrl('programms/app/timetable') ?>"><strong><?= Yii::t('app', 'Открыть') ?></strong></a>
		</div>

	</div>
</section>