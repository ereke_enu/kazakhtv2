<section class="timetable_all">
	<table class="table table-bordered timetable-header">
		<tr>
			<th width="15%" class="no-padding">
				CET <br>
				GMT&nbsp;+0
			</th>
			<th width="15%" class="no-padding">
				Moscow <br>
				GMT&nbsp;+2
			</th>
			<th width="15%" class="no-padding">
				Astana <br>
				GMT&nbsp;+5
			</th>
			<th class="text-uppercase" width="40%">
				<div class="text-left">
				<?= Yii::app()->dateFormatter->format(Yii::t('article','EEEE'), $now->format('Y-m-d')); ?><br>
				<?= $now->format('j.m.Y'); ?>
				</div>
			</th>
			<th width="15%" style="padding-top: 12px" class="no-padding">
				<a class="img-calendar" href="<?= Yii::app()->createUrl('programms/app/timetableCalendar', array('date'=>$now->format('Y-m-d'))) ?>"></a>
			</th>
		</tr>
	</table>

	<table class="table table-bordered timetable-body">
		<?php foreach ($programs as $program) {
			$isAir = false;
			if ($program->id == $onAirProgram->id) $isAir = true;

			$isBefore = false;
			if (strtotime($program->date.' '.$program->time) < strtotime($onAirProgram->date.' '.$onAirProgram->time)) $isBefore = true;
			?>
			<tr<?= $isBefore ? ' class="before"' : ''?>>
				<td class="text-center no-padding" width="15%"><?=Yii::app()->dateFormatter->format('H:mm', (strtotime($program->time) - 3600 * 4))?></td>
				<td class="text-center no-padding" width="15%"><?=Yii::app()->dateFormatter->format('H:mm', (strtotime($program->time) - 3600 * 3))?></td>
				<td class="text-center no-padding" width="15%"><?=Yii::app()->dateFormatter->format('H:mm', strtotime($program->time))?></td>
				<td class="timetable_title" width="40%">
					<?php if ($program->content !== null) { ?>
						<div class="program_content">
							<?php if($isAir) { ?>
							<div class="timetable-image">
								<div class="img-16x9 hidden-xs">
									<?php
									$url = Yii::app()->createUrl('programms/app/program', array('sefname'=>$program->content->sefname));
									$image = ProgrammsContent::getAnounceImg($program->content->image, '', '/themes/front/images/noimage.jpg');
									if(!$image) $image = '/themes/front/images/noimage.jpg';
									?>
									<a href="<?= $url ?>" class="out" style="background-image: url(<?= $image ?>)"></a>
								</div>
							</div>
							<?php } ?>

							<h3><a href="<?=Yii::app()->createUrl('programms/app/program', array('sefname' => $program->content->sefname))?>"><?=$program->title?></a></h3>

							<?php if($isAir) { ?>
							<div class="description hidden-xs">
								<div class="short_text"><?=$program->content->short_text?></div>
							</div>
							<?php } ?>

						</div>
					<?php } else { ?>
						<h3><?= $program->title ?></h3>
					<?php } ?>
				</td>
				<?= $isAir ? '<td width="15%" class="onair no-padding">'.Yii::t('app', 'В&nbsp;эфире').'</td>' : '<td width="15%" class="no-padding"></td>' ?>
			</tr>
		<?php } ?>
	</table>
</section>