<?php $programs = $category->programs; ?>

<div class="row">
<!--	<div class="clearfix"></div>-->
	<?php foreach($programs as $program) { ?>
		<?php $url = Yii::app()->createUrl('programms/app/program', array('sefname'=>$program->sefname)); ?>

		<div class="col-xs-12 col-sm-6 col-md-4 col-programm">
			<article class="program">
				<div class="img-16x9">
					<?php
					$image = ProgrammsContent::getAnounceImg($program->image, '', '/themes/front/images/noimage.jpg');
					if(!$image) $image = '/themes/front/images/noimage.jpg';
					?>
					<a href="<?= $url ?>" class="out" style="background-image: url(<?= $image ?>)"></a>
				</div>
				<h3><a href="<?= $url ?>"><?= $program->title ?></a></h3>
			</article>
		</div>

	<?php } ?>
</div>
<div class="clearfix"></div>