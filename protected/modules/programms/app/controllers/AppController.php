<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 01.07.14
 * Time: 12:50
 */

class AppController extends LXController {

	public $layout = '//layouts/app';

	public function actionIndex($id=false) {
		$this->layout = '//layouts/programms';

		if(!$id) {
			$category = ProgrammsCategories::model()->find('lang_id=:lang_id AND parent_id=:parent_id', array(':lang_id' => $this->getLangId(), ':parent_id' => 0));
		} else {
			$category = ProgrammsCategories::model()->findBySefname($id);
		}
		if ($category === null)
			throw new CHttpException(404, 'Страница не найдена');

		if ($category->isRoot()) {
			$categories = $category->children()->findAll('active=1');
		} else {

		}

		if($id == 'features') {
			$this->layout = '//layouts/app';
			$this->render('features', array('category' => $category));
		} else
			$this->render('index', array('categories' => $categories));
	}

	public function actionProgram($sefname) {

		$program = ProgrammsContent::model()->active()->findBySefname($sefname);

		// if ($program->category->sefname == 'features') {
		// 	$this->layout = '//layouts/programms';
		// } else {
		// 	$this->layout = '//layouts/viewProgram';
		// 	$this->program_category_id = $program->category->sefname;
		// 	$this->program_category_title = $program->category->title;
		// }

		if ($program === null)
			throw new CHttpException(404, 'Страница не найдена');

		$category = $program->category->sefname;

		if ($program->lang_id != $this->getLangId()) {
			$otherIds = CJSON::decode($program->other_id);

			if (is_array($otherIds) && !empty($otherIds[Yii::app()->language])) {
				$program = ProgrammsContent::model()->active()->findByPk($otherIds[Yii::app()->language]);

				if ($program === null) {
					$this->redirect(array('index', 'id' => $category));
					throw new CHttpException(404, 'Страница не найдена');
				}
			}
		}

		$archiveCriteria = new CDbCriteria;
		$archiveCriteria->order = 'views DESC';
		$archiveCriteria->with = array('timetable');
		$archiveCriteria->compare('timetable.programm_id', $program->id);
		$archiveCriteria->order = 'timetable.date DESC, timetable.time DESC';

		$programmsCount = VideoArchive::model()->count($archiveCriteria);

		$pagination = new CPagination($programmsCount);
		$pagination->pageSize = 10;
		$pagination->applyLimit($archiveCriteria);

		$archiveProgramms = VideoArchive::model()->findAll($archiveCriteria);

		$this->render('program', array('program' => $program, 'archiveProgramms' => $archiveProgramms, 'pagination' => $pagination));
	}

	public function actionView($sefname, $id) {
		$program = ProgrammsContent::model()->active()->findBySefname($sefname);
		if ($program === null)
			throw new CHttpException(404, 'Страница не найдена');

		$model = VideoArchive::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'Страница не найдена');

		$pl = array();
		$video = array();
		foreach ($model->videos as $video) {
			if ($video->upload_ext == 'flv') {
				$filename = $video->upload_file;
			} else {
				$filename = $video->filename . '_[240,320,480].mp4';
			}
			$pl['playlist'][] = array(
				'file'      => '/upload/video/archive/' . $video->page_id . '/' . $filename,
				'comment'   => $video->title,
				'poster'    => '/upload/video/archive/' . $video->page_id . '/' . $video->filename . '.jpg',
				'bigposter' => '/upload/video/archive/' . $video->page_id . '/' . $video->filename . '.jpg'
			);

			$video = array(
				'file'      => '/upload/video/archive/' . $video->page_id . '/' . $filename,
				'comment'   => $video->title,
				'poster'    => '/upload/video/archive/' . $video->page_id . '/' . $video->filename . '.jpg',
				'bigposter' => '/upload/video/archive/' . $video->page_id . '/' . $video->filename . '.jpg'
			);
		}
		$this->program_id = $model->timetable->programm_id;

		$params = array('pl' => $pl, 'file' => '');

		$this->render('view', array('model' => $model, 'video'=>$video, 'program'=>$program));
	}

	public function actionTimetable($date = 0) {
		$this->layout = '//layouts/app_fluid';
		$now = new DateTime('now', new DateTimeZone('Asia/Almaty'));

		if ($date == 0) {
			$date = $now->format('Y-m-d');
			$timest_fwd = 0;
		} elseif (strtotime($date) < strtotime($now->format('Y-m-d'))) {
			$now->setTimestamp(strtotime($date));
			$timest_fwd = 0;
		} else {
			$now->setTimestamp(strtotime($date));
			$timest_fwd = strtotime($date);
		}

		$selDate = $date;
		$criteria = new CDbCriteria;
		$criteria->select = 't.*, content.*';
		$criteria->with = array('content');
		$criteria->order = 'time ASC';
		$criteria->compare('t.active', '1');
		$criteria->compare('date', $selDate);

		$programs = ProgrammsTimetable::model()->findAll($criteria);

		// $langs = array();
		// foreach($programs as $program) {
		// 	$pTime = (int)date('Gis', strtotime($program->date .' ' . $program->time));
		// 	if(($pTime >= 30000 && $pTime < 60000) || ($pTime >= 90000 && $pTime < 120000)) {
		// 		$langs['en'][] = $program;
		// 	}
		// }

		// foreach($programs as $program) {
		// 	$pTime = (int)date('Gis', strtotime($program->date .' ' . $program->time));
		// 	if(($pTime >= 150000 && $pTime < 180000) || ($pTime >= 210000 && $pTime < 240000)) {
		// 		$langs['ru'][] = $program;
		// 	}
		// }

		// foreach($programs as $program) {
		// 	$pTime = (int)date('Gis', strtotime($program->date .' ' . $program->time));
		// 	if(($pTime >= 0 && $pTime < 30000) || ($pTime >= 60000 && $pTime < 90000) || ($pTime >= 120000 && $pTime < 150000) || ($pTime >= 180000 && $pTime < 210000)) {
		// 		$langs['kz'][] = $program;
		// 	}
		// }

		// print_r($langs);

		$onAirProgram = ProgrammsTimetable::getOnAirProgram();

		$this->render('timetable', array('now'=>$now, 'programs' => $programs, 'onAirProgram' => $onAirProgram));
	}

	// public function actionTimetableCalendar($date=0) {
	// 	$now = new DateTime('now', new DateTimeZone('Asia/Almaty'));
	// 	if ($date == 0) { }
	// 	elseif (strtotime($date) < strtotime($now->format('Y-m-d'))) { $now->setTimestamp(strtotime($date)); }
	// 	else { $now->setTimestamp(strtotime($date)); }

	// 	// $date = $now->format('Y-m-d');

	// 	// $criteria = new CDbCriteria;
	// 	// $criteria->select = 't.*, content.*';
	// 	// $criteria->with = array('content');
	// 	// $criteria->order = 'time ASC';
	// 	// $criteria->compare('t.active', '1');
	// 	// $criteria->compare('date', $date);

	// 	// $programs = ProgrammsTimetable::model()->findAll();

	// 	$years = Yii::app()->db->createCommand()
	// 		->select('YEAR(`date`) AS `year`')
	// 		->from('{{programms_timetable}}')
	// 		->where('`active`=1 AND `date` > \'2000-01-01\'')
	// 		->group('YEAR(`date`)')
	// 		->queryAll();

	// 	// print_r($years);

	// 	$this->render('calendar', array('date'=>$now, 'years'=>$years));
	// }

	public function actionTimetableCalendar($date=0) {
		$now = new DateTime('now', new DateTimeZone('Asia/Almaty'));
		if ($date == 0) { }
		elseif (strtotime($date) < strtotime($now->format('Y-m-d'))) { $now->setTimestamp(strtotime($date)); }
		else { $now->setTimestamp(strtotime($date)); }

		$nowWeek = new DateTime('now', new DateTimeZone('Asia/Almaty'));
		$i = $nowWeek->format('N')-1;

		if($i != 0)
		$nowWeek->modify('-'.$i.' days');

		$days = Yii::app()->db->createCommand()
			->select('DAY(`date`) AS `day`, `date`')
			->from('{{programms_timetable}}')
			->where('`active`=1 AND `date` >= \''.$nowWeek->format('Y-m-d').'\'')
			->group('DAY(`date`)')
			->queryAll();

		$this->render('calendar_new', array('date'=>$now, 'days'=>$days));
	}
}