<?php

class ProgrammsModule extends CWebModule {
    public function init() {


        // import the module-level models and components
        $endName = Yii::app()->endName;
        $this->setImport(array(
            "programms.models.*",
            "langs.models.*",
            "video.models.*",
            "programms.{$endName}.components.*",
        ));
        Yii::app()->onModuleCreate(new CEvent($this));


    }


}
