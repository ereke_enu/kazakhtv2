<?php

/**
 * This is the model class for table "{{categories}}".
 *
 * The followings are the available columns in table '{{categories}}':
 * @property integer $id
 * @property integer $section_id
 * @property integer $lang_id
 * @property integer $parent_id
 * @property string $title
 * @property string $description
 * @property string $sefname
 * @property integer $position
 * @property string $url
 * @property integer $new_window
 * @property string $background
 * @property string $sort_type
 * @property integer $show_pub_date
 * @property integer $show_icons
 * @property integer $show_comments
 * @property integer $active
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 */
class Categories extends CActiveRecord {
    public $delete_image;
    private static $_childCategories;


    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Categories the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{categories}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('section_id, lang_id, title, sefname', 'required'),
            array('delete_image, is_subdomain, show_pub_date, show_icons, show_comments, section_id, lang_id, parent_id, position, new_window, active, root, lft, rgt, level', 'numerical', 'integerOnly' => true),
            array('title, url', 'length', 'max' => 255),
            array('sefname, sort_type', 'length', 'max' => 50),
            array('description', 'safe'),
            array('background', 'file', 'types' => 'jpg,png,gif,jpeg', 'allowEmpty' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, section_id, lang_id, parent_id, title, description, sefname, position, url, new_window, active, root, lft, rgt, level', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lang' => array(self::BELONGS_TO, 'Langs', 'lang_id'),
            'child' => array(self::HAS_MANY, 'Categories', 'parent_id', 'order' => 'lft ASC'),
            'pages' => array(self::HAS_MANY, 'Pages', 'category_id'),
            'parent'=> array(self::BELONGS_TO, 'Categories', 'parent_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'section_id' => 'Раздел',
            'lang_id' => 'Язык',
            'parent_id' => 'Родитель',
            'title' => 'Название',
            'description' => 'Описание',
            'sefname' => 'ЧПУ',
            'position' => 'Позиция',
            'url' => 'Внешняя ссылка',
            'new_window' => 'В новом окне',
            'is_subdomain' => 'Категория, как сабдомен',
            'background' => 'Бренд фон',
            'sort_type' => 'Тип сортировки',
            'show_pub_date' => 'Показывать дату публикации',
            'show_icons' => 'Показывать иконки соц. сетей',
            'show_comments' => 'Включить комментарии',
            'delete_image' => 'Удалить бренд фон',
            'active' => 'Активность',
            'root' => 'Root',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'level' => 'Level',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('section_id', $this->section_id);
        $criteria->compare('lang_id', $this->lang_id);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('sefname', $this->sefname, true);
        $criteria->compare('position', $this->position);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('new_window', $this->new_window);
        $criteria->compare('active', $this->active);

        $criteria->order = $this->tree->hasManyRoots
            ? $this->tree->rootAttribute . ' ASC, ' . $this->tree->leftAttribute
            : $this->tree->leftAttribute;

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 99999
            )
        ));
    }

    public static function getTreeListItems($withRoot = false, $lang = false) {
        $criteria = new CDbCriteria;
        if ($lang !== false) {
            $criteria->compare('lang.code', $lang);
            $criteria->with = array('lang');
        }
        $categoryData = self::model()->cache(7200)->findAll($criteria);
        $categoryDataTree = self::model()->dbResultToForest($categoryData, 'id', 'parent_id', 'title');
        $categoryDataSelect = self::model()->converTreeArrayToSelect($categoryDataTree, 0, $withRoot);
        return CHtml::listData($categoryDataSelect, 'id', 'name');
    }

    /**
     * Build heriarhal result from DB Query result.
     * db result must conist id, parent_id, value
     *
     * @param Object $rows
     * @param string $idName name of id key in result query
     * @param string $pidName name of parent id for query result
     * @param string $labelName name of value field in query result
     * @return array heriarhical tree
     */
    public function dbResultToForest($rows, $idName, $pidName, $labelName = 'label') {
        $totalArray = array();
        $children = array(); // children of each ID
        $ids = array();
        $k = 0;
        // Collect who are children of whom.
        foreach ($rows as $i => $r) {
            $element = array('id' => $rows[$i][$idName], 'parent_id' => $rows[$i][$pidName], 'value' => $rows[$i][$labelName]);
            $totalArray[$k++] = $element;
            $row = & $totalArray[$k - 1];
            $id = $row['id'];
            if ($id === null) {
                // Rows without an ID are totally invalid and makes the result tree to
                // be empty (because PARENT_ID = null means "a root of the tree"). So
                // skip them totally.
                continue;
            }
            $pid = $row['parent_id'];
            if ($id == $pid) {
                $pid = null;
            }
            $children[$pid][$id] =& $row;
            if (!isset($children[$id])) {
                $children[$id] = array();
            }
            $row['childNodes'] = & $children[$id];
            $ids[$id] = true;
        }

        // Root elements are elements with non-found PIDs.
        $forest = array();
        foreach ($totalArray as $i => $r) {
            $row = & $totalArray[$i];
            $id = $row['id'];
            $pid = $row['parent_id'];
            if ($pid == $id) $pid = null;
            if (!isset($ids[$pid])) {
                $forest[$row[$idName]] =& $row;
            }
        }
        return $forest;
    }

    /**
     * Recursive function converting tree like array to single array with
     * delimiter. Such type of array used for generate drop down box
     *
     * @param array $data data of tree like
     * @param int $level current level of recursive function
     * @return array converted array
     */
    public function converTreeArrayToSelect($data, $level = 0, $withRoot = false) {
        if ($withRoot) {
            $returnArray[0] = array('name' => 'Корневая категория', 'id' => '0');
        }

        foreach ($data as $item) {
            $subitems = array();
            $elementName = "|" . str_repeat("--", $level * 2) . " " . $item['value'];
            $returnItem = array('name' => $elementName, 'id' => $item['id']);
            if ($item['childNodes']) {
                $subitems = $this->converTreeArrayToSelect($item['childNodes'], $level + 1);
            }

            $returnArray[] = $returnItem;

            if ($subitems != array()) {
                $returnArray = array_merge($returnArray, $subitems);
            }

        }
        return empty($returnArray) ? array() : $returnArray;
    }

    public static function getCategoryLangId($categoryId) {
        $model = self::model()->findByPk($categoryId);

        if ($model === null) {
            return 1;
        }
        return $model->lang_id;
    }


    public function behaviors() {
        return array(
            'tree' => array(
                'class' => 'ext.nestedset.NestedSetBehavior',
                // хранить ли множество деревьев в одной таблице
                'hasManyRoots' => true,
                // поле для хранения идентификатора дерева при $hasManyRoots=false; не используется
                'rootAttribute' => 'root',
                // обязательные поля для NS
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
            ),
            /* 'treeView' => array(
                 'class' => 'ext.nestedset.TreeViewTreebehavior'
             ),*/
            'treeArray' => array(
                'class' => 'ext.nestedset.ArrayTreeBehavior'
            )

        );
    }

    /**
     * Получение категории по ЧПУ
     * @param $sefname
     * @return array|false
     */
    public function findBySefname($sefname) {
        $sql = "SELECT id, title, description, section_id, background, sefname FROM {{categories}} WHERE sefname=:sefname AND active='1' AND lang_id=:lang_id LIMIT 1";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':sefname', $sefname, PDO::PARAM_STR);
        $command->bindParam(':lang_id', LXController::getLangId(), PDO::PARAM_INT);
        $result = $command->query();

        return $result->read();
    }

    /**
     * Получение категории по ЧПУ
     * @param $sefname
     * @return array|false
     */
    public function findBySefnameObj($sefname) {
        $model = Categories::model()->find('sefname=:sefname AND active=1 AND lang_id=:lang_id', array(':sefname' => $sefname, ':lang_id' => LXController::getLangId()));

        return $model;
    }

    public static function getChildCategories($sefname) {
        if (empty(self::$_childCategories[$sefname])) {
            $category = self::model()->findBySefnameObj($sefname);
            if (!empty($category->child)) {
                self::$_childCategories[$sefname] = CHtml::listData($category->children()->findAll('active=1'), 'id', 'id');
            } else {
                //self::$_childCategories[$sefname] = $category->id;
            }
        }

        return self::$_childCategories[$sefname];
    }


}