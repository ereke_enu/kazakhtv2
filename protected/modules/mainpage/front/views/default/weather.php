	<div class="section_title"  style="color:#ffffff;">
	<?php
		$cities[0] = array('35108','Уральск','Орал','Uralsk');
	$cities[1] = array('38111','Актау','Ақтау','Aktau');
	$cities[2] = array('38062','Кызылорда','Қызылорда','Kyzylorda');
	$cities[3] = array('28879','Кокшетау','Көкшетау','Kokshetau');
	$cities[4] = array('36778','Талдыкорган','Талдықорған','Taldykorgan');
	$cities[5] = array('35700','Атырау','Атырау','Atyrau');
	$cities[6] = array('36403','Усть-Каменогорск','Өскемен','Oskemen');
	$cities[7] = array('38341','Тараз','Тараз','Taraz');
	$cities[8] = array('38328','Шымкент','Шымкент','Shymkent');
	$cities[9] = array('35394','Караганда','Қарағанды','Karagandy');
	$cities[10] = array('28952','Костанай','Қостанай','Kostanay');
	$cities[11] = array('36003','Павлодар','Павлодар','Pavlodar');
	$cities[12] = array('28679','Петропавловск','Петропавл','Petropavl');
	$cities[13] = array('35188','АСТАНА','АСТАНА','ASTANA');
	$cities[14] = array('36870','Алматы','Алматы','Almaty');
	$cities[15] = array('35229','Актобе','Ақтөбе','Aktobe');

		$lang=Yii::app()->language;
		$langId = 1;
		switch ($lang) {
			case 'kz':
				$langId=2;
				echo "АУА-РАЙЫ";
				break;
			case 'ru':
				$langId=1;
				echo "ПОГОДА";
				break;
			case 'en':
				$langId=3;
				echo "WEATHER";
				break;
			default:
				$langId=1;
				echo "ПОГОДА";
				break;
		}
	?>

	</div>
<?php
	$city_id=35188;
	if(isset($_GET['city'])&&$_GET['city']!=''){
	$city_id=$_GET['city'];}
	if($city_id==''){ $city_id=35188; } 
   	 // id города
    $data_file="http://export.yandex.ru/weather-ng/forecasts/$city_id.xml"; // адрес xml файла 
	
    $xml = simplexml_load_file($data_file); // раскладываем xml на массив
	
 
    // выбираем требуемые параметры (город, температура, пиктограмма и тип погоды текстом (облачно, ясно)
     
    $attrs = $xml;

    $city=$attrs['city'];

    for($i=0;$i<16;$i++)
	{
		if($cities[$i][0]==$city_id){
			$city = $cities[$i][$langId];
		}
	}

    $temp=$xml->fact->temperature;
    $pic=$xml->fact->{'image-v3'};
    $type=$xml->fact->weather_type;
    $at_night=$xml->informer->temperature;
    // Если значение температуры положительно, для наглядности добавляем "+"
    if ($temp>0) {$temp='+'.$temp;}
    
?>
				<div id="informer">
					<table border="0" cellpadding="5px" cellspacing="5px">
						<tr>
							<td style="width:250px;">
							<div><?php echo $city; ?></div>
							<div class="informer_curr_time">
								<?php 
									$date = get_date_string(date('Y-m-d'), $langId);
									$w = date('w');

									echo $date.", ".get_week_name($w, $langId);
								?>
							</div>
							<div style="margin:0 auto;width:100px;margin-top:-20px;">
								<?php //echo ("<img src=\"http://yastatic.net/weather/i/icons/svg/$pic.svg\" alt=\"$type\" title=\"$type\" id='informer_icon' />"); ?>
							</div>
							<br />
							<div style="float:right;"><?php echo "$temp<sup>o</sup>C";?></div>
							<div class="clear"></div>
							<div style="float:right;"><?php echo "$at_night<sup>o</sup>C";?></div>
							</td>

							<?php 
							
								$col=5;
								$out = array(); // массив вывода прогноза
    							$counter = 0 ; // счетчик количества дней, для которых доступен прогноз

							    if($xml->day):
							        foreach($xml->day as $day):
							        	$counter++;
							        	if($counter>1&&$counter<=8)
							        	{
							        		$w++;
									        echo "<td align='center' class='informer_item'>";
											echo "<div><a href=''>".get_week_short_name($w, $langId)."</a></div>";
											$attrs = $day->attributes();
											$date_string= get_date_string($attrs['date'], $langId);
											echo "<div class='informer_date'>".$date_string."</div>";
											
											foreach ($day->day_part as $dp) {
												$dp_attrs = $dp->attributes();
												if($dp_attrs->type=='day_short'){
													$pic = $dp->{'image-v3'};
													$temp = get_sign($dp->temperature);
													echo "<div><img src=\"http://yastatic.net/weather/i/icons/svg/$pic.svg\" style='width:60px;' /></div>";
													echo "<div class='day_temperature'>".$temp."</div>";
												}
												if($dp_attrs->type=='night_short'){
													$temp = get_sign($dp->temperature);
													echo "<div class='day_temperature'>".$temp."</div>";
												}
											}
											echo "</td>";							            
										}
								    endforeach;
							    endif;
							    
							?>
						</tr>
					</table>			
				</div>
				<!--<div id="summary"></div>-->
				<div id="map">
<script type="text/javascript">

$(document).ready(function(){ 
	
var language = '<?=Yii::app()->language?>';
	if(language=='ru'){
		elem = $('#city35108');elem.attr('style','position:relative;left:72px;top:100px;');
		elem = $('#city38111');elem.attr('style','position:relative;left:0px;top:259px;');
		elem = $('#city38062');elem.attr('style','position:relative;left:138px;top:220px;');
		elem = $('#city28879');elem.attr('style','position:relative;left:185px;top:65px;');
		elem = $('#city36778');elem.attr('style','position:relative;left:280px;top:230px;');
		elem = $('#city35700');elem.attr('style','position:relative;left:-210px;top:175px;');
		elem = $('#city36403');elem.attr('style','position:relative;left:204px;top:105px;');
		elem = $('#city38341');elem.attr('style','position:relative;left:0px;top:300px;');
		elem = $('#city38328');elem.attr('style','position:relative;left:-100px;top:325px;');
		elem = $('#city35394');elem.attr('style','position:relative;left:-100px;top:134px;');
		elem = $('#city28952');elem.attr('style','position:relative;left:235px;top:54px;');
		elem = $('#city36003');elem.attr('style','position:relative;left:457px;top:50px;');
		elem = $('#city28679');elem.attr('style','position:relative;left:250px;top:-10px;');
		elem = $('#city35188');elem.attr('style','position:relative;left:209px;top:80px;font-size:14px;');
		elem = $('#city36870');elem.attr('style','position:relative;left:280px;top:270px;');
		elem = $('#city35229');elem.attr('style','position:relative;left:-50px;top:110px;');
	}else if(language=='kz'){
		elem = $('#city35108');elem.attr('style','position:relative;left:72px;top:100px;');
		elem = $('#city38111');elem.attr('style','position:relative;left:12px;top:252px;');
		elem = $('#city38062');elem.attr('style','position:relative;left:164px;top:220px;');
		elem = $('#city28879');elem.attr('style','position:relative;left:185px;top:65px;');
		elem = $('#city36778');elem.attr('style','position:relative;left:280px;top:230px;');
		elem = $('#city35700');elem.attr('style','position:relative;left:-198px;top:158px;');
		elem = $('#city36403');elem.attr('style','position:relative;left:228px;top:105px;');
		elem = $('#city38341');elem.attr('style','position:relative;left:49px;top:277px;');
		elem = $('#city38328');elem.attr('style','position:relative;left:-39px;top:295px;');
		elem = $('#city35394');elem.attr('style','position:relative;left:-23px;top:129px;');
		elem = $('#city28952');elem.attr('style','position:relative;left:-227px;top:54px;');
		elem = $('#city36003');elem.attr('style','position:relative;left:-91px;top:59px;');
		elem = $('#city28679');elem.attr('style','position:relative;left:327px;top:5px;');
		elem = $('#city35188');elem.attr('style','position:relative;left:287px;top:86px;font-size:14px;');
		elem = $('#city36870');elem.attr('style','position:relative;left:354px;top:248px;');
		elem = $('#city35229');elem.attr('style','position:relative;left:5px;top:92px;');
	}else if(language=='en'){
		elem = $('#city35108');elem.attr('style','position:relative;left:72px;top:100px;');
		elem = $('#city38111');elem.attr('style','position:relative;left:12px;top:252px;');
		elem = $('#city38062');elem.attr('style','position:relative;left:164px;top:220px;');
		elem = $('#city28879');elem.attr('style','position:relative;left:185px;top:65px;');
		elem = $('#city36778');elem.attr('style','position:relative;left:280px;top:230px;');
		elem = $('#city35700');elem.attr('style','position:relative;left:-198px;top:158px;');
		elem = $('#city36403');elem.attr('style','position:relative;left:228px;top:105px;');
		elem = $('#city38341');elem.attr('style','position:relative;left:49px;top:277px;');
		elem = $('#city38328');elem.attr('style','position:relative;left:-39px;top:295px;');
		elem = $('#city35394');elem.attr('style','position:relative;left:-23px;top:129px;');
		elem = $('#city28952');elem.attr('style','position:relative;left:-227px;top:54px;');
		elem = $('#city36003');elem.attr('style','position:relative;left:-91px;top:59px;');
		elem = $('#city28679');elem.attr('style','position:relative;left:327px;top:5px;');
		elem = $('#city35188');elem.attr('style','position:relative;left:287px;top:86px;font-size:14px;');
		elem = $('#city36870');elem.attr('style','position:relative;left:354px;top:248px;');
		elem = $('#city35229');elem.attr('style','position:relative;left:5px;top:92px;');
	}
});
</script>
<?php
	

	for($i=0;$i<16;$i++)
	{
		echo '<span  id="city'.$cities[$i][0].'" style="position:relative;"> <a href="?city='.$cities[$i][0].'" >'.$cities[$i][$langId].'</a></span>';
	}
?>
				</div>
				<div id="video_section" >
				<div class="video_section_title">
				<? if($langId==1){
					echo "Видео прогноз";
				}else if($langId==2){
					echo "Видео болжам";
				} else{
					echo "Video";
				}?>
					
				</div>
<iframe width="320" height="240" src="//www.youtube.com/embed/IDiEo_xNAI8" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="clear"></div>

<?php 

function get_sign($temp){
	if ($temp>0) {$temp='+'.$temp;}
	return $temp;
}

function get_date_string($date, $langId){
	$arr = explode("-", $date);
	$month = $arr[1];
	$day = $arr[2];
	if($day[0]=='0'){$day=$day[1];}
	$month_name="";
	
	if($langId==1){
		switch ($month){
			case "01":
				return $day.' января';
				break;
			case "02":
				return $day.' февраля';
				break;
			case '03':
				return $day.' марта';
				break;
			case '04':
				return $day.' апреля';
				break;
			case '05':
				return $day.' мая';
				break;
			case '06':
				return $day.' июня';
				break;
			case '07':
				return $day.' июля';
				break;
			case '08':
				return $day.' августа';
				break;
			case '09':
				return $day.' сентября';
				break;
			case '10':
				return $day.' октября';
				break;
			case '11':
				return $day.' ноября';
				break;			
			case '12':
				return $day.' декабря';
				break;				
		}
	}elseif($langId==2){
		switch ($month){
			case "01":
				return $day.' қаңтар';
				break;
			case "02":
				return $day.' ақпан';
				break;
			case '03':
				return $day.' наурыз';
				break;
			case '04':
				return $day.' сәуір';
				break;
			case '05':
				return $day.' мамыр';
				break;
			case '06':
				return $day.' маусым';
				break;
			case '07':
				return $day.' шілде';
				break;
			case '08':
				return $day.' тамыз';
				break;
			case '09':
				return $day.' қыркүйек';
				break;
			case '10':
				return $day.' қазан';
				break;
			case '11':
				return $day.' қараша';
				break;			
			case '12':
				return $day.' желтоқсан';
				break;				
		}
	}elseif($langId==3){
		switch ($month){
			case "01":
				return 'January '.$day;
				break;
			case "02":
				return 'February '.$day;
				break;
			case '03':
				return 'March '.$day;
				break;
			case '04':
				return 'April '.$day;
				break;
			case '05':
				return 'May '.$day;
				break;
			case '06':
				return 'June '.$day;
				break;
			case '07':
				return 'July '.$day;
				break;
			case '08':
				return 'August '.$day;
				break;
			case '09':
				return 'September '.$day;
				break;
			case '10':
				return 'October '.$day;
				break;
			case '11':
				return 'November '.$day;
				break;			
			case '12':
				return 'December '.$day;
				break;
		}
	}else{

	}

	
}

function get_week_name($week, $langId){

	if($week>7){$week=$week-7;}

	if($langId==1){
		switch ($week) {
		case '1':
			return "Понедельник";
			break;
		case '2':
			return "Вторник";
			break;
		case '3':
			return "Среда";
			break;
		case '4':
			return "Четверг";
			break;
		case '5':
			return "Пятница";
			break;
		case '6':
			return "Суббота";
			break;
		case '7':
			return "Воскресенье";
			break;
		}
	}elseif($langId==2){
		switch ($week) {
		case '1':
			return "Дүйсенбі";
			break;
		case '2':
			return "Сейсенбі";
			break;
		case '3':
			return "Сәрсенбі";
			break;
		case '4':
			return "Бейсенбі";
			break;
		case '5':
			return "Жұма";
			break;
		case '6':
			return "Сенбі";
			break;
		case '7':
			return "Жексенбі";
			break;
		}
	}elseif($langId==3){
		switch ($week) {
		case '1':
			return "Monday";
			break;
		case '2':
			return "Tuesday";
			break;
		case '3':
			return "Wednesday";
			break;
		case '4':
			return "Thursday";
			break;
		case '5':
			return "Friday";
			break;
		case '6':
			return "Saturday";
			break;
		case '7':
			return "Sunday";
			break;
		}
	}else{

	}
	
}	

function get_week_short_name($week, $langId){

	$week = $week%7;
	if($langId==1){
		switch ($week) {
		case '1':
			return "Пн";
			break;
		case '2':
			return "Вт";
			break;
		case '3':
			return "Ср";
			break;
		case '4':
			return "Чт";
			break;
		case '5':
			return "Пт";
			break;
		case '6':
			return "Сб";
			break;
		case '0':
			return "Вс";
			break;
		}
	}elseif($langId==2){
		switch ($week) {
		case '1':
			return "Дс";
			break;
		case '2':
			return "Сс";
			break;
		case '3':
			return "Ср";
			break;
		case '4':
			return "Бс";
			break;
		case '5':
			return "Жм";
			break;
		case '6':
			return "Сб";
			break;
		case '0':
			return "Жс";
			break;
		}
	}elseif($langId==3){
		switch ($week) {
		case '1':
			return "Mo";
			break;
		case '2':
			return "Tu";
			break;
		case '3':
			return "We";
			break;
		case '4':
			return "Th";
			break;
		case '5':
			return "Fr";
			break;
		case '6':
			return "Sa";
			break;
		case '0':
			return "Su";
			break;
		}
	}else{

	}
}	

?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#bg_top').css('background','url("/upload/bg_top.png") no-repeat');
		$('#bg_top').css('-moz-background-size','100%');
		$('#bg_top').css('-webkit-background-size','100%');
		$('#bg_top').css('-o-background-size','100%');
		$('#bg_top').css('background-size','100%');
	})
</script>