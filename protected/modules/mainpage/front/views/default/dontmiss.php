<div style="margin-left:0px;" class="section_title"><?=ttt("Не пропустите") ?>
	</div>
<div style="margin-left:-10px;">
	<?php
	 $lang = Yii::app()->language;
	 if($lang=="kz"){
	?>
	<div class="dontmiss_item">
		<table cellpadding="5" cellspacing="15">
			<tr>
				<td valign="top">
					<img src="/upload/111.jpg" />
				</td>
				<td valign="top">
					<div class="title">МЕНІҢ АТЫМ ҚОЖА</div>
					<div class="date">13 маусым 12:00 және 14 маусым 00:00</div>
					<div class="text">Қожа - ауылдың сыйлы азаматы болған Қадырдың баласы. Мектепте Қожа сотқар бала болады. Соған қарамастан, басты кейіпкеріміз шын жүректен түзелгісі келеді. Қасында Сұлтан деген досы жүрген кезде, бұл іс оған мүлдем қиынға соғады.
Режиссер: Абдулла Қарсақбаев
Рөлдерде: Н. Сегізбаев, М. Көкенов, Г. Қурабаева, Е. Құрмашев, Б. Римова, Қ. Қожабеков, Р. Мухамедьярова, З. Құрманбаева
					</div>
					<a href="http://kazakh-tv.kz/kz/programms/program/program_488_menin-atym-kozha">Толығырақ</a>
				</td>
			</tr>
		</table>
		<hr />	
	</div>
	<div class="dontmiss_item">
		<table cellpadding="5" cellspacing="15">
			<tr>
				<td valign="top">
					<img src="/upload/programms/%D0%B0%D0%BB%D0%B4%D0%B0%D1%80.jpg" />
				</td>
				<td valign="top">
					<div class="title">АЛДАР КӨСЕ</div>
					<div class="date">Әр сенбі және жексенбі 14:00</div>
					<div class="text">«Сіз кімсіз, Ка мырза?» фильмі ұзақ уақыт өзге елде түрмеде отырып, еліне оралған бұрынғы кәсіби барлаушының өмірі туралы. Кинода достық, сезім, ізгілік пен зұлымдық тайталасымен қатар, қылмыс әлемі, есірткі бизнесінің адамдар тағдырына әсері және онымен күрес сияқты көкейтесті мәселе көтеріледі. Атыс-шабыс, төбелес көп болғанмен көрермен жалықпайды.
					</div>
					<a href="http://kazakh-tv.kz/kz/programms/program/program_317_aldar-kose">Толығырақ</a>
				</td>
			</tr>
		</table>
		<hr />	
	</div>
	<div class="dontmiss_item">
		<table cellpadding="5" cellspacing="15">
			<tr>
				<td valign="top">
					<img src="/upload/%D0%BA%D0%B0.png" />
				</td>
				<td valign="top">
					<div class="title">СІЗ КІМСІЗ, КА МЫРЗА?</div>
					<div class="date">13 маусым 19:20</div>
					<div class="text">«Сіз кімсіз, Ка мырза?» фильмі ұзақ уақыт өзге елде түрмеде отырып, еліне оралған бұрынғы кәсіби барлаушының өмірі туралы. Кинода достық, сезім, ізгілік пен зұлымдық тайталасымен қатар, қылмыс әлемі, есірткі бизнесінің адамдар тағдырына әсері және онымен күрес сияқты көкейтесті мәселе көтеріледі. Атыс-шабыс, төбелес көп болғанмен көрермен жалықпайды.
					</div>
					<a href="http://kazakh-tv.kz/kz/programms/program/program_551_siz-kimsiz-ka-myrza">Толығырақ</a>
				</td>
			</tr>
		</table>
		<hr />	
	</div>
	<? } if($lang=="ru"){ ?>
	<div class="dontmiss_item">
		<table cellpadding="5" cellspacing="15">
			<tr>
				<td valign="top">
					<img src="/upload/43812.jpg" />
				</td>
				<td valign="top">
					<div class="title">СКАЗКА О ПРЕКРАСНОЙ АЙСУЛУ</div>
					<div class="date">14 июня в 15:30</div>
					<div class="text">
Однажды во время охоты молодой хан Таукель повстречал прекрасную Айсулу, дочь пастуха, и привёл её во дворец, отпраздновав вскоре пышную свадьбу. Айсулу не занимали дворцовые интриги. Девушка кормила белоснежных голубей, ухаживала за оранжерейными растениями, утоляла любопытство, беседуя в обсерватории с мудрецом-астрономом. Любовь преобразила хана Таукеля, воинственность начала перерастать в мудрость, пробуждая милосердие и доброту. Но коварная сестра хана Карашаш, давно мечтающая о власти, воспользовалась вынужденным отъездом брата и оклеветала дочь пастуха, распустив слух о том, что она родила двух щенков.
					</div>
					<a href="http://kazakh-tv.kz/ru/programms/program/program_823_skazka-o-prekrasnoi-aisulu">Подробнее</a>
				</td>
			</tr>
		</table>
		<hr />	
	</div>
	<div class="dontmiss_item">
		<table cellpadding="5" cellspacing="15">
			<tr>
				<td valign="top">
					<img src="/upload/vgorodea.jpg" />
				</td>
				<td valign="top">
					<div class="title">В ГОРОДЕ А</div>
					<div class="date">14 июня в 21:30</div>
					<div class="text">Полнометражный художественный фильм, который состоит из пяти новелл, связанных между собой главными героями, которые живут по соседству в пятиэтажном доме. По сути будущий фильм представляет собой некий срез нашего времени, т.к. рассказывает о современной жизни, о людях, живущих в сегодняшнем Казахстане. Истории, рассказанные в новеллах, узнаваемы и понятны каждому зрителю. Темы, которые затрагивают режиссеры в своих сюжетах: любовь, разочарования.
					</div>
					<a href="http://kazakh-tv.kz/ru/programms/program/program_799_v-gorode-a">Подробнее</a>
				</td>
			</tr>
		</table>
		<hr />	
	</div>
	<div class="dontmiss_item">
		<table cellpadding="5" cellspacing="15">
			<tr>
				<td valign="top">
					<img src="/upload/1.png" />
				</td>
				<td valign="top">
					<div class="title">People and economy - Экономика туризма</div>
					<div class="date">15 июня в  15:40 и 21:40</div>
					<div class="text">Программа объективно освещает экономические процессы происхоящие во всем мире, рассказывает о влиянии тех или иных экономических процессов на мировое сообщество, отдельные страны, Казахстан и на каждого гражданина страны, а также об экономической отрасли Казахстана.
					</div>
					<a href="http://kazakh-tv.kz/ru/programms/program/program_581_people-and-economy">Подробнее</a>
				</td>
			</tr>
		</table>
		<hr />	
	</div>
	<? } if($lang=="en"){ ?>
	<div class="dontmiss_item">
		<table cellpadding="5" cellspacing="15">
			<tr>
				<td valign="top">
					<img src="/upload/1.png" />
				</td>
				<td valign="top">
					<div class="title">People and economy -  The economy of the tourism</div>
					<div class="date">June 15 at 11:45 (AST)</div>
					<div class="text">The programme is an objective coverage of economic processes occurred worldwide. It reveals the influence of certain economic processes on the international community, individual countries, Kazakhstan and every citizen of the country. The programme tells about the economic sector of Kazakhstan.
					</div>
					<a href="http://kazakh-tv.kz/en/programms/program/program_581_people-and-economy">More</a>
				</td>
			</tr>
		</table>
		<hr />	
	</div>
	<? } ?>
</div>






