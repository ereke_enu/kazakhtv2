<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"/>
<title>
Banner
</title>
    <script src="/themes/front/js/jquery.js" type="text/javascript"></script>
    <script src="/themes/front/js/jquery-1.4.4.min.js" type="text/javascript"></script>
    
<style>

body{
	padding:0px;
	margin:0px;
	width:100% !important;
	height: auto !important;
	
}
	#banner_title{
		background-color: #00354C;
		width: 100% !important;
		height: 30px;
		color: #ffffff;
		font-weight: bold;
		font-size: 14px;
		
	}

	#banner_title a{
		color: #ffffff;
		text-decoration: none;
	}

	.page_title{
		font-size: 14px;
		color: #000;
		text-decoration: none;
	}

	.page_title:hover{
		color:#eb1e00;
	}

	.arrow{
		padding: 3px;
		width: 24px;
		float: right;
	}

	.pagination{
		
	}

	.page_img
	{	
		width: 100%;
		height: auto;
	}
</style>
</head>
<body>
<div id="banner_title" >
<a id="logo"   style="margin-left:50px;" href="http://kazakh-tv.kz/" target="_blank">
	<img  src="/upload/logolast.png" style="width:150px;margin:0 auto;" />
		</a>
<a id="link" style="margin-left:30px;" href="http://kazakh-tv.kz/" target="_blank">
<div  style="font-size:18px;color:white;width:200px;margin:0 auto;padding-top:5px;margin-left:0px;">WWW.KAZAKH-TV.KZ</div>
</a>
</div>
<script type="text/javascript">
var titleFlag=0;
var i=1;
var slideInterval;
$(document).ready(function() {
	$('.item').hide();
	fadeInItem();
	$('#logo').show();
	$('#link').hide();
	window.setInterval(function(){showTitle();}, 17000);
	slideInterval = window.setInterval(function(){fadeInItem();}, 5000);
});

function showTitle(){
	if(titleFlag==1)
	{
		$('#link').css('display', 'none');
		$('#link').hide();
		$('#logo').css('display', 'block');
		$('#logo').show();
		titleFlag=0;
	}else
	{

		$('#logo').css('display', 'none');
		$('#logo').hide();
		$('#link').css('display', 'block');
		$('#link').show();
		titleFlag=1;
	}
}

function fadeInItem(){
	$('.item').hide();
	$('#item'+i).fadeIn();
	i++;
	if(i>5)i=1;
}

function prevItem()
{
	i=i-2;
	if(i<=0){i=i+5;}
	fadeInItem(i);	
}

</script>

<div id="slides">
	<div style="height:30px;width:100%;">
		<img id="next" class="arrow next" onclick="fadeInItem();" src="/upload/right.png"  />
		<img id="prev" class="arrow prev" onclick="prevItem();" src="/upload/left.png" />
	</div>
	<div class="slides_container" >
	<?php
	$i=0;
		foreach ($news as $page) {
			//$page->image='/upload/anounces/'.$page->image;
			//$page->image='/themes/front/images/top_noimage.png';
			$i++;
			?>
				<div class="item" id="item<?=$i ?>" style="width: 100% !important ;height:auto !important;">
				<a class="page_title" href="<?=Yii::app()->createUrl('/pages/default/view', array('category' => $page->category->sefname, 'sefname' => $page->sefname));?>" target="_blank">
					<img src="/upload/anounces/<?=$page->image ?>" alt="<?=$page->title ?>" class="page_img" />
						<br/><?=$page->title ?></a></div>
		<?}?>
	</div>		
</div>	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-18056327-4', 'auto');
  ga('send', 'pageview');

</script>
<div style="display:none;">
	 
                <!-- Yandex.Metrika informer --><a href="http://metrika.yandex.ru/stat/?id=21175468&amp;from=informer" target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/21175468/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:21175468,lang:'ru'});return false}catch(e){}"/></a><!-- /Yandex.Metrika informer --><!-- Yandex.Metrika counter --><script type="text/javascript">var yaParams = {/*Здесь параметры визита*/};</script><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter21175468 = new Ya.Metrika({id:21175468, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true,params:window.yaParams||{ }}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script>
                <noscript>
                <div><img src="//mc.yandex.ru/watch/21175468" style="position:absolute; left:-9999px;" alt="" />
                </div></noscript><!-- /Yandex.Metrika counter -->
     
</div>
</body>
</html>


