 <?php
include "./protected/components/Translator.php";

class DefaultController extends LXController {
    public $layout = '//layouts/main2';

    public function actionIndex() {
        $this->pageTitle = Yii::t('mainpage', 'Главная страница');
        //$news = Pages::model()->cache(300)->getLimitPagesInCategorySefname ('news', 5);
		
		$lang=Yii::app()->language;
        switch ($lang) {
          case 'kz':
            $this->pageTitle="Басты бет - ҚАЗАҚСТАН РЕСПУБЛИКАСЫНЫҢ АЛҒАШҚЫ ҰЛТТЫҚ СПУТНИКТІК АРНАСЫ Kazakh TV";
            $this->metaTags="Қазақстан, жаңалықтар, 2015, алматы, астана, соңғы жаңалықтар, саясат, 24 сағат, арна, тікелей эфир, ауа-райы, KZ , қазақ тв, Kazakh tv, EXPO 2017, AstanaExpo, экономика, Назарбаев, Нұр Отан, елбасы, Қазақстан халқы Ассамблеясы,Нұрлы жол, hi tech, хайтек, әлем, мәдениет, денсаулық, кино, валюта, теңге, доллар, рубль, қолжетімді баспана";
            $this->metaDescription="Kazakh-tv.kz – тәулік бойы жаңартылатын ақпараттар легі. Қазақстан, Ресей және әлемдегі бизнес экономика, оқиға, спорт туралы соңғы жаңалықтар";
            break;
          case 'ru':
            $this->pageTitle="Главная - ПЕРВЫЙ НАЦИОНАЛЬНЫЙ СПУТНИКОВЫЙ КАНАЛ РЕСПУБЛИКИ КАЗАХСТАН Kazakh TV";
            $this->metaTags="Казахстан, новости, 2015, алматы, астана, последние новости, политика, 24 часов, канал, прямой эфир, погода, Kz, казах тв, Expo 2017, AstanaExpo, экономика, Назарбаев, нур Отан, президент, Ассемблея народов Казахстана, Нурлы жол, хайтек, мир, культура, здоровье, кино, валюта, тенге, доллар, рубль, доступное жилье";
            $this->metaDescription="Kazakh-tv.kz - круглосуточно обновляемая лента новостей. Последние новости Казахстана, России и мира о бизнесе, экономике, происшествиях, спорте";
            break;
            case 'en':
            $this->pageTitle="Home - THE FIRST NATIONAL SATELLITE CHANNEL OF THE REPUBLIC OF KAZAKHSTAN Kazakh TV";
            $this->metaTags="Kazakhstan, news, 2015, Almaty, Astana, Latest News, Politics, 24 hours, channel, Broadcast, weather, Kz, kazakh TV, Expo 2017, AstanaExpo, economy, Nazarbayev, Nur Otan, the president, the Assembly of Peoples of Kazakhstan, high-tech, world, culture, health, movies, currency, the tenge, dollars, rubles, Affordable housing";
            $this->metaDescription="Kazakh-tv.kz - 24 hours updated news. The latest business, economics, sports and accedent news from Kazakhstan, Russia and the world";
            break;
          default:
            $this->pageTitle="Басты бет - ҚАЗАҚСТАН РЕСПУБЛИКАСЫНЫҢ АЛҒАШҚЫ ҰЛТТЫҚ СПУТНИКТІК АРНАСЫ Kazakh TV";
            $this->metaTags="Қазақстан, жаңалықтар, 2015, алматы, астана, соңғы жаңалықтар, саясат, 24 сағат, арна, тікелей эфир, ауа-райы, KZ , қазақ тв, Kazakh tv, EXPO 2017, AstanaExpo, экономика, Назарбаев, Нұр Отан, елбасы, Қазақстан халқы Ассамблеясы,Нұрлы жол, hi tech, хайтек, әлем, мәдениет, денсаулық, кино, валюта, теңге, доллар, рубль, қолжетімді баспана";
            $this->metaDescription="Kazakh-tv.kz – тәулік бойы жаңартылатын ақпараттар легі. Қазақстан, Ресей және әлемдегі бизнес экономика, оқиға, спорт туралы соңғы жаңалықтар";
            break;
        }
		
        $this->render('index');

    }

    public function actionTest2() {
        $this->layout = '//layouts/main3';
        $this->render('index');
    }

    public function actionTest4()
    {
      $begin = time();
      echo "begin=".$begin."</br>";

                 $dbhost = 'localhost:3036';
                 $dbuser = 'db_kazakh-tv';
                 $dbpass = 'pzUG51Co';
                 
                 $conn = mysql_connect($dbhost, $dbuser, $dbpass);
                 mysql_set_charset('utf8',$conn);
                 
                 if(! $conn )
                 {
                    die('Could not connect: ' . mysql_error());
                 }
                 
                 $sql = "SELECT p.*, cat.sefname as category_sefname FROM lx_pages p inner join lx_categories cat on p.category_id=cat.id where p.sefname='page_125763_v-siane-proshlo-14-e-ministerskoe-soveshchanie-shos-po-ekono' and p.active=1 and is_archive=0 limit 1";
                 //echo "sql=".$sql;
                 mysql_select_db('db_kazakh-tv');
                 $retval = mysql_query( $sql, $conn );
                 
                 if(!$retval)
                 {
                    die('Could not get data: ' . mysql_error());
                 }
                 /*
                 while($row = mysql_fetch_array($retval, MYSQL_ASSOC))
                 {
                    $url='http://mail.ru';
                    $title=$row['title'];
                    $image = $row['image'];
                    $short_text=$row['short_text'];
                   echo <<<END
                    <div>
                    <a href="$url" title="$title" target="_blank"><img src="$image" width="480" height="270" alt="Slide 1"></a>
                    <div class="caption" style="bottom:0">
                    <h3><a href="$url">$title</a></h3>
                    <p><a href="$url" class="arrow">$short_text</a></p>
                    </div>
                    </div>
END;
                 }*/
                 
                 //echo "Fetched data successfully\n";
                 
                 mysql_close($conn);
                 $end = time();
                 echo "end=".$end."</br>";
                 echo "difference = ".($end-$begin);

    }

    public function actionTest5(){
      $this->layout="none";
      $cache = new CacheBuilder("topNewsBanners/topNewsBanner","ru");
      $path = $cache->getFullPath();
      echo "path=".$path;
      $txt = "ura";
      //$cache->writeToFile($txt);

      //include($cache->getFull);
      
      //echo "test5";
/*
 $category = Categories::model()->findBySefnameObj('news');
        if ($category == null) {
            return false;
        }

        $categories = $category->children()->findAll();
        $categoryId = CHtml::listData($categories, 'id', 'id');
        $categories="";
        
        foreach ($categoryId as $value) {
            $categories=$categories.$value.", ";
        }


        $categories=$categories."1";

       $criteria = new CDbCriteria;
        $date = date('Y-m-d', strtotime('-7 days'));
        $criteria->params= array(':date' =>$date,);
        $criteria->condition='date>:date and  is_archive=0 and active=1 and category_id in ('.$categories.')';
        
        //$criteria->condition='is_archive=0 and active=1 and category_id in ('.$categories.')';
        $criteria->order = 'views DESC';
        $criteria->limit = 7;
        $pages = Pages::model()->cache(60)->findAll($criteria);
      echo "count=".count($pages);*/
        //phpinfo();
    }

    public function actionWeather()
    {
        $this->layout='//layouts/singlePageLayout';
        $text="some text";
        $this->render('weather', array('text'=>$text));
    }

    public function actionWorldWide()
   {
      $this->layout='//layouts/singlePageLayout';
      $sefname="worldwide";
      $category = Categories::model()->findBySefnameObj($sefname);
     

      $criteria = new CDbCriteria;
      $criteria->compare('category_id', $category->id);
      $criteria->compare('active', '1');
      $criteria->order = 'date DESC, time DESC';

      $page = Pages::model()->find($criteria);

      $this->render('worldwide', array('category'=>$category, 'page'=>$page));
   }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
	
	public function actionTopNewsBanner1(){
        $this->layout='none';
		$categories = Categories::model()->find('t.sefname=:sefname AND lang_id=:lang_id', array(':sefname' => 'news', ':lang_id' => LXController::getLangId()));;

        $criteria = new CDbCriteria;
        $criteria->addInCondition('category_id', CHtml::listData($categories->children()->findAll(), 'id', 'id'));
        $criteria->limit = '10';
        $criteria->order = 'id DESC';
        $criteria->compare('active', 1);
        $criteria->compare('is_archive', 0);

        $news = Pages::model()->cache(14400)->findAll($criteria);
        //$news = Pages::model()->getBannerPages();
        $this->render('topNewsBanner', array('news'=>$news));
   }
   
   public function actionTopNewsBanner(){
        $this->layout='none';

        $lang=Yii::app()->language;
        $langId = Langs::getLangIdByCode($lang);

        $news = array();

        $cache = new CacheBuilder("topNewsBanners/topNewsBanner",$lang);
        if($cache->checkFile()==true){
          include($cache->getFullPath());
        }else{
          
          $dbhost = 'localhost';
          $dbuser = 'db_kazakh-tv';
          $dbpass = 'pzUG51Co';

          //$dbuser = 'root';
          //$dbpass = '';
                     
          $conn = mysql_connect($dbhost, $dbuser, $dbpass);
          mysql_set_charset('utf8',$conn);
                     
          if(!$conn){
            die('Could not connect: ' . mysql_error());
          }
          $sql = "SELECT p.*, cat.sefname as category_sefname FROM lx_pages p inner join lx_categories cat on p.category_id=cat.id where p.active=1 and is_archive=0 and p.lang_id=".$langId." order by p.id desc limit 10;";
          //echo "sql=".$sql;
          mysql_select_db('db_kazakh-tv');
          $retval = mysql_query( $sql, $conn );
                     
          if(!$retval)
            die('Could not get data: ' . mysql_error());
            
          $i=0;
          $txt="<"."?php ";           
          while($row = mysql_fetch_array($retval, MYSQL_ASSOC))
          {
            $news[$i]->category_sefname = $row['category_sefname'];
            $txt.="$"."news[".$i."]->category_sefname='".$row['category_sefname']."';";

            $news[$i]->sefname = $row['sefname'];
            $txt.="$"."news[".$i."]->sefname='".$row['sefname']."';";

            $news[$i]->image = $row['image'];
            $txt.="$"."news[".$i."]->image='".$row['image']."';";

            $news[$i]->title = $row['title'];
            $txt.="$"."news[".$i."]->title='".$row['title']."';";
            $i++;
          }
          mysql_close($conn);
          $txt.=" ?".">";
          $cache->writeToFile($txt);
        }

        $this->render('topNewsBanner1', array('model'=>$news));
   }
   public function actionTest(){
      $this->layout='none';
      
      $sourceMessageTable='SourceMessage';
      $translatedMessageTable='Message';
      $sql=<<<EOD
SELECT t1.message AS message, t2.translation AS translation
FROM {$sourceMessageTable} t1, {$translatedMessageTable} t2
WHERE t1.id=t2.id AND t1.category=:category AND t2.language=:language
EOD;

      $connection=Yii::app()->db;
      $command=$connection->createCommand($sql);
      $dataReader=$command->query(); 
      foreach($dataReader as $row) {
        echo "row=".$row['translation']."<br/>";
      }



      $this->render('test');
   }

   public function actionDontmiss(){
      //Yii::app()->db->createCommand('update message set translation="Exchange rates:" where translation="Exchange"')->execute();
      $this->layout='//layouts/second';
      $this->render('dontmiss');
   }

   public function actionTopNewsBanner2(){
      $this->layout='none';
        $arr = array();
        $lang=Yii::app()->language;
        $langId = Langs::getLangIdByCode($lang);


        $cache = new CacheBuilder("topNewsBanners/topNewsBanner",$lang);
        
        if($cache->checkFile()==1){
          echo "file is exist";
          include($cache->getFullPath());
        }else{
          echo "file is not exist";
                $dbhost = 'localhost';
                $dbuser = 'db_kazakh-tv';
                $dbpass = 'pzUG51Co';

                //$dbuser = 'root';
                //$dbpass = '';
                 
                 $conn = mysql_connect($dbhost, $dbuser, $dbpass);
                 mysql_set_charset('utf8',$conn);
                 
                 if(!$conn)
                 {
                    die('Could not connect: ' . mysql_error());
                 }
                 
                 $sql = "SELECT p.*, cat.sefname as category_sefname FROM lx_pages p inner join lx_categories cat on p.category_id=cat.id where p.active=1 and is_archive=0 and p.lang_id=".$langId." order by id desc limit 10;";
                 //echo "sql=".$sql;
                 mysql_select_db('db_kazakh-tv');
                 $retval = mysql_query( $sql, $conn );
                 
                 if(!$retval)
                 {
                    die('Could not get data: ' . mysql_error());
                 }
                 
                 $i=0;
                 $txt="<"."?php ";
                 while($row = mysql_fetch_array($retval, MYSQL_ASSOC))
                 {
                    $obj[$i]->category_sefname = $row['category_sefname'];
                    $txt.=$this->getObjectCacheTextFromRow($row, 'category_sefname',$i);

                    $obj[$i]->sefname = $row['sefname'];
                    $txt.=$this->getObjectCacheTextFromRow($row, 'sefname',$i);

                    $obj[$i]->title = $row['title'];
                    $txt.=$this->getObjectCacheTextFromRow($row, 'title',$i);

                    $obj[$i]->image = $row['image'];
                    $txt.=$this->getObjectCacheTextFromRow($row, 'image',$i);

                    $arr[$i]=$obj[$i];
                    $txt.="$"."arr[".$i."]="."$"."obj[".$i."];";
                    $i++;
                 }

         mysql_close($conn);
         $txt.=" ?".">";
         //var_dump($arr);
          $cache->writeToFile($txt);
       }
       
        $this->render('topNewsBanner1', array('model'=>$arr));

   }

   public function getObjectCacheTextFromRow($model, $propName,$i){
        return "$"."obj[".$i."]->".$propName."='".$model[$propName]."';";
    }

    public function actionRefresh($section){
      echo "section=".$section;
      $cache = new CacheBuilder('no');
      if($section=="bannerNews"){
          $cache->clearFolder("topNewsBanners");  
      }elseif($section=="programmsNews"){
          $cache->clearFolder("programmNewsWidgets");  
      }elseif($section=="popularNews"){
          $cache->clearFolder("popularPagesLists");  
      }elseif($section=="latestNews"){
          $cache->clearFolder("pagesListWidgets");  
      }elseif($section=="all"){
        $cache->clearFolder("topNewsBanners");  
        $cache->clearFolder("programmNewsWidgets");  
        $cache->clearFolder("popularPagesLists");  
        $cache->clearFolder("pagesListWidgets");  
      }
      echo "end of operation";
   }

    public function actionLive1(){
      $this->layout = '//layouts/live';
            $this->render('live1');   
    }

   /*
    public function filters()
    {
        return array(
            array(
                'COutputCache',
                'duration'=> 10,
            ),
        );
    }
    */
}