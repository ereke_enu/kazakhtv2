<div id="slider">
    <?php foreach ($pages as $page) { ?>
        <img alt="Gallery Picture" title="#caption<?=$page->id?>" src="/upload/anounces/big_<?=$page->image?>"/>
    <?php } ?>
</div>
<div class="ribbon"></div>
<?php foreach ($pages as $page) { ?>
    <div id="caption<?=$page->id?>" class="nivo-html-caption">
        <h2><?=$page->title?></h2>

        <p><?=nl2br($page->short_text)?></p>
    </div>

<?php } ?>
