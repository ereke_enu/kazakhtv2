<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 28.01.13
 * Time: 11:05
 * To change this template use File | Settings | File Templates.
 */

class CurrencyWidget extends CWidget {

    public function run() {
        $rates = array('usd' => 'Доллар', 'rub' => 'Рубль', 'eur' => 'Евро', 'cny' => 'Юань', 'gbp' => 'Фунт', 'kgs' => 'Сом');

        foreach ($rates as $table => $rate) {
            $data = Yii::app()->dbcurrency->cache(21600)->createCommand("SELECT value, shift FROM " . $table . " ORDER BY date DESC LIMIT 1")->query()->read();

            if ($data['shift'] > 0) {
                $shift = 'up';
            } elseif ($data['shift'] < 0) {
                $shift = 'down';
            } else {
                $shift = 'none';
            }

            $rates_value[$table] = array(
                'rate' => (float)$data['value'],
                'shift' => $shift,
                'title' => $rate
            );
        }

        $this->render('currencyWidget', array('rates' => $rates_value));
    }

}