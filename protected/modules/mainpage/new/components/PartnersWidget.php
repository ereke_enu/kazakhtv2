<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 21.01.13
 * Time: 11:32
 * To change this template use File | Settings | File Templates.
 */
class PartnersWidget extends CWidget {

    public $categoryId;

    public function run() {
        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $this->categoryId);
        $criteria->compare('active', '1');
        $criteria->order = 'date DESC, time DESC';

        $pages = Pages::model()->findAll($criteria);

        $this->render('PartnersWidget', array('pages' => $pages));
    }

}
