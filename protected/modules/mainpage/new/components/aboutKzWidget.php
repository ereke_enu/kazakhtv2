<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 21.02.13
 * Time: 16:42
 * To change this template use File | Settings | File Templates.
 */

class aboutKzWidget extends CWidget {

    public function run() {
        $category = Categories::model()->findBySefnameObj('main_about_kazakhstan');

        if ($category === null) {
            return false;
        }

        $page = Pages::model()->find('category_id=:category AND active=1', array(':category' => $category->id));

        $this->render('aboutKzWidget', array('page' => $page));

    }

}