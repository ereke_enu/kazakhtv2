<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 21.01.13
 * Time: 11:43
 * To change this template use File | Settings | File Templates.
 */
class HeaderWidget extends CWidget {

    public $categoryId;

    public function run() {
        $category = Categories::model()->findBySefnameObj('header_top');
        //$pages = Pages::model()->findAll('category_id=:category_id AND active=1', array(':category_id' => $this->categoryId));

        $this->render('HeaderWidget', array('pages' => $category->pages));
    }

}
