<h2><?=Yii::t('mainpage', 'Партнеры');?></h2>

<span href="" class='prev'></span>
<div class="partnerSlider">
    <div class="slider">
        <?php foreach ($pages as $page) { ?>
            <div class="slide">
                <a title="<?=$page->title?>" href="<?=$page->short_text?>" target="_blank"><img src="/upload/anounces/<?=$page->image?>"/></a>
            </div>
        <?php } ?>
    </div>
</div>
<span class='next'></span>