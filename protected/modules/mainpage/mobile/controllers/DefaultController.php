<?php

class DefaultController extends LXController {
    public $layout = '//layouts/main';

    public function actionIndex() {
        $this->pageTitle = Yii::t('mainpage', 'Главная страница');
        $news = Pages::model()->getLimitPagesInCategorySefname ('news', 5);

        $this->render('index', array('pages' => $news));

    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionTest(){
        $this->layout = 'none';
        $this->render('test');
    }
}