<div id="profile_block">
    <div class="login">
        Добро пожаловать!
    </div>
    <div class="cabinet out">
        <a href="#">Личный кабинет <i class="icon-arrow-down-blue"></i></a>
    </div>
    <div id="login_form">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'enableAjaxValidation' => false,
            'id' => 'login-form',
            'action' => Yii::app()->createUrl('/users/default/login')
        ));?>

        <?php  echo $form->textField($model, 'username', array('placeholder' => 'Введите логин')); ?>
        <?php echo $form->passwordField($model, 'password', array('placeholder' => 'Введите пароль'))?>
        <button type="submit"></button>
        <div>
            <small>Еще не зарегистрированы? <a href="<?=Yii::app()->createUrl('/users/default/registration')?>">Зарегистрироваться</a></small>
        </div>

        <?php $this->endWidget() ?>
    </div>
</div>