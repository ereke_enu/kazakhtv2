<div class="registration_form">
<h1>Регистрация нового пользователя</h1>
<hr>
<article>
    <div id="registration_form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'enableAjaxValidation' => false,
            'id' => 'registration-form'
        ));

        echo $form->errorSummary($model);
        ?>
        <table>
            <tr>
                <td><?php echo $form->label($model, 'user_type')?></td>
                <td><?php echo $form->radioButtonList($model, 'user_type', array('Физическое лицо', 'Юридическое лицо'))?>
                    <?php echo $form->error($model, 'user_type');?>
                </td>
            </tr>
            <tr>
                <td><?php echo $form->label($model, 'name')?></td>
                <td><?php echo $form->textField($model, 'name', array('class' => 'field'))?>
                    <?php echo $form->error($model, 'name');?>
                </td>
            </tr>
            <tr>
                <td><?php echo $form->label($model, 'email')?></td>
                <td><?php echo $form->textField($model, 'email', array('class' => 'field'))?>
                    <?php echo $form->error($model, 'email');?>
                </td>
            </tr>
            <tr>
                <td><?php echo $form->label($model, 'username')?></td>
                <td><?php echo $form->textField($model, 'username', array('class' => 'field'))?>
                    <?php echo $form->error($model, 'username');?>
                </td>
            </tr>
            <tr>
                <td><?php echo $form->label($model, 'password')?></td>
                <td><?php echo $form->passwordField($model, 'password', array('class' => 'field'))?>

                    <?php echo $form->error($model, 'password');?>
                </td>
            </tr>
            <tr>
                <td><?php echo $form->label($model, 'password_confirm')?></td>
                <td><?php echo $form->passwordField($model, 'password_confirm', array('class' => 'field'))?>
                    <?php echo $form->error($model, 'password_confirm');?></td>
            </tr>
            <tr>
                <td><?php echo $form->label($model, 'bin')?></td>
                <td><?php echo $form->textField($model, 'bin', array('class' => 'field'))?>
                    <?php echo $form->error($model, 'bin');?></td>
            </tr>
            <tr>
                <td><?php echo $form->label($model, 'is_gov')?></td>
                <td><?php echo $form->checkBox($model, 'is_gov')?>
                    <?php echo $form->error($model, 'is_gov');?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php  $this->widget('CCaptcha', array('buttonLabel' =>'<br />Обновить код')); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->label($model, 'captcha')?></td>
                <td>

                    <?php echo $form->textField($model, 'captcha', array('class' => 'field'))?>
                    <?php echo $form->error($model, 'captcha');?></td>
            </tr>


            <tr>
                <td></td>
                <td>
                    <button class="blue_button">Регистрация</button>
                </td>
            </tr>
        </table>


        <?php $this->endWidget();?>
    </div>
</article>


</div>