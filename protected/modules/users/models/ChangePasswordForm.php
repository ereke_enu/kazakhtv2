<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 15.12.12
 * Time: 16:27
 * To change this template use File | Settings | File Templates.
 */
class ChangePasswordForm extends CFormModel {
    public $oldPassword;
    public $newPassword;
    public $confirm_newPassword;

    public function rules() {
        return array(
            array('oldPassword, newPassword, confirm_newPassword', 'required'),
            array('newPassword', 'compare', 'compareAttribute' => 'confirm_newPassword')
        );
    }

    public function attributeLabels() {

        return array(
            'oldPassword' => Yii::t('users', 'Текущий пароль'),
            'newPassword' => Yii::t('users', 'Новый пароль'),
            'confirm_newPassword' => Yii::t('users', 'Повторите новый пароль')
        );
    }

    public function changePassword() {
        if ($this->validate()) {
		
            $user = Users::model()->findByPk(Yii::app()->user->id);

            if ($user === null) {
                throw new CHttpException(404, 'Страница не найдена');
            }

            if (Yii::app()->getModule('users')->validatePassword($user->password, $this->oldPassword)) {
                
				$user->password = Yii::app()->getModule('users')->hashPassword($this->newPassword);
				
				
                if ($user->save()) {
                    return true;
                }
            }

        }
		
		
		
        return false;
    }


}
