<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 14.12.12
 * Time: 14:06
 * To change this template use File | Settings | File Templates.
 */ 
class ProfileForm extends CFormModel{

	public $user_type;
    public $email;
    public $name;
    public $data_name;
    public $bin;
    public $is_gov;
	
	
	
	public function rules() {
        return array(
            array('data_name, user_type, email', 'required'),
            array('email', 'email'),
            array('is_gov, bin', 'numerical', 'integerOnly' => true)            
        );
    }
	
	public function attributeLabels() {
        return array(
            'user_type' => 'Тип лица',
            'email' => 'Email пользователя',
            'name' => 'Наименование компании',
            'bin' => 'БИН/ИИН',
			'data_name' => 'ФИО',
            'is_gov' => 'Государственная организация'
        );
    }
	
	public function saveProfile() {
        if ($this->validate()) {
            $model = Users::model()->findByPk(Yii::app()->user->id);
            $model->email = $this->email;
            $model->data_user_type = $this->user_type;
            $model->data_is_gov = $this->is_gov;
            $model->data_name = $this->data_name;
            $model->data_bin = $this->bin;
            $model->data_is_gov = $this->is_gov;

            if ($model->save()) {
                return true;
            }
        }

        return false;
    }

}
