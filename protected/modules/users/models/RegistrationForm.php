<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Luxorka
 * Date: 12.12.12
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
class RegistrationForm extends CFormModel {
    public $user_type;
    public $email;
    public $name;
    public $username;
    public $password;
    public $password_confirm;
    public $bin;
    public $is_gov;
    public $captcha;

    public function rules() {
        return array(
            array('name, user_type, email, username, password, password_confirm', 'required'),
            array('username', 'unique', 'className' => 'Users', 'attributeName' => 'username'),
            array('username', 'unique', 'className' => 'Users', 'attributeName' => 'email'),
            array('email', 'email'),
            array('is_gov, bin', 'numerical', 'integerOnly' => true),
            array('captcha', 'captcha')
        );
    }

    public function attributeLabels() {
        return array(
            'user_type' => 'Тип лица',
            'email' => 'Email пользователя',
            'name' => 'Наименование компании',
            'username' => 'Логин',
            'password' => 'Пароль',
            'password_confirm' => 'Подвердите пароль',
            'bin' => 'БИН/ИИН',
            'is_gov' => 'Государственная организация',
            'captcha' => 'Введите код с картинки'
        );
    }

    public function registration() {
        if ($this->validate()) {
            $model = new Users;

            $model->username = $this->username;
            $model->password = Yii::app()->getModule('users')->hashPassword($this->password);
            $model->email = $this->email;
            $model->data_user_type = $this->user_type;
            $model->data_is_gov = $this->is_gov;
            $model->data_name = $this->name;
            $model->data_bin = $this->bin;


            if ($model->save()) {
                Yii::app()->db->createCommand("INSERT IGNORE INTO AuthAssignment SET itemname=:itemname, userid=:userid")
                    ->execute(array(':itemname' => 'Authenticated', ':userid' => $model->id));
                return true;
            }
        }

        return false;
    }

}
