<?php

    class BanerShowController extends LXAController {

        public $defaultAction = 'admin';


        /**
         * Manages all models.
         */
        public function actionAdmin() {
            $model = new BanerShow('search');
            $model->unsetAttributes(); // clear any default values
            if (isset($_GET['BanerShow']))
                $model->attributes = $_GET['BanerShow'];

            $this->render('admin', array(
                'model' => $model,
            ));
        }


    }
