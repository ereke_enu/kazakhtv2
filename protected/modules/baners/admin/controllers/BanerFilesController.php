<?php

    class BanerFilesController extends LXAController {

        public $defaultAction = 'admin';

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate() {
            $model = new BanerFiles;

            if (isset($_POST['BanerFiles'])) {
                $model->attributes = $_POST['BanerFiles'];

                $model->filename = CUploadedFile::getInstance($model, 'filename');

                if ($model->filename !== null) {
                    $filename = uniqid('', true) . '.' . $model->filename->extensionName;
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/bs/';

                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }

                    if ($model->filename->saveAs($path . $filename)) {
                        $model->filename = $filename;
                    }
                }

                if ($model->save()) {
                    $this->redirect(array('admin'));
                }
            }

            $this->render('create', array(
                'model' => $model,
            ));
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
            $model = $this->loadModel($id);
            $oldFilename = $model->filename;

            if (isset($_POST['BanerFiles'])) {
                $model->attributes = $_POST['BanerFiles'];

                $model->filename = CUploadedFile::getInstance($model, 'filename');

                if ($model->filename !== null) {
                    $filename = uniqid('', true) . '.' . $model->filename->extensionName;
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/bs/';

                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }

                    if ($model->filename->saveAs($path . $filename)) {
                        $model->filename = $filename;
                    }
                } else {
                    $model->filename = $oldFilename;
                }

                if ($model->save()) {
                    $this->redirect(array('admin'));
                }
            }

            $this->render('update', array(
                'model' => $model,
            ));
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            } else
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }

        /**
         * Manages all models.
         */
        public function actionAdmin() {
            $model = new BanerFiles('search');
            $model->unsetAttributes(); // clear any default values
            if (isset($_GET['BanerFiles']))
                $model->attributes = $_GET['BanerFiles'];

            $this->render('admin', array(
                'model' => $model,
            ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer the ID of the model to be loaded
         */
        public function loadModel($id) {
            $model = BanerFiles::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');

            return $model;
        }

    }
