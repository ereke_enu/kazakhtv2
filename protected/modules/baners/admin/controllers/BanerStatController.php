<?php

    class BanerStatController extends LXAController {

        public $defaultAction = 'admin';


        /**
         * Manages all models.
         */
        public function actionAdmin() {
            $model = new BanerStat('search');
            $model->unsetAttributes(); // clear any default values
            if (isset($_GET['BanerStat']))
                $model->attributes = $_GET['BanerStat'];

            $this->render('admin', array(
                'model' => $model,
            ));
        }
    }
