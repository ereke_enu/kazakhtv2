<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Список файлов банеров</span>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li><a href="<?=
                    $this->createUrl('create'); ?>" class="">Добавить файл банера</a></li>
        </ul>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => 'baner-files-grid',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'columns'      => array(
        'id',
        'title',
        'client.name::Клиент',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>
