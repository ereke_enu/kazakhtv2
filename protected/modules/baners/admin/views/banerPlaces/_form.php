<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>
<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'                   => 'baner-places-form',
        'enableAjaxValidation' => false,
        'type'                 => 'horizontal'
    )); ?>

    <?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

    <?php echo $form->textFieldRow($model, 'width', array('class' => 'span5')); ?>

    <?php echo $form->textFieldRow($model, 'height', array('class' => 'span5')); ?>

    <?php echo $form->textFieldRow($model, 'size', array('class' => 'span5')); ?>

    <?php echo $form->checkBoxRow($model, 'active'); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>