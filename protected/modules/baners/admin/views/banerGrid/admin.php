<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Список размещенных банеров</span>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li><a href="<?=
                    $this->createUrl('create')
                    ; ?>" class="">Разместить банер</a></li>
        </ul>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => 'baner-grid-grid',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'columns'      => array(
        'id',
        'baner.title::Банер',
        'place.name::Место',
        'targeting.title::Таргетинг',
        'type_place',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>
