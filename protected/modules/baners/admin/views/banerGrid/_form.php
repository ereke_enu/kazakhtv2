<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>
<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'                   => 'baner-grid-form',
        'enableAjaxValidation' => false,
        'type'                 => 'horizontal'
    )); ?>

    <?php echo $form->dropDownListRow($model, 'baner_id', BanerFiles::getListItems(), array('class' => 'span5', 'prompt' => 'Выберите банер')); ?>

    <?php echo $form->dropDownListRow($model, 'place_id', BanerPlaces::getListItems(), array('class' => 'span5', 'prompt' => 'Выберите место размещения')); ?>

    <?php echo $form->dropDownListRow($model, 'lang_id', Langs::getListItems(), array('class' => 'span5', 'prompt' => 'Все языки')); ?>

    <?php echo $form->dropDownListRow($model, 'targeting_id', BanerTargeting::getListItems(), array('class' => 'span5', 'prompt' => 'Выберите таргетинг')); ?>

    <?php echo $form->dropDownListRow($model, 'type_place', array('tdate' => 'По дате', 'tclicks' => 'По кликам', 'tshows' => 'По показам'), array('class' => 'span5', 'prompt' => 'Выберите тип показа банера')); ?>

    <?php echo $form->textFieldRow($model, 'num_clicks', array('class' => 'span5')); ?>

    <?php echo $form->textFieldRow($model, 'num_shows', array('class' => 'span5')); ?>

    <?php echo $form->textFieldRow($model, 'date_from', array('class' => 'span5')); ?>

    <?php echo $form->textFieldRow($model, 'date_to', array('class' => 'span5')); ?>

    <?php echo $form->checkBoxRow($model, 'active'); ?>

    <?php echo $form->checkBoxRow($model, 'finish'); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>