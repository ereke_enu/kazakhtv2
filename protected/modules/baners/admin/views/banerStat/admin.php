
<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Статистика переходов(кликов)</span>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'baner-stat-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'baner_place_id',
		'date_time',
		'ip',
		'referrer',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
