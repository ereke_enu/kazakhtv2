<?php

    class BanersModule extends CWebModule {
        public function init() {
            $endName = Yii::app()->endName;
            $this->setImport(array(
                "baners.models.*",
                "langs.models.*",
                "baners.{$endName}.components.*",
            ));
            Yii::app()->onModuleCreate(new CEvent($this));
        }


    }
