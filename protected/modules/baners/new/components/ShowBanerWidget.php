<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 17.04.13
     * Time: 15:14
     * To change this template use File | Settings | File Templates.
     */

    class ShowBanerWidget extends CWidget {
        public $position;

        public function run() {
            if (empty($this->position)) {
                return false;
            }

            $criteria = new CDbCriteria;
            $criteria->with = array('place', 'baner');
            $criteria->compare('t.place_id', $this->position);
            $criteria->compare('t.active', '1');
            $criteria->compare('t.finish', '0');
            $criteria->addCondition("t.lang_id='" . LXController::getLangId() . "' OR t.lang_id='0'");

            $baners = BanerGrid::model()->findAll($criteria);
            if (empty($baners)) {
                return false;
            }

            shuffle($baners);

            $baner = $baners[0];
            if (empty($baner)) {
                return false;
            }


            switch ($baner->type_place) {
                case 'tclicks':
                    $numCriteria = new CDbCriteria;
                    $numCriteria->compare('baner_place_id', $baner->id);
                    $numClicks = BanerStat::model()->count($numCriteria);
                    if ($numClicks >= $baner->num_clicks) {
                        $baner->finish = 1;
                        $baner->save();
                        $this->run($this->position);

                        return false;
                    }
                    break;
                case 'tshows':
                    $numCriteria = new CDbCriteria;
                    $numCriteria->select = 'SUM(num) as num';
                    $numCriteria->compare('baner_place_id', $baner->id);
                    $numShows = BanerShow::model()->find($numCriteria);

                    if ($numShows->num >= $baner->num_shows) {
                        $baner->finish = 1;
                        $baner->save();
                        $this->run($this->position);

                        return false;
                    }
                    break;
                case 'tdate':
                    $date_from = $baner->date_from;
                    $date_to = $baner->date_to;

                    if (strtotime($date_from) >= time()) {
                        $this->run($this->position);

                        return false;
                    } elseif (strtotime($date_to) >= time()) {
                        $baner->finish = 1;
                        $baner->save();
                        $this->run($this->position);

                        return false;
                    }
                    break;
            }

            $criteria = new CDbCriteria;
            $criteria->compare('baner_place_id', $baner->id);
            $criteria->compare('date', date('Y-m-d', time()));
            $isShows = BanerShow::model()->find($criteria);

            $banerShow = new BanerShow;
            if (!is_null($isShows)) {
                $banerShow->updateCounters(array('num' => 1), 'id=?', array($isShows->id));
            } else {
                $banerShow->num = 1;
                $banerShow->baner_place_id = $baner->id;
                $banerShow->date = date('Y-m-d', time());
                $banerShow->save(false);
            }



            $fileType = explode('.', $baner->baner->filename);
            $fileType = $fileType[count($fileType) - 1];

            $output = '';
            if ($fileType == 'swf') {


                $output = "<div align=\"center\">
   <div align=\"left\" style=\"width:{$baner->place->width}px;\">
	<div style=\"position:absolute;z-index:100; width:".$baner->place->width."px;height:".$baner->place->height."px;\">
		<a href=\"".Yii::app()->createUrl('/baners/default/goads', array('link' => $baner->baner->id, 'place' => $baner->id))."\" title=\"{$baner->baner->alt}\" target=\"_blank\">
			<img src=\"/themes/front/images/no.gif\" width=\"{$baner->place->width}\" height=\"{$baner->place->height}\" border=\"0\" alt=\"{$baner->baner->alt}\"/>
		</a>
	</div>
    <object type=\"application/x-shockwave-flash\" width=\"{$baner->place->width}\" height=\"{$baner->place->height}\" data=\"/upload/bs/{$baner->baner->filename}\">
      <param name=\"movie\" value=\"/upload/bs/{$baner->baner->filename}\">
      <param name=\"quality\" value=\"high\">
      <param name=\"wmode\" value=\"Opaque\">
    </object>


	</div>
	</div>";

            } else {

                $link_start = $link_end = '';
                if ($baner->baner->link != '') {
                    $link_start = '<a target="_blank" href="'.Yii::app()->createUrl('/baners/default/goads', array('link' => $baner->baner->id, 'place' => $baner->id)).'">';
                    $link_end = '</a>';
                }
                $output = $link_start . '<img width="' . $baner->place->width . '" height="' . $baner->place->height . '" src="/upload/bs/' . $baner->baner->filename . '" alt="' . $baner->baner->alt . '" title="' . $baner->baner->alt . '">' . $link_end;

            }

            $this->render('showBanerWidget', array('output' => $output));
        }

    }