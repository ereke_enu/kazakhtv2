<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 17.04.13
     * Time: 15:41
     * To change this template use File | Settings | File Templates.
     */

    class DefaultController extends LXController {

        public function actionGoAds($link, $place) {
            $link = intval($link);
            $place = intval($place);

            if (empty($link) || empty($place)) {
                throw new CHttpException(404, 'Страница не найдена.');
            }

            $baner = BanerFiles::model()->findByPk($link);
            if (is_null($baner)) {
                throw new CHttpException(404, 'Страница не найдена.');
            }

            $stat = new BanerStat;
            $stat->baner_place_id = $place;
            $stat->ip = Yii::app()->request->getUserHostAddress();
            $stat->referrer = Yii::app()->request->getUrlReferrer();
            $stat->save();

            header('Location:' . $baner->link);

        }

    }