<?php

    /**
     * This is the model class for table "{{baner_targeting}}".
     *
     * The followings are the available columns in table '{{baner_targeting}}':
     * @property integer $id
     * @property string $title
     * @property string $ips
     * @property integer $active
     */
    class BanerTargeting extends CActiveRecord {
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return BanerTargeting the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{baner_targeting}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('title, ips', 'required'),
                array('active', 'numerical', 'integerOnly' => true),
                array('title', 'length', 'max' => 255),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, title, ips, active', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array();
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'     => 'ID',
                'title'  => 'Наименование',
                'ips'    => 'IP адреса',
                'active' => 'Активность',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('title', $this->title, true);
            $criteria->compare('ips', $this->ips, true);
            $criteria->compare('active', $this->active);

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }

        public static function getListItems() {
            $model = self::model()->findAll();

            return CHtml::listData($model, 'id', 'title');
        }
    }