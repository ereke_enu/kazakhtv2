<?php

    /**
     * This is the model class for table "{{baner_places}}".
     *
     * The followings are the available columns in table '{{baner_places}}':
     * @property integer $id
     * @property string $name
     * @property integer $width
     * @property integer $height
     * @property integer $size
     * @property integer $active
     */
    class BanerPlaces extends CActiveRecord {
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return BanerPlaces the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{baner_places}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('name', 'required'),
                array('width, height, size, active', 'numerical', 'integerOnly' => true),
                array('name', 'length', 'max' => 255),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, name, width, height, size, active', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array();
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'     => 'ID',
                'name'   => 'Наименование',
                'width'  => 'Ширина',
                'height' => 'Высота',
                'size'   => 'Размер',
                'active' => 'Активность',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('name', $this->name, true);
            $criteria->compare('width', $this->width);
            $criteria->compare('height', $this->height);
            $criteria->compare('size', $this->size);
            $criteria->compare('active', $this->active);

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }

        public static function getListItems() {
            $model = self::model()->findAll('active=1');

            return CHtml::listData($model, 'id', 'name');

        }
    }