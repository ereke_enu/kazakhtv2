<?php

    /**
     * This is the model class for table "{{baner_stat}}".
     *
     * The followings are the available columns in table '{{baner_stat}}':
     * @property integer $id
     * @property integer $baner_place_id
     * @property string $date_time
     * @property string $ip
     * @property string $referrer
     */
    class BanerStat extends CActiveRecord {
        public $num_clicks = 0;

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return BanerStat the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{baner_stat}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('baner_place_id', 'required'),
                array('baner_place_id', 'numerical', 'integerOnly' => true),
                array('ip', 'length', 'max' => 15),
                array('referrer', 'length', 'max' => 255),
                array('date_time', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, baner_place_id, date_time, ip, referrer', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array();
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'             => 'ID',
                'baner_place_id' => 'Банер',
                'date_time'      => 'Дата',
                'ip'             => 'IP',
                'referrer'       => 'Страница клика',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('baner_place_id', $this->baner_place_id);
            $criteria->compare('date_time', $this->date_time, true);
            $criteria->compare('ip', $this->ip, true);
            $criteria->compare('referrer', $this->referrer, true);

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }
    }