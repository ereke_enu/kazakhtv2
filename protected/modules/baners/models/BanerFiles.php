<?php

    /**
     * This is the model class for table "{{baner_files}}".
     *
     * The followings are the available columns in table '{{baner_files}}':
     * @property integer $id
     * @property string $filename
     * @property string $link
     * @property string $alt
     * @property string $html
     * @property integer $client_id
     */
    class BanerFiles extends CActiveRecord {
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return BanerFiles the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{baner_files}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('title, filename, link, client_id', 'required'),
                array('filename', 'file', 'types' => 'jpg,png,gif,swf', 'allowEmpty' => true),
                array('client_id', 'numerical', 'integerOnly' => true),
                array('link, alt', 'length', 'max' => 255),
                array('html', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, filename, link, alt, html, client_id', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'client' => array(self::BELONGS_TO, 'BanerClients', 'client_id')
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'        => 'ID',
                'title'     => 'Наименование',
                'filename'  => 'Файл',
                'link'      => 'Ссылка',
                'alt'       => 'Подпись',
                'html'      => 'HTML код',
                'client_id' => 'Клиент',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('title', $this->title, true);
            $criteria->compare('filename', $this->filename, true);
            $criteria->compare('link', $this->link, true);
            $criteria->compare('alt', $this->alt, true);
            $criteria->compare('html', $this->html, true);
            $criteria->compare('client_id', $this->client_id);

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }

        public static function getListItems() {
            $model = self::model()->findAll();


            return CHtml::listData($model, 'id', 'title');
        }
    }