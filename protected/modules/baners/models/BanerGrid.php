<?php

    /**
     * This is the model class for table "{{baner_grid}}".
     *
     * The followings are the available columns in table '{{baner_grid}}':
     * @property integer $id
     * @property integer $baner_id
     * @property integer $place_id
     * @property integer $targeting_id
     * @property string $type_place
     * @property integer $num_clicks
     * @property integer $num_shows
     * @property string $date_from
     * @property string $date_to
     * @property integer $active
     * @property integer $finish
     */
    class BanerGrid extends CActiveRecord {
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return BanerGrid the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{baner_grid}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('baner_id, place_id, type_place, targeting_id', 'required'),
                array('lang_id, baner_id, place_id, targeting_id, num_clicks, num_shows, active, finish', 'numerical', 'integerOnly' => true),
                array('type_place', 'length', 'max' => 7),
                array('date_from, date_to', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, baner_id, place_id, targeting_id, type_place, num_clicks, num_shows, date_from, date_to, active, finish', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'baner'     => array(self::BELONGS_TO, 'BanerFiles', 'baner_id'),
                'place'     => array(self::BELONGS_TO, 'BanerPlaces', 'place_id'),
                'targeting' => array(self::BELONGS_TO, 'BanerTargeting', 'targeting_id')
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'           => 'ID',
                'baner_id'     => 'Файл банера',
                'place_id'     => 'Место',
                'targeting_id' => 'Таргетинг по региону',
                'type_place'   => 'Тип показа',
                'num_clicks'   => 'Кол-во кликов',
                'num_shows'    => 'Кол-во показов',
                'date_from'    => 'Дата начала',
                'date_to'      => 'Дата окончания',
                'active'       => 'Активность',
                'finish'       => 'Ротация завершена',
                'lang_id'      => 'Язык'
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('baner_id', $this->baner_id);
            $criteria->compare('place_id', $this->place_id);
            $criteria->compare('targeting_id', $this->targeting_id);
            $criteria->compare('type_place', $this->type_place, true);
            $criteria->compare('num_clicks', $this->num_clicks);
            $criteria->compare('num_shows', $this->num_shows);
            $criteria->compare('date_from', $this->date_from, true);
            $criteria->compare('date_to', $this->date_to, true);
            $criteria->compare('active', $this->active);
            $criteria->compare('finish', $this->finish);

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }
    }