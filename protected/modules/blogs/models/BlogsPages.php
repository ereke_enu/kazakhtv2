<?php

/**
 * This is the model class for table "{{blogs_pages}}".
 *
 * The followings are the available columns in table '{{blogs_pages}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $blog_id
 * @property string $title
 * @property string $text
 * @property integer $views
 * @property integer $rating
 * @property string $sefname
 * @property string $date_create
 * @property string $date_update
 * @property integer $active
 */
class BlogsPages extends CActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return BlogsPages the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{blogs_pages}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, blog_id, title, text, sefname, date_create, date_update', 'required'),
            array('user_id, blog_id, views, rating, active', 'numerical', 'integerOnly' => true),
            array('title, sefname', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, user_id, blog_id, title, text, views, rating, sefname, date_create, date_update, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'blog_id' => 'Блоги',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'views' => 'Просмотров',
            'rating' => 'Рейтинг',
            'sefname' => 'ЧПУ',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
            'active' => 'Активность',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('blog_id', $this->blog_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('views', $this->views);
        $criteria->compare('rating', $this->rating);
        $criteria->compare('sefname', $this->sefname, true);
        $criteria->compare('date_create', $this->date_create, true);
        $criteria->compare('date_update', $this->date_update, true);
        $criteria->compare('active', $this->active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}