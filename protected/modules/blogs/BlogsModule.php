<?php

class BlogsModule extends CWebModule {
    public function init() {
        $endName = Yii::app()->endName;
        $this->setImport(array(
            "blogs.models.*",
            "users.models.*",
            "blogs.{$endName}.components.*",
        ));
        Yii::app()->onModuleCreate(new CEvent($this));
    }


}
