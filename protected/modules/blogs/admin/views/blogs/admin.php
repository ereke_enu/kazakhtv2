<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Блоги</span>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li><a href="<?=$this->createUrl('create');?>" class="">Создать блог</a></li>
        </ul>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'stripped condensed bordered hover',
    'id' => 'blogs-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'user_id',
        'title',
        'date_update',
        'active',
        /*
        'sefname',

        */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>
