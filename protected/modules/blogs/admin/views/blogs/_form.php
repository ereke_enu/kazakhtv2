<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>
<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'blogs-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal'
    )); ?>


    <?php echo $form->dropDownListRow($model, 'user_id', Users::getListBloggers(), array('class' => 'span8')); ?>

    <?php echo $form->textFieldRow($model, 'title', array('class' => 'span8', 'maxlength' => 255)); ?>

    <?php echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

    <?php echo $form->checkBoxRow($model, 'active'); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
