<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>
<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'blogs-pages-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal'
    )); ?>

    <?php echo $form->dropDownListRow($model, 'blog_id', array(), array('class' => 'span8')); ?>

    <?php echo $form->textFieldRow($model, 'title', array('class' => 'span8', 'maxlength' => 255)); ?>

    <?php echo $form->ckEditorRow($model, 'text', array('options' => CMap::mergeArray(array('fullpage' => 'js:true', 'width' => '640', 'resize_maxWidth' => '640', 'resize_minWidth' => '320'), Yii::app()->params['editorOptions']))); ?>

    <?php echo $form->checkBoxRow($model, 'active'); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
