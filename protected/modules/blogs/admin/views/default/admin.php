<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Страницы блогов</span>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>
            <li><a href="<?=$this->createUrl('create');?>" class="">Создать страницу</a></li>
        </ul>
    </div>
</div>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'blogs-pages-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_id',
		'blog_id',
		'title',
		'active',
		/*
		'rating',
		'sefname',
		'date_create',
		'date_update',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
