<?php
$this->breadcrumbs=array(
	'Blogs Pages',
);

$this->menu=array(
	array('label'=>'Create BlogsPages','url'=>array('create')),
	array('label'=>'Manage BlogsPages','url'=>array('admin')),
);
?>

<h1>Blogs Pages</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
