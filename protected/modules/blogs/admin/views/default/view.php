<?php
$this->breadcrumbs=array(
	'Blogs Pages'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List BlogsPages','url'=>array('index')),
	array('label'=>'Create BlogsPages','url'=>array('create')),
	array('label'=>'Update BlogsPages','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete BlogsPages','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BlogsPages','url'=>array('admin')),
);
?>

<h1>View BlogsPages #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'blog_id',
		'title',
		'text',
		'views',
		'rating',
		'sefname',
		'date_create',
		'date_update',
		'active',
	),
)); ?>
