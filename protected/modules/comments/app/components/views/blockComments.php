<h3><?=Yii::t('comments', 'Комментарии');?> <span>(<?= $count ?>)</span></h3>

<div id="comments">
	<?php Comments::getAppTree($comments, 0); ?>
</div>

<div class="app-comments-form">
	<div class="text-center">
		<a class="btn btn-grey btn-comments" id="addComment" href="#"><?= Yii::t('comments', 'Добавить комментарий') ?></a>
	</div>

	<div class="form" style="display: none">
		<h3 class="text-center"><?= Yii::t('comments', 'Комментарий') ?></h3>
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'commentsViewForm',
			'action' => Yii::app()->createUrl('comments/app/addComment'),
			'enableAjaxValidation' => false,
			'enableClientValidation' => true
		)); ?>

		<?= $form->hiddenField($model, 'reply'); ?>
		<?= $form->hiddenField($model, 'type', array('value' => $this->type)); ?>
		<?= $form->hiddenField($model, 'cid', array('value' => $this->id)); ?>

		<div class="form-group">
			<label for="Comments_name"><?= Yii::t('comments', 'Ваше имя') ?></label>
			<?= $form->textfield($model, 'name', array('value' => ((Yii::app()->user->isGuest)?'':Yii::app()->user->name), 'class' => 'form-control')); ?>
			<?= $form->error($model, 'name'); ?>
		</div>
		<div class="form-group">
			<label for="Comments_email"><?= Yii::t('comments', 'Ваш email') ?></label>
			<?= $form->textfield($model, 'email', array('class' => 'form-control')); ?>
			<?= $form->error($model, 'email'); ?>
		</div>
		<div class="form-group">
			<label for="Comments_message"><?= Yii::t('comments', 'Комментарий') ?></label>
			<?= $form->textArea($model, 'message', array('class' => 'form-control', 'rows'=>5)); ?>
			<?= $form->error($model, 'message'); ?>
		</div>

		<div class="row">
			<div class="col-xs-6">
				<?= CHtml::button(Yii::t('comments', 'Отмена'), array('id' => 'closeComment', 'class'=>'btn btn-grey btn-block')); ?>
			</div>
			<div class="col-xs-6">
				<?= CHtml::button(Yii::t('comments', 'Отправить'), array('id' => 'sendComment', 'class'=>'btn btn-grey btn-block')); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>
	</div>

</div>

<?php
Yii::app()->clientScript->registerScript('comments_script1', "
/*<![CDATA[*/
$(function () {
		$('#addComment').on('click', function(e) {
			e.preventDefault();
			$('.app-comments-form .form').fadeToggle();
		});

		$('#closeComment').on('click', function(e) {
			e.preventDefault();
			$('.app-comments-form .form').fadeToggle();
		});

		$('#sendComment').click(function () {
			$.ajax({
				type: 'POST',
				dataType: 'json',
				data: $('#commentsViewForm').serialize(),
				url: $('#commentsViewForm').attr('action'),
				success: function (data) {
					if (!data.errors) {
						$('#commentsViewForm')[0].reset();
						$('#Comments_reply').val('0');
						$('#comments').html(data.data);
						$('.app-comments-form .form').fadeOut();
						location.href = '#comment_' + data.id;
					} else {
						console.log(data.errors);
					}
				}
			});
		});

        $('.comment_reply').live('click', function () {
            var reply = $(this).data('reply');

            $('#Comments_reply').val(reply);
            location.href = '#comments';
            $('#Comments_message').focus();

            return false;
        });

    });
/*]]>*/
", CClientScript::POS_END);
?>