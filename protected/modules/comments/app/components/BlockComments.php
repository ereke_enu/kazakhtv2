<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Фарух
 * Date: 11.01.13
 * Time: 15:07
 * To change this template use File | Settings | File Templates.
 */

class BlockComments extends CWidget{

    public $type;
    public $id;

    public function run(){
        $model = new Comments;
        $comments = Comments::buildTree($this->id, $this->type);
        $this->render('blockComments', array('model' => $model, 'comments' => $comments[0], 'count' => $comments[1]));
    }
}