<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Фарух
 * Date: 11.01.13
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */


class AppController extends LXController {

	public $layout = '//layouts/app';
	private $_type;

    public function actionNews($sefname) {
	    $this->_type = 'pages';
	    $page = Pages::model()->findBySefname($sefname);
	    if ($page === null)
		    throw new CHttpException(404, 'Страница не найдена');

	    $model = new Comments;
	    $comments = Comments::buildTree($page->id, $this->_type);

	    $this->render('view', array('model' => $model, 'comments' => $comments[0], 'count' => $comments[1], 'type'=>$this->_type, 'id'=>$page->id));
    }

	public function actionCategory($sefname) {
		if($sefname == 'about') $sefname = 'about_company';

		$this->_type = 'pages';
		$category = Categories::model()->findBySefnameObj($sefname);
		if ($category === null)
			throw new CHttpException(404, 'Категория не найдена');

		$criteria = new CDbCriteria;
		$criteria->compare('category_id', $category->id);
		$criteria->compare('active', '1');
		$criteria->order = 'date DESC, time DESC';

		$page = Pages::model()->find($criteria);
		if ($page === null)
			throw new CHttpException(404, 'Страница не найдена');

		$model = new Comments;
		$comments = Comments::buildTree($page->id, $this->_type);

		$this->render('view', array('model' => $model, 'comments' => $comments[0], 'count' => $comments[1], 'type'=>$this->_type, 'id'=>$page->id));
	}

	public function actionVideo($sefname, $id) {
		$this->_type = 'videoArchive';

		$program = ProgrammsContent::model()->active()->findBySefname($sefname);
		if ($program === null)
			throw new CHttpException(404, 'Страница не найдена');

		$video = VideoArchive::model()->findByPk($id);
		if ($video === null)
			throw new CHttpException(404, 'Страница не найдена');

		$model = new Comments;
		$comments = Comments::buildTree($video->id, $this->_type);

		$this->render('view', array('model' => $model, 'comments' => $comments[0], 'count' => $comments[1], 'type'=>$this->_type, 'id'=>$video->id));
	}

    public function actionAddComment() {
        if (!Yii::app()->request->isAjaxRequest && !Yii::app()->request->isPostRequest)
            throw new CHttpException(400, 'Bad request');

        $model = new Comments;

        if (isset($_POST['Comments'])) {
            $model->attributes = $_POST['Comments'];
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $model->message = strip_tags($_POST['Comments']['message']);
            $model->date = date('Y-m-d', time());
            $model->time = date('H:i:s', time());
            if (!$model->save()) {
                echo CJSON::encode(array('errors' => $model->errors));
                Yii::app()->end();
            }
        }

        $comments = Comments::buildTree($model->cid, $model->type);

        ob_start();
        Comments::getAppTree($comments[0], 0);
        $data = ob_get_clean();

        echo CJSON::encode(array('data' => $data, 'id' => $model->id, 'errors' => false));

    }


}