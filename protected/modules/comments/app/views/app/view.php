<div class="block-view">
	<a href="#" class="btn btn-grey col-xs-12" id="showForm"><?= Yii::t('comments', 'Добавить комментарий') ?></a>

	<div id="comments">
		<?php Comments::getAppTree($comments, 0); ?>
	</div>
</div>

<div class="block-form hidden">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'commentsViewForm',
		'action' => Yii::app()->createUrl('comments/app/addComment'),
		'enableAjaxValidation' => false,
		'enableClientValidation' => true
	)); ?>

	<?= $form->hiddenField($model, 'reply'); ?>
	<?= $form->hiddenField($model, 'type', array('value' => $type)); ?>
	<?= $form->hiddenField($model, 'cid', array('value' => $id)); ?>

	<div class="form-group">
		<label for="Comments_name"><?= Yii::t('comments', 'Ваше имя') ?></label>
		<?= $form->textfield($model, 'name', array('value' => ((Yii::app()->user->isGuest)?'':Yii::app()->user->name), 'class' => 'form-control')); ?>
		<?= $form->error($model, 'name'); ?>
	</div>
	<div class="form-group">
		<label for="Comments_email"><?= Yii::t('comments', 'Ваш email') ?></label>
		<?= $form->textfield($model, 'email', array('class' => 'form-control')); ?>
		<?= $form->error($model, 'email'); ?>
	</div>
	<div class="form-group">
		<label for="Comments_message"><?= Yii::t('comments', 'Комментарий') ?></label>
		<?= $form->textArea($model, 'message', array('class' => 'form-control', 'rows'=>5)); ?>
		<?= $form->error($model, 'message'); ?>
	</div>

	<?= CHtml::button(Yii::t('comments', 'Отправить'), array('id' => 'sendComment', 'class'=>'btn btn-grey pull-right')); ?>

	<?php $this->endWidget(); ?>
</div>

<?php
Yii::app()->clientScript->registerScript('comments_script1', "
/*<![CDATA[*/
$(function () {
	$('#sendComment').click(function () {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			data: $('#commentsViewForm').serialize(),
			url: $('#commentsViewForm').attr('action'),
			success: function (data) {
				if (!data.errors) {
					$('#commentsViewForm')[0].reset();
					$('#Comments_reply').val('0');
					$('#comments').html(data.data);
					$('.block-form').toggleClass('hidden');
					location.href = '#comment_' + data.id;
				} else {
					console.log(data.errors);
				}
			}
		});
	});

	$('#showForm').on('click', function(e) {
		e.preventDefault();

		$('.block-form').toggleClass('hidden');
	});
});
/*]]>*/
", CClientScript::POS_END);
?>
<script>

</script>