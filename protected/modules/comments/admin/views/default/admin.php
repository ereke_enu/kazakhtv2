<div class="navbar">
    <div class="navbar-inner">
        <span class="brand">Список Комментарий</span>
        <ul class="nav nav-pills">
            <li class="divider-vertical"></li>

        </ul>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'comments-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'name',
        'email',
        'message',
        'date',
        array(
            'name' => 'cidLink',
            'type' => 'raw'
        ),
        /*
        'time',
        'type',
        'cid',
        'reply',
        'spam',
        */
        array(
            'class'               => 'bootstrap.widgets.TbButtonColumn',
            'template'            => '{update} {delete}',
            'updateButtonOptions' => array(
                'class' => 'btn btn-small update'
            ),
            'deleteButtonOptions' => array(
                'class' => 'btn btn-small delete'
            ),
            'htmlOptions'         => array('width' => 100)
        ),
    ),
)); ?>
