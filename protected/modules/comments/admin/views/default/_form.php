<div class="alert alert-info">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</div>

<div class="well">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'                   => 'comments-form',
        'enableAjaxValidation' => false,
        'type'                 => 'horizontal'
    )); ?>


    <?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

    <?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>

    <?php echo $form->ckEditorRow($model, 'message', array('options' => CMap::mergeArray(array('fullpage' => 'js:true', 'width' => '640', 'resize_maxWidth' => '640', 'resize_minWidth' => '320'), Yii::app()->params['editorOptions']))); ?>

    <?php echo $form->checkBoxRow($model, 'spam'); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => $model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>