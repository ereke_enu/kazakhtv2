<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Фарух
 * Date: 11.01.13
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */


class CommentsModule extends CWebModule {
    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        parent::init();
        Yii::app()->setComponents(array(
            'errorHandler' => array(
                'errorAction' => 'comments/default/error',
            ),
        ));
        // import the module-level models and components
        $endName = Yii::app()->endName;
        $this->setImport(array(
            "comments.models.*",
            "langs.models.*",
            "pages.models.*",
            "categories.models.*",
            "comments.{$endName}.components.*",
        ));
        Yii::app()->onModuleCreate(new CEvent($this));
    }

}
