<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Фарух
     * Date: 11.01.13
     * Time: 11:20
     * To change this template use File | Settings | File Templates.
     */


    /**
     * This is the model class for table "{{comments}}".
     *
     * The followings are the available columns in table '{{comments}}':
     * @property integer $id
     * @property integer $user_id
     * @property string $name
     * @property string $email
     * @property string $message
     * @property string $date
     * @property string $time
     * @property string $type
     * @property integer $cid
     * @property integer $reply
     * @property integer $spam
     */
    class Comments extends CActiveRecord {
        public $cidLink;

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return Comments the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{comments}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('name, email, message', 'required'),
                array('email', 'email'),
                array('user_id, cid, reply, spam', 'numerical', 'integerOnly' => true),
                array('name, email, type', 'length', 'max' => 255),
                array('message, date, time', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, user_id, name, email, message, date, time, type, cid, reply, spam', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'user'    => array(self::BELONGS_TO, 'Users', 'user_id'),
                'reply'   => array(self::BELONGS_TO, 'Comments', 'reply'),
                'page'    => array(self::BELONGS_TO, 'Pages', 'cid'),
                'program' => array(self::BELONGS_TO, 'ProgrammsContent', 'cid'),
                'videoArchive' => array(self::BELONGS_TO, 'VideoArchive', 'cid')
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'      => 'ID',
                'user_id' => Yii::t('comments', 'User ID'),
                'name'    => Yii::t('comments', 'Пользователь'),
                'email'   => Yii::t('comments', 'email'),
                'message' => Yii::t('comments', 'Ваш комментарий'),
                'date'    => Yii::t('comments', 'Дата'),
                'time'    => Yii::t('comments', 'Время'),
                'type'    => Yii::t('comments', 'Тип модуля'),
                'cid'     => Yii::t('comments', 'Cid'),
                'reply'   => Yii::t('comments', 'Ответ на'),
                'spam'    => Yii::t('comments', 'Опубликовать'),
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('user_id', $this->user_id);
            $criteria->compare('name', $this->name, true);
            $criteria->compare('email', $this->email, true);
            $criteria->compare('message', $this->message, true);
            $criteria->compare('date', $this->date, true);
            $criteria->compare('time', $this->time, true);
            $criteria->compare('type', $this->type, true);
            $criteria->compare('cid', $this->cid);
            $criteria->compare('reply', $this->reply);
            $criteria->compare('spam', $this->spam);

            $criteria->order = 'id DESC';

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }


        public function buildTree($cid, $type) {
            $criteria = new CDbCriteria;
            $criteria->compare('cid', $cid);
            $criteria->compare('type', $type);
            $criteria->compare('spam', 0);
            $criteria->order = 'reply ASC, id ASC';


            $comments = Comments::model()->findAll($criteria);
            $count = count($comments);
            $with_reply = array();
            for ($i = 0, $c = $count; $i < $c; $i++) {
                $with_reply[$comments[$i]['reply']][] = $comments[$i];

            }

            return array($with_reply, $count);
        }

        public static function getTree($data, $parent = 0, $level = 0, $padding = 0) {

            $arr = $data[$parent];


            for ($i = 0; $i < count($arr); $i++) {
                ?>
                <div id="comment_<?= $arr[$i]['id'] ?>" class="comment" style="margin-left:<?= $level ?>px">
                    <div style="padding-left: <?= $padding ?>px">
                        <span class="comment_name"><?= $arr[$i]['name']; ?></span>
                    <span
                        class="comment_date"> | <?= date('d/m/Y', strtotime($arr[$i]['date'])); ?> <?= $arr[$i]['time']; ?></span><br/>
                        <span class="comment_text"><?= $arr[$i]['message']; ?></span> <br/>

                        <a class="comment_reply" href=""
                           data-reply='<?= $arr[$i]['id']; ?>'><?= Yii::t('comments', 'Ответить'); ?></a>
                    </div>
                    <div class="comment_for_border"></div>
                    <?php if (isset($data[$arr[$i]['id']])) {
                        self::getTree($data, $arr[$i]['id'], 40, 20);
                    } ?>
                </div>

            <?php
            }
        }

	    public static function getAppTree($data, $parent = 0, $level = 0, $padding = 0) {
		    if(count($data) == 0)
			    return '';

		    $arr = $data[$parent];
		    $cnt = count($arr);

		    for ($i = 0; $i < $cnt; $i++) {
?>
			    <div id="comment_<?= $arr[$i]['id'] ?>" class="comment" style="margin-left: <?= $level ?>px">
				    <div style="padding-left: <?= $padding ?>px">
					    <div class="comment-name"><?= $arr[$i]['name']; ?></div>
                        <div class="comment-date"><?= date('j.m.Y H:i', strtotime($arr[$i]['date'].' '.$arr[$i]['time'])); ?></div>
					    <div class="comment-text"><?= $arr[$i]['message']; ?></div>
				    </div>
				    <hr>
				    <?php if (isset($data[$arr[$i]['id']])) {
					    self::getAppTree($data, $arr[$i]['id'], 40, 20);
				    } ?>
			    </div>
<?php
		    }
	    }

        public function afterFind() {
            if ($this->type == 'pages') {
                $this->cidLink =
                    '<a class="btn" target="_blank" href="/' . Langs::getLangCodeById($this->page->lang_id) . '/view/' . $this->page->category->sefname . '/' . $this->page->sefname . '">
                Перейти
                </a>';
            } elseif ($this->type == 'programms') {
                $this->cidLink =
                    '<a class="btn" target="_blank" href="/' . Langs::getLangCodeById($this->program->lang_id) . '/programms/program/' . $this->program->sefname . '">
                Перейти
                </a>';
            } elseif ($this->type == 'videoArchive') {
                $this->cidLink =
                    '<a class="btn" target="_blank" href="/' . Langs::getLangCodeById($this->videoArchive->timetable->content->lang_id) . '/programms/viewArchive?id=' . $this->videoArchive->id . '">
                Перейти
                </a>';
            }
        }

        public function getCidLink() {
            if ($this->type == 'pages') {
                $this->cidLink =
                    '<a class="btn" target="_blank" href="/' . Langs::getLangCodeById($this->page->lang_id) . '/view/' . $this->page->category->sefname . '/' . $this->page->sefname . '">
                Перейти
                </a>';
            } elseif ($this->type == 'programms') {
                $this->cidLink =
                    '<a class="btn" target="_blank" href="/' . Langs::getLangCodeById($this->program->lang_id) . '/programms/program/' . $this->program->sefname . '">
                Перейти
                </a>';
            } elseif ($this->type == 'videoArchive') {
                $this->cidLink =
                    '<a class="btn" target="_blank" href="/' . Langs::getLangCodeById($this->videoArchive->timetable->content->lang_id) . '/programms/viewArchive?id=' . $this->videoArchive->id . '">
                Перейти
                </a>';
            }
        }

        public function beforeValidate() {

            if (empty($this->reply)) {
                $this->reply = 0;
            }

            return parent::beforeValidate();
        }

    }