<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'comments-view-form',
        'action' => '',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true
    )); ?>

    <div class="row">
        <?php echo $form->textfield($model, 'name', array('value' => ((Yii::app()->user->isGuest)?'':Yii::app()->user->name), 'class' => 'textfield', 'placeholder' => Yii::t('comments', 'Ваше имя'))); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->textfield($model, 'email', array('class' => 'textfield', 'placeholder' => Yii::t('comments', 'Ваш email'))); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>
    <?php if (Yii::app()->user->isGuest) { ?>
        <div class="row">
            <?= Yii::t('users', ttt('Войти через социальные сети')); ?>:
            <?php $this->widget('ext.eauth.EAuthWidget', array('action' => '/users/default/login')) ?>
        </div>
    <?php } ?>
    <div class="row">
        <?php echo $form->textArea($model, 'message', array('class' => 'textarea', 'placeholder' => Yii::t('comments', 'Оставить комментарий'))); ?>
        <?php echo $form->error($model, 'message'); ?>
        <?php echo $form->hiddenField($model, 'reply'); ?>
        <?php echo $form->hiddenField($model, 'type', array('value' => $this->type)); ?>
        <?php echo $form->hiddenField($model, 'cid', array('value' => $this->id)); ?>
    </div>


    <div class="row buttons button">
        <?php echo CHtml::button(ttt('Отправить'), array('id' => 'sendComment')); ?>
    </div>

    <?php $this->endWidget(); ?>


</div><!-- form -->
<?php if($count>0) { ?>
<div style="margin-top:40px;margin-left:15px;margin-right:15px;padding-bottom:10px;"><h3>Комментарии <?=$count?></h3></div>

    <div id="comments">
    
        <?php
        
        Comments::getTree($comments, 0); 
       
        ?>
    
    </div>
<? } ?>
<script>
    $('#sendComment').click(function () {
        var message = $('#Comments_message').val();
            var type = $('#Comments_type').val();
            var id = $('#Comments_id').val();

            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: $('#comments-view-form').serialize(),
                url: '<?=Yii::app()->createUrl('/comments/default/addComment/')?>',
                success: function (data) {
                    if (data.errors.length === 0) {
                        $('#comments').html(data.data);
                        $('#Comments_message').val('');
                        $('#Comments_name').val('');
                        $('#Comments_email').val('');
                        $('#Comments_reply').val('0');
                        location.href = '#comment_' + data.id;
                    } else {
                        var msg = '';
                        $.each(data.errors, function (item, value) {
                            msg += value + "\n";
                        });
                        alert(msg);
                    }

                }
            });

            $('.comment_reply').live('click', function () {
                var reply = $(this).data('reply');

                $('#Comments_reply').val(reply);
                location.href = '#comments';
                $('#Comments_message').focus();

                return false;
            });
            
    });
</script>