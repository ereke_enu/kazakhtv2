<<div class="lastComment">
    <div class="title">
        <h2>Последние комментарии</h2>
    </div>
    <div class="comments_content">
        <ul>
            <?php foreach ($comments as $comment) {
                $messageLen = mb_strlen($comment->message, 'UTF8');
                $numSumbols = 120;
                $dotted = '';
                if ($messageLen > $numSumbols) {
                    $dotted = '...';
                }
                if ($comment->type == 'pages') {
                    ?>
                    <li>
                        <a href="<?=Yii::app()->createUrl('pages/default/view', array('sefname' => $comment->page->sefname, 'category' => $comment->page->category->sefname))?>">
                            <?=mb_substr($comment->message, 0, 120, 'UTF8').$dotted?>
                        </a>
                    </li>
                <?php
                } elseif ($comment->type == 'programms') {
                    ?>
                    <li>
                        <a href="<?=Yii::app()->createUrl('/programms/default/program', array('sefname' => $comment->program->sefname))?>"><?=mb_substr($comment->message, 0, 120, 'UTF8').$dotted?>
                            </a>
                    </li>
                <?php
                }
                ?>
            <?php } ?>
        </ul>
    </div>
</div>