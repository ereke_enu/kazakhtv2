<?php
    /**
     * Created by JetBrains PhpStorm.
     * User: Luxorka
     * Date: 18.01.13
     * Time: 16:19
     * To change this template use File | Settings | File Templates.
     */
    class LastCommentsWidget extends CWidget {

        public function run() {
            $criteria = new CDbCriteria;
            $criteria->compare('spam', 0);
            $criteria->order = 'date DESC, time ASC';
            $criteria->group = 'type, cid';
            $criteria->limit = 10;
            $comments = Comments::model()->findAll($criteria);

            $this->render('LastCommentsWidget', array('comments' => $comments));
        }

    }
