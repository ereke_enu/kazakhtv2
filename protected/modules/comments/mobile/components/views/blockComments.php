<br /><br />
<hr class="bold" />
<h2><?=Yii::t('comments', 'Комментировать');?> <?= $count ?></h2>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'comments-view-form',
        'action' => '',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true
    )); ?>

    <div class="col-sm-12 margin-bottom">
        <?= $form->textfield($model, 'name', array('value' => ((Yii::app()->user->isGuest)?'':Yii::app()->user->name), 'class' => 'form-control', 'placeholder' => Yii::t('comments', 'Ваше имя'))); ?>
        <span class="text-danger"><?= $form->error($model, 'name'); ?></span>
    </div>
    <div class="col-sm-12 margin-bottom">
        <?= $form->textfield($model, 'email', array('class' => 'form-control textfield', 'placeholder' => Yii::t('comments', 'Ваш email'))); ?>
        <span class="text-danger"><?= $form->error($model, 'email'); ?></span>
    </div>
    <?php if (Yii::app()->user->isGuest) { ?>
        <div class="col-sm-12 margin-bottom">
            <?= Yii::t('users', 'Войти через социальные сети'); ?>:
            <?php $this->widget('ext.eauth.EAuthWidget', array('action' => '/users/default/login')) ?>
        </div>
    <?php } ?>
    <div class="col-sm-12 margin-bottom">
        <?= $form->textArea($model, 'message', array('class' => 'form-control', 'rows'=>'5', 'placeholder' => Yii::t('comments', 'Оставить комментарий'))); ?>
        <span class="text-danger"><?= $form->error($model, 'message'); ?></span>
        <?= $form->hiddenField($model, 'reply'); ?>
        <?= $form->hiddenField($model, 'type', array('value' => $this->type)); ?>
        <?= $form->hiddenField($model, 'cid', array('value' => $this->id)); ?>
    </div>


    <div class="col-sm-12 margin-bottom">
        <?= CHtml::button(Yii::t('comments', 'Отправить'), array('id' => 'sendComment', 'class' => 'btn')); ?>
    </div>

    <?php $this->endWidget(); ?>


</div><!-- form -->

<div id="comments">
    <?php
    Comments::getTree($comments, 0); ?>
</div>

<script>
    $(function () {
        $('#sendComment').click(function () {
            var message = $('#Comments_message').val();
            var type = $('#Comments_type').val();
            var id = $('#Comments_id').val();

            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: $('#comments-view-form').serialize(),
                url: '<?=Yii::app()->createUrl('/comments/default/addComment/')?>',
                success: function (data) {
                    if (data.errors.length === 0) {
                        $('#comments').html(data.data);
                        $('#Comments_message').val('');
                        $('#Comments_name').val('');
                        $('#Comments_email').val('');
                        $('#Comments_reply').val('0');
                        location.href = '#comment_' + data.id;
                    } else {
                        var msg = '';
                        $.each(data.errors, function (item, value) {
                            msg += value + "\n";
                        });
                        alert(msg);
                    }

                }
            });
        });

        $('.comment_reply').live('click', function () {
            var reply = $(this).data('reply');

            $('#Comments_reply').val(reply);
            location.href = '#comments';
            $('#Comments_message').focus();

            return false;
        });

    });
</script>