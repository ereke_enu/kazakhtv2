<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Фарух
 * Date: 11.01.13
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */


class DefaultController extends LXController {

    public function actionIndex() {
        $this->render('index');
    }

    public function actionAddComment() {
        if (!Yii::app()->request->isAjaxRequest && !Yii::app()->request->isPostRequest) {
            throw new CHttpException(400, 'Bad request');
        }

        $model = new Comments;

        if (isset($_POST['Comments'])) {
            $model->attributes = $_POST['Comments'];
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $model->message = strip_tags($_POST['Comments']['message']);
            $model->date = date('Y-m-d', time());
            $model->time = date('H:i:s', time());
            if (!$model->save()) {
                echo CJSON::encode(array('errors' => $model->errors));
                Yii::app()->end();
            }
        }

        $comments = Comments::model()->buildTree($model->cid, $model->type);
        ob_start();
        Comments::getTree($comments, 0);
        $data = ob_get_clean();

        echo CJSON::encode(array('data' => $data, 'id' => $model->id, 'errors' => ''));

    }


}