<? include "./protected/components/Translator.php"; ?>
<div class="h_fon">
    <h1 class="ib"><em class='h1_left_pic'></em><?=Yii::t('search', 'Поиск')?></h1>
</div>
<article id="text" class="searchResult">
    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $results,
        'itemView' => '_searchItem',
        'itemsTagName' => 'table',
        'template' => '{summary}{items} {pager}',
        'summaryText' => 'Найдено {count} страниц. Показано {start} - {end}',
        'pager' => array(
            'header' => '',
            'prevPageLabel' => '<img src="/themes/front/images/left_btn_partners.png">',
            'nextPageLabel' => '<img src="/themes/front/images/right_btn_partners.png">'
        )
    ));
    ?>
</article>