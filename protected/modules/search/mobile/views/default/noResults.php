<div class="col-sm-12">
    <hr class="bold" />
    <h1><?=Yii::t('search', 'Поиск');?></h1>

    <article id="text" class="article-text">
        <form action="<?=$this->createUrl('/search/default/index')?>">
            <div class="input-group">
                <input type="search" name="query" value="<?=$query?>" class="form-control" />
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><?=Yii::t('search', 'Поиск');?></button>
                </span>
            </div>
        </form>

        <p><i><?=Yii::t('search', 'По Вашему запросу ничего не найдено!');?></i></p>
    </article>
</div>