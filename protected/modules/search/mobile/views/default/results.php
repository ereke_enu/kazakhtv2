<div class="col-sm-12">
    <hr class="bold" />
    <h1><?=Yii::t('search', 'Поиск')?></h1>
    <hr class="margin-bottom" />
    
    <div class="scroll">
        <article id="text" class="searchResult article-text">
            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $results,
                'itemView' => '_searchItem',
                'itemsTagName' => 'table',
                'template' => '{summary}{items} {pager}',
                'summaryText' => 'Найдено {count} страниц. Показано {start} - {end}',
                'pager' => array(
                    'header' => '',
                    'nextPageLabel' => '&raquo;',
                    'prevPageLabel' => '&laquo;',
                    'maxButtonCount' => 0,
                    'htmlOptions' => array(
                        'id' => 'iasNews',
                        'class' => 'pager'
                    )
                )
            ));
            ?>
        </article>
    </div>
</div>