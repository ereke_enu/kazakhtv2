<?php
class CacheBuilder {
	private $fileName="";
	private $lang="";

	public function __construct($fileName, $lang=NULL){
		$this->fileName=$fileName;
		if($lang!=NULL)$this->lang=$lang;
	}
	
	public function getFullPath()
	{
		if($this->lang!='')
			return "protected/runtime/mycache/".$this->fileName."_".$this->lang.".php";
		return "protected/runtime/mycache/".$this->fileName.".php";
	}
	
	public function writeToFile($txt){
		$path = $this->getFullPath();
		file_put_contents($path, $txt);
		
	}
	
	public function checkFile(){
		return file_exists($this->getFullPath());
	}

	public function getFileName(){
		return $this->fileName;
	}

	public function deleteFile(){
		return unlink($this->getFullPath());
	}

	public function clearFolder($folderName){

		$path= "protected/runtime/mycache/".$folderName;	
		$files = glob($path."/*"); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
		    unlink($file); // delete file
		}
	}
}

?>