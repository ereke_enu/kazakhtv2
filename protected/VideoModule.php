<?php

class VideoModule extends CWebModule {
    public function init() {
        $endName = Yii::app()->endName;
        $this->setImport(array(
            "categories.models.*",
            "programms.models.*",
            "pages.models.*",
            "langs.models.*",
            "video.models.*",
            "video.{$endName}.components.*",
        ));
        Yii::app()->onModuleCreate(new CEvent($this));
    }

}
