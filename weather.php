﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Index</title>
    <script type="text/javascript" src="/themes/front/js/jquery.js"></script>
    <style type="text/css">
	

    body{
    	
    	padding: 0px;
    	margin: 0px;
    }

	.wrapper{
		margin:0 auto;
		width: 975px;
		border: 1px solid transparent;
	}
	
	#header{
		width: 100%;
		height: 37px;
		background-color: #00354C;
	}

	#menu{
		list-style: none;
		float: right;
		margin-top: 25px;
	}

	#menu li{
		float:left;
		padding-left:20px;
		border: 1px solid #00354C;
		margin-top:-18px;
	}

	#menu li a {
		padding-top:10px;
		color: #ffffff;
		text-decoration: none;
	}

	#menu li a:hover {
		text-decoration: underline;
	}

	#logo{
		float:left;
	}

	#logo img{
		width: 180px;
		height:auto;
	}

	#content{
	}

	#search{
		background-color:#E4E4E4;
		padding: 2px;
	}

	#bg_top{
		width: 100%;
		background:url('upload/bg_top.png') ;
		height: 320px;
	}

	#content{
		min-height: 500px;
	}

/* --------------------------------- informer ----------------------------------------*/
	#informer{
		width: 975px;
		height: 236px;
		background-color: #0F5474;
		font-size: 24px;
		color: #ffffff;
		font-weight: bold;

	}

	#informer a{
		color: white;
		text-decoration: none;
	}

	#informer a:hover{
		text-decoration: underline;
	}

	.informer_item{
		width: 100px;
	}

	#informer_icon{
		width: 100px;
		margin: 0px auto;
	}

	.informer_curr_time{
		font-weight: normal;
		font-size:18px;
	}

	#informer img{
		margin-top: 20px;
	}
	
	.day_temperature{
		font-weight: normal;
		font-size: 20px;
	}

	.informer_date{
		font-weight: normal;
		font-size: 14px;
	}
	/*------------------------------------end informer-----------------------------------------------*/

	#summary{
		width: 625px;
		height: 105px;
		background-color: #B0CF00;
	}

	/*-----------------------------------MAP-----------------------------------------------------*/
	#map{
		font-size: 12px;
		
		width: 625px; 
		height: 373px;
		
		margin: 0 auto;
		margin-top:25px;
		background: url('upload/map.png');

	}

	#map a{
		text-decoration: none;
		color: gray;
	}

	#map a:hover{
		text-decoration: underline;
		color:blue;

	}

/*-----------------------------------------------------------------------------------------------*/
	#footer{
		margin-top: 370px;
		width: 100%;

	}

	#footer_content{
		border-top:1px solid #4c4c4c;

	}
	.clear{
		clear: both;
	}

	#footer_menu{
		float: right;
		list-style: none;
	}

	#footer_menu li{
		float: left;
		padding-left:20px;
	}

	.align_center{
		
		display:inline-block;
		margin: 0px auto;
	}

	#footer_menu a{
		text-decoration: none;
		color: gray;
	}

	#footer_menu a:hover{
		text-decoration: underline;
	}

	.temp_indicator{
		width: 20px;
		height: 20px;
	}
	</style>
<script type="text/javascript">
$(document).ready(function(){
	elem = $('#city35108');elem.attr('style','position:relative;left:72px;top:100px;');
	elem = $('#city38111');elem.attr('style','position:relative;left:0px;top:259px;');
	elem = $('#city38062');elem.attr('style','position:relative;left:138px;top:220px;');
	elem = $('#city28879');elem.attr('style','position:relative;left:155px;top:65px;');
	elem = $('#city36778');elem.attr('style','position:relative;left:250px;top:230px;');
	elem = $('#city35700');elem.attr('style','position:relative;left:-260px;top:175px;');
	elem = $('#city36403');elem.attr('style','position:relative;left:154px;top:105px;');
	elem = $('#city38341');elem.attr('style','position:relative;left:-115px;top:300px;');
	elem = $('#city38328');elem.attr('style','position:relative;left:-180px;top:325px;');
	elem = $('#city35394');elem.attr('style','position:relative;left:380px;top:134px;');
	elem = $('#city28952');elem.attr('style','position:relative;left:170px;top:34px;');
	elem = $('#city36003');elem.attr('style','position:relative;left:307px;top:49px;');
	elem = $('#city28679');elem.attr('style','position:relative;left:100px;top:-10px;');
	elem = $('#city35188');elem.attr('style','position:relative;left:9px;top:80px;font-size:14px;');//Astana
	elem.css('text-transform','uppercase');
	elem = $('#city36870');elem.attr('style','position:relative;left:80px;top:270px;');
	elem = $('#city35229');elem.attr('style','position:relative;left:-290px;top:100px;');
})
</script>
    </head>    

	<body>    
		<div id="header">
			<div class="wrapper" style="height:37px;">
				<div id="logo"><img src="upload/LOGOBETA2.png"  /></div>
				<ul id="menu">
					<li><a href="/ru/mainpage/index">ГЛАВНАЯ</a></li>
					<li><a href="/ru/allNews/news">Новости</a></li>
					<li><a href="/ru/category/programms">Программы</a></li>
					<li><a href="/ru/category/kazakhstan_overview">Казахстан</a></li>
					<li><a href="/ru/category/about_company">О нас</a></li>
					<!--<li><a href="#">Не пропустите</a></li>
					<li><a href="#">Программа передач</a></li>-->
					<li><a href="/ru/category/worldwide">WORLDWIDE</a></li>
					<li><a href="/ru/category/live_now">Прямой эфир</a></li>
					<li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
		<div id="bg_top"> 
			<div class="wrapper" id="content">
				<h2 style="color:#ffffff">WEATHER</h2>
<?php
	$city_id=$_REQUEST['city'];
	if($city_id==''){ $city_id=35188; } 
   	 // id города
    $data_file="http://export.yandex.ru/weather-ng/forecasts/$city_id.xml"; // адрес xml файла 
	
    $xml = simplexml_load_file($data_file); // раскладываем xml на массив
	
 
    // выбираем требуемые параметры (город, температура, пиктограмма и тип погоды текстом (облачно, ясно)
     
    $attrs = $xml;

    $city=$attrs['city'];
    $temp=$xml->fact->temperature;
    $pic=$xml->fact->{'image-v3'};
    $type=$xml->fact->weather_type;
    $at_night=$xml->informer->temperature;
    // Если значение температуры положительно, для наглядности добавляем "+"
    if ($temp>0) {$temp='+'.$temp;}
?>
				<div id="informer">
					<table border="0" cellpadding="5px" cellspacing="0">
						<tr>
							<td style="width:250px;">
							<div ><?php echo ("<a href='http://pogoda.yandex.ru/$city_id/'>".$city."</a>");?></div>
							<div class="informer_curr_time">
								<?php 
									$date = get_date_string(date('Y-m-d'));
									$w = date('W');
									$w+=3;
									echo $date.", ".get_week_name($w);
								?>
							</div>
							<div style="margin:0 auto;width:100px;margin-top:-20px;">
								<?php echo ("<img src=\"http://yastatic.net/weather/i/icons/svg/$pic.svg\" alt=\"$type\" title=\"$type\" id='informer_icon' />"); ?>
							</div>
							<div style="float:right;"><?php echo "$temp<sup>o</sup>C";?></div>
							<div class="clear"></div>
							<div style="float:right;"><?php echo "$at_night<sup>o</sup>C";?></div>
							</td>

							<?php 
								$col=5;
								$out = array(); // массив вывода прогноза
    							$counter = 0 ; // счетчик количества дней, для которых доступен прогноз
							    if($xml->day):
							        foreach($xml->day as $day):
							        	$counter++;
							        	if($counter>1&&$counter<=8)
							        	{
							        		$w++;
									        echo "<td align='center' class='informer_item'>";
											echo "<div><a href=''>".get_week_short_name($w)."</a></div>";
											$attrs = $day->attributes();
											$date_string= get_date_string($attrs['date']);
											echo "<div class='informer_date'>".$date_string."</div>";
											
											foreach ($day->day_part as $dp) {
												$dp_attrs = $dp->attributes();
												if($dp_attrs->type=='day_short'){
													$pic = $dp->{'image-v3'};
													$temp = get_sign($dp->temperature);
													echo "<div><img src=\"http://yastatic.net/weather/i/icons/svg/$pic.svg\" style='width:60px;' /></div>";
													echo "<div class='day_temperature'>".$temp."</div>";
												}
												if($dp_attrs->type=='night_short'){
													$temp = get_sign($dp->temperature);
													echo "<div class='day_temperature'>".$temp."</div>";
												}
											}
											echo "</td>";							            
										}
								    endforeach;
							    endif;
							?>
						</tr>

					</table>			
				</div>
				<!--<div id="summary"></div>-->
				<div id="map" >
<?php
/*
$cities = array(35108,38111,38062,28879,36778,35700,36403,38341,38328,35394,28952,36003,28679,35188,36870,35229);
for($i=0;$i<sizeof($cities);$i++)
{
    $data_file="http://export.yandex.ru/weather-ng/forecasts/$cities[$i].xml"; // адрес xml файла 
    $xml = simplexml_load_file($data_file); // раскладываем xml на массив 
    // выбираем требуемые параметры (город, температура, пиктограмма и тип погоды текстом (облачно, ясно)
    $attrs = $xml;
    $city=$attrs['city'];
    $temp=$xml->fact->temperature;
    $color = getTemperatureColor($temp);
    if($temp>0)$temp='+'.$temp;
    echo "<a id='city$cities[$i]' href='?city=$cities[$i]' style='position:relative;'><span class='temp_indicator' style='background-color:#$color'>".$temp."</span>$city</a>";
}
*/
?>
<a id="city35108" href="?city=35108" style="position:relative;">Уральск</a>
<a id="city38111" href="?city=38111" style="position:relative;">Актау</a>
<a id="city38062" href="?city=38062" style="position:relative;">Кызылорда</a>
<a id="city28879" href="?city=28879" style="position:relative;">Кокшетау</a>
<a id="city36778" href="?city=36778" style="position:relative;">Талдыкорган</a>
<a id="city35700" href="?city=35700" style="position:relative;">Атырау</a>
<a id="city36403" href="?city=36403" style="position:relative;">Усть-Каменогорск</a>
<a id="city38341" href="?city=38341" style="position:relative;">Тараз</a>
<a id="city38328" href="?city=38328" style="position:relative;">Шымкент</a>
<a id="city35394" href="?city=35394" style="position:relative;">Караганда</a>
<a id="city28952" href="?city=28952" style="position:relative;">Костанай</a>
<a id="city36003" href="?city=36003" style="position:relative;">Павлодар</a>
<a id="city28679" href="?city=28679" style="position:relative;">Петропавловск</a>
<a id="city35188" href="?city=35188" style="position:relative;">АСТАНА</a>
<a id="city36870" href="?city=36870" style="position:relative;">Алматы</a>					
<a id="city35229" href="?city=35229" style="position:relative;">Актобе</a>					
					
				</div>
			</div>
		</div>
		<br/>

		<div id="footer">
			<div class="wrapper" id="footer_content">
			
			<div style="margin-top:10px;float:left;font-size:12px;"></div>
					<ul id="footer_menu">
					<li><a href="/ru/mainpage/index">ГЛАВНАЯ</a></li>
					<li><a href="/ru/allNews/news">Новости</a></li>
					<li><a href="/ru/category/programms">Программы</a></li>
					<li><a href="/ru/category/kazakhstan_overview">Казахстан</a></li>
					<li><a href="/ru/category/about_company">О нас</a></li>
					<li><a href="#">Не пропустите</a></li>
					<li><a href="#">Программа передач</a></li>
					<li><a href="/ru/category/worldwide">WORLDWIDE</a></li>
					<li><a href="/ru/category/live_now">Прямой эфир</a></li>
				</ul>

			</div>
		</div>
    </body>

</html>


<?php
function get_sign($temp){
	if ($temp>0) {$temp='+'.$temp;}
	return $temp;
}

function get_date_string($date){
	$arr = explode("-", $date);
	$month = $arr[1];
	$day = $arr[2];
	if($day[0]=='0'){$day=$day[1];}
	$month_name="";
	switch ($month){
		case "01":
			return $day.' января';
			break;
		case "02":
			return $day.' февраля';
			break;
		case '03':
			return $day.' марта';
			break;
		case '04':
			return $day.' апреля';
			break;
		case '05':
			return $day.' мая';
			break;
		case '06':
			return $day.' июня';
			break;
		case '07':
			return $day.' июля';
			break;
		case '08':
			return $day.' августа';
			break;
		case '09':
			return $day.' сентября';
			break;
		case '10':
			return $day.' октября';
			break;
		case '11':
			return $day.' ноября';
			break;			
		case '12':
			return $day.' декабря';
			break;				
	}
}

function get_week_name($week){
	if($week>7){$week=$week-7;}
	switch ($week) {
		case '1':
			return "Понедельник";
			break;
		case '2':
			return "Вторник";
			break;
		case '3':
			return "Среда";
			break;
		case '4':
			return "Четверг";
			break;
		case '5':
			return "Пятница";
			break;
		case '6':
			return "Суббота";
			break;
		case '7':
			return "Воскресенье";
			break;
	}
}	

function get_week_short_name($week){
	if($week>7){$week=$week-7;}
	switch ($week) {
		case '1':
			return "Пн";
			break;
		case '2':
			return "Вт";
			break;
		case '3':
			return "Ср";
			break;
		case '4':
			return "Чт";
			break;
		case '5':
			return "Пт";
			break;
		case '6':
			return "Сб";
			break;
		case '7':
			return "Вс";
			break;
	}
}

function getTemperatureColor($temp)
{
	if($temp<0)
	{
		return '69C3FF';
	}elseif($temp>=0&&$temp<10)
	{
		return 'FFF83B';
	}elseif ($temp>=10&&$temp<20) {
		return 'C3FF5D';
	}else{
		return 'FF9B25';
	}
}	
?>